
CREATE TABLE android_metadata (locale TEXT);
INSERT INTO "android_metadata" VALUES('en_US');
CREATE TABLE check_in_table (_id integer primary key AUTO_INCREMENT, LOGIN_ID INTEGER ,TIME INTEGER ,PAIN_INTENSITY TEXT ,EATING_DISORDER TEXT ,IMAGE_FILE_NAME TEXT  ,SYNCHRONISED INTEGER ,SYNCHRONISED_PHOTO INTEGER  );
CREATE TABLE medication_table (_id integer primary key AUTO_INCREMENT, LOGIN_ID INTEGER ,NAME TEXT ,START_DATE DATETIME ,END_DATE DATETIME  );
INSERT INTO "medication_table" VALUES(45,4,'Lortab','2014-10-01 04:00:00','2014-12-22 05:00:00');
INSERT INTO "medication_table" VALUES(46,4,'OxyContin','2014-10-01 04:00:00','2014-12-23 05:00:00');
INSERT INTO "medication_table" VALUES(47,4,'Panadol','2014-10-01 04:00:00','2014-12-23 05:00:00');
CREATE TABLE medication_usage_table (_id integer primary key AUTO_INCREMENT, CHECK_IN_ID INTEGER ,NAME TEXT ,TIME DATETIME, TAKEN TEXT  );
CREATE TABLE user_credentials_table (_id integer primary key AUTO_INCREMENT, LOGIN_ID INTEGER ,USERNAME TEXT ,TYPE TEXT ,SERVER TEXT ,DOCTOR TEXT ,FIRST_NAME TEXT ,LAST_NAME TEXT ,DOB DATETIME ,MEDICAL_RECORD TEXT ,DOCTOR_ID TEXT ,TOKEN TEXT ,LAST_TIME_SYNC DATETIME ,LAST_TIME_FAILED DATETIME ,LOGIN_TIME DATETIME ,VALID INTEGER  );
INSERT INTO "user_credentials_table" VALUES(1,4,'patient1','PATIENT','https://10.0.3.2:8443','doctor1','Pit','Mayer','1968-08-15 04:00:00','23JGH785FGH','','33c75dfe-c9e5-4cc7-8031-3c66a40447e4','2014-11-16 10:36:07','1970-01-01 00:00:00','2014-11-16 10:36:07',0);
INSERT INTO "user_credentials_table" VALUES(2,1,'doctor1','DOCTOR','https://10.0.3.2:8443','','Bob','Franklin','','','Licence:10293','72f1c09e-e869-4721-a27f-0e206b247b79','1970-01-01 00:00:00','1970-01-01 00:00:00','2014-11-16 10:36:29',0);
INSERT INTO "user_credentials_table" VALUES(3,2,'doctor2','BOTH','https://10.0.3.2:8443','doctor1','Denise','Mayer','1955-03-02 05:00:00','2DJJH678SGH','Licence:54637','b7d57664-7f01-4a0e-ba8a-9dae98ff429e','2014-11-16 10:37:23','1970-01-01 00:00:00','2014-11-16 10:37:23',0);
INSERT INTO "user_credentials_table" VALUES(4,4,'patient1','PATIENT','https://10.0.3.2:8443','doctor1','Pit','Mayer','1968-08-15 04:00:00','23JGH785FGH','','33c75dfe-c9e5-4cc7-8031-3c66a40447e4','2014-11-16 13:45:48','1970-01-01 00:00:00','2014-11-16 10:45:47',1);
CREATE TABLE alerted_patient_table (_id integer primary key AUTO_INCREMENT, LOGIN_ID INTEGER ,USERNAME TEXT ,NOT_IN_ALERT INTEGER  );
INSERT INTO "alerted_patient_table" VALUES(1,1,'doctor2','');
INSERT INTO "alerted_patient_table" VALUES(2,1,'patient2','');
CREATE TABLE doctor_details_table (_id integer primary key AUTO_INCREMENT, USERNAME TEXT ,FIRST_NAME TEXT ,LAST_NAME TEXT ,DOCTOR_ID TEXT  );
INSERT INTO "doctor_details_table" VALUES(1,'doctor1','Bob','Franklin','Licence:10293');
DELETE FROM sqlite_sequence;
INSERT INTO "sqlite_sequence" VALUES('user_credentials_table',4);
INSERT INTO "sqlite_sequence" VALUES('doctor_details_table',1);
INSERT INTO "sqlite_sequence" VALUES('medication_table',47);
INSERT INTO "sqlite_sequence" VALUES('alerted_patient_table',2);

