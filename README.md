## Project Requirements

Please see:

https://bytebucket.org/selena775/systemmanagement/raw/7280de9cbd71de82f3eeeae2547b01e63ce92f9d/SymptomManagementProjectRequirements.pdf


## Project Detailed Implementation

Please see:

https://bytebucket.org/selena775/systemmanagement/raw/7280de9cbd71de82f3eeeae2547b01e63ce92f9d/SystemManagementProjectImplementation.pdf

## Project Presentation

Please see:

1. https://www.youtube.com/watch?v=L_SDzsVvUVA
2. https://www.youtube.com/watch?v=zC3nLjpBVNg
3. https://www.youtube.com/watch?v=-QOvKBYnNAE
4. https://www.youtube.com/watch?v=jq8mgBL5kpI
5. https://www.youtube.com/watch?v=zC3nLjpBVNg


## Warning

UNDER NO CIRCUMSTANCES SHOULD YOU USE THE INCLUDED KEYSTORE IN A PRODUCTION APP!!!
UNDER NO CIRCUMSTANCES SHOULD YOU USE THIS APP "AS IS" IN PRODUCTION!!!

## Running the Server Application

Please read the instructions carefully.

To run the application:


## SERVER SIDE
On the command line

This command will start the embedded tomcat server
./gradlew bootRun

To run SuiteOneGroupe JUnit tests on the embedded server and fill the database with users
./gradlew testEmbedded
the results go in build/test-results/SuiteOneEmbedded


## CLIENT SIDE

Those commands will uninstall, install and start client on a device.
Please fill the server memory database by running ./gradlew testEmbedded
Start you emulator.

adb uninstall org.selena.patient.client
./gradlew uninstallAll

./gradlew clean assembleDebug
./gradlew installDebug

adb shell am start -n org.selena.patient.client.debug/org.selena.patient.client.activity.StartActivity

 The server ip can be changed by using Settings Menu in order to match the emulator


## SERVER SIDE REMOTE WEB SERVERS

The list of servers is retrieved from  ~/.ssh/config

To deploy war on the server
./gradlew deploy<ServerName>

To restart tomcat on the server
./gradlew restartTomcat<ServerName>

To run SuiteOneGroupe JUnit tests on the server
./gradlew test<ServerName>
the results go in build/test-results/SuiteOne<ServerName>

Note: To address localhost previous tasks, the serverName is "Local", ie  deployLocal, restartTomcatLocal and testLocal


Users for testing.

After running Junit tests with command testEmbedded, the users are following

Patient user names: patient1, doctor2, patient2, patient3, patient4, patient5, patient
Doctor usernames: doctor1 and doctor2 who is also patient of doctor1

doctor1 patients are: patient1, doctor2, patient2, patient3, patient4, patient5
doctor2 patients are: patient

all the accessing password are pass


## Accessing the Service

Note: you need to use "https" and port "8443":

https://localhost:8443/patient
https://localhost:8443/doctor
https://localhost:8443/doctorAndPatient
https://localhost:8443//user/search/findByUserName


You will almost certainly see a warning about the site's certificate in your browser. This
warning is being generated because the keystore includes a certificate that has not been
signed by a certificate authority. 

If you try to access the above URL in your browser, the server is going to generate an error 
that looks something like "An Authentication object was not found in the SecurityContext." 
If you want to use your browser to test the service, you will need to use a plug-in like 
Postman and an understanding of how to use it to manually construct and obtain a bearer token.

The server application shows how to programmatically access the patient service. You should
look at the SecuredRestBuilder class that is used to automatically intercept requests to the
VideoSvcApi methods, automatically obtain an OAuth 2.0 bearer token if needed, and add this
bearer token to HTTP requests. 

## Overview

This example covers a very small piece of the OAuth 2.0 specification that is
relevant to mobile developers that would like to use their *own* service and
authenticate to that service using OAuth 2.0. The entire OAuth 2.0 specification
covers many more uses cases and is described in detail here:

http://oauth.net/2/

The two key classes to look at in this example are OAuth2SecurityConfiguration and
SecuredRestBuilder. These two classes handle the server-side of setting up and
enforcing OAuth 2.0 security and the client-side of authenticating with an
OAuth 2.0 protected service via a password grant.

For detailed information on configuring a REAL certificate for an application
using Tomcat, please refer to:

http://tomcat.apache.org/tomcat-7.0-doc/ssl-howto.html

UNDER NO CIRCUMSTANCES SHOULD YOU USE THE INCLUDED KEYSTORE IN A PRODUCTION APP!!!

## What to Pay Attention to

In this version of the server service application, OAuth 2.0 has been used to protect
the various server service endpoints. Clients must request access through the /oauth/token
endpoint and then insert the token obtained into future requests for authorization.

The two key classes to look at in this example are OAuth2SecurityConfiguration and
SecuredRestBuilder. All the tests show how the client uses the SecuredRestBuilder.
