package org.selena.project.management.repository;

import com.fluentinterface.ReflectionBuilder;
import com.fluentinterface.builder.Builder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Collection;
import java.util.Date;

/**
 * A simple object to represent a patient
 * 
 * @author sklasnja
 */
@Entity
@DiscriminatorValue(value = "BOTH" )
public class DoctorAndPatient extends Patient implements IDoctor, IPatient, IUserName {

    public static DoctorAndPatientBuilder createDoctorAndPatient() {
        return ReflectionBuilder.implementationFor(DoctorAndPatientBuilder.class).create();
    }

    public interface DoctorAndPatientBuilder extends Builder<DoctorAndPatient> {
        public DoctorAndPatientBuilder withFirstName(String name);
        public DoctorAndPatientBuilder withLastName(String name);
        public DoctorAndPatientBuilder withDob(Date dob);
        public DoctorAndPatientBuilder withMedicalRecord(String name);
        public DoctorAndPatientBuilder withUserName(String userName);
        public DoctorAndPatientBuilder withDoctor(String doctor);
        public DoctorAndPatientBuilder withDoctorId(String doctor);
        public DoctorAndPatientBuilder withPassword(String password);
    }

    private String doctorId;

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(final String doctorId) {
        this.doctorId = doctorId;
    }


    //    @Override
//    public int hashCode() {
//        // Google Guava provides great utilities for hashing
//        return Objects.hashCode(getFirstName(), getLastName(), getMedicalRecord(), getDob(), getDoctor(), getDoctorId());
//    }
//
//    /**
//     * Two Videos are considered equal if they have exactly the same values for
//     * their firstName, url, and duration.
//     *
//     */
//    @Override
//    public boolean equals(Object obj) {
//        if (obj instanceof DoctorAndPatient) {
//            DoctorAndPatient other = (DoctorAndPatient) obj;
//            // Google Guava provides great utilities for equals too!
//            return Objects.equal(getFirstName(), other.getFirstName())
//                    && Objects.equal(getLastName(), other.getLastName())
//                    && Objects.equal(getMedicalRecord(), other.getMedicalRecord())
//                    && Objects.equal(getDob(), other.getDob())
//                    && Objects.equal(getDoctor(), other.getDoctor())
//                    && Objects.equal(getDoctorId(), other.getDoctorId());
//        } else {
//            return false;
//        }
//    }


    @Override
    public UserType getType() {
        return UserType.BOTH;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return AuthorityUtils.createAuthorityList("DOCTOR", "PATIENT");
    }
}
