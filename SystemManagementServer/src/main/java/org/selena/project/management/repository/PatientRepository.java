package org.selena.project.management.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;


/**
 * An interface for a repository that can store Patient
 * objects and allow them to be searched by different criteria.
 *
 * @author sklasnja
 *
 */
@Repository
public interface PatientRepository extends CrudRepository<Patient, Long>{

    // Find all patients with a matching name
    public Collection<Patient> findByFirstName(String name);

    public Collection<Patient> findByDoctor(String name);

    public Collection<Patient> findByDoctorAndInAlarm(String name, boolean alarm);

    public Patient findByDoctorAndUserName(String doctorName, String userName);

    public Patient findByMedicalRecord(String record);

    @Query("FROM Patient AS p WHERE p.doctor = ?1 and (upper(p.firstName) LIKE %?2% OR upper(p.lastName) LIKE %?2%) ")
    public Collection<Patient> findByDoctorAndPatientName(String doctorUserName, String pattern);

}
