package org.selena.project.management.repository;

import com.fluentinterface.ReflectionBuilder;
import com.fluentinterface.builder.Builder;
import com.google.common.base.Objects;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

/**
 * A simple object to represent a patient
 * 
 * @author sklasnja
 */
@Entity
@DiscriminatorValue(value = "PATIENT" )
public class Patient extends UserName implements IPatient, IUserName {

    public static PatientBuilder create() {
        return ReflectionBuilder.implementationFor(PatientBuilder.class).create();
    }

    public interface PatientBuilder extends Builder<Patient> {
        public PatientBuilder withFirstName(String name);
        public PatientBuilder withLastName(String name);
        public PatientBuilder withDob(Date dob);
        public PatientBuilder withMedicalRecord(String name);
        public PatientBuilder withUserName(String userName);
        public PatientBuilder withDoctor(String doctor);
        public PatientBuilder withPassword(String password);
    }

    private String doctor;

    private String medicalRecord;

    private boolean inAlarm;

    @OneToMany
    private Collection<Medication> medications = new HashSet<Medication>();

    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(final String doctor) {
        this.doctor = doctor;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(final String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public Collection<Medication> getMedications() {
        return medications;
    }

    public void setMedications(final Collection<Medication> medications) {
        this.medications = medications;
    }

    public boolean isInAlarm() {
        return inAlarm;
    }

    public void setInAlarm(final boolean inAlarm) {
        this.inAlarm = inAlarm;
    }

    /**
	 * Two Videos will generate the same hashcode if they have exactly the same
	 * values for their firstName, url, and duration.
	 * 
	 */

	@Override
	public int hashCode() {
		// Google Guava provides great utilities for hashing
		return Objects.hashCode(getFirstName(), getLastName(), getMedicalRecord(), getDob(), getDoctor());
	}

	/**
	 * Two Videos are considered equal if they have exactly the same values for
	 * their firstName, url, and duration.
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Patient) {
			Patient other = (Patient) obj;
			// Google Guava provides great utilities for equals too!
			return Objects.equal(getFirstName(), other.getFirstName())
                    && Objects.equal(getLastName(), other.getLastName())
                    && Objects.equal(getMedicalRecord(), other.getMedicalRecord())
                    && Objects.equal(getDob(), other.getDob())
					&& Objects.equal(getDoctor(), other.getDoctor());
		} else {
			return false;
		}
	}


    @Override
    public UserType getType() {
        return UserType.PATIENT;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return AuthorityUtils.createAuthorityList("PATIENT");
    }
}
