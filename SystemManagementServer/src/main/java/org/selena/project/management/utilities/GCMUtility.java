/*
 * Copyright (c) 2015.
 * Copyright from gradle @copyright@
 * Version @version@
 */

package org.selena.project.management.utilities;

import org.selena.project.management.json.GsonHttpMessageConverter;
import org.selena.project.management.pojo.GCMResponse;
import org.selena.project.management.pojo.GcmMessage;
import org.selena.project.management.repository.UserName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.List;

import static org.selena.project.management.utility.ProjectConstants.GCM_SERVER_URL;
import static org.selena.project.management.utility.ProjectConstants.MY_API_KEY;


public class GCMUtility {

    @Autowired
    private GsonHttpMessageConverter gsonHttpMessageConverter;

    private final HttpHeaders headers= new HttpHeaders();
    private final RestTemplate restTemplate = new RestTemplate();

    @PostConstruct
    public void init() {

        headers.add( HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString());
        headers.add( HttpHeaders.AUTHORIZATION, "key=" + MY_API_KEY);

        restTemplate.getMessageConverters().add( gsonHttpMessageConverter );
    }

    public void sendMessage( UserName to, String title, String message  ) {


        final List<String> regIds= to.getRegistrationIDs();
        if( regIds.isEmpty()) return;

        final GcmMessage gcmMessage = new GcmMessage();
        gcmMessage.setRegistration_ids(regIds);
        gcmMessage.setTitle(title);
        gcmMessage.setMessage(message);

        final HttpEntity<GcmMessage> entity = new HttpEntity<>(gcmMessage, headers);


        try {
            GCMResponse response = restTemplate.postForObject(GCM_SERVER_URL, entity, GCMResponse.class);

        } catch (HttpClientErrorException e) {

            String reason= e.getResponseBodyAsString();
        }
    }
}
