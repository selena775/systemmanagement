/*
 * Copyright (c) 2015.
 * Copyright from gradle @copyright@
 * Version @version@
 */

package org.selena.project.management.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;


/**
 * An interface for a repository that can store DoctorAndPatient
 * objects and allow them to be searched by title.
 *
 * @author sklasnja
 *
 */
@Repository
public interface UserNameRepository extends CrudRepository<UserName, Long>{

    public UserName findByUserName(String name);

    public Collection<UserName> findByUserNameNot(String name);

}
