package org.selena.project.management.controller;

import org.selena.project.management.repository.*;
import org.selena.project.management.utilities.Utility;
import org.selena.project.management.utility.Pair;
import org.selena.project.management.utility.ProjectConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;

import static org.selena.project.management.utility.PathConstants.*;

/**
 * Created by sklasnja on 11/4/14.
 */
@Controller
public class UserController {

    @Autowired
    private UserNameRepository userRepository;

    @Autowired
    private CheckInRepository checkIns;

    @Autowired
    private MedicationRepository medicationRep;

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = USER_SVC_PATH, method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteUsers() {

        medicationRep.deleteAll();

        Collection<UserName> users= userRepository.findByUserNameNot(ProjectConstants.ADMIN_USER);
        userRepository.delete(users);

        checkIns.deleteAll();

        return new ResponseEntity<Void>(HttpStatus.OK);
    }


    @PreAuthorize("(hasRole('PATIENT') or hasRole('DOCTOR')) and (#userName.equals(#p.name))")
    @RequestMapping(value = CHECK_ROLES, method = RequestMethod.GET)
    public
    @ResponseBody
    ResponseEntity<Collection<String>> roleByUserName(@RequestParam(USERNAME_PARAMETER) String userName, Principal p
    ) {

        final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return new ResponseEntity<>(getRoles(auth.getAuthorities()), HttpStatus.OK);

    }

    // Receives GET requests to /Patient/find and returns Patient
    // that have a userName (e.g., Patient.userName) matching the "patientCode" request
    // parameter value that is passed by the client
    @RequestMapping(value = FIND_BY_USER_NAME, method = RequestMethod.GET)
    public
    @ResponseBody
    ResponseEntity<IUserName> findByUserName(
            @RequestParam(USERNAME_PARAMETER) String name
    ) {

        UserName userName = userRepository.findByUserName(name);

        if (userName == null) return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        if (!Utility.isAuthorisedUser(userName)) return new ResponseEntity<>(HttpStatus.FORBIDDEN);

        return new ResponseEntity<IUserName>(userName, HttpStatus.OK);

    }

   // Updating RegistrationId for the user with the username equal to principal name
    @RequestMapping(value = USER_SVC_PATH, method = RequestMethod.POST)
    public
    @ResponseBody
    ResponseEntity<Void> addRegistrationId(
            @RequestBody final Pair<String,String > registrationID, final Principal principal
    ) {
        UserName userName = userRepository.findByUserName(principal.getName());

        if (userName == null) return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        userName.getRegistrationIDs().remove(registrationID.getRight());
        userName.getRegistrationIDs().add(registrationID.getLeft());

        userRepository.save(userName);

        return new ResponseEntity<Void>(HttpStatus.OK);
    }


    public static Collection<String> getRoles(Collection<? extends GrantedAuthority> roles) {
        Collection<String> authorities = new ArrayList<>(roles.size());

        for (GrantedAuthority role : roles) {
            authorities.add(role.toString());
        }

        return authorities;
    }

}
