package org.selena.project.management.utilities;

import org.selena.project.management.repository.Patient;
import org.selena.project.management.repository.UserName;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.TimeZone;


public class Utility {
    private static Date s_currentDateInSeconds;

    public static boolean isAuthorisedPatientOrDoctorOrAdmin(final Patient patient)
    {
        final Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        Collection<? extends GrantedAuthority> authorities = auth.getAuthorities();
        boolean bAdmin = authorities.contains(new SimpleGrantedAuthority("ADMIN"));
        boolean bDoctor = authorities.contains(new SimpleGrantedAuthority("DOCTOR"));

        return bAdmin || ( bDoctor && patient.getDoctor()!=null && auth.getName().equals(patient.getDoctor()) ||
                (patient.getUserName()!=null && auth.getName().equals(patient.getUserName() )));

    }

    public static boolean isAuthorisedUser(final UserName userName)
    {

        Patient p= null;
        if ( Patient.class.isAssignableFrom( userName.getClass())) {
            p = (Patient)userName;
        }

        final Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        Collection<? extends GrantedAuthority> authorities = auth.getAuthorities();
        boolean bAdmin = authorities.contains(new SimpleGrantedAuthority("ADMIN"));
        boolean bDoctor = authorities.contains(new SimpleGrantedAuthority("DOCTOR"));

        return bAdmin || auth.getName().equals(userName.getUserName() ) || ( bDoctor && p!=null && auth.getName().equals(p.getDoctor()) );

    }

    public static boolean isAuthorisedPatient(final Patient patient)
    {
        final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth.getName().equals(patient.getUserName());

    }

    public static boolean isAuthorisedDoctorOrAdmin(final Patient patient)
    {
        final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Collection<? extends GrantedAuthority> authorities = auth.getAuthorities();
        boolean bAdmin = authorities.contains(new SimpleGrantedAuthority("ADMIN"));
        boolean bDoctor = authorities.contains(new SimpleGrantedAuthority("DOCTOR"));

        return bAdmin || ( bDoctor && auth.getName().equals(patient.getDoctor()) );

    }

    public static boolean isAuthorisedDoctor(final Patient patient)
    {

        final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Collection<? extends GrantedAuthority> authorities = auth.getAuthorities();

        boolean bDoctor = authorities.contains(new SimpleGrantedAuthority("DOCTOR"));

        return bDoctor && auth.getName().equals(patient.getDoctor());

    }



    public static Date getTestDateWithOffset(int hours, int minutes, int seconds) {
       return getTestDateWithOffset( 0, hours, minutes, seconds );
    }

    public static Date getTestDateWithOffset(int days, int hours, int minutes, int seconds) {
        long time= System.currentTimeMillis() - ( days * DateTimeUtility.DAY_IN_MILLIS +
                hours * DateTimeUtility.HOUR_IN_MILLIS + minutes * DateTimeUtility.MINUTE_IN_MILLIS + seconds);
        return new Date(time);
    }

    public static Date getTestDateWithOffset(int seconds) {

        return getTestDateWithOffset(0,0,seconds);
    }

    public static Date trim(final long dateInMills, final TimeZone tz) {
        Calendar gc = Calendar.getInstance( tz );
        gc.clear();
        gc.setTimeInMillis(dateInMills);
        gc.set(Calendar.HOUR_OF_DAY, 0);
        gc.set(Calendar.MINUTE, 0);
        gc.set(Calendar.SECOND, 0);
        gc.set(Calendar.MILLISECOND, 0);
        return gc.getTime();
    }

    public static Date ceil(final long dateInMills, final TimeZone tz) {
        Calendar gc = Calendar.getInstance( tz );
        gc.clear();
        gc.setTimeInMillis(dateInMills);
        gc.set(Calendar.HOUR_OF_DAY, 0);
        gc.set(Calendar.MINUTE, 0);
        gc.set(Calendar.SECOND, 0);
        gc.set(Calendar.MILLISECOND, 0);
        gc.add(Calendar.DAY_OF_MONTH, 1);
        return gc.getTime();
    }

    public static long getRealUtcTime( Calendar cal, long localTimeInUtc )
    {
        final Calendar utcCal = Calendar.getInstance( TimeZone.getTimeZone( "UTC" ));

        utcCal.clear(); // Ensuring all fields that were previously set are cleared.
        utcCal.setTimeInMillis(localTimeInUtc);


        int second = utcCal.get( Calendar.SECOND );
        int minute = utcCal.get( Calendar.MINUTE );
        int hour = utcCal.get( Calendar.HOUR_OF_DAY );
        int date = utcCal.get( Calendar.DATE );
        int month = utcCal.get( Calendar.MONTH ); //NOTE: January == 0
        int year = utcCal.get( Calendar.YEAR );

        //
        // Get the real offset from Epoch, which specifies the market's
        // local date and time in its correct time zone (which may NOT
        // be UTC).
        //
        cal.clear(); // Ensuring all fields that were previously set are cleared.
        cal.set(year, month, date, hour, minute, second);

        long milliSeconds= localTimeInUtc % 1000;

        // Magic happens here - it'll automatically take into account DST
        return cal.getTimeInMillis() + milliSeconds;
    }


}
