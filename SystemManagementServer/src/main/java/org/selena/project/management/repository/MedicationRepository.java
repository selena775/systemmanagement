package org.selena.project.management.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Date;


/**
 * An interface for a repository that can store Medication
 * objects and allow them to be searched by title.
 *
 * @author sklasnja
 *
 */
@Repository
public interface MedicationRepository extends CrudRepository<Medication, Long>{


    // Find all patients with a matching name
    public Collection<Medication> findByPatient(Patient patient);

    // Find all patients with a matching name
    public Collection<Medication> findByPatientAndStartDateBeforeAndEndDateAfter(Patient patient,
                                                                                 Date date1,
                                                                                Date date2);

    // Find all patients with a matching name
    @Query("FROM Medication m WHERE m.patient = ?1 and m.valid = true")
    public Collection<Medication> findValidByPatient(Patient patient);

    @Modifying
    @Transactional
    @Query("update Medication m set m.valid=false, m.modificationTime=:nowDate WHERE m.id = :id")
    public void invalidateMedication(@Param("id") long id, @Param("nowDate") Date now);

    @Modifying
    @Transactional
    @Query("update Medication m set m.startDate=:startDate, m.endDate=:endDate, m.modificationTime=:nowDate, m.valid = true WHERE m.id=:id")
    public void updateMedication(@Param("id") long id, @Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("nowDate") Date now);
}


