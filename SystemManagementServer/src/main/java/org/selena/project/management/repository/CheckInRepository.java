package org.selena.project.management.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Date;


/**
 * An interface for a repository that can store CheckIn
 * objects and allow them to be searched by title.
 *
 * @author sklasnja
 *
 */
@Repository
public interface CheckInRepository extends CrudRepository<CheckIn, Long>{

    // Find all patients with a matching name
    public Collection<CheckIn> findByPatientId(long id);

    // Find all patients with a matching name
    public Collection<CheckIn> findByPatientIdOrderByTimestampAsc(long id);

    // Find all patients with a matching name
    public Collection<CheckIn> findByPatientIdOrderByTimestampDesc(long id);

    // Find all patients with a matching name
    public CheckIn findByPatientIdAndTimestamp(long id, Date date);

}
