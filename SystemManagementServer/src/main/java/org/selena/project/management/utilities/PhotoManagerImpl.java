package org.selena.project.management.utilities;

import javax.servlet.ServletOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.*;

public class PhotoManagerImpl implements PhotoManager {

    public static String STORAGE = System.getProperty("storage.dir", System.getProperty("user.home") + "/storage");

    @Override
    public String savePhotoData(long id, long cId, final InputStream photoData)throws IOException {
        Path dirPath = Paths.get(STORAGE);
        Files.createDirectories(dirPath);

        Path target = dirPath.resolve(String.valueOf(cId) + ".jpg");
        Files.copy(photoData, target, StandardCopyOption.REPLACE_EXISTING);
        return target.getFileName().toString();
    }

    @Override
    public String getPhotoFilePath(final long id, final long cId){
        return STORAGE + "/" + cId + ".jpg";
    }

    @Override
    public void copyPhotoData(final long id, final long cId,final ServletOutputStream out) throws IOException {
        Path dirPath = Paths.get(STORAGE);
        Path source = dirPath.resolve(String.valueOf(cId) + ".jpg");
        if(!Files.exists(source)){
            throw new FileNotFoundException("Unable to find the photo for checkIn:"+source.getFileName());
        }
        Files.copy(source, out);
    }

    @Override
    public void deleteStorage() throws IOException {
        deleteRecursive( new File(STORAGE));
    }

    void deleteRecursive(final File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (final File child : fileOrDirectory.listFiles())
                deleteRecursive(child);

        fileOrDirectory.delete();
    }

}
