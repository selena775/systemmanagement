/*
 * Copyright (c) 2015.
 * Copyright from gradle @copyright@
 * Version @version@
 */

package org.selena.project.management.utility;

/**
 * Created by sklasnja on 3/3/15.
 */
public class ProjectConstants {
    public static final String ADMIN_USER = "admin";

    public static final String GCM_SERVER_URL = "https://android.googleapis.com/gcm/send";
    public static final String PATIENT_WITH_ALARMING_SYMPTOMS = "PATIENT_WITH_ALARMING_SYMPTOMS";
    public static final String MY_API_KEY = "AIzaSyCL_FSELL8CYCGCQ3D3jOGOdCxadaQe6gY";
}
