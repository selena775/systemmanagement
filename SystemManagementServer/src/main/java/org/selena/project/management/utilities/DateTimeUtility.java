package org.selena.project.management.utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by sklasnja on 10/23/14.
 */
public class DateTimeUtility {


    public static final long SECOND_IN_MILLIS = 1000;
    public static final long MINUTE_IN_MILLIS = SECOND_IN_MILLIS * 60;
    public static final long HOUR_IN_MILLIS = MINUTE_IN_MILLIS * 60;
    public static final long DAY_IN_MILLIS = HOUR_IN_MILLIS * 24;
    public static final long WEEK_IN_MILLIS = DAY_IN_MILLIS * 7;

    private static final SimpleDateFormat dateFormatUTC = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    private static final SimpleDateFormat dateOnlyFormatUTC = new SimpleDateFormat("dd-MM-yyyy");

    public static String formatUtcDateTime(Date date) {

        return dateFormatUTC.format(date);
    }

    public static Date parseUtcDateTime(String text) throws ParseException {
        return dateFormatUTC.parse(text);
    }

    static {
//        dateFormatUTC.setTimeZone(TimeZone.getTimeZone("UTC"));
//        dateOnlyFormatUTC.setTimeZone(TimeZone.getTimeZone("UTC"));
    }


    public static String formatUtcDateOnly(Date date) {

        return dateOnlyFormatUTC.format(date);
    }

    public static Date parseUtcDateOnly(String text) throws ParseException {
        return dateOnlyFormatUTC.parse(text);
    }

}
