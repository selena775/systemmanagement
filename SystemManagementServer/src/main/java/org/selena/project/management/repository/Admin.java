/*
 * Copyright (c) 2015.
 * Copyright from gradle @copyright@
 * Version @version@
 */

package org.selena.project.management.repository;

import com.fluentinterface.ReflectionBuilder;
import com.fluentinterface.builder.Builder;
import com.google.common.base.Objects;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Collection;
import java.util.Date;

/**
 * A simple object to represent a patient
 * 
 * @author sklasnja
 */
@Entity
@DiscriminatorValue(value = "ADMIN" )
public class Admin extends UserName implements IUserName {

    public static AdminBuilder create() {
        return ReflectionBuilder.implementationFor(AdminBuilder.class).create();
    }

    public interface AdminBuilder extends Builder<Admin> {
        public AdminBuilder withFirstName(String name);
        public AdminBuilder withLastName(String name);
        public AdminBuilder withDob(Date dob);
        public AdminBuilder withUserName(String userName);
        public AdminBuilder withPassword(String password);
    }


	@Override
	public int hashCode() {
		// Google Guava provides great utilities for hashing
		return Objects.hashCode(getFirstName(), getLastName(), getDob());
	}

	/**
	 * Two Videos are considered equal if they have exactly the same values for
	 * their firstName, url, and duration.
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Admin) {
			Admin other = (Admin) obj;
			// Google Guava provides great utilities for equals too!
			return Objects.equal(getFirstName(), other.getFirstName())
                    && Objects.equal(getLastName(), other.getLastName())
                    && Objects.equal(getDob(), other.getDob());
		} else {
			return false;
		}
	}


    @Override
    public UserType getType() {
        return UserType.ADMIN;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return AuthorityUtils.createAuthorityList("ADMIN");
    }

}
