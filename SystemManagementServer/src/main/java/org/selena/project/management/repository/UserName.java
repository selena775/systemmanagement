package org.selena.project.management.repository;

import com.google.gson.annotations.SerializedName;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Date;
import java.util.List;


@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@Table(name = "USER_NAME", uniqueConstraints={@UniqueConstraint(name="USERNAME_COLUMNS_CONSTRAINT", columnNames={"userName"}),
        @UniqueConstraint(name="MEDICAL_RECORD_USERNAME_COLUMN_CONSTRAINT", columnNames={"medicalRecord", "userName"}),
        @UniqueConstraint(name="DOCTOR_ID_USERNAME_COLUMN_CONSTRAINT", columnNames={"doctorId", "userName"})})
@DiscriminatorColumn(name="USER_TYPE", discriminatorType = DiscriminatorType.STRING)
public abstract class UserName implements IUserName, UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    // Should be unique across the whole system and it is related to access the system
    private String userName;

    private String firstName;

    private String lastName;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dob;

    // Security fields
    // for now it is a plain password
    private String password;

    // For now let say a user owns only one device
    // we will use this for GCM
    @ElementCollection
    private List<String> registrationIDs;

    // Accessor
    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(final String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(final Date dob) {
        this.dob = dob;
    }

    @Override
    public List<String> getRegistrationIDs() {
        return registrationIDs;
    }

    public void setRegistrationIDs(List<String> registrationID) {
        this.registrationIDs = registrationID;
    }

    @Transient
    @SerializedName("userType")
    protected String type= getType().name();

    @Override
    public abstract UserType getType();


    // Security setters and getters
    public String getPassword() { return password; }

    public void setPassword(String password) { this.password = password;}

    @Override
    public String getUsername() {
        return getUserName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

}
