/*
 * Copyright (c) 2015.
 * Copyright from gradle @copyright@
 * Version @version@
 */

package org.selena.project.management;

import org.selena.project.management.json.GsonHttpMessageConverter;
import org.selena.project.management.repository.PatientRepository;
import org.selena.project.management.utilities.GCMUtility;
import org.selena.project.management.utilities.PhotoManager;
import org.selena.project.management.utilities.PhotoManagerImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.embedded.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.servlet.MultipartConfigElement;
import java.util.List;


// Tell Spring to automatically create a JPA implementation of our
// VideoRepository
@EnableJpaRepositories(basePackageClasses = PatientRepository.class)

// Tell Spring to turn on WebMVC (e.g., it should enable the DispatcherServlet
// so that requests can be routed to our Controllers)
@EnableWebMvc
@EnableScheduling



// Tell Spring that this object represents a Configuration for the
// application
@Configuration
//Tell Spring to automatically inject any dependencies that are marked in
//our classes with @Autowired
@EnableAutoConfiguration
// Tell Spring to go and scan our controller package (and all sub packages) to
// find any Controllers or other components that are part of our applciation.
// Any class in this package that is annotated with @Controller is going to be
// automatically discovered and connected to the DispatcherServlet.
@ComponentScan


//@SpringBootApplication
public class Application extends WebMvcConfigurerAdapter {

    private static final String MAX_REQUEST_SIZE = "150MB";

	// The app now requires that you pass the location of the keystore and
	// the password for your private key that you would like to setup HTTPS
	// with. In Eclipse, you can set these options by going to:
	//    1. Run->Run Configurations
	//    2. Under Java Applications, select your run configuration for this app
	//    3. Open the Arguments tab
	//    4. In VM Arguments, provide the following information to use the
	//       default keystore provided with the sample code:
	//
	//       -Dkeystore.file=src/main/resources/private/keystore -Dkeystore.pass=changeit
	//
	//    5. Note, this keystore is highly insecure! If you want more securtiy, you 
	//       should obtain a real SSL certificate:
	//
	//       http://tomcat.apache.org/tomcat-7.0-doc/ssl-howto.html
	//
	// Tell Spring to launch our app!
	public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    // This configuration element adds the ability to accept multipart
    // requests to the web container.
    @Bean
    public MultipartConfigElement multipartConfigElement() {
        // Setup the application container to be accept multipart requests
        final MultipartConfigFactory factory = new MultipartConfigFactory();
        // Place upper bounds on the size of the requests to ensure that
        // clients don't abuse the web container by sending huge requests
        factory.setMaxFileSize(MAX_REQUEST_SIZE);
        factory.setMaxRequestSize(MAX_REQUEST_SIZE);

        // Return the configuration to setup multipart in the container
        return factory.createMultipartConfig();
    }

    @Bean
    public PhotoManager getPhotoManager() {
        return new PhotoManagerImpl();

    }
    @Bean
    public InitDbUsers initialiseDatabase() {
        return new InitDbUsers();
    }

//    @Override
//    protected void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
//        config.exposeIdsFor(Patient.class, CheckIn.class, Doctor.class, DoctorAndPatient.class);
//    }



    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(getGsonConverter());
    }

    @Bean
    public GsonHttpMessageConverter getGsonConverter(){
        return new GsonHttpMessageConverter();
    }

    @Bean
    public GCMUtility getGCMUtility(){
        return new GCMUtility();
    }

    @Bean
    public PatientInAlarmCheck getCheckForPatientInAlarm() {
       return new PatientInAlarmCheck();
    }

}
