package org.selena.project.management.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

/**
 * An interface for a repository that can store Doctor
 * objects and allow them to be searched by title.
 *
 * @author sklasnja
 *
 */
@Repository
public interface DoctorRepository extends CrudRepository<Doctor, Long>{

    public Collection<Doctor> findByFirstName(String firstName);
    public Doctor findByDoctorId(String doctorId);

}
