package org.selena.project.management.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;


/**
 * An interface for a repository that can store DoctorAndPatient
 * objects and allow them to be searched by title.
 *
 * @author sklasnja
 *
 */
@Repository
public interface DoctorAndPatientRepository extends CrudRepository<DoctorAndPatient, Long>{

    // Find all patients with a matching name
    public Collection<DoctorAndPatient> findByFirstName(String name);

    public Collection<DoctorAndPatient> findByDoctor(String name);

    public DoctorAndPatient findByDoctorId(String doctorId);

    public DoctorAndPatient findByDoctorAndUserName(String doctorName, String userName);

}
