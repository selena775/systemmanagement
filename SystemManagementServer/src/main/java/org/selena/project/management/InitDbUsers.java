/*
 * Copyright (c) 2015.
 * Copyright from gradle @copyright@
 * Version @version@
 */

package org.selena.project.management;

import org.selena.project.management.repository.Admin;
import org.selena.project.management.repository.AdminRepository;
import org.selena.project.management.utility.ProjectConstants;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

public class InitDbUsers {

    @Autowired
    private AdminRepository adminRepository;

    @PostConstruct
    public void initDb(){
        adminRepository.save(Admin.create().withUserName(ProjectConstants.ADMIN_USER).withPassword("pass").build());
    }
}
