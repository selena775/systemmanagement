package org.selena.project.management;

import org.selena.project.management.controller.CheckInController;
import org.selena.project.management.repository.Patient;
import org.selena.project.management.repository.PatientRepository;
import org.selena.project.management.utilities.DateTimeUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

public class PatientInAlarmCheck {

    @Autowired
    private PatientRepository patients;

    @Autowired
    CheckInController checkInController;

    @Scheduled(fixedRate=30 * DateTimeUtility.MINUTE_IN_MILLIS)
    public void checkPatients() {
        for ( Patient patient: patients.findAll()) {
            checkInController.checkForAnAlert(patient);
        }
        System.out.println("**** Check for the patients with alarming symptoms *****");
    }

}