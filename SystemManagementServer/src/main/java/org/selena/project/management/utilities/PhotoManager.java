package org.selena.project.management.utilities;

import javax.servlet.ServletOutputStream;
import java.io.IOException;
import java.io.InputStream;

public interface PhotoManager {

    String savePhotoData(long id, long cId, final InputStream photoData)throws IOException;

    String getPhotoFilePath(final long id, final long cId);

    void copyPhotoData(final long id, final long cId, ServletOutputStream outputStream) throws IOException;

    void deleteStorage() throws IOException;
}
