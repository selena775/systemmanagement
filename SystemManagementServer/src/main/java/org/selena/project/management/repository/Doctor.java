package org.selena.project.management.repository;

import com.fluentinterface.ReflectionBuilder;
import com.fluentinterface.builder.Builder;
import com.google.common.base.Objects;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Collection;
import java.util.Date;

/**
 * A simple object to represent a patient
 * 
 * @author sklasnja
 */
@Entity
@DiscriminatorValue(value = "DOCTOR" )
public class Doctor extends UserName implements IDoctor, IUserName {

    public static DoctorBuilder create() {
        return ReflectionBuilder.implementationFor(DoctorBuilder.class).create();
    }

    public interface DoctorBuilder extends Builder<Doctor> {
        public DoctorBuilder withFirstName(String name);
        public DoctorBuilder withLastName(String name);
        public DoctorBuilder withDob(Date dob);
        public DoctorBuilder withUserName(String userName);
        public DoctorBuilder withDoctorId(String doctor);
        public DoctorBuilder withPassword(String password);
    }

    private String doctorId;

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(final String doctorId) {
        this.doctorId = doctorId;
    }

    /**
	 * Two Videos will generate the same hashcode if they have exactly the same
	 * values for their firstName, url, and duration.
	 * 
	 */

	@Override
	public int hashCode() {
		// Google Guava provides great utilities for hashing
		return Objects.hashCode(getFirstName(), getLastName(), doctorId, getDob());
	}

	/**
	 * Two Videos are considered equal if they have exactly the same values for
	 * their firstName, url, and duration.
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Doctor) {
			Doctor other = (Doctor) obj;
			// Google Guava provides great utilities for equals too!
			return Objects.equal(getFirstName(), other.getFirstName())
                    && Objects.equal(getLastName(), other.getLastName())
                    && Objects.equal(getDoctorId(), other.getDoctorId())
                    && Objects.equal(getDob(), other.getDob());
		} else {
			return false;
		}
	}


    @Override
    public UserType getType() {
        return UserType.DOCTOR;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return AuthorityUtils.createAuthorityList("DOCTOR");
    }

}
