package org.selena.project.management.controller;

import org.selena.project.management.repository.CheckIn;
import org.selena.project.management.repository.CheckInRepository;
import org.selena.project.management.repository.Patient;
import org.selena.project.management.repository.PatientRepository;
import org.selena.project.management.utilities.PhotoManager;
import org.selena.project.management.utilities.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;

import static org.selena.project.management.utility.PathConstants.*;

/**
 * Created by sklasnja on 10/13/14.
 */
@Controller
public class PhotoController {

    @Autowired
    private PatientRepository patients;

    @Autowired
    private CheckInRepository checkIns;

    @Autowired
    private PhotoManager photoDataMgr;

    @PreAuthorize("hasRole('PATIENT')")
    @RequestMapping(value = PATIENT_CHECK_IN_PHOTO, method = RequestMethod.POST)
    public
    @ResponseBody
    ResponseEntity<String> addCheckInPhoto(@PathVariable(ID) final long id,
                                           @PathVariable(CHECK_IN_ID) final long chkId,
                                           @RequestParam(PHOTO_PARAMETER) MultipartFile photoData) throws IOException {

        Patient patient = patients.findOne(id);
        if (patient == null) return new ResponseEntity<String>(HttpStatus.NOT_FOUND);

        if (!Utility.isAuthorisedPatient(patient)) return new ResponseEntity<String>(HttpStatus.FORBIDDEN);

        CheckIn checkIn = checkIns.findOne(chkId);
        if (checkIn == null) return new ResponseEntity<String>(HttpStatus.NOT_FOUND);

        return new ResponseEntity<String>(photoDataMgr.savePhotoData(id, chkId, photoData.getInputStream()), HttpStatus.OK);
    }

    @RequestMapping(value = PATIENT_CHECK_IN_PHOTO, method = RequestMethod.GET)
    public
    @ResponseBody
    ResponseEntity<Void> getPatientCheckInPhoto(@PathVariable(ID) final long id,
                                                                @PathVariable(CHECK_IN_ID) final long chkId,
                                                                HttpServletResponse response) throws IOException {

        Patient patient = patients.findOne(id);

        if (patient == null) new ResponseEntity<Collection<CheckIn>>(HttpStatus.NOT_FOUND);

        if (!Utility.isAuthorisedDoctorOrAdmin(patient)) return new ResponseEntity<Void>(HttpStatus.FORBIDDEN);

        CheckIn checkIn = checkIns.findOne(chkId);
        if (checkIn == null) return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        try {
            photoDataMgr.copyPhotoData(id, chkId, response.getOutputStream());
            response.setContentType("image/jpg");
            response.flushBuffer();
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (FileNotFoundException ex) {
            return new ResponseEntity<Void>(HttpStatus.OK);
        }
    }


    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = STORAGE_PATH, method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteStorage() throws IOException {

        photoDataMgr.deleteStorage();
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
