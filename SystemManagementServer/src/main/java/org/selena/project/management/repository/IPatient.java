package org.selena.project.management.repository;

import java.util.Collection;


public interface IPatient extends IUserName {

    public String getDoctor();

    public void setDoctor(final String doctor);

    public String getMedicalRecord();

    public void setMedicalRecord(final String medicalRecord);

    public Collection<Medication> getMedications();

    public void setMedications(final Collection<Medication> medications);

}
