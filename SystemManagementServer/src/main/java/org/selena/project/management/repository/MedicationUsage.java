package org.selena.project.management.repository;

import com.fluentinterface.ReflectionBuilder;
import com.fluentinterface.builder.Builder;
import com.google.common.base.Objects;
import org.selena.project.management.json.ExcludeGson;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by sklasnja on 10/13/14.
 */
@Entity
public class MedicationUsage {

    public static MedicationBuilder create() {
        return ReflectionBuilder.implementationFor(MedicationBuilder.class).create();
    }

    public interface MedicationBuilder extends Builder<MedicationUsage> {
        public MedicationBuilder withName(String name);
        public MedicationBuilder withTaken(MedicineTaken taken);
        public MedicationBuilder withTime(Date time);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @ManyToOne
    @ExcludeGson
    private CheckIn checkin;

    private String name;

    @Enumerated(EnumType.STRING)
    private MedicineTaken taken;

    @Temporal(TemporalType.TIMESTAMP)
    private Date time;

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public CheckIn getCheckin() {
        return checkin;
    }

    public void setCheckin(final CheckIn checkin) {
        this.checkin = checkin;
    }

    public MedicineTaken getTaken() {
        return taken;
    }

    public void setTaken(final MedicineTaken taken) {
        this.taken = taken;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(final Date time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Two Videos will generate the same hashcode if they have exactly the same
     * values for their name, url, and duration.
     *
     */
    @Override
    public int hashCode() {
        // Google Guava provides great utilities for hashing
        return Objects.hashCode(name);
    }

    /**
     * Two Videos are considered equal if they have exactly the same values for
     * their name, url, and duration.
     *
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MedicationUsage) {
            MedicationUsage other = (MedicationUsage) obj;
            // Google Guava provides great utilities for equals too!
            return  name.equals( other.name );
        } else {
            return false;
        }
    }

    public static enum MedicineTaken {
        NO, YES
    }

}
