package org.selena.project.management.controller;

import com.google.common.collect.Lists;
import org.selena.project.management.repository.Medication;
import org.selena.project.management.repository.MedicationRepository;
import org.selena.project.management.repository.Patient;
import org.selena.project.management.repository.PatientRepository;
import org.selena.project.management.utilities.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import static org.selena.project.management.utility.PathConstants.*;

@Controller
public class MedicationController {

    @Autowired
    private PatientRepository patients;


    @Autowired
    private MedicationRepository medicationRep;

    @PreAuthorize("hasRole('DOCTOR')")
    @RequestMapping(value = PATIENT_ID_MEDICATIONS, method = RequestMethod.POST)
    public
    @ResponseBody
    ResponseEntity<Void> addMedications(@PathVariable(ID) final long id, @RequestBody final Collection<Medication> medications ) {

        Patient patient = patients.findOne(id);

        if (patient == null) new ResponseEntity<Patient>(HttpStatus.NOT_FOUND);

        if (!Utility.isAuthorisedDoctor(patient)) return new ResponseEntity<>(HttpStatus.FORBIDDEN);

        List<Medication> newMedications= Lists.newArrayList(medications);

        Date now = new Date();

        for( Medication oldMed: medicationRep.findByPatient(patient) ){
            int pos;
            if ( -1 != (pos = newMedications.indexOf(oldMed))){
                // changed medication
                Medication newMed= newMedications.get(pos);
                if( !newMed.equals(oldMed) || !oldMed.isValid() ) {
                    medicationRep.updateMedication(oldMed.getId(), newMed.getStartDate(), newMed.getEndDate(), now);
                }
                newMedications.remove(pos);
            } else if (oldMed.isValid()) {
                //deleted Medication
                medicationRep.invalidateMedication(oldMed.getId(), now);
            }
        }
        for ( Medication newMed: newMedications ) {
            newMed.setPatient(patient);
            newMed.setId(0);
            medicationRep.save(newMed);
        }

        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = PATIENT_ID_MEDICATIONS, method = RequestMethod.GET)
    public
    @ResponseBody
    ResponseEntity<Collection<Medication>> getPatientMedications(@PathVariable(ID) final long id) {

        Patient patient = patients.findOne(id);

        if (patient == null) new ResponseEntity<Collection<String>>(HttpStatus.NOT_FOUND);

        if (!Utility.isAuthorisedPatientOrDoctorOrAdmin(patient)) return new ResponseEntity<>(HttpStatus.FORBIDDEN);

        return new ResponseEntity<>(medicationRep.findValidByPatient(patient), HttpStatus.OK);

    }

    @RequestMapping(value = FIND_BY_PATIENT_MEDICATION_AND_DATE, method = RequestMethod.GET)
    public
    @ResponseBody
    ResponseEntity<Collection<Medication>> getPatientMedicationsForTheDay(@PathVariable(ID) final long id,
                                                                          @RequestParam(DATE_PARAMETER) long date) {

        Patient patient = patients.findOne(id);

        if (patient == null) new ResponseEntity<Collection<String>>(HttpStatus.NOT_FOUND);

        if (!Utility.isAuthorisedPatientOrDoctorOrAdmin(patient)) return new ResponseEntity<Collection<Medication>>(HttpStatus.FORBIDDEN);

        Collection<Medication> medicationCollection =
                medicationRep.findByPatientAndStartDateBeforeAndEndDateAfter(patient, new Date(date + 1000), new Date(date));

        return new ResponseEntity<Collection<Medication>>(medicationCollection, HttpStatus.OK);

    }

}
