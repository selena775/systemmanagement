package org.selena.project.management.controller;

import org.selena.project.management.repository.*;
import org.selena.project.management.utilities.DateTimeUtility;
import org.selena.project.management.utilities.GCMUtility;
import org.selena.project.management.utilities.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static org.selena.project.management.utility.PathConstants.*;
import static org.selena.project.management.utility.ProjectConstants.*;

@Controller
public class CheckInController {

    @Autowired
    private GCMUtility gcmUtility;

    @Autowired
    private PatientRepository patients;

    @Autowired
    private CheckInRepository checkIns;

    @Autowired
    private MedicationUsageRepository medicationUsages;

    @Autowired
    private UserNameRepository users;

    @RequestMapping(value = PATIENT_CHECK_IN_SVC_PATH, method = RequestMethod.GET)
    public
    @ResponseBody
    ResponseEntity<Collection<CheckIn>> getPatientCheckIns(@PathVariable(ID) final long id) {

        Patient patient = patients.findOne(id);

        if (patient == null) new ResponseEntity<Collection<CheckIn>>(HttpStatus.NOT_FOUND);

        if (!Utility.isAuthorisedDoctorOrAdmin(patient)) return new ResponseEntity<Collection<CheckIn>>(HttpStatus.FORBIDDEN);

        Collection<CheckIn> checkInCol = checkIns.findByPatientIdOrderByTimestampDesc(id);
        for ( CheckIn chkIn : checkInCol ) {
            chkIn.setMedicationUsage(medicationUsages.findByCheckin(chkIn));
        }

        return new ResponseEntity<Collection<CheckIn>>(checkInCol, HttpStatus.OK);

    }

    @PreAuthorize("hasRole('PATIENT')")
    @RequestMapping(value = PATIENT_CHECK_IN_SVC_PATH, method = RequestMethod.POST)
    public
    @ResponseBody
    ResponseEntity<Long> addCheckIn(@PathVariable(ID) final long id,
                                       @RequestBody CheckIn checkIn) {

        final Patient patient = patients.findOne(id);

        if (patient == null) return new ResponseEntity<Long>(HttpStatus.NOT_FOUND);

        if (!Utility.isAuthorisedPatient(patient)) return new ResponseEntity<Long>(HttpStatus.FORBIDDEN);

        if (id != checkIn.getPatientId()) return new ResponseEntity<Long>(HttpStatus.CONFLICT);

        Date date= checkIn.getTimestamp();
        if (date == null) throw new MissingDateException();

        if (checkIns.findByPatientIdAndTimestamp(id, date)!= null)
            return new ResponseEntity<Long>(HttpStatus.ALREADY_REPORTED);

        checkIn.setChkInId(0);
        CheckIn chIn = checkIns.save(checkIn);

        Set<MedicationUsage> medicationUsage = new HashSet<MedicationUsage>(checkIn.getMedicationUsage().size());
        for ( MedicationUsage muO :checkIn.getMedicationUsage() ){
            MedicationUsage mu= new MedicationUsage();
            mu.setId(0);
            mu.setCheckin(chIn);
            mu.setName(muO.getName());
            mu.setTaken(muO.getTaken());
            mu.setTime(muO.getTime());
            medicationUsage.add(mu);
        }

        medicationUsages.save(medicationUsage);

        checkForAnAlert( patient );

        return new ResponseEntity<Long>(chIn.getChkInId(), HttpStatus.OK);
    }

    public synchronized void checkForAnAlert(final Patient patient) {

        Collection<CheckIn> checkInList = checkIns.findByPatientIdOrderByTimestampAsc(patient.getId());

        CheckIn.PainIntensity lastKnownPainBefore12H = null;
        CheckIn.PainIntensity lastKnownPainBefore16H = null;
        CheckIn.EatingDisorder lastEatingBefore12HAgo = null;

        boolean notEatingIn12H = true, severePainIn12H = true, moderatePainIn16H = true;

        long time12hAgo= System.currentTimeMillis() - 12 * DateTimeUtility.HOUR_IN_MILLIS;
        long time16hAgo= System.currentTimeMillis() - 16 * DateTimeUtility.HOUR_IN_MILLIS;

        for ( CheckIn checkin : checkInList ) {

            long chekInTimeInMills= checkin.getTimestamp().getTime();

            if ( chekInTimeInMills >  time12hAgo ){
                if (CheckIn.EatingDisorder.CANNOT_EAT != checkin.getEatingDisorder()) {
                    notEatingIn12H = false;
                }
                if (CheckIn.PainIntensity.SEVERE != checkin.getPainIntensity()) {
                    severePainIn12H = false;
                }
            } else {
                lastKnownPainBefore12H= checkin.getPainIntensity();
                lastEatingBefore12HAgo= checkin.getEatingDisorder();
            }

            if ( chekInTimeInMills > time16hAgo ){

                if (CheckIn.PainIntensity.WELL_CONTROLLED == checkin.getPainIntensity()) {
                    moderatePainIn16H = false;
                }
            } else {
                lastKnownPainBefore16H= checkin.getPainIntensity();

            }
        }

        if ( (notEatingIn12H && CheckIn.EatingDisorder.CANNOT_EAT==lastEatingBefore12HAgo) ||
                (severePainIn12H && CheckIn.PainIntensity.SEVERE==lastKnownPainBefore12H) ||
                (moderatePainIn16H &&
                        ( CheckIn.PainIntensity.SEVERE==lastKnownPainBefore16H || CheckIn.PainIntensity.MODERATE==lastKnownPainBefore16H)) ) {
            patient.setInAlarm(true);
            patients.save(patient);

            notifyDoctor( patient );

        } else {
            patient.setInAlarm(false);
            patients.save(patient);

        }
    }

    private void notifyDoctor(final Patient patient) {

        final UserName doctor = users.findByUserName(patient.getDoctor());

        gcmUtility.sendMessage(doctor, PATIENT_WITH_ALARMING_SYMPTOMS,
                MessageFormat.format("{0},{1}", doctor.getUserName(), patient.getUserName()));
    }


    @RequestMapping(value = PATIENT_CHECK_IN_ID_SVC_PATH, method = RequestMethod.GET)
    public
    @ResponseBody
    ResponseEntity<CheckIn> getCheckInById(@PathVariable(ID) final long id,
                                           @PathVariable(CHECK_IN_ID) final long cId) {

        Patient patient = patients.findOne(id);

        if (patient == null) return new ResponseEntity<CheckIn>(HttpStatus.NOT_FOUND);

        if (!Utility.isAuthorisedDoctorOrAdmin(patient)) return new ResponseEntity<CheckIn>(HttpStatus.FORBIDDEN);

        CheckIn chkIn = checkIns.findOne(cId);
        chkIn.setMedicationUsage(medicationUsages.findByCheckin(chkIn));

        return new ResponseEntity<CheckIn>(chkIn, HttpStatus.OK);
    }


    @ResponseStatus(value= HttpStatus.NOT_ACCEPTABLE, reason="Check in date is required.")
    public class MissingDateException extends RuntimeException {
    }
}