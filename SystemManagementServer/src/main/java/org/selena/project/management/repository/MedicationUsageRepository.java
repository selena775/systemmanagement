package org.selena.project.management.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;


/**
 * An interface for a repository that can store MedicationUsage
 * objects and allow them to be searched by title.
 *
 * @author sklasnja
 *
 */
@Repository
public interface MedicationUsageRepository extends CrudRepository<MedicationUsage, Long>{

    // Find all patients with a matching name
    public Collection<MedicationUsage> findByCheckin(CheckIn checkIn);
}
