package org.selena.project.management.repository;

import com.fluentinterface.ReflectionBuilder;
import com.fluentinterface.builder.Builder;
import com.google.common.base.Objects;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;


@Entity
public class CheckIn {

    public static CheckInBuilder create() {
        return ReflectionBuilder.implementationFor(CheckInBuilder.class).create();
    }

    public interface CheckInBuilder extends Builder<CheckIn> {
        public CheckInBuilder withPatientId(long patientId);
        public CheckInBuilder withTimestamp(Date timestamp);
        public CheckInBuilder withPainIntensity(PainIntensity painIntensity);
        public CheckInBuilder withEatingDisorder(EatingDisorder eatingDisorder);
        public CheckInBuilder withMedicationUsage(Collection<MedicationUsage> medicationUsages);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long chkInId;

    private long patientId;

    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;

    @Enumerated(EnumType.STRING)
    private PainIntensity painIntensity;

    @Enumerated(EnumType.STRING)
    private EatingDisorder eatingDisorder;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "checkin", fetch = FetchType.EAGER )
    private Collection<MedicationUsage> medicationUsage = new HashSet<MedicationUsage>();

    public long getChkInId() {
        return chkInId;
    }

    public void setChkInId(final long chkInId) {
        this.chkInId = chkInId;
    }

    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(final long patientId) {
        this.patientId = patientId;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(final Date timestamp) {
        this.timestamp = timestamp;
    }

    public PainIntensity getPainIntensity() {
        return painIntensity;
    }

    public void setPainIntensity(final PainIntensity painIntensity) {
        this.painIntensity = painIntensity;
    }

    public EatingDisorder getEatingDisorder() {
        return eatingDisorder;
    }

    public void setEatingDisorder(final EatingDisorder eatingDisorder) {
        this.eatingDisorder = eatingDisorder;
    }

    public Collection<MedicationUsage> getMedicationUsage() {
        return medicationUsage;
    }

    public void setMedicationUsage(final Collection<MedicationUsage> medicationUsage) {
        this.medicationUsage = medicationUsage;
    }

    /**
     * Two Videos will generate the same hashcode if they have exactly the same
     * values for their name, url, and duration.
     *
     */
    @Override
    public int hashCode() {
        // Google Guava provides great utilities for hashing
        return Objects.hashCode(patientId, timestamp.getTime()/1000);
    }

    /**
     * Two Videos are considered equal if they have exactly the same values for
     * their name, url, and duration.
     *
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof CheckIn) {
            CheckIn other = (CheckIn) obj;
            // Google Guava provides great utilities for equals too!
            return     patientId == other.patientId
                    && timestamp.getTime()/1000L == other.timestamp.getTime()/1000L;
        } else {
            return false;
        }
    }


    public static enum EatingDisorder {
        NO, SOME, CANNOT_EAT
    }

    public static enum PainIntensity {
        WELL_CONTROLLED, MODERATE, SEVERE
    }
}
