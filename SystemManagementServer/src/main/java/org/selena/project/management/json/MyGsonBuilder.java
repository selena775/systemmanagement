package org.selena.project.management.json;

import com.google.gson.*;
import org.selena.project.management.repository.*;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;

import java.lang.reflect.Type;

/**
 * Created by sklasnja on 11/4/14.
 */
public class MyGsonBuilder {

    private GsonBuilder gsonBuilder= new GsonBuilder()
            .registerTypeAdapter(IDoctor.class, new IDoctorDeserializer())
            .registerTypeAdapter(IDoctor.class, new IDoctorSerializer())
            .registerTypeAdapter(DefaultOAuth2AccessToken.class, new DefaultOAuth2AccessTokenAccessTokenSerializer())
            .registerTypeAdapter(IUserName.class, new IUserNameDeserializer())
            .registerTypeAdapter(IUserName.class, new IUserNameSerializer())
            .serializeNulls()
            .setExclusionStrategies(new ExcludeGsonAnnotationExclusionStrategy());

    public Gson create() {

       return gsonBuilder.create();

    }

    public static class IDoctorDeserializer implements JsonDeserializer<IDoctor> {
        @Override
        public IDoctor deserialize(JsonElement json, Type type, JsonDeserializationContext jdc)
                throws JsonParseException {

            final JsonObject jsonObject = json.getAsJsonObject();

            try {
                final String userType = jsonObject.get("userType").getAsString();

                if ( userType.equals("BOTH")) {

                    return jdc.deserialize(json, DoctorAndPatient.class);
                } else {

                    return jdc.deserialize(json, Doctor.class);
                }

            } catch (Throwable e) {
                throw new RuntimeException(e);
            }

        }
    }

    public static class IUserNameDeserializer implements JsonDeserializer<IUserName> {
        @Override
        public IUserName deserialize(JsonElement json, Type type, JsonDeserializationContext jdc)
                throws JsonParseException {

            final JsonObject jsonObject = json.getAsJsonObject();

            try {
                final String userType = jsonObject.get("userType").getAsString();

                if ( userType.equals("BOTH")) {

                    return jdc.deserialize(json, DoctorAndPatient.class);
                } else if ( userType.equals("DOCTOR")){

                    return jdc.deserialize(json, Doctor.class);
                } else {
                    return jdc.deserialize(json, Patient.class);
                }

            } catch (Throwable e) {
                throw new RuntimeException(e);
            }

        }
    }


    public static class IDoctorSerializer implements JsonSerializer<IDoctor> {

        @Override
        public JsonElement serialize(final IDoctor doctor, final Type typeOfSrc, final JsonSerializationContext context) {

            JsonElement element;
            if (IUserName.UserType.DOCTOR == doctor.getType()) {
                element = context.serialize((Doctor)doctor, typeOfSrc );
            } else {
                element = context.serialize((DoctorAndPatient)doctor, typeOfSrc );
            }
            return element;
        }
    }


    public static class DefaultOAuth2AccessTokenAccessTokenSerializer implements JsonSerializer<DefaultOAuth2AccessToken> {

        @Override
        public JsonElement serialize(final DefaultOAuth2AccessToken accessToken, final Type typeOfSrc, final JsonSerializationContext context) {

            JsonObject jsonObject= new JsonObject();

            jsonObject.addProperty("access_token", accessToken.getValue());
            jsonObject.addProperty("token_type", accessToken.getTokenType());
            jsonObject.addProperty("expires_in", accessToken.getExpiresIn());
            JsonArray jsonArray= new JsonArray();
            for ( String scope : accessToken.getScope() ) {
                jsonArray.add( new JsonPrimitive(scope));
            }
            jsonObject.add("scope", jsonArray);
            return jsonObject;

        }
    }

    public static class IUserNameSerializer implements JsonSerializer<IUserName> {

        @Override
        public JsonElement serialize(final IUserName userName, final Type typeOfSrc, final JsonSerializationContext context) {

            JsonElement element;
            if (IUserName.UserType.PATIENT == userName.getType()) {
                element= context.serialize((Patient)userName, typeOfSrc );

            } else {
                element = context.serialize((IDoctor)userName, typeOfSrc );
            }
            return element;
        }
    }

    // Excludes any field (or class) that is tagged with an "&#64FooAnnotation"
   private static class ExcludeGsonAnnotationExclusionStrategy implements ExclusionStrategy {
       public boolean shouldSkipClass(Class clazz) {
             return clazz.getAnnotation(ExcludeGson.class) != null;
           }

               public boolean shouldSkipField(FieldAttributes f) {
            return f.getAnnotation(ExcludeGson.class) != null;
           }
     }
}
