package org.selena.project.management.repository;

import com.fluentinterface.ReflectionBuilder;
import com.fluentinterface.builder.Builder;
import com.google.common.base.Objects;
import org.hibernate.annotations.Type;
import org.selena.project.management.json.ExcludeGson;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table( uniqueConstraints={@UniqueConstraint(name="MEDICATION_NAME_ID_COLUMNS_CONSTRAINT", columnNames={"name", "id"})} )
public class Medication {

    public static MedicationBuilder create() {
        return ReflectionBuilder.implementationFor(MedicationBuilder.class).create();
    }

    public interface MedicationBuilder extends Builder<Medication> {
        public MedicationBuilder withName(String name);
        public MedicationBuilder withStartDate(Date startDate);
        public MedicationBuilder withEndDate(Date endDate);

    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String name;

    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

    @ManyToOne
    @ExcludeGson
    private Patient patient;

    @Type(type="yes_no")
    @ExcludeGson
    private boolean valid= true;

    @ExcludeGson
    @Temporal(TemporalType.TIMESTAMP)
    private Date modificationTime;

    @ExcludeGson
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationTime= new Date();

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(final Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(final Date endDate) {
        this.endDate = endDate;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(final Patient patient) {
        this.patient = patient;
    }

    public Date getModificationTime() {
        return modificationTime;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public boolean isValid() {
        return valid;
    }

    /**
     * Two Videos will generate the same hashcode if they have exactly the same
     * values for their name, url, and duration.
     *
     */
    @Override
    public int hashCode() {
        // Google Guava provides great utilities for hashing
        return Objects.hashCode(name, startDate, endDate);
    }

    /**
     * Two Videos are considered equal if they have exactly the same values for
     * their name, url, and duration.
     *
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Medication) {
            Medication other = (Medication) obj;
            // Google Guava provides great utilities for equals too!
            return  name.equals( other.name ) && startDate.equals( other.startDate ) && endDate.equals( other.endDate );
        } else {
            return false;
        }
    }
}
