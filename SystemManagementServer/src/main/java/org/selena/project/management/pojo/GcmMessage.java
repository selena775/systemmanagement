/*
 * Copyright (c) 2015.
 * Copyright from gradle @copyright@
 * Version @version@
 */

package org.selena.project.management.pojo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by sklasnja on 3/3/15.
 */
public class GcmMessage {
    private Map<String, String> data= new HashMap<>();
    private List<String> registration_ids;

    public void setTitle(String type) {
        data.put("title", type );
    }

    public void setMessage(String message) {
        data.put("message", message );
    }

    public Map<String, String> getData() {
        return data;
    }

    public void setData(Map<String, String> data) {
        this.data = data;
    }

    public List<String> getRegistration_ids() {
        return registration_ids;
    }

    public void setRegistration_ids(List<String> registration_ids) {
        this.registration_ids = registration_ids;
    }

}
