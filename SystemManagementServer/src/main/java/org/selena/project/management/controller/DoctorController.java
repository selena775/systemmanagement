package org.selena.project.management.controller;

import com.google.common.collect.Lists;
import org.selena.project.management.repository.*;
import org.selena.project.management.utilities.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;

import static org.selena.project.management.utility.PathConstants.*;

@Controller
public class DoctorController {

    @Autowired
    private UserNameRepository userRepository;

    @Autowired
    private DoctorRepository doctors;

    @Autowired
    private DoctorAndPatientRepository doctorAndPatients;

    @Autowired
    private PatientRepository patients;

    @RequestMapping(value = PATIENT_DOCTOR, method = RequestMethod.GET)
    public
    @ResponseBody
    ResponseEntity<UserName> getPatientsDoctor(@PathVariable(ID) final long id) {

        Patient patient = patients.findOne(id);

        if ( patient==null ) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        if ( patient.getDoctor()==null ) throw new PatientController.MissingDoctorException();

        if ( !Utility.isAuthorisedPatientOrDoctorOrAdmin(patient) ) return new ResponseEntity<>(HttpStatus.FORBIDDEN);

        return new ResponseEntity<>(userRepository.findByUserName(patient.getDoctor()), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = DOCTOR_SVC_PATH, method = RequestMethod.POST)
    public
    @ResponseBody
    ResponseEntity<Doctor> addDoctor(
            @RequestBody Doctor doctor) {

        if (doctor.getDoctorId() == null) throw new MissingDoctorIdException();

        if ( userRepository.findByUserName(doctor.getUserName()) !=null ||
                doctors.findByDoctorId(doctor.getDoctorId())!=null ||
                    doctorAndPatients.findByDoctorId(doctor.getDoctorId())!=null )
            return new ResponseEntity<>(HttpStatus.CONFLICT);

        doctor.setId(0);
        return new ResponseEntity<>(doctors.save(doctor), HttpStatus.OK);
    }


    @PreAuthorize("hasRole('ADMIN') or hasRole('DOCTOR')")
    @RequestMapping(value = DOCTOR_AND_PATIENT_SVC_PATH, method = RequestMethod.POST)
    public
    @ResponseBody
    ResponseEntity<DoctorAndPatient> addDoctorAndPatient(
            @RequestBody DoctorAndPatient doctorAndPatient) {

        if (doctorAndPatient.getDoctor() == null) throw new PatientController.MissingDoctorException();
        if (userRepository.findByUserName(doctorAndPatient.getDoctor())==null ) throw new PatientController.MissingDoctorException();
        if (doctorAndPatient.getMedicalRecord() == null) throw new PatientController.MissingMedicalRecord();
        if (doctorAndPatient.getDoctorId() == null) throw new MissingDoctorIdException();

        if ( userRepository.findByUserName(doctorAndPatient.getUserName()) != null ||
                doctors.findByDoctorId(doctorAndPatient.getDoctorId())!=null ||
                    doctorAndPatients.findByDoctorId(doctorAndPatient.getDoctorId())!=null ||
                        patients.findByMedicalRecord(doctorAndPatient.getMedicalRecord())!=null )
            return new ResponseEntity<>(HttpStatus.CONFLICT);

        doctorAndPatient.setId(0);
        return new ResponseEntity<>(doctorAndPatients.save(doctorAndPatient), HttpStatus.OK);
    }


    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = DOCTOR_FIRST_NAME_SEARCH_PATH, method = RequestMethod.GET)
    public
    @ResponseBody
    Collection<IDoctor> findByFirstName(
            @RequestParam(FIRST_NAME_PARAMETER) String name) {
        Collection<IDoctor> listDoctors= new ArrayList<>();
        listDoctors.addAll(doctors.findByFirstName(name));
        listDoctors.addAll(doctorAndPatients.findByFirstName(name));
        return listDoctors;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = DOCTOR_SVC_PATH, method = RequestMethod.GET)
    public
    @ResponseBody
    Collection<IDoctor> getDoctors() {
        Collection<IDoctor> listDoctors= new ArrayList<>();
        listDoctors.addAll(Lists.newArrayList(doctors.findAll()));
        listDoctors.addAll(Lists.newArrayList(doctorAndPatients.findAll()));
        return listDoctors;
    }

    @ResponseStatus(value= HttpStatus.NOT_ACCEPTABLE, reason="Doctor unique id is required.")
    public static class MissingDoctorIdException extends RuntimeException {
    }
}
