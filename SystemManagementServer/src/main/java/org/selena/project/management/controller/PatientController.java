/*
 * 
 * Copyright 2014 Jules White
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.selena.project.management.controller;

import com.google.common.collect.Lists;
import org.selena.project.management.repository.*;
import org.selena.project.management.utilities.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Collection;

import static org.selena.project.management.utility.PathConstants.*;

@Controller
public class PatientController {

    @Autowired
    private UserNameRepository userRepository;

    @Autowired
    private PatientRepository patients;

    @Autowired
    private CheckInRepository checkIns;

    @Autowired
    private MedicationRepository medicationRep;


    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = PATIENT_SVC_PATH, method = RequestMethod.GET)
    public
    @ResponseBody
    Collection<Patient> getPatients() {
        return Lists.newArrayList(patients.findAll());
    }

    @PreAuthorize("hasRole('ADMIN') or hasRole('DOCTOR')")
    @RequestMapping(value = PATIENT_SVC_PATH, method = RequestMethod.POST)
    public
    @ResponseBody
    ResponseEntity<Patient> addPatient(
            @RequestBody Patient patient) {

        if (patient.getDoctor() == null) throw new MissingDoctorException();
        if (patient.getMedicalRecord() == null) throw new MissingMedicalRecord();
        if (userRepository.findByUserName(patient.getDoctor())==null ) throw new MissingDoctorException();

        if ( userRepository.findByUserName(patient.getUserName()) !=null ||
                patients.findByMedicalRecord(patient.getMedicalRecord())!=null )
            return new ResponseEntity<>(HttpStatus.CONFLICT);

        if ( patient.isInAlarm()) patient.setInAlarm(false);
        patient.setId(0);
        return new ResponseEntity<>(patients.save(patient), HttpStatus.OK);
    }




    @RequestMapping(value = PATIENT_ID_SVC_PATH, method = RequestMethod.GET)
    public
    @ResponseBody
    ResponseEntity<Patient> getPatientById(@PathVariable(ID) final long id) {

        Patient patient = patients.findOne(id);

        if ( patient==null ) return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        if ( !Utility.isAuthorisedPatientOrDoctorOrAdmin(patient) ) return new ResponseEntity<>(HttpStatus.FORBIDDEN);

        return new ResponseEntity<>(patient, HttpStatus.OK);
    }

    /**
     * Receives GET requests to /Patient/find and returns all Patients
     * that have a name  (e.g., Patient.name) matching the "name" request
     * parameter value that is passed by the client
     */
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = FIND_BY_PATIENT_NAME, method = RequestMethod.GET)
    public
    @ResponseBody
    Collection<Patient> findByFirstName(
            @RequestParam(NAME_PARAMETER) String name) {

        return patients.findByFirstName(name);
    }


    /**
     * Receives GET requests to /Patient/find and returns all Patients
     * that have a doctor (e.g., Patient.doctor) matching the "doctor" request
     * parameter value that is passed by the client
     * The doctor can access only his/her patients
     */
    @PreAuthorize("hasRole('ADMIN') or (hasRole('DOCTOR') and #doctor.equals(#p.name))")
    @RequestMapping(value = FIND_BY_DOCTOR, method = RequestMethod.GET)
    public
    @ResponseBody
    Collection<Patient> findByDoctor(
            @RequestParam(DOCTOR_PARAMETER) String doctor, final Principal p
    ) {
        return patients.findByDoctor(doctor);
    }

    @PreAuthorize("hasRole('ADMIN') or (hasRole('DOCTOR') and #doctor.equals(#p.name))")
    @RequestMapping(value = FIND_ALERTED_PATIENTS_BY_DOCTOR, method = RequestMethod.GET)
    public
    @ResponseBody
    Collection<Patient> findAlertedPatientByDoctor(
            @RequestParam(DOCTOR_PARAMETER) String doctor, final Principal p
    ) {

        return  patients.findByDoctorAndInAlarm(doctor, true);
    }

    /**
     * Receives GET requests to /Patient/find and returns Patient
     * that have a doctor (e.g., Patient.doctor) matching the "doctor" and "patientCode" request
     * parameter value that is passed by the client.
     * The doctor can access only his/her patient
     */
    @PreAuthorize("hasRole('ADMIN') or (hasRole('DOCTOR') and #doctor.equals(#p.name))")
    @RequestMapping(value = FIND_BY_DOCTOR_PATIENT_USERNAME, method = RequestMethod.GET)
    public
    @ResponseBody
    Patient findByDoctorAndPatientUserName(
            @RequestParam(DOCTOR_PARAMETER) String doctor,
            @RequestParam(USERNAME_PARAMETER) String name, final Principal p
    ) {

        return patients.findByDoctorAndUserName(doctor, name);
    }

    @PreAuthorize("hasRole('ADMIN') or (hasRole('DOCTOR') and #doctor.equals(#p.name))")
    @RequestMapping(value = FIND_BY_DOCTOR_PATIENT_NAME, method = RequestMethod.GET)
    public
    @ResponseBody
    Collection<Patient> findByDoctorAndPatientName(
            @RequestParam(DOCTOR_PARAMETER) String doctor,
            @RequestParam(PATTERN_PARAMETER) String pattern, final Principal p
    ) {

        return patients.findByDoctorAndPatientName(doctor, pattern.toUpperCase());
    }


    @ResponseStatus(value= HttpStatus.NOT_ACCEPTABLE, reason="Doctor input is required.")
    public static class MissingDoctorException extends RuntimeException {
    }
    @ResponseStatus(value= HttpStatus.NOT_ACCEPTABLE, reason="Medical Record input is required.")
    public static class MissingMedicalRecord extends RuntimeException {
    }

}
