package org.selena.project.management.repository;

public interface IDoctor extends IUserName {

    public String getDoctorId();

}
