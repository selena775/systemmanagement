/*
 * Copyright (c) 2015.
 * Copyright from gradle @copyright@
 * Version @version@
 */

package org.selena.project.management.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


/**
 * An interface for a repository that can store Patient
 * objects and allow them to be searched by different criteria.
 *
 * @author sklasnja
 *
 */
@Repository
public interface AdminRepository extends CrudRepository<Admin, Long>{

}
