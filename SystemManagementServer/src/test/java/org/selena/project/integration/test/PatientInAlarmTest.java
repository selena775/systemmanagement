package org.selena.project.integration.test;

import org.junit.Test;
import org.selena.project.management.repository.Medication;
import org.selena.project.management.repository.Patient;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class PatientInAlarmTest extends SystemManagementApiTest {

    @Test
    public void testAlertedPatientsDb() throws Exception {

        saveAllUsers();

        long id1 = getPatientId(m_patient1);
        long id2 = getPatientId(m_patient2);
        long id3 = getPatientId(m_doctor2);

        populateCheckIns(id1, patientService1);

        populateCheckInsAlarmModerate(id2, patientService2);

        populateCheckInsAlarm(id3, doctorService2);

        doctorService1.addMedications(id1, Arrays.asList(
                Medication.create().withName("Lortab").withStartDate(testDate1).withEndDate(testDate2).build(),
                Medication.create().withName("OxyContin").withStartDate(testDate1).withEndDate(testDate3).build(),
                Medication.create().withName("Panadol").withStartDate(testDate1).withEndDate(testDate3).build()));

        doctorService1.addMedications(id2, Arrays.asList(
                Medication.create().withName("Aleve").withStartDate(testDate1).withEndDate(testDate2).build(),
                Medication.create().withName("Atarax").withStartDate(testDate1).withEndDate(testDate3).build(),
                Medication.create().withName("Celebrex").withStartDate(testDate1).withEndDate(testDate3).build()));


        doctorService1.addMedications(id3, Arrays.asList(
                Medication.create().withName("Medicine1").withStartDate(testDate1).withEndDate(testDate2).build(),
                Medication.create().withName("Medicine2").withStartDate(testDate1).withEndDate(testDate3).build(),
                Medication.create().withName("Medicine3").withStartDate(testDate1).withEndDate(testDate3).build(),
                Medication.create().withName("Medicine4").withStartDate(testDate1).withEndDate(testDate3).build(),
                Medication.create().withName("Medicine5").withStartDate(testDate1).withEndDate(testDate3).build()));

        Collection<Patient> patientsWithDoctor = doctorService1.findAlertedPatientsByDoctor(m_doctor1.getUserName());
        assertEquals(2, patientsWithDoctor.size());
        assertTrue(patientsWithDoctor.contains(m_patient2));
        assertTrue(patientsWithDoctor.contains(m_doctor2));

//        doctorService2.addCheckIn(id3, CheckIn.create().withPatientId(id3).withTimestamp(Utility.getTestDateWithOffset(0, 2, 0))
//        .withPainIntensity(CheckIn.PainIntensity.MODERATE).withEatingDisorder(CheckIn.EatingDisorder.SOME)
//        .build());
//
//        assertEquals(1, patientsWithDoctor.size());
//        assertTrue(!patientsWithDoctor.contains(m_doctor2));
//
//        patientService2.addCheckIn(id2, CheckIn.create().withPatientId(id2).withTimestamp(Utility.getTestDateWithOffset(0, 2, 0))
//                .withPainIntensity(CheckIn.PainIntensity.WELL_CONTROLLED).withEatingDisorder(CheckIn.EatingDisorder.CANNOT_EAT)
//                .build());
//
//        assertTrue(patientsWithDoctor.isEmpty());

    }
}

