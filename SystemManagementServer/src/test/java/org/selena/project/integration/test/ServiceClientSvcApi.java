package org.selena.project.integration.test;

import org.selena.project.management.repository.*;
import org.selena.project.management.utility.Pair;
import retrofit.client.Response;
import retrofit.http.*;
import retrofit.mime.TypedFile;

import java.util.Collection;
import java.util.List;

import static org.selena.project.management.utility.PathConstants.*;

/**
 * This interface defines an API for a SMA Svc. The
 * interface is used to provide a contract for path/server
 * interactions. The interface is annotated with Retrofit
 * annotations so that clients can automatically convert the
 * 
 * 
 * @author sklasnja
 *
 */
public interface ServiceClientSvcApi {


	public static final String TOKEN_PATH = "/oauth/token";

    @DELETE(STORAGE_PATH)
    public Void deleteStorage();

    @GET(PATIENT_SVC_PATH)
	public Collection<Patient> getPatientList();
	
	@POST(PATIENT_SVC_PATH)
	public Patient addPatient(@Body Patient v);

    @POST(DOCTOR_AND_PATIENT_SVC_PATH)
    public DoctorAndPatient addDoctorAndPatient(@Body DoctorAndPatient v);

    @POST(DOCTOR_SVC_PATH)
    public Doctor addDoctor(@Body Doctor d);

    @GET(DOCTOR_FIRST_NAME_SEARCH_PATH)
    public Collection<IDoctor> findDoctorByFirstName(@Query(FIRST_NAME_PARAMETER) String firstName);

    @DELETE(USER_SVC_PATH)
    public Void deleteUsers();

    @GET(DOCTOR_SVC_PATH)
    public Collection<IDoctor> getDoctorList();

    @GET(PATIENT_ID_SVC_PATH)
    public Patient getPatientById(@Path(ID) long id);


	@GET(FIND_BY_PATIENT_NAME)
	public Collection<Patient> findByFirstName(@Query(NAME_PARAMETER) String name);

	@GET(FIND_BY_USER_NAME)
	public IUserName findByUserName(@Query(USERNAME_PARAMETER) String name);

    @POST(USER_SVC_PATH)
    public Void addRegistrationId(@Body Pair<String, String> registrationID);

    @GET(CHECK_ROLES)
    public Collection<String> getRoles(@Query(USERNAME_PARAMETER) String name);


    @GET(FIND_BY_DOCTOR)
    public Collection<Patient> findByDoctor(@Query(DOCTOR_PARAMETER) String name);

    @GET(FIND_ALERTED_PATIENTS_BY_DOCTOR)
    public Collection<Patient> findAlertedPatientsByDoctor(@Query(DOCTOR_PARAMETER) String name);

    @GET(FIND_BY_DOCTOR_PATIENT_USERNAME)
    public Patient findByDoctorAndPatientUserName(@Query(DOCTOR_PARAMETER) String doctorName, @Query(USERNAME_PARAMETER) String userName);

    @GET(FIND_BY_DOCTOR_PATIENT_NAME)
    public Collection<Patient> findByDoctorAndPatientName(@Query(DOCTOR_PARAMETER) String doctorName, @Query(PATTERN_PARAMETER) String pattern);

    @GET(PATIENT_DOCTOR)
    public IUserName getPatientsDoctor(@Path(ID) long id);

    @GET(PATIENT_CHECK_IN_SVC_PATH)
    public Collection<CheckIn> getPatientCheckIns(@Path(ID) long id);


    @POST(PATIENT_CHECK_IN_SVC_PATH)
    public long addCheckIn(@Path(ID) long id, @Body CheckIn checkIn);

    @GET(PATIENT_CHECK_IN_ID_SVC_PATH)
    public CheckIn geCheckIn(@Path(ID) long id, @Path(CHECK_IN_ID) long cId);


    /**
     * This endpoint allows clients to set the jpeg image data for previously
     * added Video objects by sending multipart POST requests to the server.
     *
     * @return
     */
    @Multipart
    @POST(PATIENT_CHECK_IN_PHOTO)
    public String setPhoto(@Path(ID) long id, @Path(CHECK_IN_ID) long cId, @Part(PHOTO_PARAMETER) TypedFile photoData);


    @Streaming
    @GET(PATIENT_CHECK_IN_PHOTO)
    Response getPhoto(@Path(ID) long id, @Path(CHECK_IN_ID) long cId);


    @POST(PATIENT_ID_MEDICATIONS)
    public Void addMedications(@Path(ID) long id, @Body List<Medication> medications);

    @GET(PATIENT_ID_MEDICATIONS)
    public Collection<Medication> getPatientMedications(@Path(ID) long id);

    @GET(FIND_BY_PATIENT_MEDICATION_AND_DATE)
    public Collection<Medication> getPatientMedicationsForTheDay(@Path(ID) long id,
                                                                 @Query(DATE_PARAMETER) long day);

}
