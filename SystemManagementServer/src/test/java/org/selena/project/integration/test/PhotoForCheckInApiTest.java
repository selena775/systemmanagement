package org.selena.project.integration.test;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.selena.project.management.repository.CheckIn;
import org.selena.project.management.utilities.Utility;
import org.springframework.http.HttpStatus;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class PhotoForCheckInApiTest extends SystemManagementApiTest {

    private File photo = new File( "src/test/resources/throat.jpg");

    @Test
    public void testPhoto() throws Exception {

        adminService.addDoctor(m_doctor1);
        adminService.addPatient(m_patient1);
        long id = getPatientId(m_patient1);
        CheckIn ch1 = CheckIn.create().withPatientId(id).withTimestamp(Utility.getTestDateWithOffset(1))
                .withPainIntensity(CheckIn.PainIntensity.WELL_CONTROLLED).withEatingDisorder(CheckIn.EatingDisorder.NO).build();
        long cId = patientService1.addCheckIn(id, ch1);

        Response response = doctorService1.getPhoto(id, cId);
        assertEquals(HttpStatus.OK.value(), response.getStatus());

        patientService1.setPhoto(id, cId, new TypedFile("image/jpg", photo));

        response = doctorService1.getPhoto(id, cId);
        assertEquals(200, response.getStatus());

        InputStream retreivedPhoto = response.getBody().in();
        byte[] originalFile = IOUtils.toByteArray(new FileInputStream(photo));
        byte[] retrievedFile = IOUtils.toByteArray(retreivedPhoto);
        assertTrue(Arrays.equals(originalFile, retrievedFile));
    }
}