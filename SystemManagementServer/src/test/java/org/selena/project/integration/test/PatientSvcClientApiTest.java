package org.selena.project.integration.test;

import org.junit.Test;
import org.selena.project.management.repository.IUserName;
import org.selena.project.management.repository.Medication;
import org.selena.project.management.repository.Patient;
import org.selena.project.management.utility.Pair;
import org.springframework.http.HttpStatus;
import retrofit.RetrofitError;

import java.util.Arrays;
import java.util.Collection;
import java.util.UUID;

import static org.junit.Assert.*;


public class PatientSvcClientApiTest extends SystemManagementApiTest {


    /**
     * This test creates a Patient, adds the Patient to the PatientSvc, and then
     * checks that the Patient is included in the list when getPatientList() is
     * called.
     *
     * @throws Exception
     */
    @Test
    public void testPatientAddAndList() throws Exception {

        // Add the management
        adminService.addDoctor(m_doctor1);
        adminService.addPatient(m_patient1);

        // We should get back the management that we added above
        Collection<Patient> patients = adminService.getPatientList();
        assertTrue(patients.contains(m_patient1));

        Collection<Patient> patientsWithName = adminService.findByFirstName(m_patient1.getFirstName());
        assertTrue(patientsWithName.contains(m_patient1));
    }


    @Test
    public void testDoctorAddPatient() throws Exception {

        adminService.addDoctor(m_doctor1);
        Patient p = doctorService1.addPatient(m_patient1);

        assertEquals( m_doctor1, patientService1.getPatientsDoctor(p.getId()));

        Patient patient = doctorService1.getPatientById(getPatientId(m_patient1));

        assertEquals(m_patient1, patient);

        assertEquals(m_patient1, p);
    }



    @Test
    public void testAddPatientConditions() throws Exception {


        try {
            adminService.addPatient(Patient.create().withFirstName(UUID.randomUUID().toString()).
                    withUserName(UUID.randomUUID().toString()).withMedicalRecord("JIDYJ1234HG").build());
            fail("No missing doctor is accepted");
        } catch (RetrofitError e) {
            assertEquals(HttpStatus.NOT_ACCEPTABLE.value(), e.getResponse().getStatus());
        }

        try {
            adminService.addPatient(Patient.create().withFirstName(UUID.randomUUID().toString()).
                    withUserName(UUID.randomUUID().toString()).withMedicalRecord("JIDYJ1234HG").withDoctor("docotrx").build());
            fail("There is no doctor in the database");
        } catch (RetrofitError e) {
            assertEquals(HttpStatus.NOT_ACCEPTABLE.value(), e.getResponse().getStatus());
        }

        try {
            adminService.addPatient(Patient.create().withFirstName(UUID.randomUUID().toString()).
                    withUserName(UUID.randomUUID().toString()).withDoctor("doctorX").build());
            fail("No missing medical record is accepted");
        } catch (RetrofitError e) {
            assertEquals(HttpStatus.NOT_ACCEPTABLE.value(), e.getResponse().getStatus());
        }

        adminService.addDoctor(m_doctor1);
        adminService.addPatient(m_patient1);

        try {
            adminService.addPatient(Patient.create().withFirstName(UUID.randomUUID().toString()).
                    withUserName(UUID.randomUUID().toString()).withDoctor(m_doctor1.getUserName()).
                        withMedicalRecord(m_patient1.getMedicalRecord()).build());
            fail("No duplicate medical records are accepted");
        } catch (RetrofitError e) {
            assertEquals(HttpStatus.CONFLICT.value(), e.getResponse().getStatus());
        }

    }


    /**
     * This test creates a Patient, adds the Patient to the PatientSvc, and then
     * checks that the Patient is included in the list when getPatientList() is
     * called.
     *
     * @throws Exception
     */
    @Test
    public void testPatientWithDoctorAddAndList() throws Exception {

        saveAllUsers();

        Collection<Patient> patientsWithDoctor = adminService.findByDoctor(m_doctor1.getUserName());
        assertEquals(6, patientsWithDoctor.size());
        assertTrue(patientsWithDoctor.contains(m_patient1));
        assertTrue(patientsWithDoctor.contains(m_patient2));
        assertTrue(patientsWithDoctor.contains(m_patient3));
        assertTrue(patientsWithDoctor.contains(m_patient4));
        assertTrue(patientsWithDoctor.contains(m_patient5));
        assertTrue(patientsWithDoctor.contains(m_doctor2));

    }

    @Test
    public void testFindByPatientName() throws Exception {

        saveAllUsers();

        Collection<Patient> patientsWithName = doctorService1.findByDoctorAndPatientName(m_doctor1.getUserName(), "mayer");
        assertEquals(2, patientsWithName.size());
        assertTrue(patientsWithName.contains(m_patient1));
        assertTrue(patientsWithName.contains(m_doctor2));

    }


    /**
     * This test creates a Patient, adds the Patient to the PatientSvc, and then
     * checks that the Patient is included in the list when getPatientList() is
     * called.
     *
     * @throws Exception
     */
    @Test
    public void testFindNoExistingPatient() throws Exception {


        try {
            adminService.getPatientById(Long.MAX_VALUE);
            fail("No such patient");
        } catch (RetrofitError e) {
            assertEquals(HttpStatus.NOT_FOUND.value(), e.getResponse().getStatus());
        }
    }


    @Test
    public void testAddMedications() throws Exception {

        Medication med1, med2, med3, med4;
        // Add the management
        adminService.addDoctor(m_doctor1);
        Patient patient = adminService.addPatient(m_patient1);

        long id = patient.getId();
        doctorService1.addMedications(id, Arrays.asList(
                med1 = Medication.create().withName("Lortab").withStartDate(testDate1).withEndDate(testDate2).build(),
                med2 = Medication.create().withName("OxyContin").withStartDate(testDate2).withEndDate(testDate3).build(),
                med3 = Medication.create().withName("Panadol").withStartDate(testDate1).withEndDate(testDate3).build()));

        patient= patientService1.getPatientById(id);
        Collection<Medication> medications;

        medications = patientService1.getPatientMedications(id);
        assertEquals(3, medications.size());
        assertTrue(medications.contains(med1));
        assertTrue(medications.contains(med2));
        assertTrue(medications.contains(med3));


        medications = patientService1.getPatientMedicationsForTheDay(id, testDate2.getTime());
        assertEquals(2, medications.size());
        assertTrue(medications.contains(med2));
        assertTrue(medications.contains(med3));


        //old
        doctorService1.addMedications(id, Arrays.asList(
                // same
                med1 = Medication.create().withName("Lortab").withStartDate(testDate1).withEndDate(testDate2).build(),
                //changed
                med2 = Medication.create().withName("OxyContin").withStartDate(testDate1).withEndDate(testDate2).build(),
                //deleted Panadol
                // new
                med3 = Medication.create().withName("NewMedicine").withStartDate(testDate1).withEndDate(testDate2).build()) );

        medications = patientService1.getPatientMedications(id);
        assertEquals(3, medications.size());
        assertTrue(medications.contains(med1));
        assertTrue(medications.contains(med2));
        assertTrue(medications.contains(med3));

        //old
        doctorService1.addMedications(id, Arrays.asList(
                // same
                med1 = Medication.create().withName("Lortab").withStartDate(testDate1).withEndDate(testDate2).build(),
                //changed
                med2 = Medication.create().withName("OxyContin").withStartDate(testDate1).withEndDate(testDate2).build(),
                //Panadol back
                med3 = Medication.create().withName("Panadol").withStartDate(testDate1).withEndDate(testDate3).build(),
                // new
                med4 = Medication.create().withName("NewMedicine").withStartDate(testDate1).withEndDate(testDate2).build()) );
        medications = patientService1.getPatientMedications(id);
        assertEquals(4, medications.size());
        assertTrue(medications.contains(med1));
        assertTrue(medications.contains(med2));
        assertTrue(medications.contains(med3));
        assertTrue(medications.contains(med4));

    }

    @Test
    public void testRoles() throws Exception {

        saveAllUsers();

        Collection<String> roles= doctorService2.getRoles(m_doctor2.getUserName());
        assertEquals(2,roles.size());
        assertTrue(roles.contains("PATIENT"));
        assertTrue(roles.contains("DOCTOR"));

        roles= doctorService1.getRoles(m_doctor1.getUserName());
        assertEquals(1,roles.size());
        assertTrue(roles.contains("DOCTOR"));


        roles= patientService1.getRoles(m_patient1.getUserName());
        assertEquals(1,roles.size());
        assertTrue(roles.contains("PATIENT"));

    }
    @Test
    public void testRegistrationId() {

        adminService.addDoctor(m_doctor1);
        IUserName patient = adminService.addPatient(m_patient1);
        assertNull(patient.getRegistrationIDs());

        final String testRegistrationID= "testRegId";
        patientService1.addRegistrationId(Pair.create(testRegistrationID, ""));
        patient= patientService1.findByUserName(m_patient1.getUserName());
        assertEquals(testRegistrationID, patient.getRegistrationIDs().iterator().next());
    }
}
