package org.selena.project.integration.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
@RunWith(Suite.class)
@Suite.SuiteClasses({
        PatientSvcClientApiTest.class,
        DoctorSvcClientApiTest.class,
        CheckInApiTest.class,
        PhotoForCheckInApiTest.class,
        SecurityAccessTest.class,
        PatientInAlarmTest.class
})
public class SuiteOne {

}

