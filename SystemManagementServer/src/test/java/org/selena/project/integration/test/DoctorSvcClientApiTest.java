package org.selena.project.integration.test;

import org.junit.Test;
import org.selena.project.management.repository.Doctor;
import org.selena.project.management.repository.DoctorAndPatient;
import org.selena.project.management.repository.IDoctor;
import org.selena.project.management.repository.Patient;
import org.springframework.http.HttpStatus;
import retrofit.RetrofitError;

import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class DoctorSvcClientApiTest extends SystemManagementApiTest {


    /**
     * This test creates a Patient, adds the Patient to the PatientSvc, and then
     * checks that the Patient is included in the list when getPatientList() is
     * called.
     *
     * @throws Exception
     */
    @Test
    public void testDoctorAddAndList() throws Exception {

        Doctor doctor1= adminService.addDoctor(m_doctor1);
        DoctorAndPatient doctor2= adminService.addDoctorAndPatient(m_doctor2);

        IDoctor doctor;
        // We should get back the management that we added above
        Collection<IDoctor> doctors = adminService.findDoctorByFirstName(m_doctor1.getFirstName());
        assertEquals(m_doctor1, doctor= doctors.iterator().next());
        assertEquals(doctor, doctorService1.findByUserName(m_doctor1.getUserName()));

        try {
            doctorService2.findByUserName(m_doctor1.getUserName());
            fail("Doctor 2 cannot read doctor1 data");
        } catch (RetrofitError e) {
            assertEquals(HttpStatus.FORBIDDEN.value(), e.getResponse().getStatus());
        }

        doctors = adminService.findDoctorByFirstName(m_doctor2.getFirstName());
        assertEquals(m_doctor2, doctor= doctors.iterator().next());
        assertEquals(doctor, doctorService2.findByUserName(m_doctor2.getUserName()));
        assertEquals(doctor, doctorService1.findByUserName(m_doctor2.getUserName()));

    }

    @Test
    public void testGetDoctors() throws Exception {

        Doctor doctor1= adminService.addDoctor(m_doctor1);
        DoctorAndPatient doctor2= adminService.addDoctorAndPatient(m_doctor2);

        // We should get back the management that we added above
        Collection<IDoctor> doctors = adminService.getDoctorList();
        assertEquals(2, doctors.size());
        assertTrue(doctors.contains(m_doctor1));
        assertTrue(doctors.contains(m_doctor2));

    }

    @Test
    public void testDuplicateUserName() throws Exception {

        Doctor doctor1= Doctor.create().withUserName("username").withFirstName("Bob").withLastName("Franklin")
                .withDoctorId("Licence:10293").build();
        DoctorAndPatient doctor2= DoctorAndPatient.createDoctorAndPatient().withUserName("username").withFirstName("Denise").withLastName("Mayer")
                .withDoctorId("Licence:54637").withDob(dob3).withMedicalRecord("RandomRecord").withDoctor(doctor1.getUserName()).build();
        Patient patient = Patient.create().withFirstName("Mike").withLastName("Smith").withDob( dob1).
                withMedicalRecord("Random1").withUserName("username").withDoctor(doctor1.getUserName()).build();

        adminService.addDoctor(doctor1);

        try {
            adminService.addDoctorAndPatient(doctor2);
            fail("Username already exists");
        } catch (RetrofitError e) {
            assertEquals(HttpStatus.CONFLICT.value(), e.getResponse().getStatus());
        }

        try {
            adminService.addPatient(patient);
            fail("Username already exists");
        } catch (RetrofitError e) {
            assertEquals(HttpStatus.CONFLICT.value(), e.getResponse().getStatus());
        }

    }

    @Test
    public void testAddingDoctor() throws Exception {

        try {
            adminService.addDoctor(Doctor.create().withUserName("username").withFirstName("Bob").withLastName("Franklin").build());
            fail("No missing doctor unique id is accepted");
        } catch (RetrofitError e) {
            assertEquals(HttpStatus.NOT_ACCEPTABLE.value(), e.getResponse().getStatus());
        }

        adminService.addDoctor(m_doctor1);
        try {
            adminService.addDoctor(Doctor.create().withUserName("username").withFirstName("Bob").withLastName("Franklin").
                    withDoctorId(m_doctor1.getDoctorId()).build());
            fail("No duplicate doctor id is accepted");
        } catch (RetrofitError e) {
            assertEquals(HttpStatus.CONFLICT.value(), e.getResponse().getStatus());
        }
    }

    @Test
    public void testAddingDoctorAndPatient() throws Exception {

        try {
            adminService.addDoctorAndPatient(DoctorAndPatient.createDoctorAndPatient().withUserName("username").withFirstName("Bob").
                    withLastName("Franklin").withMedicalRecord("JKHD3564GJ").build());
            fail("No missing doctor unique id is accepted");
        } catch (RetrofitError e) {
            assertEquals(HttpStatus.NOT_ACCEPTABLE.value(), e.getResponse().getStatus());
        }

        try {
            adminService.addDoctorAndPatient(DoctorAndPatient.createDoctorAndPatient().withUserName("username").withFirstName("Bob").
                    withLastName("Franklin").withDoctorId("JKHD3564GJ").build());
            fail("No missing medical record is accepted");
        } catch (RetrofitError e) {
            assertEquals(HttpStatus.NOT_ACCEPTABLE.value(), e.getResponse().getStatus());
        }

        adminService.addDoctor(m_doctor1);
        try {
            adminService.addDoctorAndPatient(DoctorAndPatient.createDoctorAndPatient().withDoctor(m_doctor1.getUserName()). withUserName("username").withFirstName("Bob").
                    withLastName("Franklin").withMedicalRecord("JKHD3564GJ").withDoctorId(m_doctor1.getDoctorId()).build());
            fail("No duplicate doctor id is accepted");
        } catch (RetrofitError e) {
            assertEquals(HttpStatus.CONFLICT.value(), e.getResponse().getStatus());
        }

        adminService.addPatient(m_patient1);
        try {
            adminService.addDoctorAndPatient(DoctorAndPatient.createDoctorAndPatient().withDoctor(m_doctor1.getUserName()).withUserName("username").withFirstName("Bob").
                    withLastName("Franklin").withMedicalRecord(m_patient1.getMedicalRecord()).withDoctorId("HDJDG5765JHH").build());
            fail("No duplicate medical record is accepted");
        } catch (RetrofitError e) {
            assertEquals(HttpStatus.CONFLICT.value(), e.getResponse().getStatus());
        }

        try {
            adminService.addDoctorAndPatient(DoctorAndPatient.createDoctorAndPatient().withUserName("username").withFirstName("Bob").
                    withLastName("Franklin").withMedicalRecord("JKHD3564GJ").withDoctorId("BFKJDGIFUY").build());
            fail("Doctor field is missing!");
        } catch (RetrofitError e) {
            assertEquals(HttpStatus.NOT_ACCEPTABLE.value(), e.getResponse().getStatus());
        }
    }

}