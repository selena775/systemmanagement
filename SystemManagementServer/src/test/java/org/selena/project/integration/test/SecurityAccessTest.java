package org.selena.project.integration.test;

import com.google.gson.JsonObject;
import org.junit.Test;
import org.selena.project.management.repository.Patient;
import org.springframework.http.HttpStatus;
import retrofit.RetrofitError;

import java.util.Collection;

import static org.junit.Assert.*;


public class SecurityAccessTest extends SystemManagementApiTest {

    @Test
    public void testDoctorAccessPatients() throws Exception {

        saveAllUsers();

        // doctor1 access his/her patients
        Collection<Patient> patientsWithDoctor = doctorService1.findByDoctor(m_doctor1.getUserName());
        assertEquals(6, patientsWithDoctor.size());

        // doctor1 should have acess to m_patient1 record
        doctorService1.getPatientById(getPatientId(m_patient1));

        // doctor2 access his/her patients
        try {
            doctorService1.findByDoctor(m_doctor2.getUserName());
            fail("The server should have prevented the doctor from listing patients"
                    + " treated by another doctor");
        } catch (RetrofitError e) {
            assertEquals( HttpStatus.FORBIDDEN.value(), e.getResponse().getStatus()); //Forbidden
        }

        // doctor1 access other patient
        try {
            doctorService1.getPatientById(getPatientId(m_patient));
            fail("The server should have prevented the doctor from accessing patients"
                    + " treated by another doctor");
        } catch (RetrofitError e) {
            assertEquals( HttpStatus.FORBIDDEN.value(), e.getResponse().getStatus()); //Forbidden
        }

    }



    /**
     * This test creates a Patient, adds the Patient to the PatientSvc, and then
     * checks that the Patient is included in the list when getPatientList() is
     * called.
     *
     * @throws Exception
     */
    @Test
    public void testPatientAccess() throws Exception {

        saveAllUsers();

        long id= getPatientId( m_patient );

        // patient access his/her account
        patientService.getPatientById(id);


        // doctor2 access his/her patients
        try {
            patientService.getPatientById(getPatientId(m_patient1));
            fail("The server should have prevented a patient from accessing other patients records");
        } catch (RetrofitError e) {
            assertEquals( HttpStatus.FORBIDDEN.value(), e.getResponse().getStatus()); //Forbidden
        }
    }

    /**
     * This test ensures that clients with invalid credentials cannot get
     * access to patients.
     *
     * @throws Exception
     */
    @Test
    public void testAccessDeniedWithIncorrectCredentials() throws Exception {

        try {
            // Add the management
            invalidClientPatientService.addPatient(m_patient);

            fail("The server should have prevented the path from adding a management"
                    + " because it presented invalid path/user credentials");
        } catch (RetrofitError e) {
            assert (e.getCause() instanceof SecuredRestException);
        }
    }

    /**
     * This test ensures that read-only clients can access the management list
     * but not add new patients.
     *
     * @throws Exception
     */
    @Test
    public void testReadOnlyClientAccess() throws Exception {

        Collection<Patient> patients = readOnlyPatientService.getPatientList();
        assertNotNull(patients);

        try {
            // Add the management
            readOnlyPatientService.addPatient(m_patient);

            fail("The server should have prevented the path from adding a management"
                    + " because it is using a read-only path ID");
        } catch (RetrofitError e) {
            JsonObject body = (JsonObject)e.getBodyAs(JsonObject.class);
            assertEquals("insufficient_scope", body.get("error").getAsString());
        }
    }


    @Test
    public void testPatientAccessToCheckIn() throws Exception {

        adminService.addDoctor(m_doctor1);
        final Patient patient = adminService.addPatient(m_patient1);
        populateCheckIns(patient.getId(), patientService1);

        try {
            patientService1.getPatientCheckIns(patient.getId());
            fail("Not allowed patient to check his/her checkIns");
        } catch (RetrofitError e) {
            assertEquals(HttpStatus.FORBIDDEN.value(), e.getResponse().getStatus());
        }

    }

}
