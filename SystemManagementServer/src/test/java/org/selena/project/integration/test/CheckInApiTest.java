package org.selena.project.integration.test;

import org.junit.Test;
import org.selena.project.management.repository.CheckIn;
import org.selena.project.management.repository.Patient;
import org.selena.project.management.utilities.Utility;
import org.springframework.http.HttpStatus;
import retrofit.RetrofitError;

import java.util.Collection;

import static org.junit.Assert.*;


public class CheckInApiTest extends SystemManagementApiTest {


    @Test
    public void testAddingAndGettingCheckIn() throws Exception {

        adminService.addDoctor(m_doctor1);
        final Patient patient = adminService.addPatient(m_patient1);
        long id = patientService1.findByUserName(patient.getUserName()).getId();

        populateCheckIns(id, patientService1);

        // doctor1 access his/her patients
        Collection<CheckIn> checkIns = doctorService1.getPatientCheckIns(id);
        CheckIn checkIn = checkIns.iterator().next();
        assertEquals(checkIn, doctorService1.geCheckIn(id, checkIn.getChkInId()));

    }

    @Test
    public void testDoctorAccessCheckIns() throws Exception {

        adminService.addDoctor(m_doctor1);
        final Patient patient = adminService.addPatient(m_patient1);
        long id = patient.getId();

        populateCheckIns(patient.getId(), patientService1);
        // doctor1 access his/her patients
        Collection<CheckIn> checkIns = doctorService1.getPatientCheckIns(id);
        assertEquals(4, checkIns.size());
        assertTrue(checkIns.contains(ch1));
        assertTrue(checkIns.contains(ch2));
        assertTrue(checkIns.contains(ch3));
        assertTrue(checkIns.contains(ch4));
    }

    @Test
    public void testDoctorAccessEmptyCheckIns() throws Exception {

        saveAllUsers();

        long patient1Id= getPatientId(m_patient1);
        // doctor1 access his/her patients
        Collection<CheckIn> checkIns = doctorService1.getPatientCheckIns(patient1Id);
        assertEquals(0, checkIns.size());

        populateCheckIns(patient1Id, patientService1);
        checkIns = doctorService1.getPatientCheckIns(patient1Id);
        assertEquals(4, checkIns.size());

    }

    @Test
    public void testOtherDoctorAccessCheckIns() throws Exception {
        saveAllUsers();
        populateCheckIns(getPatientId(m_patient1), patientService1);
        try {
            doctorService1.getPatientCheckIns(getPatientId(m_patient));
            fail("Doctor cannot access to patients of other doctors record");
        } catch (RetrofitError e) {
            assertEquals(HttpStatus.FORBIDDEN.value(), e.getResponse().getStatus());
        }
    }

    @Test
    public void testDoctorAddCheckIns() throws Exception {
        adminService.addDoctor(m_doctor1);
        final Patient patient = adminService.addPatient(m_patient1);
        try {
            doctorService1.addCheckIn(patient.getId(), CheckIn.create().withPatientId(patient.getId()).withTimestamp(Utility.getTestDateWithOffset(10))
                    .withPainIntensity(CheckIn.PainIntensity.WELL_CONTROLLED).
                    withEatingDisorder(CheckIn.EatingDisorder.NO).build());
            fail("Only patient can add a checkIn");
        } catch (RetrofitError e) {
            assertEquals(HttpStatus.FORBIDDEN.value(), e.getResponse().getStatus());
        }
    }

    @Test
    public void testPatientToOtherPatientAddCheckIns() throws Exception {
        saveAllUsers();
        long id = getPatientId(m_patient1);
        populateCheckIns(id, patientService1);
        try {
            patientService.addCheckIn(id, CheckIn.create().withPatientId(id).withTimestamp(Utility.getTestDateWithOffset(0))
                    .withPainIntensity(CheckIn.PainIntensity.WELL_CONTROLLED).withEatingDisorder(CheckIn.EatingDisorder.NO).build());
            fail("Patient can only add a checkIn to his/her account");
        } catch (RetrofitError e) {
            assertEquals(HttpStatus.FORBIDDEN.value(), e.getResponse().getStatus());
        }
    }

    @Test
    public void testPatientReadOtherPatientCheckIns() throws Exception {
        saveAllUsers();
        long id = getPatientId(m_patient1);
        populateCheckIns(id, patientService1);

        try {
            patientService.getPatientCheckIns(id);
            fail("Patient can only add a checkIn to his/her account");
        } catch (RetrofitError e) {
            assertEquals(HttpStatus.FORBIDDEN.value(), e.getResponse().getStatus());
        }


    }

}