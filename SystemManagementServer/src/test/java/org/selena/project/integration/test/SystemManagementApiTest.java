package org.selena.project.integration.test;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.selena.project.management.json.MyGsonBuilder;
import org.selena.project.management.repository.*;
import org.selena.project.management.utilities.Utility;
import retrofit.RestAdapter.LogLevel;
import retrofit.client.ApacheClient;
import retrofit.converter.GsonConverter;
import retrofit.mime.TypedFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


public class SystemManagementApiTest {

    private static String ADMIN, PASSWORD, CLIENT_ID, READ_ONLY_CLIENT_ID, TEST_URL;

    protected ServiceClientSvcApi adminService, doctorService1, doctorService2, patientService, patientService1, patientService2, readOnlyPatientService, invalidClientPatientService;

    protected Admin m_admin;
    protected Patient m_patient1, m_patient2, m_patient3, m_patient4, m_patient5, m_patient;
    protected Doctor m_doctor1;
    protected DoctorAndPatient m_doctor2;
    protected CheckIn ch1, ch2, ch3, ch4;
    protected Date testTime1, testTime2, testTime3;
    protected Date testDate1, testDate2, testDate3;
    protected Date dob1, dob2, dob3, dob4, dob5;

    @BeforeClass
    public static void initClass() throws IOException {
        Properties prop = getPropValues();

        //get the properties from gradle test task for TEST_URL
        prop.putAll(System.getProperties());

        TEST_URL=prop.getProperty("TEST_URL");
        ADMIN=prop.getProperty("ADMIN");
        PASSWORD=prop.getProperty("PASSWORD");
        CLIENT_ID=prop.getProperty("CLIENT_ID");
        READ_ONLY_CLIENT_ID=prop.getProperty("READ_ONLY_CLIENT_ID");
    }

    @Before
    public void init() throws ParseException {

        final SimpleDateFormat dateOnlyFormat = new SimpleDateFormat("dd-MM-yyyy");
        dateOnlyFormat.setTimeZone(TimeZone.getTimeZone("America/Toronto"));
        dob1 = dateOnlyFormat.parse("01-10-1965");
        dob2 = dateOnlyFormat.parse("15-08-1968");
        dob3 = dateOnlyFormat.parse("30-02-1955");
        dob4 = dateOnlyFormat.parse("12-11-1949");
        dob5 = dateOnlyFormat.parse("11-05-1971");

        m_doctor1= Doctor.create().withUserName("doctor1").withFirstName("Bob").withLastName("Franklin")
                .withDoctorId("Licence:10293").withPassword(PASSWORD).build();
        m_doctor2= DoctorAndPatient.createDoctorAndPatient().withUserName("doctor2").withFirstName("Denise").withLastName("Mayer")
                .withDoctorId("Licence:54637").withDob(dob3).withMedicalRecord("2DJJH678SGH").withDoctor(m_doctor1.getUserName())
                .withPassword(PASSWORD).build();

        m_admin = Admin.create().withFirstName("Selena").withLastName("Klasnja").withDob(dob1)
                .withUserName("admin").withPassword(PASSWORD).build();

        m_patient = Patient.create().withFirstName("Mike").withLastName("Smith").withDob(dob1).
                withMedicalRecord("1C76GT786JHG").withUserName("patient").withDoctor(m_doctor2.getUserName())
                .withPassword(PASSWORD).build();
        m_patient1 = Patient.create().withFirstName("Pit").withLastName("Mayer").withDob(dob2)
                .withMedicalRecord("23JGH785FGH").withUserName("patient1").withDoctor(m_doctor1.getUserName())
                .withPassword(PASSWORD).build();
        m_patient2 = Patient.create().withFirstName("Alice").withLastName("Johns").withDob(dob5)
                .withMedicalRecord("YU789GJR897").withUserName("patient2").withDoctor(m_doctor1.getUserName())
                .withPassword(PASSWORD).build();
        m_patient3 = Patient.create().withFirstName("Mike").withLastName("Brown").withDob(dob4)
                .withMedicalRecord("685R67TYOI9").withUserName("patient3").withDoctor(m_doctor1.getUserName())
                .withPassword(PASSWORD).build();
        m_patient4 = Patient.create().withFirstName("George").withLastName("Jonson").withDob(dob1).
                withMedicalRecord("9564FGH8b88").withUserName("patient4").withDoctor(m_doctor1.getUserName())
                .withPassword(PASSWORD).build();
        m_patient5 = Patient.create().withFirstName("Jim").withLastName("Smith").withDob(dob1).
                withMedicalRecord("123POI678UJ").withUserName("patient5").withDoctor(m_doctor1.getUserName())
                .withPassword(PASSWORD).build();



        adminService = creteServiceForUserAndClient(m_admin.getUsername(), PASSWORD, CLIENT_ID);
        doctorService1 = creteServiceForUserAndClient(m_doctor1.getUserName(), PASSWORD, CLIENT_ID);
        doctorService2 = creteServiceForUserAndClient(m_doctor2.getUserName(), PASSWORD, CLIENT_ID);
        patientService = creteServiceForUserAndClient(m_patient.getUserName(), PASSWORD, CLIENT_ID);
        patientService1 = creteServiceForUserAndClient(m_patient1.getUserName(), PASSWORD, CLIENT_ID);
        patientService2 = creteServiceForUserAndClient(m_patient2.getUserName(), PASSWORD, CLIENT_ID);
        readOnlyPatientService = creteServiceForUserAndClient(ADMIN, PASSWORD, READ_ONLY_CLIENT_ID);
        invalidClientPatientService = creteServiceForUserAndClient(UUID.randomUUID().toString(),
                UUID.randomUUID().toString(), UUID.randomUUID().toString());

        try {
            adminService.deleteUsers();
            adminService.deleteStorage();
        } catch ( Exception ex ){
            // doNothing, probably db doesn't exists
        }

        final SimpleDateFormat timeFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS");
        timeFormat.setTimeZone(TimeZone.getTimeZone("America/Toronto"));
        testTime1 = timeFormat.parse("01-10-2014 10:20:56.000");
        testTime2 = timeFormat.parse("02-11-2014 00:00:01.000");
        testTime3 = timeFormat.parse("03-11-2014 05:18:35.000");

        testDate1 = timeFormat.parse("01-10-2014 00:00:00.000");
        testDate2 = timeFormat.parse("22-12-2017 00:00:00.000");
        testDate3 = timeFormat.parse("23-12-2017 00:00:00.000");

    }

    private static Gson gson = new MyGsonBuilder().create();

    private static ServiceClientSvcApi creteServiceForUserAndClient(final String username, final String password, final String client_ID) {

        return new SecuredRestBuilder()
                .setLoginEndpoint(TEST_URL + ServiceClientSvcApi.TOKEN_PATH)
                .setUsername(username)
                .setPassword(password)
                .setClientId(client_ID)
                .setClient(new ApacheClient(UnsafeHttpsClient.createUnsafeClient()))
                .setEndpoint(TEST_URL).setLogLevel(LogLevel.FULL)
                .setConverter(new GsonConverter(gson))
                .build()
                .create(ServiceClientSvcApi.class);
    }

    private void savePatients(final Patient... patients) {
        // Add the management
        for (final Patient patient : patients) {
            adminService.addPatient(patient);
        }
    }

    private void saveAllPatients() {
        adminService.addDoctorAndPatient(m_doctor2);
        savePatients(m_patient, m_patient1, m_patient2, m_patient3, m_patient4, m_patient5);
    }
    protected void saveAllUsers() {
        adminService.addDoctor(m_doctor1);
        saveAllPatients();

    }

    protected long getPatientId(final Patient patient) {
        return adminService.findByUserName(patient.getUserName()).getId();
    }


    protected void populateCheckIns(final long id, ServiceClientSvcApi service) {

        MedicationUsage med1 = MedicationUsage.create().withName("Lortab").withTaken(MedicationUsage.MedicineTaken.YES).withTime(testTime1).build();
        MedicationUsage med2 = MedicationUsage.create().withName("OxyContin").withTaken(MedicationUsage.MedicineTaken.NO).build();
        MedicationUsage med3 = MedicationUsage.create().withName("Panadol").withTaken(MedicationUsage.MedicineTaken.YES).withTime(testTime3).build();
        Collection<MedicationUsage> medicationUsages = Lists.newArrayList(med1, med2, med3);

        service.addCheckIn(id, ch1 = CheckIn.create().withPatientId(id).withTimestamp(Utility.getTestDateWithOffset(21,0,0))
                .withPainIntensity(CheckIn.PainIntensity.WELL_CONTROLLED).withEatingDisorder(CheckIn.EatingDisorder.NO)
                .build());
        service.addCheckIn(id, ch2 = CheckIn.create().withPatientId(id).withTimestamp(Utility.getTestDateWithOffset(18,0,0))
                .withPainIntensity(CheckIn.PainIntensity.WELL_CONTROLLED).withEatingDisorder(CheckIn.EatingDisorder.NO)
                .withMedicationUsage(medicationUsages).build());
        service.addCheckIn(id, ch3 = CheckIn.create().withPatientId(id).withTimestamp(Utility.getTestDateWithOffset(9,0,0))
                .withPainIntensity(CheckIn.PainIntensity.MODERATE).withEatingDisorder(CheckIn.EatingDisorder.NO)
                .build());
        service.addCheckIn(id, ch4 = CheckIn.create().withPatientId(id).withTimestamp(Utility.getTestDateWithOffset(2,0,0))
                .withPainIntensity(CheckIn.PainIntensity.MODERATE).withEatingDisorder(CheckIn.EatingDisorder.SOME)
                .build());


    }


    protected void populateCheckInsAlarmModerate(final long id, ServiceClientSvcApi service) {

        MedicationUsage med1 = MedicationUsage.create().withName("Aleve").withTaken(MedicationUsage.MedicineTaken.YES).withTime(testTime1).build();
        MedicationUsage med2 = MedicationUsage.create().withName("Atarax").withTaken(MedicationUsage.MedicineTaken.NO).build();
        MedicationUsage med3 = MedicationUsage.create().withName("Celebrex").withTaken(MedicationUsage.MedicineTaken.YES).withTime(testTime3).build();
        Collection<MedicationUsage> medicationUsages = Lists.newArrayList(med1, med2, med3);

        service.addCheckIn(id, CheckIn.create().withPatientId(id).withTimestamp(Utility.getTestDateWithOffset(3,1,0,0))
                .withPainIntensity(CheckIn.PainIntensity.WELL_CONTROLLED).withEatingDisorder(CheckIn.EatingDisorder.NO)
                .build());
        service.addCheckIn(id, CheckIn.create().withPatientId(id).withTimestamp(Utility.getTestDateWithOffset(2,15,0,0))
                .withPainIntensity(CheckIn.PainIntensity.WELL_CONTROLLED).withEatingDisorder(CheckIn.EatingDisorder.NO)
                .withMedicationUsage(medicationUsages).build());

        service.addCheckIn(id, CheckIn.create().withPatientId(id).withTimestamp(Utility.getTestDateWithOffset(2,10,0,0))
                .withPainIntensity(CheckIn.PainIntensity.WELL_CONTROLLED).withEatingDisorder(CheckIn.EatingDisorder.NO)
                .withMedicationUsage(medicationUsages).build());
        service.addCheckIn(id, CheckIn.create().withPatientId(id).withTimestamp(Utility.getTestDateWithOffset(2,5,0,0))
                .withPainIntensity(CheckIn.PainIntensity.WELL_CONTROLLED).withEatingDisorder(CheckIn.EatingDisorder.SOME)
                .build());
        service.addCheckIn(id, CheckIn.create().withPatientId(id).withTimestamp(Utility.getTestDateWithOffset(2,1,0,0))
                .withPainIntensity(CheckIn.PainIntensity.WELL_CONTROLLED).withEatingDisorder(CheckIn.EatingDisorder.SOME)
                .build());
        service.addCheckIn(id, CheckIn.create().withPatientId(id).withTimestamp(Utility.getTestDateWithOffset(1,21,0,0))
                .withPainIntensity(CheckIn.PainIntensity.WELL_CONTROLLED).withEatingDisorder(CheckIn.EatingDisorder.SOME)
                .withMedicationUsage(medicationUsages).build());
        service.addCheckIn(id, CheckIn.create().withPatientId(id).withTimestamp(Utility.getTestDateWithOffset(1,15,0,0))
                .withPainIntensity(CheckIn.PainIntensity.MODERATE).withEatingDisorder(CheckIn.EatingDisorder.SOME)
                .build());

        service.addCheckIn(id, CheckIn.create().withPatientId(id).withTimestamp(Utility.getTestDateWithOffset(1,7,0,0))
                .withPainIntensity(CheckIn.PainIntensity.MODERATE).withEatingDisorder(CheckIn.EatingDisorder.SOME)
                .build());
        service.addCheckIn(id, CheckIn.create().withPatientId(id).withTimestamp(Utility.getTestDateWithOffset(1,3,0,0))
                .withPainIntensity(CheckIn.PainIntensity.WELL_CONTROLLED).withEatingDisorder(CheckIn.EatingDisorder.SOME)
                .withMedicationUsage(medicationUsages).build());
        service.addCheckIn(id, CheckIn.create().withPatientId(id).withTimestamp(Utility.getTestDateWithOffset(21,0,0))
                .withPainIntensity(CheckIn.PainIntensity.MODERATE).withEatingDisorder(CheckIn.EatingDisorder.SOME)
                .build());
        service.addCheckIn(id, CheckIn.create().withPatientId(id).withTimestamp(Utility.getTestDateWithOffset(14,0,0))
                .withPainIntensity(CheckIn.PainIntensity.SEVERE).withEatingDisorder(CheckIn.EatingDisorder.SOME)
                .build());
        service.addCheckIn(id, CheckIn.create().withPatientId(id).withTimestamp(Utility.getTestDateWithOffset(5,0,0))
                .withPainIntensity(CheckIn.PainIntensity.MODERATE).withEatingDisorder(CheckIn.EatingDisorder.SOME)
                .build());



    }


    protected void populateCheckInsAlarm(final long id, ServiceClientSvcApi service) {

        MedicationUsage med1 = MedicationUsage.create().withName("Medicine1").withTaken(MedicationUsage.MedicineTaken.YES).withTime(testTime1).build();
        MedicationUsage med2 = MedicationUsage.create().withName("Medicine2").withTaken(MedicationUsage.MedicineTaken.NO).withTime(testTime2).build();
        MedicationUsage med3 = MedicationUsage.create().withName("Medicine3").withTaken(MedicationUsage.MedicineTaken.YES).withTime(testTime3).build();
        MedicationUsage med4 = MedicationUsage.create().withName("Medicine4").withTaken(MedicationUsage.MedicineTaken.NO).withTime(testTime2).build();
        MedicationUsage med5 = MedicationUsage.create().withName("Medicine5").withTaken(MedicationUsage.MedicineTaken.NO).withTime(testTime2).build();
        Collection<MedicationUsage> medicationUsages = Lists.newArrayList(med1, med2, med3, med4, med5);

        service.addCheckIn(id, CheckIn.create().withPatientId(id).withTimestamp(Utility.getTestDateWithOffset(1,9,0,0))
                .withPainIntensity(CheckIn.PainIntensity.WELL_CONTROLLED).withEatingDisorder(CheckIn.EatingDisorder.NO)
                .build());
        service.addCheckIn(id, CheckIn.create().withPatientId(id).withTimestamp(Utility.getTestDateWithOffset(1,4,0,0))
                .withPainIntensity(CheckIn.PainIntensity.MODERATE).withEatingDisorder(CheckIn.EatingDisorder.NO)
                .build());
        service.addCheckIn(id, CheckIn.create().withPatientId(id).withTimestamp(Utility.getTestDateWithOffset(20,0,0))
                .withPainIntensity(CheckIn.PainIntensity.MODERATE).withEatingDisorder(CheckIn.EatingDisorder.SOME)
                .build());
        service.addCheckIn(id, CheckIn.create().withPatientId(id).withTimestamp(Utility.getTestDateWithOffset(17,0,0))
                .withPainIntensity(CheckIn.PainIntensity.WELL_CONTROLLED).withEatingDisorder(CheckIn.EatingDisorder.SOME)
                .build());

        long cId= service.addCheckIn(id, CheckIn.create().withPatientId(id).withTimestamp(Utility.getTestDateWithOffset(13,0,0))
                .withPainIntensity(CheckIn.PainIntensity.SEVERE).withEatingDisorder(CheckIn.EatingDisorder.NO)
                .build());
        service.setPhoto(id, cId, new TypedFile("image/jpg", new File( "src/test/resources/throat.jpg")));

        service.addCheckIn(id, CheckIn.create().withPatientId(id).withTimestamp(Utility.getTestDateWithOffset(7, 0, 0))
                .withPainIntensity(CheckIn.PainIntensity.SEVERE).withEatingDisorder(CheckIn.EatingDisorder.NO)
                .withMedicationUsage(medicationUsages).build());
        cId = service.addCheckIn(id, CheckIn.create().withPatientId(id).withTimestamp(Utility.getTestDateWithOffset(2, 0, 0))
                .withPainIntensity(CheckIn.PainIntensity.SEVERE).withEatingDisorder(CheckIn.EatingDisorder.SOME)
                .build());

        service.setPhoto(id, cId, new TypedFile("image/jpg", new File( "src/test/resources/76.jpg")));

    }



    public static Properties getPropValues() throws IOException {

        String result = "";
        Properties prop = new Properties();
        String propFileName = "test.properties";

        InputStream inputStream = SystemManagementApiTest.class.getClassLoader().getResourceAsStream(propFileName);

        if (inputStream != null) {
            prop.load(inputStream);
            return prop;
        } else {
            throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
        }
    }

    @AfterClass
    public static void cleanFiles() throws IOException {
        //creteServiceForUserAndClient(ADMIN, PASSWORD, CLIENT_ID).deleteStorage();
    }

}