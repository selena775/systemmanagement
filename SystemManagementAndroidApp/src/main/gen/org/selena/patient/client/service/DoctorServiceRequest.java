/*___Generated_by_IDEA___*/

/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: /home/sklasnja/aradni/android/radni/systemmanagement/SystemManagementAndroidApp/src/org/selena/patient/client/service/DoctorServiceRequest.aidl
 */
package org.selena.patient.client.service;
/**
 * @class GeoNamesRequest
 *
 * @brief An AIDL interface used to get the results of a web service
 *        call in another process.  The caller provides a
 *        GeoNamesCallback object so that the Service process can
 *        return a result across process boundaries asynchronously.
 *
 *	  This file generates a Java interface in the gen folder.
 */
public interface DoctorServiceRequest extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements org.selena.patient.client.service.DoctorServiceRequest
{
private static final java.lang.String DESCRIPTOR = "org.selena.patient.client.service.DoctorServiceRequest";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an org.selena.patient.client.service.DoctorServiceRequest interface,
 * generating a proxy if needed.
 */
public static org.selena.patient.client.service.DoctorServiceRequest asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof org.selena.patient.client.service.DoctorServiceRequest))) {
return ((org.selena.patient.client.service.DoctorServiceRequest)iin);
}
return new org.selena.patient.client.service.DoctorServiceRequest.Stub.Proxy(obj);
}
@Override public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_getPatientList:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
org.selena.patient.client.service.DoctorServiceCallback _arg1;
_arg1 = org.selena.patient.client.service.DoctorServiceCallback.Stub.asInterface(data.readStrongBinder());
this.getPatientList(_arg0, _arg1);
return true;
}
case TRANSACTION_getPatientCheckIns:
{
data.enforceInterface(DESCRIPTOR);
long _arg0;
_arg0 = data.readLong();
org.selena.patient.client.service.DoctorServiceCallback _arg1;
_arg1 = org.selena.patient.client.service.DoctorServiceCallback.Stub.asInterface(data.readStrongBinder());
this.getPatientCheckIns(_arg0, _arg1);
return true;
}
case TRANSACTION_getPatientMedications:
{
data.enforceInterface(DESCRIPTOR);
long _arg0;
_arg0 = data.readLong();
org.selena.patient.client.service.DoctorServiceCallback _arg1;
_arg1 = org.selena.patient.client.service.DoctorServiceCallback.Stub.asInterface(data.readStrongBinder());
this.getPatientMedications(_arg0, _arg1);
return true;
}
case TRANSACTION_saveMedications:
{
data.enforceInterface(DESCRIPTOR);
long _arg0;
_arg0 = data.readLong();
java.util.List<org.selena.patient.client.model.MedicationUI> _arg1;
_arg1 = data.createTypedArrayList(org.selena.patient.client.model.MedicationUI.CREATOR);
org.selena.patient.client.service.DoctorServiceCallback _arg2;
_arg2 = org.selena.patient.client.service.DoctorServiceCallback.Stub.asInterface(data.readStrongBinder());
this.saveMedications(_arg0, _arg1, _arg2);
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements org.selena.patient.client.service.DoctorServiceRequest
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
@Override public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
/**
    * Invoke a call to the GeoNames webservice at the provided
    * Internet uri.  The Service uses the GeoNamesCallback parameter
    * to return a string containing the results back to the Activity.
    */
@Override public void getPatientList(java.lang.String pattern, org.selena.patient.client.service.DoctorServiceCallback callback) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(pattern);
_data.writeStrongBinder((((callback!=null))?(callback.asBinder()):(null)));
mRemote.transact(Stub.TRANSACTION_getPatientList, _data, null, android.os.IBinder.FLAG_ONEWAY);
}
finally {
_data.recycle();
}
}
@Override public void getPatientCheckIns(long patientId, org.selena.patient.client.service.DoctorServiceCallback callback) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeLong(patientId);
_data.writeStrongBinder((((callback!=null))?(callback.asBinder()):(null)));
mRemote.transact(Stub.TRANSACTION_getPatientCheckIns, _data, null, android.os.IBinder.FLAG_ONEWAY);
}
finally {
_data.recycle();
}
}
@Override public void getPatientMedications(long patientId, org.selena.patient.client.service.DoctorServiceCallback callback) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeLong(patientId);
_data.writeStrongBinder((((callback!=null))?(callback.asBinder()):(null)));
mRemote.transact(Stub.TRANSACTION_getPatientMedications, _data, null, android.os.IBinder.FLAG_ONEWAY);
}
finally {
_data.recycle();
}
}
@Override public void saveMedications(long patientId, java.util.List<org.selena.patient.client.model.MedicationUI> toSave, org.selena.patient.client.service.DoctorServiceCallback callback) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeLong(patientId);
_data.writeTypedList(toSave);
_data.writeStrongBinder((((callback!=null))?(callback.asBinder()):(null)));
mRemote.transact(Stub.TRANSACTION_saveMedications, _data, null, android.os.IBinder.FLAG_ONEWAY);
}
finally {
_data.recycle();
}
}
}
static final int TRANSACTION_getPatientList = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
static final int TRANSACTION_getPatientCheckIns = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
static final int TRANSACTION_getPatientMedications = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
static final int TRANSACTION_saveMedications = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
}
/**
    * Invoke a call to the GeoNames webservice at the provided
    * Internet uri.  The Service uses the GeoNamesCallback parameter
    * to return a string containing the results back to the Activity.
    */
public void getPatientList(java.lang.String pattern, org.selena.patient.client.service.DoctorServiceCallback callback) throws android.os.RemoteException;
public void getPatientCheckIns(long patientId, org.selena.patient.client.service.DoctorServiceCallback callback) throws android.os.RemoteException;
public void getPatientMedications(long patientId, org.selena.patient.client.service.DoctorServiceCallback callback) throws android.os.RemoteException;
public void saveMedications(long patientId, java.util.List<org.selena.patient.client.model.MedicationUI> toSave, org.selena.patient.client.service.DoctorServiceCallback callback) throws android.os.RemoteException;
}
