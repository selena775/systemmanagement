/*___Generated_by_IDEA___*/

/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: /home/sklasnja/aradni/android/radni/systemmanagement/SystemManagementAndroidApp/src/org/selena/patient/client/service/DoctorServiceCallback.aidl
 */
package org.selena.patient.client.service;
/**
 * @class
 *
 * @brief An AIDL Interface used to receive results from a prior call
 *        to GeoNamesRequest.callWebService().
 *
 *	  This file generates a Java interface in the gen folder.
 */
public interface DoctorServiceCallback extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements org.selena.patient.client.service.DoctorServiceCallback
{
private static final java.lang.String DESCRIPTOR = "org.selena.patient.client.service.DoctorServiceCallback";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an org.selena.patient.client.service.DoctorServiceCallback interface,
 * generating a proxy if needed.
 */
public static org.selena.patient.client.service.DoctorServiceCallback asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof org.selena.patient.client.service.DoctorServiceCallback))) {
return ((org.selena.patient.client.service.DoctorServiceCallback)iin);
}
return new org.selena.patient.client.service.DoctorServiceCallback.Stub.Proxy(obj);
}
@Override public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_sendPatientList:
{
data.enforceInterface(DESCRIPTOR);
java.util.List<org.selena.patient.client.model.PatientUI> _arg0;
_arg0 = data.createTypedArrayList(org.selena.patient.client.model.PatientUI.CREATOR);
java.lang.String _arg1;
_arg1 = data.readString();
this.sendPatientList(_arg0, _arg1);
return true;
}
case TRANSACTION_sendCheckInList:
{
data.enforceInterface(DESCRIPTOR);
java.util.List<org.selena.patient.client.model.server.CheckIn> _arg0;
_arg0 = data.createTypedArrayList(org.selena.patient.client.model.server.CheckIn.CREATOR);
java.lang.String _arg1;
_arg1 = data.readString();
this.sendCheckInList(_arg0, _arg1);
return true;
}
case TRANSACTION_sendMedicationList:
{
data.enforceInterface(DESCRIPTOR);
java.util.List<org.selena.patient.client.model.MedicationUI> _arg0;
_arg0 = data.createTypedArrayList(org.selena.patient.client.model.MedicationUI.CREATOR);
java.lang.String _arg1;
_arg1 = data.readString();
this.sendMedicationList(_arg0, _arg1);
return true;
}
case TRANSACTION_sendCheckInPhoto:
{
data.enforceInterface(DESCRIPTOR);
long _arg0;
_arg0 = data.readLong();
java.lang.String _arg1;
_arg1 = data.readString();
java.lang.String _arg2;
_arg2 = data.readString();
this.sendCheckInPhoto(_arg0, _arg1, _arg2);
return true;
}
case TRANSACTION_sendSaveMedicationsResult:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
this.sendSaveMedicationsResult(_arg0);
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements org.selena.patient.client.service.DoctorServiceCallback
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
@Override public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
/**
     * Send a string containing the results from the call to the Web
     * service back to the client.
     */
@Override public void sendPatientList(java.util.List<org.selena.patient.client.model.PatientUI> results, java.lang.String reason) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeTypedList(results);
_data.writeString(reason);
mRemote.transact(Stub.TRANSACTION_sendPatientList, _data, null, android.os.IBinder.FLAG_ONEWAY);
}
finally {
_data.recycle();
}
}
@Override public void sendCheckInList(java.util.List<org.selena.patient.client.model.server.CheckIn> results, java.lang.String reason) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeTypedList(results);
_data.writeString(reason);
mRemote.transact(Stub.TRANSACTION_sendCheckInList, _data, null, android.os.IBinder.FLAG_ONEWAY);
}
finally {
_data.recycle();
}
}
@Override public void sendMedicationList(java.util.List<org.selena.patient.client.model.MedicationUI> results, java.lang.String reason) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeTypedList(results);
_data.writeString(reason);
mRemote.transact(Stub.TRANSACTION_sendMedicationList, _data, null, android.os.IBinder.FLAG_ONEWAY);
}
finally {
_data.recycle();
}
}
@Override public void sendCheckInPhoto(long chkId, java.lang.String fileName, java.lang.String reason) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeLong(chkId);
_data.writeString(fileName);
_data.writeString(reason);
mRemote.transact(Stub.TRANSACTION_sendCheckInPhoto, _data, null, android.os.IBinder.FLAG_ONEWAY);
}
finally {
_data.recycle();
}
}
@Override public void sendSaveMedicationsResult(java.lang.String reasult) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(reasult);
mRemote.transact(Stub.TRANSACTION_sendSaveMedicationsResult, _data, null, android.os.IBinder.FLAG_ONEWAY);
}
finally {
_data.recycle();
}
}
}
static final int TRANSACTION_sendPatientList = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
static final int TRANSACTION_sendCheckInList = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
static final int TRANSACTION_sendMedicationList = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
static final int TRANSACTION_sendCheckInPhoto = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
static final int TRANSACTION_sendSaveMedicationsResult = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
}
/**
     * Send a string containing the results from the call to the Web
     * service back to the client.
     */
public void sendPatientList(java.util.List<org.selena.patient.client.model.PatientUI> results, java.lang.String reason) throws android.os.RemoteException;
public void sendCheckInList(java.util.List<org.selena.patient.client.model.server.CheckIn> results, java.lang.String reason) throws android.os.RemoteException;
public void sendMedicationList(java.util.List<org.selena.patient.client.model.MedicationUI> results, java.lang.String reason) throws android.os.RemoteException;
public void sendCheckInPhoto(long chkId, java.lang.String fileName, java.lang.String reason) throws android.os.RemoteException;
public void sendSaveMedicationsResult(java.lang.String reasult) throws android.os.RemoteException;
}
