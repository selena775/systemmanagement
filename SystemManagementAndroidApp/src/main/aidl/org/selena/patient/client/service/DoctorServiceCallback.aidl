package org.selena.patient.client.service;
import org.selena.patient.client.model.PatientUI;
import org.selena.patient.client.model.server.CheckIn;
import org.selena.patient.client.model.MedicationUI;
/**
 * @class
 *
 * @brief An AIDL Interface used to receive results from a prior call
 *        to GeoNamesRequest.callWebService().
 *
 *	  This file generates a Java interface in the gen folder.
 */
interface DoctorServiceCallback {
    /**
     * Send a string containing the results from the call to the Web
     * service back to the client.
     */
    oneway void sendPatientList (in List<PatientUI> results, in String reason);

    oneway void sendCheckInList (in List<CheckIn> results, in String reason);

    oneway void sendMedicationList (in List<MedicationUI> results, in String reason);

    oneway void sendCheckInPhoto (in long chkId, in String fileName, in String reason);

    oneway void sendSaveMedicationsResult ( in String reasult);

}
