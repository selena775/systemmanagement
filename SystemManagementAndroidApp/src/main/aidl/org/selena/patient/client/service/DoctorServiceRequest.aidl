package org.selena.patient.client.service;

import org.selena.patient.client.service.DoctorServiceCallback;
import org.selena.patient.client.model.MedicationUI;


/**
 * @class GeoNamesRequest
 *
 * @brief An AIDL interface used to get the results of a web service
 *        call in another process.  The caller provides a
 *        GeoNamesCallback object so that the Service process can
 *        return a result across process boundaries asynchronously.
 *
 *	  This file generates a Java interface in the gen folder.
 */
interface DoctorServiceRequest {
   /**
    * Invoke a call to the GeoNames webservice at the provided
    * Internet uri.  The Service uses the GeoNamesCallback parameter
    * to return a string containing the results back to the Activity.
    */



    oneway void getPatientList (in String pattern,
                                in DoctorServiceCallback callback);

    oneway void getPatientCheckIns (in long patientId,
                                in DoctorServiceCallback callback);

    oneway void getPatientMedications (in long patientId,
                                 in DoctorServiceCallback callback);

    oneway void saveMedications (in long patientId, in List<MedicationUI> toSave,
                                in DoctorServiceCallback callback);

}
