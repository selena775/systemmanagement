/* 
**
** Copyright 2014, Jules White
**
** 
*/
package org.selena.patient.client.utility;

public interface TaskCallback<T> {

    public void success(T result);

    public void error(Exception e);

}
