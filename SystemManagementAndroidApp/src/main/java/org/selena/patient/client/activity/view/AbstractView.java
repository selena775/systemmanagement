package org.selena.patient.client.activity.view;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Messenger;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import org.selena.patient.client.R;
import org.selena.patient.client.activity.StartActivity;
import org.selena.patient.client.invariable.MessageConstants;
import org.selena.patient.client.model.server.IUserName;
import org.selena.patient.client.model.LoggedInUser;

import static org.selena.patient.client.utility.UserCredentialsUtility.getProjectPreferences;
import static org.selena.patient.client.utility.UserCredentialsUtility.signOut;


public abstract class AbstractView implements UserView, View.OnCreateContextMenuListener {


    public static final int MENU_SETTINGS = Menu.FIRST;
    public static final int MENU_VIEW = Menu.FIRST + 1;
    public static final int MENU_SYNC = Menu.FIRST + 2;
    public static final int MENU_NEW_ALERTED = Menu.FIRST + 3;
    public static final int MENU_ALL_ALERTED = Menu.FIRST + 4;
    public static final int DOCTOR_DETAILS = Menu.FIRST + 5;
    public static final int MY_DETAILS = Menu.FIRST + 6;
    public static final int MENU_SIGN_OUT = Menu.FIRST + 7;
    public static final int MENU_EXIT = Menu.FIRST + 8;

    private LoggedInUser user;

    private Context context;

    private Messenger messenger;

    protected abstract Class getPreferenceViewClass();

    protected abstract String getViewString();

    public boolean onPrepareOptionsMenu(Menu menu){

        if(getUser().getUserType()== IUserName.UserType.BOTH ) {
            menu.add(0, MENU_VIEW, Menu.NONE, R.string.switch_view).setIcon(R.drawable.ic_contact_picture);
        }
        menu.add(0, MENU_SETTINGS, Menu.NONE, R.string.settings).setIcon(R.drawable.ic_contact_picture);
        menu.add(0, MENU_SIGN_OUT, Menu.NONE, R.string.sign_out).setIcon(R.drawable.ic_contact_picture);
        menu.add(0, MENU_EXIT, Menu.NONE, R.string.exit).setIcon(R.drawable.ic_contact_picture);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case MENU_VIEW: {
                SharedPreferences prefs = getProjectPreferences(getContext());

                SharedPreferences.Editor editor = prefs.edit();
                editor.putString(getContext().getString(R.string.view), MessageConstants.PATIENT.equals(getViewString()) ? MessageConstants.DOCTOR : MessageConstants.PATIENT);
                editor.commit();

                getContext().startActivity(new Intent(getContext(), StartActivity.class));
                return true;
            }
            case MENU_SETTINGS: {
                showSettingsActivity();
                return true;
            }
            case MENU_SIGN_OUT: {
                signOut(context);
                Intent intent = new Intent(context, StartActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                return true;
            }
            case MENU_EXIT: {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
            default:
                return false;
        }
    }


    public LoggedInUser getUser() {
        return user;
    }

    public void setUser(LoggedInUser user) {
        this.user = user;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public Messenger getMessenger() {
        return messenger;
    }

    public void setMessenger(Messenger messenger) {
        this.messenger = messenger;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

    }

    public void showSettingsActivity() {
        context.startActivity(new Intent(context, getPreferenceViewClass()));
    }

}
