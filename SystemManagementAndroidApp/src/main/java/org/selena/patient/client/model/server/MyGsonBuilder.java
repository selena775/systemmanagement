package org.selena.patient.client.model.server;

import com.google.gson.*;

import java.lang.reflect.Type;

/**
 * Created by sklasnja on 11/4/14.
 */
public class MyGsonBuilder {

    private GsonBuilder gsonBuilder= new GsonBuilder()
            .registerTypeAdapter(IDoctor.class, new IDoctorDeserializer())
            .registerTypeAdapter(IDoctor.class, new IDoctorSerializer())
            .registerTypeAdapter(IUserName.class, new IUserNameDeserializer())
            .registerTypeAdapter(IUserName.class, new IUserNameSerializer())
            .serializeNulls();

    public Gson create() {

       return gsonBuilder.create();

    }

    public static class IDoctorDeserializer implements JsonDeserializer<IDoctor> {
        @Override
        public IDoctor deserialize(JsonElement json, Type type, JsonDeserializationContext jdc)
                throws JsonParseException {

            final JsonObject jsonObject = json.getAsJsonObject();

            try {
                final String userType = jsonObject.get("userType").getAsString();

                if ( userType.equals("BOTH")) {

                    return jdc.deserialize(json, DoctorAndPatient.class);
                } else {

                    return jdc.deserialize(json, Doctor.class);
                }

            } catch (Throwable e) {
                throw new RuntimeException(e);
            }

        }
    }

    public static class IUserNameDeserializer implements JsonDeserializer<IUserName> {
        @Override
        public IUserName deserialize(JsonElement json, Type type, JsonDeserializationContext jdc)
                throws JsonParseException {

            final JsonObject jsonObject = json.getAsJsonObject();

            try {
                final String userType = jsonObject.get("userType").getAsString();

                if ( userType.equals("BOTH")) {

                    return jdc.deserialize(json, DoctorAndPatient.class);
                } else if ( userType.equals("DOCTOR")){

                    return jdc.deserialize(json, Doctor.class);
                } else {
                    return jdc.deserialize(json, Patient.class);
                }

            } catch (Throwable e) {
                throw new RuntimeException(e);
            }

        }
    }


    public static class IDoctorSerializer implements JsonSerializer<IDoctor> {

        @Override
        public JsonElement serialize(final IDoctor doctor, final Type typeOfSrc, final JsonSerializationContext context) {

            JsonElement element;
            if (IUserName.UserType.DOCTOR == doctor.getType()) {
                element = context.serialize((Doctor)doctor, typeOfSrc );
            } else {
                element = context.serialize((DoctorAndPatient)doctor, typeOfSrc );
            }
            return element;
        }
    }


    public static class IUserNameSerializer implements JsonSerializer<IUserName> {

        @Override
        public JsonElement serialize(final IUserName userName, final Type typeOfSrc, final JsonSerializationContext context) {

            JsonElement element;
            if (IUserName.UserType.PATIENT == userName.getType()) {
                element= context.serialize((Patient)userName, typeOfSrc );

            } else {
                element = context.serialize((IDoctor)userName, typeOfSrc );
            }
            return element;
        }
    }
}
