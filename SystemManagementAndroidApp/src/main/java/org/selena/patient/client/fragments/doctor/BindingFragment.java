package org.selena.patient.client.fragments.doctor;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import org.selena.patient.client.activity.doctor.ErrorNotificationActivity;
import org.selena.patient.client.fragments.UserFragment;
import org.selena.patient.client.invariable.MessageConstants;
import org.selena.patient.client.model.MedicationUI;
import org.selena.patient.client.model.server.CheckIn;
import org.selena.patient.client.service.DoctorServiceCallback;
import org.selena.patient.client.service.DoctorServiceRequest;

import java.util.List;

/**
 * Created by sklasnja on 11/6/14.
 */
public abstract class BindingFragment extends UserFragment  {

    private DoctorServiceRequest mDoctorServiceRequest = null;
    private DoctorServiceCallback.Stub mDoctorServiceCallback = null;


    private void notifyDialogUser(final Context ctx, final Intent intent, final String reason) {

        Intent pIntent = new Intent(ctx, ErrorNotificationActivity.class);
        pIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Bundle bundle = new Bundle(intent.getExtras());
        bundle.putString(MessageConstants.ERROR_KEY, reason);
        pIntent.putExtras(bundle);
        ctx.startActivity(pIntent);

    }

    protected void  onDisconnect(){}

    public void serviceConnected(final DoctorServiceRequest doctorServiceRequest, final DoctorServiceCallback.Stub doctorServiceCallback){
        mDoctorServiceRequest = doctorServiceRequest;
        mDoctorServiceCallback = doctorServiceCallback;
    }

    public void onServiceDisconnected() {
        mDoctorServiceRequest= null;
        mDoctorServiceCallback = null;
    }

    protected DoctorServiceRequest getDoctorServiceRequest() {
        return mDoctorServiceRequest;
    }

    public DoctorServiceCallback.Stub getDoctorServiceCallback() {
        return mDoctorServiceCallback;
    }

    public abstract int getTabTitle();

    public void onSendCheckInList(final List<CheckIn> results){};

    public void onSendCheckInPhoto(final long chkId, final String fileName){};

    public void onSendSaveMedicationsResult() { }

    public void onSendMedicationList(final List<MedicationUI> results) {}

}
