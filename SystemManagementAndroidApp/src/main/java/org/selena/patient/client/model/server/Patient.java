package org.selena.patient.client.model.server;

import com.google.common.base.Objects;

import java.util.Collection;
import java.util.HashSet;

/**
 * A simple object to represent a patient
 * 
 * @author sklasnja
 */

public class Patient extends UserName implements IPatient, IUserName {

    private String doctor;

    private String medicalRecord;

    private boolean inAlarm;

    private Collection<Medication> medications = new HashSet<Medication>();

    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(final String doctor) {
        this.doctor = doctor;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(final String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public Collection<Medication> getMedications() {
        return medications;
    }

    public void setMedications(final Collection<Medication> medications) {
        this.medications = medications;
    }

    public boolean isInAlarm() {
        return inAlarm;
    }

    public void setInAlarm(final boolean inAlarm) {
        this.inAlarm = inAlarm;
    }

    /**
	 * Two Videos will generate the same hashcode if they have exactly the same
	 * values for their firstName, url, and duration.
	 * 
	 */

	@Override
	public int hashCode() {
		// Google Guava provides great utilities for hashing
		return Objects.hashCode(getFirstName(), getLastName(), getMedicalRecord(), getDob(), getDoctor());
	}

	/**
	 * Two Videos are considered equal if they have exactly the same values for
	 * their firstName, url, and duration.
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Patient) {
			Patient other = (Patient) obj;
			// Google Guava provides great utilities for equals too!
			return Objects.equal(getFirstName(), other.getFirstName())
                    && Objects.equal(getLastName(), other.getLastName())
                    && Objects.equal(getMedicalRecord(), other.getMedicalRecord())
                    && Objects.equal(getDob(), other.getDob())
					&& Objects.equal(getDoctor(), other.getDoctor());
		} else {
			return false;
		}
	}

    @Override
    public UserType getType() {
        return UserType.PATIENT;
    }

}
