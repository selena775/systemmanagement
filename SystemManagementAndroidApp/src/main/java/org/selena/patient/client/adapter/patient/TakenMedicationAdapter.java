package org.selena.patient.client.adapter.patient;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import butterknife.ButterKnife;
import butterknife.InjectView;
import org.selena.patient.client.R;
import org.selena.patient.client.widget.TimeDatePickerDialog;
import org.selena.patient.client.model.MedicationUsage;
import org.selena.provider.tables.MedicationT;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static org.selena.patient.client.utility.DateTimeUtility.formatMedicineDate;

public class TakenMedicationAdapter extends CursorAdapter {

    private ArrayList<MedicationUsage> list = new ArrayList<>();
    private static LayoutInflater inflater = null;
    private Context mContext;


    public TakenMedicationAdapter(Context context, Cursor cursor, int flags) {
        super(context, cursor, flags);

        mContext = context;
        inflater = LayoutInflater.from(mContext);

    }

    @Override
    public Cursor swapCursor(Cursor newCursor) {
        super.swapCursor(newCursor);

        if (null != newCursor) {

            if (newCursor.moveToFirst()) {
                list.clear();
                do {
                    list.add(getMedicationFromCursor(newCursor));

                } while (newCursor.moveToNext());
            }
        }
        return newCursor;

    }

    // returns a new PlaceRecord for the data at the cursor's
    // current position
    private MedicationUsage getMedicationFromCursor(Cursor cursor) {

        MedicationUsage medicationUsage = new MedicationUsage();
        medicationUsage.setId(cursor.getLong(cursor
                        .getColumnIndex(MedicationT.Cols.ID)));
        medicationUsage.setClientId(cursor.getLong(cursor
                        .getColumnIndex(MedicationT.Cols.LOGIN_ID)));
        medicationUsage.setName(cursor.getString(cursor
                        .getColumnIndex(MedicationT.Cols.NAME)));

        int index;
        if ((index= cursor.getColumnIndex("DATE"))!=-1) {
            medicationUsage.setTime(new Date(cursor.getLong(index)));
        }
        if ((index= cursor.getColumnIndex("TAKEN"))!=-1) {
            medicationUsage.setTaken(org.selena.patient.client.model.MedicationUsage.MedicineTaken.isTaken(cursor.getInt(index)));
        }

        return medicationUsage;

    }

    public int getCount() {
        return list.size();
    }

    public Object getItem(int position) {
        return list.get(position);
    }

    public long getItemId(int position) {
        return position;
    }


    public ArrayList<MedicationUsage> getList() {
        return list;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        final Date zeroDate= new Date(0);

        final MedicationUsage medicationUsage = list.get(cursor.getPosition());

        final ViewHolder holder = (ViewHolder) view.getTag();

        holder.nameView.setText(context.getString(R.string.medicine_taken_string) + " " + medicationUsage.getName() + "?");

        holder.yesNoSpinner.setAdapter(ArrayAdapter.createFromResource(
                context, R.array.yes_no_array, R.layout.dropdown_item));
        holder.yesNoSpinner.setSelection(medicationUsage.getTaken().ordinal());

        holder.date.setText(formatMedicineDate(medicationUsage.getTime()));

        holder.yesNoSpinner.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(final AdapterView<?> parent, final View view, final int position, final long id) {

                medicationUsage.setTaken(org.selena.patient.client.model.MedicationUsage.MedicineTaken.isTaken(position));
                if ( position==1){
                    if ( zeroDate.equals(medicationUsage.getTime())) {
                        TimeDatePickerDialog.create(mContext)
                                .setTitle(medicationUsage.getName() + " time")
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .addDateTimeListener(new TimeDatePickerDialog.DateTimeListener() {
                                    @Override
                                    public void onDateTimeSet(final Calendar calendar) {
                                        medicationUsage.setTime(calendar.getTime());
                                        holder.date.setVisibility(View.VISIBLE);
                                        holder.date.setText(formatMedicineDate(medicationUsage.getTime()));
                                    }

                                    @Override
                                    public void onCancel() {
                                        holder.yesNoSpinner.setSelection(0);
                                        holder.date.setVisibility(View.INVISIBLE);
                                        medicationUsage.setTime(new Date(0));
                                        holder.date.setText(formatMedicineDate(medicationUsage.getTime()));
                                    }
                                })
                                .show();
                    } else {
                        holder.date.setVisibility(View.VISIBLE);
                    }
                } else {
                    holder.date.setVisibility(View.INVISIBLE);
                    medicationUsage.setTime(zeroDate);
                    holder.date.setText(formatMedicineDate(medicationUsage.getTime()));
                }
            }

            @Override
            public void onNothingSelected(final AdapterView<?> parent) {

            }
        });

    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {

        View newView = inflater.inflate(R.layout.medicine_item, null);
        ViewHolder holder = new ViewHolder(newView);
        newView.setTag(holder);
        return newView;
    }

    public void add(final MedicationUsage medicationUsage) {
        list.add(medicationUsage);
    }

    static class ViewHolder {

        @InjectView(R.id.name) TextView nameView;
        @InjectView(R.id.yes_no_answer) Spinner yesNoSpinner;
        @InjectView(R.id.medicine_date) TextView date;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }

}
