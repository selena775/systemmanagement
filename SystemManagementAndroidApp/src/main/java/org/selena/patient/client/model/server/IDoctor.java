package org.selena.patient.client.model.server;

public interface IDoctor extends IUserName {

    public String getDoctorId();

}
