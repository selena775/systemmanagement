package org.selena.patient.client.activity;

import android.app.Activity;
import android.os.Bundle;
import android.widget.*;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import org.selena.patient.client.R;
import org.selena.patient.client.invariable.MessageConstants;
import org.selena.patient.client.model.DoctorDetails;

/**
 * Created by sklasnja on 11/12/14.
 */
public class DoctorDetailsActivity extends Activity {

    @InjectView(R.id.first_name)
    protected EditText first_name;

    @InjectView(R.id.last_name)
    protected EditText last_name;

    @InjectView(R.id.record)
    protected EditText record;

    @InjectView(R.id.record_text)
    protected TextView record_text;

    @InjectView(R.id.submit)
    protected Button submit;

    @InjectView(R.id.main_view)
    protected LinearLayout main_view;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.person_details_layout);

        ButterKnife.inject(this);

        record_text.setText(getString(R.string.doctor_id));
        submit.setText(getString(R.string.edit));

        first_name.setEnabled(false);
        last_name.setEnabled(false);
        record.setEnabled(false);

        DoctorDetails doctorDetails= getIntent().getParcelableExtra(MessageConstants.OBJECT);
        if( doctorDetails==null ) {
            Toast.makeText(this, getString(R.string.search_empty), Toast.LENGTH_LONG).show();
            finish();
        }

        first_name.setText(doctorDetails.getFirstName());
        last_name.setText(doctorDetails.getLastName());
        record.setText(doctorDetails.getDoctorId());

        if( !getIntent().getBooleanExtra(MessageConstants.EDITABLE, false) && main_view.findViewById(R.id.submit)!=null)
            main_view.removeView(submit);

    }

    @OnClick(R.id.submit)
    public void OnButtonClick(){

        if( getString(R.string.submit).equals(submit.getText().toString())) {
            // TODO update doctor's details

        } else {
            first_name.setEnabled(true);
            last_name.setEnabled(true);
            record.setEnabled(true);
            submit.setText(getString(R.string.submit));
        }

    }


}