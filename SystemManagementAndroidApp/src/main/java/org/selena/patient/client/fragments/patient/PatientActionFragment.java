package org.selena.patient.client.fragments.patient;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import org.selena.patient.client.R;
import org.selena.patient.client.activity.patient.CheckInActivity;
import org.selena.patient.client.activity.patient.PatientViewAndUpdatePreferencesActivity;
import org.selena.patient.client.fragments.UserFragment;

public class PatientActionFragment extends UserFragment {

    @InjectView(R.id.checkIn)
    protected Button checkIn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.patient_action_fragment, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @OnClick(R.id.pref_button)
    public void onPreferencesClick(View v) {
        startActivity(new Intent(getActivity(), PatientViewAndUpdatePreferencesActivity.class));
    }

    @OnClick(R.id.checkIn)
    public void onCheckIn(View v) {
        Intent intent= new Intent(getActivity(), CheckInActivity.class);
        intent.putExtra("Parent", "ActionFragment");
        startActivity(intent);
    }


    @Override
    public void updateUI() {
        if ( getActivity()==null ) return;
        checkIn.setEnabled(!getUser().isNone());
    }
}
