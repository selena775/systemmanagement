package org.selena.patient.client.invariable;

/**
 * Created by sklasnja on 10/7/14.
 */
public interface PathConstants {

    public static final String ID = "id";
    public static final String CHECK_IN_ID = "cId";

    public static final String FIND_BY_USER_NAME = "/user/search/findByUserName";
    public static final String USER_SVC_PATH = "/user";
    public static final String USERNAME_PARAMETER = "userName";

    // The path where we expect the Patient to live
    public static final String PATIENT_SVC_PATH = "/patient";
    public static final String PATIENT_ID_SVC_PATH = PATIENT_SVC_PATH + "/{" + ID + "}";
    public static final String PATIENT_DOCTOR = PATIENT_ID_SVC_PATH + "/doctor";
    public static final String PATIENT_ID_MEDICATIONS =PATIENT_ID_SVC_PATH + "/medications";
    public static final String PATIENT_CHECK_IN_SVC_PATH = PATIENT_ID_SVC_PATH + "/checkIn";
    public static final String PATIENT_CHECK_IN_ID_SVC_PATH = PATIENT_CHECK_IN_SVC_PATH + "/{" + CHECK_IN_ID + "}";
    public static final String PATIENT_CHECK_IN_PHOTO = PATIENT_CHECK_IN_ID_SVC_PATH + "/photo";

    public static final String CHECK_ROLES="/roles";
    public static final String DOCTOR_AND_PATIENT_SVC_PATH = "/doctorAndPatient";

    public static final String DOCTOR_SVC_PATH = "/doctor";
    public static final String DOCTOR_ID_SVC_PATH = DOCTOR_SVC_PATH + "/{" + ID + "}";
    // The path to search videos by title
    public static final String DOCTOR_FIRST_NAME_SEARCH_PATH = DOCTOR_SVC_PATH + "/search/findByFirstName";

    // The path to search videos by title
    public static final String FIND_BY_PATIENT_NAME = PATIENT_SVC_PATH + "/search/findByFirstName";
    public static final String FIND_ALERTED_PATIENTS_BY_DOCTOR = PATIENT_SVC_PATH + "/search/findAlertedPatientsByDoctor";
    public static final String FIND_BY_DOCTOR = PATIENT_SVC_PATH + "/search/findByDoctor";
    public static final String FIND_BY_DOCTOR_PATIENT_USERNAME = PATIENT_SVC_PATH + "/search/findByDoctorAndPatientUserName";
    public static final String FIND_BY_DOCTOR_PATIENT_NAME = PATIENT_SVC_PATH + "/search/findByDoctorAndPatientName";

    public static final String FIND_BY_PATIENT_ID_AND_TIME = PATIENT_CHECK_IN_SVC_PATH + "/search/findByPatientIdAndTimestamp";
    public static final String FIND_BY_PATIENT_MEDICATION_AND_DATE = PATIENT_ID_MEDICATIONS + "/search/findByDate";


    public static final String NAME_PARAMETER = "patient";
    public static final String PATTERN_PARAMETER = "pattern";
    public static final String FIRST_NAME_PARAMETER = "firstName";

    public static final String DOCTOR_PARAMETER = "doctor";
    public static final String PHOTO_PARAMETER = "photo";
    public static final String CHECK_IN_TIMESTAMP_PARAMETER = "time";
    public static final String DATE_PARAMETER = "date";

}
