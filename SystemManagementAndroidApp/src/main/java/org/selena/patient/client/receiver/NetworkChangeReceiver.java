package org.selena.patient.client.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import org.selena.patient.client.service.SyncService;

import java.util.Date;

import static org.selena.patient.client.utility.UserCredentialsUtility.getLastTimeFailed;

/**
 * Created by sklasnja on 10/21/14.
 */
public class NetworkChangeReceiver extends BroadcastReceiver {
    private static final String TAG = NetworkChangeReceiver.class.getName();

    @Override
    public void onReceive(Context context, Intent intent) {


        if (isOnline(context)) {
            if (!getLastTimeFailed(context).equals(new Date(0))) {
                Log.d(TAG, "!!!!!!!!!!!!!!!Start syn service again !!!!!!!!!!");
                SyncService.restartScheduledSync(context);
            }
        }
    }

    public boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        //should check null because in air plan mode it will be null
        if (netInfo != null && netInfo.isConnected()) {
            return true;
        }
        return false;
    }
}
