package org.selena.patient.client.activity.patient;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.selena.patient.client.R;
import org.selena.patient.client.adapter.patient.CheckInTimesArrayAdapter;
import org.selena.patient.client.service.SyncService;
import org.selena.patient.client.model.LoggedInUser;

import java.util.Set;
import java.util.TreeSet;

import static org.selena.patient.client.utility.UserCredentialsUtility.anyValidUser;
import static org.selena.patient.client.utility.UserCredentialsUtility.getProjectPreferences;
import static org.selena.patient.client.widget.TimeDatePicker.TimeAction;
import static org.selena.patient.client.widget.TimeDatePicker.showPicker;

/**
 * Created by sklasnja on 10/27/14.
 */
public class ScheduleListPreference extends DialogPreference {

    private static final String TAG = ScheduleListPreference.class.getName();


    private ListView lw;
    private Set<String> defaultSet;
    private static String prefKey = "scheduled_times";

    CheckInTimesArrayAdapter arrayAdapter;

    public ScheduleListPreference(Context ctxt, AttributeSet attrs) {
        super(ctxt, attrs);

        setPersistent(false);

        defaultSet = Sets.newHashSet(ctxt.getResources().getStringArray(R.array.initial_timing));

        setDialogLayoutResource(R.layout.list_view_layout);

        setPositiveButtonText(ctxt.getString(R.string.set));
        setNegativeButtonText(android.R.string.cancel);

    }

    @Override
    protected void onBindDialogView(View v) {

        super.onBindDialogView(v);

        Context ctx= v.getContext();

        Set<String> timeArray = new TreeSet<>(getProjectPreferences(getContext()).getStringSet(prefKey, defaultSet));

        arrayAdapter= new CheckInTimesArrayAdapter(ctx,R.layout.item_edit_remove, Lists.newArrayList(timeArray));

        lw = (ListView) v.findViewById(R.id.listView);
        lw.setFooterDividersEnabled(true);


        LayoutInflater inflater = (LayoutInflater) ctx.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View footerView= inflater.inflate(R.layout.footer_view, null, false);

        lw.addFooterView( footerView );
        lw.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        footerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View footerView) {
                showPicker(footerView.getContext(), "New Scheduled Time", new TimeAction() {
                    @Override
                    public void onTimePickerSelected(String time) {
                        arrayAdapter.add(time);
                        arrayAdapter.notifyDataSetChanged();
                    }
                });

            }
        });
        lw.setAdapter(arrayAdapter);

    }


    @Override
    protected void showDialog(Bundle state)
    {
        super.showDialog(state);

        final AlertDialog d = (AlertDialog)getDialog();


        d.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                int errorCode = isValid();
                Boolean canCloseDialog = (errorCode == 0);

                if(canCloseDialog)
                {
                    d.dismiss();
                    onDialogClosed(true);
                }
                else
                {
                    String errorMessage = getContext().getString(errorCode);
                    Toast t = Toast.makeText(getContext(), errorMessage, Toast.LENGTH_LONG);
                    t.setGravity(Gravity.CENTER, 0, 0);
                    t.show();
                }
            }
        });
    }

    private int isValid() {
        Set<String> timeArray = new TreeSet<>(arrayAdapter.getList());
        if ( timeArray.size() < 4 ) {

            return R.string.not_enough_check_ins;

        } else {

            SharedPreferences.Editor editor = getProjectPreferences(getContext()).edit();
            editor.putStringSet(prefKey, timeArray);
            editor.commit();

            LoggedInUser user = anyValidUser(getContext());
            if (user.isPatient()) {
                SyncService.restartScheduledCheck(getContext());
            }
        }
        return 0;
    }


    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return defaultSet;
    }

}
