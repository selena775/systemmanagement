package org.selena.patient.client.model.server;

import java.util.Date;



public abstract class UserName implements IUserName{

    private long id;

    // Should be unique across the whole system and it is related to access the system
    private String userName;

    private String firstName;

    private String lastName;

    private Date dob;

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(final String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(final Date dob) {
        this.dob = dob;
    }

    protected String type= getType().name();

    @Override
    public abstract UserType getType();
}
