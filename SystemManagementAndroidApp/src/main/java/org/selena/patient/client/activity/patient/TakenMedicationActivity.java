package org.selena.patient.client.activity.patient;

import android.app.Activity;
import android.app.ListActivity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.Bundle;
import org.selena.patient.client.adapter.patient.TakenMedicationAdapter;
import org.selena.patient.client.model.MedicationUsage;
import org.selena.patient.client.model.LoggedInUser;
import org.selena.provider.tables.MedicationT;

import java.util.ArrayList;
import java.util.Date;

import static org.selena.patient.client.utility.DateTimeUtility.formatUtcSqlDateTime;
import static org.selena.patient.client.utility.UserCredentialsUtility.anyValidUser;

public class TakenMedicationActivity extends ListActivity implements
        LoaderManager.LoaderCallbacks<Cursor> {

    private static final String TAG = TakenMedicationActivity.class.getName();

    private TakenMedicationAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Create a new TodoListAdapter for this ListActivity's ListView
        mAdapter = new TakenMedicationAdapter( this, null, 0);

        setListAdapter(mAdapter);

        ArrayList<MedicationUsage> medicationsUsage=
                getIntent().getParcelableArrayListExtra(CheckInActivity.MEDICATIONS_KEY);

        if ( medicationsUsage!=null ) {
            MatrixCursor mCursor= new MatrixCursor(
                    new String[]{MedicationT.Cols.ID, MedicationT.Cols.LOGIN_ID, MedicationT.Cols.NAME, "TAKEN", "DATE"}, medicationsUsage.size());

            for ( MedicationUsage medicationUsage : medicationsUsage ) {
                mCursor.addRow(new Object[]{medicationUsage.getId(), medicationUsage.getClientId(),
                        medicationUsage.getName(), medicationUsage.getTaken().ordinal(), medicationUsage.getTime().getTime()});
            }
            mAdapter.swapCursor(mCursor);
        }
        else {
            // Initialize a CursorLoader
            getLoaderManager().initLoader(0, null, this);
        }
    }


    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        String where = String.format("%s =? AND %s <= ? AND ( %s==? OR %s > ? )",
                MedicationT.Cols.LOGIN_ID, MedicationT.Cols.START_DATE, MedicationT.Cols.END_DATE, MedicationT.Cols.END_DATE);


        LoggedInUser user= anyValidUser(this);

        Date date = new Date();
        String[] whereArgs = new String[] {String.valueOf(user.getLogin_id()), formatUtcSqlDateTime(date), "", formatUtcSqlDateTime(date) };


        return new CursorLoader(this, MedicationT.CONTENT_URI, new String[]
                {MedicationT.Cols.ID, MedicationT.Cols.LOGIN_ID, MedicationT.Cols.NAME},
                where, whereArgs, MedicationT.Cols.NAME + " ASC");


    }

    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        mAdapter.swapCursor(data);
    }

    public void onLoaderReset(Loader<Cursor> loader) {

        mAdapter.swapCursor(null);

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putParcelableArrayListExtra(CheckInActivity.MEDICATIONS_KEY, mAdapter.getList());
        setResult(Activity.RESULT_OK, intent);
        super.onBackPressed();
    }

}