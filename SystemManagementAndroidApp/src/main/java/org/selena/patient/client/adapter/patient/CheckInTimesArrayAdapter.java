package org.selena.patient.client.adapter.patient;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import org.selena.patient.client.R;
import org.selena.patient.client.widget.TimeDatePicker;

import java.util.Collection;
import java.util.List;

import static org.selena.patient.client.widget.TimeDatePicker.showPicker;

/**
 * Created by sklasnja on 10/29/14.
 */
public class CheckInTimesArrayAdapter extends ArrayAdapter<String> {

    int resource;
    private final List<String> list;

    public CheckInTimesArrayAdapter(Context context, int resource, List<String> objects) {

        super(context, resource, objects);
        this.resource = resource;
        list = objects;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final LinearLayout view;
        try {

            if (convertView == null) {
                view = new LinearLayout(getContext());
                String inflater = Context.LAYOUT_INFLATER_SERVICE;
                LayoutInflater vi = (LayoutInflater) getContext()
                        .getSystemService(inflater);
                vi.inflate(resource, view, true);
            } else {
                view = (LinearLayout) convertView;
            }

            TextView textView = (TextView) view
                    .findViewById(R.id.text);
            textView.setText( list.get(position));

            ((ImageView)view.findViewById(R.id.edit_item)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {

                    showPicker(view.getContext(), "Select Scheduled Time", new TimeDatePicker.TimeAction() {
                        @Override
                        public void onTimePickerSelected(String time) {
                            list.set(position, time);
                            notifyDataSetInvalidated();
                        }
                    });
                }
            });

            ((ImageView)view.findViewById(R.id.delete_item)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {

                    list.remove(position);
                    notifyDataSetChanged();
                }
            });
            return view;

        } catch (Exception e) {
            Toast.makeText(getContext(),
                    "exception in ArrayAdpter: " + e.getMessage(),
                    Toast.LENGTH_SHORT).show();
            return null;
        }

    }


    public Collection<String> getList() {
        return list;
    }
}
