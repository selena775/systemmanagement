package org.selena.patient.client.fragments.doctor;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import org.selena.patient.client.R;
import org.selena.patient.client.invariable.MessageConstants;
import org.selena.patient.client.model.PatientUI;
import org.selena.patient.client.utility.DateTimeUtility;
/**
 * Created by sklasnja on 11/6/14.
 */
public class PatientDetailsFragment extends BindingFragment {

    @InjectView(R.id.photo_icon)
    ImageView patientPhoto;
    @InjectView(R.id.patient_first_name)
    TextView firstName;
    @InjectView(R.id.patient_last_name) TextView lastName;
    @InjectView(R.id.patient_dob) TextView patientDob;
    @InjectView(R.id.med_record) TextView medRecord;
    @InjectView(R.id.alarm_icon) ImageView alarmIcon;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.patient_item, container, false);
        ButterKnife.inject(this, view);
        return view;
    }



    @Override
    public void onResume() {
        super.onResume();
        final PatientUI patientUI = (PatientUI)getArguments().get(MessageConstants.PATIENT_KEY);
        if ( patientUI==null )return;

        firstName.setText(patientUI.getFirstName());
        lastName.setText(patientUI.getLastName());
        patientDob.setText(DateTimeUtility.formatDob(patientUI.getDob()));
        medRecord.setText(patientUI.getMedicalRecord());
        if( patientUI.isInAlarm()) {
            alarmIcon.setImageResource(R.drawable.alert);
        } else {
            alarmIcon.setImageURI(null);
        }

    }

    @Override
    public int getTabTitle() {
        return R.string.patients_details;
    }
}