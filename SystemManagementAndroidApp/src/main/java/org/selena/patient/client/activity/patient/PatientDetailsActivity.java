package org.selena.patient.client.activity.patient;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import org.selena.patient.client.R;
import org.selena.patient.client.invariable.MessageConstants;
import org.selena.patient.client.model.LoggedInUser;

/**
 * Created by sklasnja on 11/12/14.
 */
public class PatientDetailsActivity extends Activity {

    @InjectView(R.id.first_name)
    protected EditText first_name;

    @InjectView(R.id.last_name)
    protected EditText last_name;

    @InjectView(R.id.record)
    protected EditText record;

    @InjectView(R.id.record_text)
    protected TextView record_text;

    @InjectView(R.id.submit)
    protected Button submit;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.person_details_layout);

        ButterKnife.inject(this);

        record_text.setText( getString(R.string.medical_record));

        submit.setText(getString(R.string.edit));

        LoggedInUser patientDetails= getIntent().getParcelableExtra(MessageConstants.OBJECT);

        first_name.setText(patientDetails.getFirstName());
        last_name.setText(patientDetails.getLastName());
        record.setText(patientDetails.getMedicalRecord());

        first_name.setEnabled(false);
        last_name.setEnabled(false);
        record.setEnabled(false);

    }

    @OnClick(R.id.submit)
    public void OnButtonClick(){

        if( getString(R.string.submit).equals(submit.getText().toString())) {
            // TODO update patient's details

        } else {
            first_name.setEnabled(true);
            last_name.setEnabled(true);
            record.setEnabled(true);
            submit.setText(getString(R.string.submit));
        }

    }
}