/* 
 **
 ** Copyright 2014, Jules White
 **
 ** 
 */
package org.selena.patient.client.oauth;

/**
 * A special class made to specify exceptions that are thrown by our
 * SecuredRestBuilder.
 * <p/>
 * A more robust implementation would probably have fields for tracking
 * the type of exception (e.g., bad password, etc.).
 *
 * @author jules
 */
public class SecuredRestException extends RuntimeException {

    private int status;

    public SecuredRestException() {
        super();
    }

    public SecuredRestException(String message, Throwable cause) {
        super(message, cause);
    }

    public SecuredRestException(String message) {
        super(message);
    }

    public SecuredRestException(String message, int status) {
        super(message);
        this.status = status;
    }

    public SecuredRestException(Throwable cause) {
        super(cause);
    }

    public int getStatus() {
        return status;
    }
}
