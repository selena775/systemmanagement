package org.selena.patient.client.adapter.doctor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import org.selena.patient.client.R;
import org.selena.patient.client.model.MedicationUsage;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.selena.patient.client.utility.DateTimeUtility.formatMedicineDate;

public class TakenMedicationAdapter extends BaseAdapter {

    private List<MedicationUsage> wholeList;
    private List<MedicationUsage> list = new ArrayList<>();
    private static LayoutInflater inflater = null;
    private Context mContext;

    public TakenMedicationAdapter(Context context, final List<MedicationUsage> list){

        mContext= context;
        this.list = list;
        wholeList= new ArrayList<>();
        wholeList.addAll(list);
        inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
    }

    public int getCount() {
        return list.size();
    }

    public MedicationUsage getItem(int position) {
        return list.get(position);
    }

    public long getItemId(int position) {
        return wholeList.indexOf(list.get(position));
    }

    public List<MedicationUsage> getList() {
        return list;
    }


    public View getView(int position, View view, ViewGroup parent) {

        final MedicationUsage medicationUsage = list.get(position);

        ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = inflater.inflate(R.layout.medicine_item_view, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }

        holder.nameView.setText(medicationUsage.getName());

        holder.date.setText(formatMedicineDate(medicationUsage.getTime()));

        if ( medicationUsage.getTaken()== org.selena.patient.client.model.MedicationUsage.MedicineTaken.YES ) {
            holder.yesIcon.setImageResource(R.drawable.yes);
        } else {
            holder.yesIcon.setImageResource(R.drawable.no);
        }
        return view;
    }


    public void setList(final List<MedicationUsage> list) {
        wholeList= new ArrayList<>();
        wholeList.addAll(list);
        this.list = list;
    }

    static class ViewHolder {

        @InjectView(R.id.name) TextView nameView;
        @InjectView(R.id.yes_icon) ImageView yesIcon;
        @InjectView(R.id.medicine_date) TextView date;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }

    public void removeAllViews(){
        list.clear();
        this.notifyDataSetChanged();
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        list.clear();
        if (charText.length() == 0) {
            list.addAll(wholeList);
        }
        else
        {
            for (MedicationUsage medicationUsage : wholeList)
            {
                if (medicationUsage.getName().toLowerCase(Locale.getDefault()).contains(charText) )
                {
                    list.add(medicationUsage);
                }
            }
        }
        notifyDataSetChanged();
    }

}
