package org.selena.patient.client.utility;

import android.util.Log;

import java.net.ConnectException;

/**
 * Created by sklasnja on 11/5/14.
 */
public class ExceptionUtility {

    private static final String TAG = ExceptionUtility.class.getName();

    public static boolean isLocalNetworkDown( Exception ex ) {
        Throwable cause = ex;
        while (cause.getCause() != null) {
            cause = cause.getCause();
            if (cause.getClass().equals(ConnectException.class)) {

                if (cause.getMessage().contains("ENETUNREACH")) {
                    Log.i(TAG, "@@@@ Local Network Down");
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean isServerDown( Exception ex ) {
        Throwable cause = ex;
        while (cause.getCause() != null) {
            cause = cause.getCause();
            if (cause.getClass().equals(ConnectException.class)) {

                if (cause.getMessage().contains("ETIMEDOUT")) {
                    Log.i(TAG, "@@@@ Server Time Out Exception");
                    return true;
                }
            }
        }
        return false;
    }
}
