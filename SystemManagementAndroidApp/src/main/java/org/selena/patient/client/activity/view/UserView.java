package org.selena.patient.client.activity.view;

import android.view.Menu;
import android.view.MenuItem;
import org.selena.patient.client.model.LoggedInUser;

/**
 * Created by sklasnja on 11/12/14.
 */
public interface UserView {

    boolean onPrepareOptionsMenu(Menu menu);

    boolean onOptionsItemSelected(MenuItem item);

    String getIntroductionText();

    String getInfoText();

    void setUser(LoggedInUser user);

    void showSettingsActivity();

}
