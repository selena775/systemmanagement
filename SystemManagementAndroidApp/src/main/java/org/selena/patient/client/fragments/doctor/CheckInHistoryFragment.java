package org.selena.patient.client.fragments.doctor;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import org.selena.patient.client.R;
import org.selena.patient.client.activity.doctor.ShowPatientActivity;
import org.selena.patient.client.adapter.doctor.CheckInAdapter;
import org.selena.patient.client.model.server.CheckIn;

import java.util.Collections;
import java.util.List;


/**
 * Created by sklasnja on 11/6/14.
 */
public class CheckInHistoryFragment extends BindingFragment {

    private final String TAG = getClass().getName();

    @InjectView(R.id.check_in_list)
    protected ListView mlistView;

    @InjectView(R.id.photo_view)
    protected ImageView photo;

    @InjectView(R.id.parent)
    protected LinearLayout parent;


    private CheckInAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.check_in_histogram, container, false);
        ButterKnife.inject(this, view);

        parent.removeView(photo);


        mlistView.setHeaderDividersEnabled(true);

        ViewGroup header = (ViewGroup)inflater.inflate(R.layout.check_in_item_header, mlistView, false);
        mlistView.addHeaderView(header, null, false);

        return view;
    }

    @Override
    public void onActivityCreated(final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mlistView.setAdapter(mAdapter = new CheckInAdapter(getActivity(), this,
                Collections.<CheckIn>emptyList(),
                Collections.<Long, CheckIn>emptyMap()));


    }


    @Override
    public void onResume() {
        super.onResume();
        final ShowPatientActivity activity= (ShowPatientActivity)getActivity();
        if( activity==null ) return;
        if ( activity.getCheckInList()!=null){
            updateDataModel(activity);
        }
    }

    @Override
    public void onSendCheckInPhoto(final long chkId, final String fileName) {
        showResultsFor(chkId, fileName);
    }

    @Override
    public void onSendCheckInList(final List<CheckIn> results) {

        final ShowPatientActivity activity= (ShowPatientActivity)getActivity();
        if ( activity!=null ) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateDataModel(activity);
                }
            });
        }
    }

    private void updateDataModel(final ShowPatientActivity activity) {
        if ( mAdapter==null ){
            mAdapter = new CheckInAdapter(getActivity(), this, activity.getCheckInList(), activity.getCheckInMap() );
        } else {
            mAdapter.setList(activity.getCheckInList(), activity.getCheckInMap());

        }
    }


    private void showResultsFor(final long chkId, final String fileName) {
        final Activity activity= getActivity();
        if ( activity!=null ) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if ( mAdapter!=null ){
                        mAdapter.addPhoto(fileName, chkId);
                    }
                }
            });
        }
    }
    @Override
    public int getTabTitle() {
        return R.string.check_in_history;
    }

    public void onUpdatePhoto(Bitmap bitmap) {
        if ( getActivity()!=null) {
            if ( bitmap!=null ) {

                if ( null == parent.findViewById(R.id.photo_view))
                    parent.addView(photo);
                photo.setImageBitmap(bitmap);

            } else {
                photo.setImageBitmap(null);
                if ( null != parent.findViewById(R.id.photo_view))
                    parent.removeView(photo);
            }
        }
    }

}