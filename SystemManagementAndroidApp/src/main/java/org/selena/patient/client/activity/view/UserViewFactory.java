package org.selena.patient.client.activity.view;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Messenger;
import org.selena.patient.client.R;
import org.selena.patient.client.invariable.MessageConstants;
import org.selena.patient.client.model.LoggedInUser;

import static org.selena.patient.client.utility.UserCredentialsUtility.getProjectPreferences;

/**
 * Created by sklasnja on 11/12/14.
 */
public class UserViewFactory {

    public static NoneView NONE_VIEW = new NoneView();
    public static DoctorView DOCTOR_VIEW = new DoctorView();
    public static PatientView PATIENT_VIEW = new PatientView();

    public static UserView getUserView(final Context ctx, final LoggedInUser user, final Handler handler ) {

        AbstractView userView;
        if (user.isNone()) userView = NONE_VIEW;
        else {
            userView = PATIENT_VIEW;

            switch (user.getUserType()) {

                case BOTH:
                    SharedPreferences prefs = getProjectPreferences(ctx);
                    if (MessageConstants.DOCTOR.equals(prefs.getString(ctx.getString(R.string.view), MessageConstants.DOCTOR)))
                        userView = DOCTOR_VIEW;
                    break;
                case DOCTOR:
                    userView = DOCTOR_VIEW;
                    break;
                default:
                    break;
            }
        }

        userView.setContext(ctx);
        userView.setUser( user );
        if( handler!=null)
            userView.setMessenger(new Messenger(handler));
        return userView;
    }
}
