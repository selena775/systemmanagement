package org.selena.patient.client.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import org.selena.patient.client.R;
import org.selena.patient.client.activity.view.UserView;

import static org.selena.patient.client.activity.view.UserViewFactory.getUserView;



public class InfoFragment extends UserFragment {

    @InjectView(R.id.introduction_text)
    protected TextView introductionText;

    @InjectView(R.id.info_text)
    protected TextView infoText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.welcome_fragment, container, false);
        ButterKnife.inject(this, view);
        return view;
    }


    public void updateUI() {
        if ( getActivity()==null ) return;

       UserView userView= getUserView(getActivity(), getUser(), null );
        introductionText.setText(userView.getIntroductionText());
        infoText.setText(userView.getInfoText());

    }
}


