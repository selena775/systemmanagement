package org.selena.patient.client.activity;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.Toast;
import butterknife.ButterKnife;
import butterknife.InjectView;
import org.selena.patient.client.R;
import org.selena.patient.client.activity.view.DoctorView;
import org.selena.patient.client.activity.view.PatientView;
import org.selena.patient.client.activity.view.UserView;
import org.selena.patient.client.fragments.InfoFragment;
import org.selena.patient.client.fragments.LoginFragment;
import org.selena.patient.client.fragments.UserFragment;
import org.selena.patient.client.fragments.doctor.DoctorActionFragment;
import org.selena.patient.client.fragments.patient.PatientActionFragment;
import org.selena.patient.client.invariable.MessageConstants;
import org.selena.patient.client.model.MedicationUI;
import org.selena.patient.client.model.PatientUI;
import org.selena.patient.client.model.server.CheckIn;
import org.selena.patient.client.service.DoctorServiceAsync;
import org.selena.patient.client.service.DoctorServiceCallback;
import org.selena.patient.client.service.DoctorServiceRequest;
import org.selena.patient.client.service.SyncService;

import java.util.List;

import static org.selena.patient.client.activity.view.UserViewFactory.getUserView;

public class StartActivity extends BaseActivity implements ServiceConnection {

    private final String TAG = getClass().getName();

    @InjectView(R.id.info_fragment)
    protected FrameLayout infoFragmentLayout;

    @InjectView(R.id.login_fragment)
    protected FrameLayout loginFragmentLayout;

    @InjectView(R.id.action_fragment)
    protected FrameLayout actionFragmentLayout;

    private LoginFragment loginFragment = new LoginFragment();
    private PatientActionFragment patientFragment = new PatientActionFragment();
    private DoctorActionFragment doctorFragment = new DoctorActionFragment();

    DoctorServiceRequest mDoctorServiceRequest = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

        setContentView(R.layout.start_screen);

        ButterKnife.inject(this);

        Bundle bundle = new Bundle();
        bundle.putParcelable(MessageConstants.LISTENER, new Messenger(new ServiceListener(this)));
        loginFragment.setArguments(bundle);

        FragmentTransaction fragmentTransaction = getFragmentManager()
                .beginTransaction();
        fragmentTransaction.add(R.id.info_fragment, new InfoFragment());
        fragmentTransaction.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateUser();
        updateLoginFragmentUI();
        updateActionFragmentUI();
    }

    protected void updateUser() {

        if ( getUser().isDoctor()) {
            if (mDoctorServiceRequest == null)
                bindService(DoctorServiceAsync.makeIntent(this),
                        this,
                        BIND_AUTO_CREATE);
        } else {
            if( mDoctorServiceRequest!=null){
                unbindService(this);
                mDoctorServiceRequest=null;
            }
        }

        getInfoFragment().setUser(getUser());

        loginFragment.setUser(getUser());

        patientFragment.setUser(getUser());

        doctorFragment.setUser(getUser());

    }

    protected void updateUI() {


        updateLoginFragmentUI();

        updateActionFragmentUI();

        getInfoFragment().updateUI();

        if (loginFragment.isAdded()) getLoginFragment().updateUI();

        UserFragment actionFragment = getActionFragment();
        if (actionFragment != null) {
            getActionFragment().updateUI();
        }
    }

    public UserFragment getInfoFragment() {
        return (InfoFragment) getFragmentManager().findFragmentById(R.id.info_fragment);
    }

    public UserFragment getLoginFragment() {
        return loginFragment;
    }

    public UserFragment getActionFragment() {
        return (UserFragment) getFragmentManager().findFragmentById(R.id.action_fragment);
    }

    private void updateLoginFragmentUI() {

        switch (getUser().getStatus()) {
            case NONE:
                showLogin();
                break;
            case NOT_VALID:
                showLogin();
                break;
            case VALID:
                hideLogin();
                break;
            default:
                break;
        }
    }

    private void updateActionFragmentUI() {
        UserView view = getUserView(this, getUser(), null);

        if ( view instanceof DoctorView ) {
            switchFragment(doctorFragment, patientFragment);
        } else if ( view instanceof PatientView ) {
            switchFragment(patientFragment, doctorFragment);
        }

    }

    private synchronized void switchFragment(final Fragment fragmentToAdd, final Fragment fragmentToRemove) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        if (fragmentToRemove.isAdded())
            fragmentTransaction.remove(fragmentToRemove);
        if (!fragmentToAdd.isAdded())
            fragmentTransaction.add(R.id.action_fragment, fragmentToAdd);
        fragmentTransaction.commit();
    }


    private synchronized void showLogin() {
        if (!loginFragment.isAdded()) {

            FragmentTransaction fragmentTransaction = getFragmentManager()
                    .beginTransaction().setCustomAnimations(R.animator.slide_up,
                            R.animator.slide_down);
            fragmentTransaction.add(R.id.login_fragment, loginFragment);
            fragmentTransaction.commit();
        }
    }

    private void hideLogin() {

        if (loginFragment.isAdded()) {
            FragmentTransaction fragmentTransaction = getFragmentManager()
                    .beginTransaction().setCustomAnimations(R.animator.slide_up,
                            R.animator.slide_down);
            fragmentTransaction.remove(loginFragment);
            fragmentTransaction.commit();

            // execute transaction now
            getFragmentManager().executePendingTransactions();
        }

    }


    /**
     * This ServiceConnection is used to receive the DoctorServiceRequest
     * proxy after binding to the DoctorServiceServiceAsync Service using
     * bindService().
     */

    /**
     * Called after the KeyGeneratorService is connected to
     * convey the result returned from onBind().
     */
    @Override
    public void onServiceConnected(ComponentName name,
                                   IBinder service) {

        mDoctorServiceRequest = DoctorServiceRequest.Stub.asInterface(service);
        doctorFragment.serviceConnected(mDoctorServiceRequest, mDoctorServiceCallback);

    }

    /**
     * Called if the Service crashes and is no longer
     * available.  The ServiceConnection will remain bound,
     * but the service will not respond to any requests.
     */
    @Override
    public void onServiceDisconnected(ComponentName name) {
        mDoctorServiceRequest=null;
        doctorFragment.onServiceDisconnected();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mDoctorServiceRequest == null)
            bindService(DoctorServiceAsync.makeIntent(this),
                    this,
                    BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        if( mDoctorServiceRequest!=null){
            unbindService(this);
            mDoctorServiceRequest=null;
        }
        super.onStop();
    }


    /**
     * The implementation of the DoctorServiceCallback AIDL
     * Interface. Should be passed to the DoctorServiceServiceAsync
     * Service using the DoctorServiceRequest.callWebService() method.
     *
     * This implementation of DoctorServiceCallback.Stub plays the role of
     * Invoker in the Broker Pattern.
     */
    DoctorServiceCallback.Stub mDoctorServiceCallback = new DoctorServiceCallback.Stub() {
        @Override
        synchronized public void sendPatientList(final List<PatientUI> results, final String reason) throws RemoteException {
            // do nothing

            StartActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                    setProgressBarIndeterminateVisibility(false);
                }
            });
            if ("OK".equals(reason)) {
                if (doctorFragment.isAdded()) {
                    final Activity activity = StartActivity.this;
                    if (results != null && !results.isEmpty()) {

                        activity.startActivity(SyncService.getIntentForPatientList(activity, results));
                    } else {
                        activity.runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(activity,
                                        getString(R.string.no_patients),
                                        Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                }

            } else {

//
//                activity.runOnUiThread(new Runnable() {
//                    public void run() {
//                        Toast.makeText(activity,
//                                "lost credentials or something, notify use",
//                                Toast.LENGTH_LONG).show();
//                    }
//                });
//                return

                Log.d(TAG, "Error " + reason);
                StartActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(StartActivity.this, "Error: " + reason, Toast.LENGTH_LONG).show();
                    }
                });
            }
        }

        @Override
        synchronized public void sendCheckInList(final List<CheckIn> results, final String reason) throws RemoteException {
            // do nothing
        }

        @Override
        public void sendMedicationList(final List<MedicationUI> results, final String reason) throws RemoteException {

        }

        @Override
        synchronized public void sendCheckInPhoto(final long chkId, final String fileName, final String reason) throws RemoteException {
            // do nothing
        }

        @Override
        public void sendSaveMedicationsResult(final String reasult) throws RemoteException {

        }

    };



}
