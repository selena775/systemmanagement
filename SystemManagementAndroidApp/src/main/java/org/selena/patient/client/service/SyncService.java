package org.selena.patient.client.service;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.*;
import android.os.Bundle;
import android.os.Messenger;
import android.os.SystemClock;
import android.text.format.DateUtils;
import android.util.Log;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.selena.patient.client.PatientSvcConnection;
import org.selena.patient.client.R;
import org.selena.patient.client.ServiceClientSvcApi;
import org.selena.patient.client.activity.StartActivity;
import org.selena.patient.client.activity.doctor.PatientListActivity;
import org.selena.patient.client.activity.doctor.ShowPatientActivity;
import org.selena.patient.client.activity.patient.CheckInActivity;
import org.selena.patient.client.invariable.MessageConstants;
import org.selena.patient.client.model.DoctorDetails;
import org.selena.patient.client.model.LoggedInUser;
import org.selena.patient.client.model.MedicationUI;
import org.selena.patient.client.model.PatientUI;
import org.selena.patient.client.model.server.CheckIn;
import org.selena.patient.client.model.server.IUserName;
import org.selena.patient.client.oauth.SecuredRestException;
import org.selena.patient.client.utility.GCMUtility;
import org.selena.patient.client.utility.Pair;
import org.selena.provider.PatientSchema;
import org.selena.provider.tables.AlertedPatientT;
import org.selena.provider.tables.CheckInT;
import org.selena.provider.tables.MedicationT;
import org.selena.provider.tables.UserCredentialsT;
import retrofit.mime.TypedFile;

import java.io.File;
import java.text.ParseException;
import java.util.*;

import static org.selena.patient.client.invariable.MessageConstants.PROPERTY_REG_ID_TO_REMOVE;
import static org.selena.patient.client.utility.DateTimeUtility.*;
import static org.selena.patient.client.utility.DbUtility.*;
import static org.selena.patient.client.utility.ExceptionUtility.isLocalNetworkDown;
import static org.selena.patient.client.utility.NotificationUtility.*;
import static org.selena.patient.client.utility.UserCredentialsUtility.*;

public class SyncService extends IntentService {

    private static final String TAG = SyncService.class.getName();
    public static final int SYNC_ALARM_ID = -1;

    private static ServiceClientSvcApi clientApiSvc;

    /**
     * The default constructor for this service. Simply forwards
     * construction to IntentService, passing in a name for the Thread
     * that the service runs in.
     */
    public SyncService() {
        super("SyncService Worker Thread");
    }

    /**
     * Optionally allow the instantiator to specify the name of the
     * thread this service runs in.
     */
    public SyncService(String name) {
        super(name);
    }


    public static void restartScheduledSync(Context context) {
        restartScheduledSync(context, MessageConstants.SYNC_TYPE.SYNC);
    }

    public static void restartScheduledSync(Context context, MessageConstants.SYNC_TYPE syncType) {
        restartScheduledSync(context, syncType, null);
    }

    public static void restartScheduledSync(Context context, MessageConstants.SYNC_TYPE syncType, Messenger messenger) {

        stopScheduledSync(context);

        final AlarmManager mAlarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        Intent intent= getIntentForSync(context, syncType, messenger);

        context.startService(intent);

        final PendingIntent pendingIntent = PendingIntent.getService(
                context, SYNC_ALARM_ID, getIntentForSync(context), PendingIntent.FLAG_UPDATE_CURRENT);
        //patient
        mAlarmManager.setRepeating(
                AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + MessageConstants.INITIAL_ALARM_DELAY + MessageConstants.INTERVAL_TIME,
                MessageConstants.INTERVAL_TIME,
                pendingIntent);
    }

    public static void stopScheduledSync(Context context) {
        final PendingIntent pendingSyncIntent = PendingIntent.getService(
                context, SYNC_ALARM_ID, getIntentForSync(context), PendingIntent.FLAG_NO_CREATE);
        final AlarmManager mAlarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);

        mAlarmManager.cancel(pendingSyncIntent);
    }


    public static void restartScheduledCheck(Context context) {

        SyncService.stopScheduledCheck(context);

        final Set<String> defaultSet= Sets.newHashSet(context.getResources().getStringArray(R.array.initial_timing));
        final Set<String> times = getProjectPreferences(context).getStringSet("scheduled_times", defaultSet);

        final AlarmManager mAlarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);

        int i = 0;
        for ( final String time : times) {
            String values[]= time.split(":");
            Calendar calendar= Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, Integer.valueOf(values[0]));
            calendar.set(Calendar.MINUTE, Integer.valueOf(values[1]));

            if ( calendar.getTimeInMillis() < System.currentTimeMillis() ) {
                calendar.add(Calendar.DAY_OF_YEAR, 1);
            }

            final PendingIntent pendingIntent = PendingIntent.getService(
                    context, i++, getIntentForCheckIn(context) , PendingIntent.FLAG_UPDATE_CURRENT);
            //patient
            mAlarmManager.setRepeating( AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                    DateUtils.DAY_IN_MILLIS, pendingIntent);
            // System.out.println( (calendar.getTimeInMillis() - System.currentTimeMillis())/(1000*60) + " minute");
        }

        SharedPreferences.Editor editor = getProjectPreferences(context).edit();
        editor.putInt("alarm_number", times.size());
        editor.commit();

    }

    public static void stopScheduledCheck(Context context) {

        int alarmToCancel = getProjectPreferences(context).getInt("alarm_number", 0);
        for ( int i = 0; i< alarmToCancel; i++ ) {
            final PendingIntent pendingIntent = PendingIntent.getService(
                    context, i, getIntentForCheckIn(context), PendingIntent.FLAG_NO_CREATE);
            final AlarmManager mAlarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);

            mAlarmManager.cancel(pendingIntent);
        }
    }


    public static Intent getIntentForCheckIn(Context context) {
        return new Intent(MessageConstants.CHECKIN_ACTION, null, context, SyncService.class);
    }

    public static Intent getIntentForSync( Context context) {
        return getIntentForSync(context, MessageConstants.SYNC_TYPE.SYNC, null);
    }

    public static Intent getIntentForSync(Context context, MessageConstants.SYNC_TYPE syncType, Messenger messenger) {
        Intent intent = new Intent( context, SyncService.class);
        intent.setAction(syncType.name());
        if ( messenger!=null ){
            intent.putExtra(MessageConstants.LISTENER, messenger);
        }

        return intent;
    }


    /**
     * Hook method called when a component calls startService() with
     * the proper intent.  This method serves as the Executor in the
     * Command Processor Pattern. It receives an Intent, which serves
     * as the Command, and executes some action based on that intent
     * in the context of this service.
     * <p/>
     * This method is also a Hook Method in the Template Method
     * Pattern. The Template class has an overall design goal and
     * strategy, but it allows subclasses to how some steps in the
     * strategy are implemented. For example, IntentService handles
     * the creation and lifecycle of a started service, but allows a
     * user to define what happens when an Intent is actually handled.
     */
    @Override
    protected void onHandleIntent(Intent intent) {

        if ( MessageConstants.CHECKIN_ACTION.equals(intent.getAction()) ) {
            doTheCheckIn();
        } else if (MessageConstants.LOGIN_ACTION.equals(intent.getAction())) {

            String user = intent.getStringExtra(MessageConstants.USERNAME);
            String server = intent.getStringExtra(MessageConstants.SERVER);
            String pass = intent.getStringExtra(MessageConstants.PASSWORD);
            Messenger messenger = intent.getParcelableExtra(MessageConstants.LISTENER);
            try {
                clientApiSvc = PatientSvcConnection.init(server, user, pass, this);
                IUserName userName = clientApiSvc.findByUserName(user);

                signOut(this);
                signIn(this, server, userName);

                if(userIsPatient(userName)) {
                    fillTheDoctor(this, clientApiSvc.getPatientsDoctor(userName.getId()));
                }

                MessageConstants.sendMessage(messenger, MessageConstants.LOGIN_ACTION, MessageConstants.SUCCESS, "Logged in");


            } catch (Exception ex ) {
                if (ex instanceof SecuredRestException) {
                    MessageConstants.sendMessage(messenger, MessageConstants.LOGIN_ACTION, String.valueOf(((SecuredRestException) ex).getStatus()), ex.getMessage());
                } else {
                    MessageConstants.sendMessage(messenger, MessageConstants.LOGIN_ACTION, MessageConstants.FAILED, ex.getMessage());
                }
            }

        } else if (MessageConstants.LOGIN_GCM_REG_ID_ACTION.equals(intent.getAction())) {
            String regId = GCMUtility.getRegistrationId(this);
            if ( !regId.isEmpty() ) {

                // this value will be used to inform the backend the reg id is not valid any more
                final SharedPreferences prefs = getProjectPreferences(this);
                final String regIdToRemove= prefs.getString(PROPERTY_REG_ID_TO_REMOVE, "");

                // You should send the registration ID to your server over HTTP,
                // so it can use GCM/HTTP or CCS to send messages to your app.
                // The request to your server should be authenticated if your app
                // is using accounts.
                clientApiSvc.addRegistrationId(Pair.create(regId, regIdToRemove));

                // Persist the registration ID - no need to register again.
                GCMUtility.storeRegistrationId(this, regId);
            }

        } else if (MessageConstants.DOCTOR_DETAILS_ACTION.equals(intent.getAction())) {

            final LoggedInUser user= anyValidUser(this);

            if(LoggedInUser.USER_STATUS.VALID==user.getStatus() && user.isPatient()) {
                DoctorDetails doctor= getDoctor(this, user.getDoctor());
                Messenger messenger = intent.getParcelableExtra(MessageConstants.LISTENER);
                if (messenger != null) {
                    if( doctor!=null) {
                        MessageConstants.sendMessage(messenger, MessageConstants.DOCTOR_DETAILS_ACTION,
                                MessageConstants.SUCCESS, "Doctor found", doctor);
                    } else {
                        MessageConstants.sendMessage(messenger, MessageConstants.DOCTOR_DETAILS_ACTION,
                                MessageConstants.FAILED, "Doctor not found");
                    }
                }
            }
        } else {

            final LoggedInUser user= anyValidUser(this);

            if(LoggedInUser.USER_STATUS.VALID==user.getStatus()) {
                clientApiSvc = PatientSvcConnection.init(user.getServer(), user.getToken());

                try {
                    if ( user.isPatient()) {
                        doTheDownloadSync(user.getLogin_id());
                        doTheUploadSync();
                    }
                    // TODO comment this line if you want gcm
                    if( user.isDoctor()) {
                        //check for the alert patient
                        checkForTheAlertedPatients(intent, user);
                    }

                } catch (Exception ex) {
                    doTheException(ex, user.getId());
                    return;
                }
                if ( user.isPatient()) {
                    updateSynchData(user.getId());
                }

            } else {
                notifyUser(this, new Intent(this, StartActivity.class), SYNC_ERROR_CREDENTIALS,
                        getString(R.string.missing_credentials_title), getString(R.string.missing_credentials_text),
                        getString(R.string.missing_credentials_ticker_text), true);

            }
        }
     }

    private void checkForTheAlertedPatients(Intent reqIntent, LoggedInUser doctor) {
        Collection<PatientUI> fromServer= clientApiSvc.findAlertedPatientsByDoctor(doctor.getUsername());

        Map<String, PatientUI> fromServerUserNames= new HashMap<>();
        for( PatientUI patientUI : fromServer ){
            fromServerUserNames.put(patientUI.getUserName(), patientUI);
        }

        Collection<AlertedPatientT.HOLDER> fromContentProvider= retrieveAlertedPatientsFromDb(doctor.getLogin_id(), this);


        PatientUI patientUI;
        for ( AlertedPatientT.HOLDER alreadyAlerted: fromContentProvider) {
            // already in history
            if ( null != fromServerUserNames.get(alreadyAlerted.getUserName())){
                if ( alreadyAlerted.notInAlert()){
                    updatePatientInAlert( this, alreadyAlerted.getId(), false);
                } else {
                    fromServerUserNames.remove(alreadyAlerted.getUserName());
                }
            } else {
                // no more in alert
                updatePatientInAlert( this, alreadyAlerted.getId(), true);
            }
        }

        Collection<PatientUI> newlyAdded = fromServerUserNames.values();
        if ( newlyAdded.size() > 0 ) {
            insertAlertedPatients( newlyAdded, doctor.getLogin_id() );
        }

        MessageConstants.SYNC_TYPE type= MessageConstants.SYNC_TYPE.valueOf(reqIntent.getAction());
        Messenger messenger = reqIntent.getParcelableExtra(MessageConstants.LISTENER);
        switch (type) {
            case SYNC:
                if ( !newlyAdded.isEmpty()) {
                    notifyUser(this, getIntentForPatientList(this, newlyAdded), ALERTED_PATIENTS,
                            getString(R.string.alert_patients_title), getString(R.string.alert_patients_text),
                            getString(R.string.alert_patients_ticker_text), true);
                }
                break;

            case SHOW_NEW_ALERTED:
                if ( fromServerUserNames.isEmpty()) {
                    if (messenger!=null) MessageConstants.sendMessage(messenger, type.name(), MessageConstants.SUCCESS, "Empty list");
                } else {
                    startActivity(getIntentForPatientList(this, fromServerUserNames.values()));
                }
                break;

            case SHOW_ALL_ALERTED:
                if ( fromServer.isEmpty()) {
                    if (messenger!=null) MessageConstants.sendMessage(messenger, type.name(), MessageConstants.SUCCESS, "Empty list");
                } else {
                    startActivity(getIntentForPatientList(this, fromServer));
                }
                break;
        }

    }

    public static Intent getIntentForPatientList( Context ctx,  Collection<PatientUI> patients ){
        Intent intent = new Intent();
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        Bundle bundle = new Bundle();
        if (patients.size() == 1) {
            intent.setClass(ctx, ShowPatientActivity.class);
            bundle.putParcelable(MessageConstants.PATIENT_KEY, patients.iterator().next());
        } else {
            intent.setClass(ctx, PatientListActivity.class);
            bundle.putParcelableArrayList(MessageConstants.PATIENT_LIST_KEY, Lists.newArrayList(patients));
        }
        intent.putExtras(bundle);
        return intent;
    }


    private void doTheException(final Exception ex, final long id) {

        Date date = new Date();

        ContentValues updateValues = new ContentValues();
        updateValues.put(UserCredentialsT.Cols.LAST_TIME_FAILED, formatUtcSqlDateTime(date));
        updateSingleUserCredentials(this, id, updateValues);

        if ( isLocalNetworkDown(ex) ) {
            stopScheduledSync(this);
        } else {
            Log.i(TAG, "@@@@ Server Exception. Error: " + ex.getMessage());

            notifyUser(this, new Intent(this, StartActivity.class), SYNC_ERROR_SERVER,
                    getString(R.string.synch_error_title), getString(R.string.synch_error_text),
                    getString(R.string.synch_error_ticker_text), true);
        }

    }

    private void updateSynchData(final long id) {
        Date syncDate = new Date();

        ContentValues updateValues = new ContentValues();
        updateValues.put(UserCredentialsT.Cols.LAST_TIME_SYNC, formatUtcSqlDateTime(syncDate));
        updateValues.put(UserCredentialsT.Cols.LAST_TIME_FAILED, formatZeroUtcSqlDateTime());

        updateSingleUserCredentials(this, id, updateValues);

        Log.d(TAG, "@@@@ last time sync " + formatLongDateText(syncDate));
    }

    private void doTheCheckIn(){
        Log.d(TAG, "%%%%%%%%%% ChweckIn time " + new Date());

        Intent checkInIntent = new Intent(this, CheckInActivity.class);

        checkInIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        notifyUser(this, checkInIntent, CHECK_IN_INTENT,
                getString(R.string.checkin_title), getString(R.string.checkin_text),
                getString(R.string.checkin_ticker_text), true);
    }

    private void doTheDownloadSync(final long login_id) {
        Collection<MedicationUI> medications = clientApiSvc.getPatientMedications(login_id);
        insertMedications(medications, login_id);
    }



    private void doTheUploadSync() throws ParseException {

        for (CheckIn checkIn : retrieveCheckInFromDb(this)) {
            Log.d(TAG, print(checkIn));
            long serverCheckInId = clientApiSvc.addCheckIn(checkIn.getPatientId(), checkIn);
            updateCheckInSynchronised(this, checkIn.getChkInId(), serverCheckInId);
        }

        for (CheckInT.PhotoHolder photoHolder : retrieveImageFileNameFromDb(this)) {
            clientApiSvc.setPhoto(photoHolder.getLogin_Id(), photoHolder.getServerCheckInId(),
                    new TypedFile("image/jpg", new File(photoHolder.getImageFileName())));
            updateCheckInSynchronisedImage(this, photoHolder.getId());
        }
    }

    private String print(final CheckIn checkIn) {

        return PatientSvcConnection.gson.toJson(checkIn);

    }

    // Insert all new contacts into Contacts ContentProvider
    private void insertMedications(Collection<MedicationUI> medications, final long lastLoginId) {

        // Set up a batch operation on Contacts ContentProvider
        ArrayList<ContentProviderOperation> batchOperation = new ArrayList<ContentProviderOperation>();

        batchOperation.add(ContentProviderOperation.newDelete(MedicationT.CONTENT_URI).build());


        for (final MedicationUI medication : medications) {
            // First part of operation
            Date endDate;
            batchOperation.add(ContentProviderOperation.newInsert(MedicationT.CONTENT_URI)
                    .withValue(MedicationT.Cols.NAME, medication.getName())
                    .withValue(MedicationT.Cols.LOGIN_ID, lastLoginId)
                    .withValue(MedicationT.Cols.START_DATE, formatUtcSqlDateTime(medication.getStartDate()))
                    .withValue(MedicationT.Cols.END_DATE, (endDate = medication.getEndDate()) != null ?
                            formatUtcSqlDateTime(endDate) : "")
                    .build());
        }

        try {

            // Apply all batched operations
            getContentResolver().applyBatch(PatientSchema.AUTHORITY,
                    batchOperation);

        } catch (Exception e) {
            Log.i(TAG, "Provider Exception" + e.getMessage());
        }

    }

    // Insert all new contacts into Contacts ContentProvider
    private void insertAlertedPatients(Collection<PatientUI> patients, final long lastLoginId) {

        // Set up a batch operation on Contacts ContentProvider
        ArrayList<ContentProviderOperation> batchOperation = new ArrayList<ContentProviderOperation>();

        for (final PatientUI patient : patients) {
            // First part of operation
            Date endDate;
            batchOperation.add(ContentProviderOperation.newInsert(AlertedPatientT.CONTENT_URI)
                    .withValue(AlertedPatientT.Cols.USERNAME, patient.getUserName())
                    .withValue(AlertedPatientT.Cols.LOGIN_ID, lastLoginId)
                    .build());
        }

        try {

            // Apply all batched operations
            getContentResolver().applyBatch(PatientSchema.AUTHORITY,
                    batchOperation);

        } catch (Exception e) {
            Log.i(TAG, "Provider Exception" + e.getMessage());
        }

    }

}
