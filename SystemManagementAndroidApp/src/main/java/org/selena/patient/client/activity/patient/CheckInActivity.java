package org.selena.patient.client.activity.patient;

import android.app.AlertDialog;
import android.content.ContentProviderOperation;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.RemoteException;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.*;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import org.selena.patient.client.R;
import org.selena.patient.client.activity.BaseActivity;
import org.selena.patient.client.activity.StartActivity;
import org.selena.patient.client.model.LoggedInUser;
import org.selena.patient.client.model.MedicationUsage;
import org.selena.patient.client.model.server.CheckIn;
import org.selena.patient.client.service.SyncService;
import org.selena.patient.client.utility.CallableTask;
import org.selena.patient.client.utility.DateTimeUtility;
import org.selena.patient.client.utility.TaskCallback;
import org.selena.patient.client.widget.NDSpinner;
import org.selena.provider.PatientSchema;
import org.selena.provider.tables.CheckInT;
import org.selena.provider.tables.MedicationUsageT;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.Callable;

import static org.selena.patient.client.utility.DateTimeUtility.formatMedicineDate;
import static org.selena.patient.client.utility.DateTimeUtility.formatUtcSqlDateTime;
import static org.selena.patient.client.utility.NotificationUtility.SYNC_ERROR_CREDENTIALS;
import static org.selena.patient.client.utility.NotificationUtility.notifyUser;
import static org.selena.patient.client.utility.UserCredentialsUtility.anyValidUser;

import static android.widget.LinearLayout.LayoutParams;

/**
 * Created by sklasnja on 10/17/14.
 */
public class CheckInActivity extends BaseActivity {

    private static final int PICK_MEDICATIONS_REQUEST = 0;
    private static final int CAMERA_PIC_REQUEST = 1;

    private static final String PAIN_SELECTION = "PAIN_SELECTION";
    private static final String MEDICATION_SELECTION = "MEDICATION_SELECTION";
    private static final String EATING_SELECTION = "EATING_SELECTION";
    public static final String MEDICATIONS_KEY = "MEDICATIONS_KEY";
    public static final String FILE_NAME_KEY = "FILE_NAME_KEY";

    private ArrayList<MedicationUsage> medicationsUsage;
    private String fileName= "";

    @InjectView(R.id.pain_answer)
    protected Spinner painSpinner;

//    @InjectView(R.id.yes_no_answer)
    protected NDSpinner medicationSpinner;

    @InjectView(R.id.eating_answer)
    protected Spinner eatingSpinner;

    @InjectView(R.id.medications)
    protected TextView medicationsTextView;

    @InjectView(R.id.photo)
    protected ImageView imageView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.check_in_layout);

        // TODO protect if there is no login_id
        // Notify for the last sync time

        ButterKnife.inject(this);

        medicationSpinner= (NDSpinner)findViewById(R.id.yes_no_answer);

        medicationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(final AdapterView<?> parent, final View view, final int position, final long id) {
                onMedicationUseSelected(position);
            }

            @Override
            public void onNothingSelected(final AdapterView<?> parent) {

            }
        });

        painSpinner.setAdapter(ArrayAdapter.createFromResource(
                this, R.array.pain_array, R.layout.dropdown_item));

        medicationSpinner.setAdapter(ArrayAdapter.createFromResource(
                this, R.array.yes_no_array, R.layout.dropdown_item));

        eatingSpinner.setAdapter(ArrayAdapter.createFromResource(
                this, R.array.eat_array, R.layout.dropdown_item));

        // Check for previously saved state
        if (savedInstanceState != null) {
            fileName = savedInstanceState.getString(FILE_NAME_KEY);
            painSpinner.setSelection(savedInstanceState.getInt(PAIN_SELECTION));
            medicationSpinner.setSelection(savedInstanceState.getInt(MEDICATION_SELECTION));
            eatingSpinner.setSelection(savedInstanceState.getInt(EATING_SELECTION));
            medicationsUsage =  savedInstanceState.getParcelableArrayList(MEDICATIONS_KEY);
            updateMedicationsTextView();
        }

//        long periodSync= System.currentTimeMillis() - getLastTimeSynchronised(this).getTime();
//        if (  periodSync > MAX_TIME_SYNC_INTERVAL * DateUtils.HOUR_IN_MILLIS) {
//            Toast.makeText(this,
//                    "Last time sync was " + DateTimeUtility.prettyTime(periodSync) + " .Please do the Synchronize Now.",
//                    Toast.LENGTH_LONG).show();
//
//        }
    }

    //@OnItemSelected(R.id.yes_no_answer)
    public void onMedicationUseSelected(int position) {
        if (position == 1) {
            Intent intent= new Intent(this, TakenMedicationActivity.class);
            if (medicationsUsage!=null)
                intent.putParcelableArrayListExtra(MEDICATIONS_KEY, medicationsUsage);
            startActivityForResult(intent, PICK_MEDICATIONS_REQUEST);
        } else {
            //TODO maybe it should store medication supposed to be taken
            medicationsUsage = null;
            updateMedicationsTextView();
        }
    }

    @OnClick(R.id.photo)
    public void takePhoto() {
        try {
            launchCameraIntent();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @OnClick(R.id.checkInSubmit)
    public void onCheckInSubmit() {

        final LoggedInUser user= anyValidUser(this);
        // sanity check
        if (!user.isPatient()) return;


        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
                .setTitle(getString(R.string.conformation_chekin_title))
                .setMessage(getString(R.string.conformation_chekin_text))
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(R.string.conformation_yes_text, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        doSubmit(user);
                    }
                })
                .setNegativeButton(R.string.conformation_no_text, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        final AlertDialog alert = builder.create();
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button posButton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
                Button negButton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);

                posButton.setTextSize(18);
                negButton.setTextSize(18);

                posButton.setTextColor(getResources().getColor(android.R.color.holo_blue_light));
                negButton.setTextColor(getResources().getColor(android.R.color.holo_blue_light));

                LayoutParams posParams = (LayoutParams) posButton.getLayoutParams();
                posParams.weight = 1;
                posParams.width = LayoutParams.MATCH_PARENT;
                posParams.bottomMargin = 5;

                LayoutParams negParams = (LayoutParams) negButton.getLayoutParams();
                negParams.weight = 1;
                negParams.width = LayoutParams.MATCH_PARENT;
                negParams.bottomMargin = 5;

                posButton.setLayoutParams(posParams);
                negButton.setLayoutParams(negParams);


            }
        });
        alert.show();



    }

    private void doSubmit(final LoggedInUser user) {
        if( user.getStatus()!= LoggedInUser.USER_STATUS.VALID)
            notifyUser(this, new Intent(this, StartActivity.class), SYNC_ERROR_CREDENTIALS,
                    getString(R.string.missing_credentials_title), getString(R.string.missing_credentials_text),
                    getString(R.string.missing_credentials_ticker_text), true);

        CallableTask.invoke(new Callable<Void>() {

            @Override
            public Void call() throws Exception {
                writeCheckInIntoDb(user.getLogin_id());
                return null;
            }
        }, new TaskCallback<Void>() {

            @Override
            public void success(Void result) {
                Toast.makeText(CheckInActivity.this, "Check-In Successfully Submitted!", Toast.LENGTH_LONG).show();
                finish();
            }

            @Override
            public void error(Exception e) {
                Toast.makeText(CheckInActivity.this, "Unable to insert check-in. Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        if (requestCode == PICK_MEDICATIONS_REQUEST) {
            if (resultCode == RESULT_OK) {
                // A contact was picked.  Here we will just display it
                // to the user.
                medicationsUsage =  data.getParcelableArrayListExtra(MEDICATIONS_KEY);
                updateMedicationsTextView();
            }
        } else if (requestCode == CAMERA_PIC_REQUEST) {
            if (resultCode == RESULT_OK) {
                imageView.setImageURI(Uri.parse(fileName));
            } else if (resultCode == RESULT_CANCELED) {
                fileName= "";
            } else {
                Toast.makeText(CheckInActivity.this, "Image capture failed!", Toast.LENGTH_SHORT).show();
                fileName= "";
            }
        }
    }


    private void launchCameraIntent() throws IOException {

        String state = Environment.getExternalStorageState();
        if (!Environment.MEDIA_MOUNTED.equals(state)) {
            return;
        }

        File mediaStorageDir= getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("MyCameraApp", "failed to create directory");
                return;
            }
        }
        String timeStamp = DateTimeUtility.formatFileDateText(new Date());
        File mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "IMG_" + timeStamp + ".jpg");
        fileName = mediaFile.getAbsolutePath();

        final Intent captureImageIntent= new Intent();
        captureImageIntent.setAction( MediaStore.ACTION_IMAGE_CAPTURE );
        captureImageIntent.putExtra( MediaStore.EXTRA_OUTPUT, Uri.fromFile(mediaFile));

        startActivityForResult( captureImageIntent, CAMERA_PIC_REQUEST );

    }

    private void writeCheckInIntoDb(long loginId) throws RemoteException, OperationApplicationException {

        final ArrayList<ContentProviderOperation> batchOperation = new ArrayList<ContentProviderOperation>();

        // First part of operation
        batchOperation.add(ContentProviderOperation.newInsert(CheckInT.CONTENT_URI)
                .withValue(CheckInT.Cols.LOGIN_ID, loginId)
                .withValue(CheckInT.Cols.PAIN_INTENSITY, CheckIn.PainIntensity.values()[painSpinner.getSelectedItemPosition()].name())
                .withValue(CheckInT.Cols.EATING_DISORDER, CheckIn.EatingDisorder.values()[eatingSpinner.getSelectedItemPosition()].name())
                .withValue(CheckInT.Cols.TIME, formatUtcSqlDateTime(new Date()))
                .withValue(CheckInT.Cols.IMAGE_FILE_NAME, fileName)
                .build());


        if (medicationsUsage != null) {
            for (MedicationUsage mUsage : medicationsUsage) {
                batchOperation.add(ContentProviderOperation.newInsert(MedicationUsageT.CONTENT_URI)
                        .withValueBackReference(MedicationUsageT.Cols.CHECK_IN_ID, 0)
                        .withValue(MedicationUsageT.Cols.NAME, mUsage.getName())
                        .withValue(MedicationUsageT.Cols.TIME, formatUtcSqlDateTime(mUsage.getTime()))
                        .withValue(MedicationUsageT.Cols.TAKEN, mUsage.getTaken().name())
                        .build());
            }
        }

        // Apply all batched operations
        getContentResolver().applyBatch(PatientSchema.AUTHORITY,
                batchOperation);

        SyncService.restartScheduledSync(this);

    }


    private void updateMedicationsTextView() {
        if ( medicationsUsage ==null) {
            medicationsTextView.setVisibility(View.INVISIBLE);
            medicationsTextView.setText("");
            return;
        }
        medicationsTextView.setVisibility(View.VISIBLE);
        medicationsTextView.setLines(Math.min(medicationsUsage.size(), 3));

        StringBuffer sb = new StringBuffer();
        for ( final MedicationUsage medication : medicationsUsage)
        {
            if ( medication.getTaken()== org.selena.patient.client.model.MedicationUsage.MedicineTaken.NO) continue;
            if (sb.length() != 0) sb.append(", ");
            sb.append(medication.getName() + " at " + formatMedicineDate(medication.getTime()));
        }
        medicationsTextView.setText(sb.toString());

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putString(FILE_NAME_KEY, fileName);
        savedInstanceState.putInt(PAIN_SELECTION, painSpinner.getSelectedItemPosition());
        savedInstanceState.putInt(MEDICATION_SELECTION, medicationSpinner.getSelectedItemPosition());
        savedInstanceState.putInt(EATING_SELECTION, eatingSpinner.getSelectedItemPosition());
        savedInstanceState.putParcelableArrayList(MEDICATIONS_KEY, medicationsUsage);
    }

}