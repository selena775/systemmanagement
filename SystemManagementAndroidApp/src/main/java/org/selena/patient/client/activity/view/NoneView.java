package org.selena.patient.client.activity.view;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import org.selena.patient.client.R;
import org.selena.patient.client.activity.BasicViewAndUpdatePreferencesActivity;
import org.selena.patient.client.invariable.MessageConstants;

/**
 * Created by sklasnja on 11/12/14.
 */
public class NoneView  extends AbstractView  implements UserView, View.OnCreateContextMenuListener {

    public boolean onPrepareOptionsMenu(Menu menu){
        menu.clear();
        return super.onPrepareOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public String getIntroductionText() {
        return getContext().getString(R.string.welcome_user);
    }

    public String getInfoText(){
        return getContext().getString(R.string.register_text);
    }

    @Override
    protected Class getPreferenceViewClass() {
        return  BasicViewAndUpdatePreferencesActivity.class;
    }

    protected String getViewString(){
        return MessageConstants.NONE;
    }
}
