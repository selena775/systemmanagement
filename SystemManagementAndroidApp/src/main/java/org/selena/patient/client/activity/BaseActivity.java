package org.selena.patient.client.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import org.selena.patient.client.R;
import org.selena.patient.client.invariable.MessageConstants;
import org.selena.patient.client.model.LoggedInUser;
import org.selena.patient.client.service.SyncService;

import java.lang.ref.WeakReference;

import static org.selena.patient.client.activity.view.UserViewFactory.getUserView;
import static org.selena.patient.client.invariable.MessageConstants.MAX_PLAY_SERVICE_ATTEMPT;
import static org.selena.patient.client.invariable.MessageConstants.PLAY_SERVICES_RESOLUTION_REQUEST;
import static org.selena.patient.client.utility.GCMUtility.*;
import static org.selena.patient.client.utility.UserCredentialsUtility.anyValidUser;


public abstract class BaseActivity extends Activity {

    private static final String TAG= BaseActivity.class.getName();

    private LoggedInUser user;

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        return getUserView(this, getUser(),  new ServiceListener(this)).onPrepareOptionsMenu(menu) || super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
       return  getUserView(this, getUser(),  new ServiceListener(this)).onOptionsItemSelected(item) || super.onOptionsItemSelected(item);

    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user = anyValidUser(this);
        checkForGCMRegistrationId();
    }

    public void setUser(final LoggedInUser user) {
        this.user = user;
    }

    public LoggedInUser getUser() {
        return user;
    }

    /**
     * This is the handler used for handling messages sent by a
     * Messenger.  It receives a message containing a pathname to an
     * image and displays that image in the ImageView.
     * <p/>
     * The handler plays several roles in the Active Object pattern,
     * including Proxy, Future, and Servant.
     * <p/>
     * Please use displayBitmap() defined in DownloadBase
     */
    static class ServiceListener extends Handler {

        // A weak reference to the enclosing class
        WeakReference<BaseActivity> outerClass;

        /**
         * A constructor that gets a weak reference to the enclosing class.
         * We do this to avoid memory leaks during Java Garbage Collection.
         *
         * @see for https://groups.google.com/forum/#!msg/android-developers/1aPZXZG6kWk/lIYDavGYn5UJ
         */
        public ServiceListener(BaseActivity outer) {
            outerClass = new WeakReference<BaseActivity>(outer);
        }

        // Handle any messages that get sent to this Handler
        @Override
        public void handleMessage(Message msg) {

            // Get an actual reference to the DownloadActivity
            // from the WeakReference.
            final BaseActivity activity = outerClass.get();

            // If DownloadActivity hasn't been garbage collected
            // (closed by user), display the sent image.
            if (activity != null) {
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        activity.setProgressBarIndeterminateVisibility(false);
                    }
                });
                Bundle bundle = msg.getData();
                if (MessageConstants.SUCCESS.equals(bundle.getString(MessageConstants.RESULT))) {

                    activity.onSuccess(bundle.getString(MessageConstants.ACTION), bundle.getString(MessageConstants.MESSAGE), bundle.getParcelable(MessageConstants.OBJECT));

                } else {
                    activity.onError(bundle.getString(MessageConstants.ACTION), bundle.getString(MessageConstants.MESSAGE), bundle.getInt(MessageConstants.RESULT));
                }

            }
        }
    }

    protected void retrieveUser(){
        user = anyValidUser(this);
        updateUser();
    }
    protected void updateUser(){}
    protected void updateUI(){}

    private void onError(String action, String message, int result){
        // DO the notification
        if( MessageConstants.LOGIN_ACTION.equals(action)) {
            retrieveUser();
            updateUI();
            Toast.makeText(this, getString(R.string.credentials_failed), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        }

    }

    protected void onSuccess(String action, String message, Parcelable object){

        if( MessageConstants.LOGIN_ACTION.equals(action)) {
            retrieveUser();
            updateUI();
            checkForGCMRegistrationId();
        } else if (MessageConstants.DOCTOR_DETAILS_ACTION.equals(action) ) {
            Intent intent = new Intent( this, DoctorDetailsActivity.class);
            intent.putExtra(MessageConstants.OBJECT, object);
            intent.putExtra(MessageConstants.EDITABLE, false);
            startActivity(intent);

        } else if ( MessageConstants.SYNC_TYPE.SHOW_NEW_ALERTED.name().equals(action) ||
                MessageConstants.SYNC_TYPE.SHOW_ALL_ALERTED.name().equals(action) ) {

            if ("Empty list".equals(message)) {
                Toast.makeText(this, getString(R.string.search_empty), Toast.LENGTH_LONG).show();
            }
        }
    }

    private void checkForGCMRegistrationId() {
        if ( user.isDoctor() && usesGCM(this) && getGCMPreferences(this).isEmpty() && checkPlayServices()) {

            // do the retrieving the gcm reg od in a separate process
            final Intent intent= new Intent(this, SyncService.class);
            intent.setAction(MessageConstants.LOGIN_GCM_REG_ID_ACTION);
            startService(intent);
        }
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices( ) {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {

            // increase failure attempt
            incPreferencePlayServiceInstallAttempt(this);

            // if it exceeds maximum, offer the client app settings
            if (getPreferencePlayServiceInstallAttempt(this) > MAX_PLAY_SERVICE_ATTEMPT ) {

                // Use the Builder class for convenient dialog construction
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(R.string.play_store_error)
                        .setPositiveButton(R.string.go_to_settings, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // show preference settings
                                getUserView(BaseActivity.this, getUser(), null ).showSettingsActivity();
                            }
                        })
                        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // User cancelled the dialog
                            }
                        });
                // Create and show the AlertDialog object and return it
                builder.create().show();

                resetPreferencePlayServiceInstallAttempt(this);
                return false;
            }

            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();

            } else {
                Log.i(TAG, "This device is not supported.");
            }

            return false;
        }
        return true;
    }
}
