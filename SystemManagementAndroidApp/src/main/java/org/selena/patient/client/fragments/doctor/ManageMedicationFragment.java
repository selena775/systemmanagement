package org.selena.patient.client.fragments.doctor;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import butterknife.*;
import com.google.common.collect.Lists;
import org.selena.patient.client.R;
import org.selena.patient.client.activity.doctor.AddMedicationActivity;
import org.selena.patient.client.activity.doctor.ShowPatientActivity;
import org.selena.patient.client.adapter.doctor.ManageMedicationAdapter;
import org.selena.patient.client.invariable.MessageConstants;
import org.selena.patient.client.model.MedicationUI;
import org.selena.patient.client.model.PatientUI;

import java.util.List;
import java.util.Locale;

/**
 * Created by sklasnja on 11/6/14.
 */
public class ManageMedicationFragment extends BindingFragment {

    private static final int ADD_MEDICATION = 2;
    public static final String NEW_MEDICATION = "NEW_MEDICATION";

    private final String TAG = getClass().getName();

    @InjectView(R.id.listWithFilter)
    protected ListView patientView;

    @InjectView(R.id.filter_to_list)
    protected EditText filterToList;


    private ManageMedicationAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.list_with_filter, container, false);
        ButterKnife.inject(this, view);
        filterToList.setHint( getString(R.string.medication_filter_hint));

        View footerView= inflater.inflate(R.layout.two_button_footer_view, null, false);
        patientView.addFooterView( footerView );
        patientView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        footerView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View footerView) {
                startActivityForResult(new Intent(container.getContext(), AddMedicationActivity.class), ADD_MEDICATION);

            }
        });
        footerView.findViewById(R.id.submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View footerView) {
                saveMedications();
            }
        });

        //submitButton.setEnabled(true);

        return view;
    }

//    @OnItemClick(R.id.listWithFilter)
//    public void onListItemClicked(){
//        submitButton.setEnabled(true);
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        if (requestCode == ADD_MEDICATION && resultCode== Activity.RESULT_OK) {
            MedicationUI med= data.getParcelableExtra(NEW_MEDICATION);
            if ( med!=null ) {
                mAdapter.add( med );
          //      submitButton.setEnabled(true);
            }
        }
    }

    @OnTextChanged(value = R.id.filter_to_list, callback = OnTextChanged.Callback.TEXT_CHANGED)
    void onFilterTextChanged(CharSequence text) {
        if ( mAdapter!=null ) {
            String textToFilter = text.toString().toLowerCase(Locale.getDefault());
            mAdapter.filter(textToFilter);
        }
    }

    private void saveMedications(){
        final ShowPatientActivity activity= (ShowPatientActivity)getActivity();
        if( activity!=null && getDoctorServiceRequest() != null && getDoctorServiceCallback() != null) {
            //setProgressBarVisible(true);
            try {
                Log.d(TAG, "Calling oneway DoctorServiceServiceAsync.callWebService()");

                // Invoke the oneway call, which doesn't block.
                final PatientUI patientUI = (PatientUI) getArguments().get(MessageConstants.PATIENT_KEY);
                getDoctorServiceRequest().saveMedications(patientUI.getId(), mAdapter.getList(),
                        getDoctorServiceCallback());
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onActivityCreated(final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final ShowPatientActivity activity= (ShowPatientActivity)getActivity();
        patientView.setAdapter(mAdapter= new ManageMedicationAdapter(activity));
    }

//    @Override
//    public void onResume() {
//        super.onResume();
//        final PatientUI patientUI = (PatientUI)getArguments().get(PATIENT_KEY);
//
//        mAdapter.setList(activity.getMedicationUsageList());
//
//    }

    @Override
    public void onResume() {
        super.onResume();
        final ShowPatientActivity activity= (ShowPatientActivity)getActivity();
        if( activity!=null&& activity.getMedicationList()!=null){
            updateDataModel(activity);
        }
    }

    @Override
    public void onSendMedicationList(final List<MedicationUI> results) {
        final ShowPatientActivity activity= (ShowPatientActivity)getActivity();
        if ( activity!=null ) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateDataModel(activity);
                }
            });
        }

    }

    private void updateDataModel(final ShowPatientActivity activity) {
        if ( mAdapter==null ){
            mAdapter = new ManageMedicationAdapter(activity, Lists.newArrayList(activity.getMedicationList()));
        } else if (!mAdapter.isDirtyFlag()){
            mAdapter.setList(Lists.newArrayList(activity.getMedicationList()));
        }
    }

    @Override
    public int getTabTitle() {
        return R.string.manage_medication;
    }

}