package org.selena.patient.client.widget;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TimePicker;
import org.selena.patient.client.R;

import java.util.Calendar;

/**
 * Created by sklasnja on 10/22/14.
 */
public class TimeDatePickerDialog implements DialogInterface.OnClickListener {

    public static TimeDatePickerDialog create(final Context mContext) {
        return new TimeDatePickerDialog(mContext);
    }

    private DateTimeListener listener;

    private final Context context;

    private String title;

    private int icon;

    private View view;


    public TimeDatePickerDialog(final Context context) {
        this.context = context;
    }

    public void show() {
        Context context = getContext();

        LayoutInflater inflater = LayoutInflater.from(context);
        view = inflater.inflate(R.layout.date_time, null);

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(context)
                .setTitle(getTitle())
                .setIcon(getIcon())
                .setPositiveButton(context.getString(R.string.set), this)
                .setNegativeButton(android.R.string.cancel, this)
                .setView(view);


        mBuilder.create().show();
    }

    public TimeDatePickerDialog addDateTimeListener( DateTimeListener  listener) {
        this.listener = listener;
        return  this;
    }

    @Override
    public void onClick(final DialogInterface dialog, final int which) {
        if ( which==DialogInterface.BUTTON_POSITIVE) {
            final TimePicker tp = (TimePicker) view.findViewById(R.id.timePicker);
            final DatePicker dp = (DatePicker) view.findViewById(R.id.datePicker);

            final Calendar c = Calendar.getInstance();
            c.set(Calendar.HOUR_OF_DAY, tp.getCurrentHour());
            c.set(Calendar.MINUTE, tp.getCurrentMinute());
            c.set(Calendar.SECOND, 0);
            c.set(Calendar.MILLISECOND, 0);
            c.set(Calendar.YEAR, dp.getYear());
            c.set(Calendar.MONTH, dp.getMonth());
            c.set(Calendar.DAY_OF_MONTH, dp.getDayOfMonth());
            if ( listener != null ){
                listener.onDateTimeSet(c);
            }
        } else {
            if ( listener != null ){
                listener.onCancel();
            }
        }
    }


    public Context getContext() {
        return context;
    }

    public String getTitle() {
        return title;
    }

    public int getIcon() {
        return icon;
    }

    public TimeDatePickerDialog setIcon(final int icon) {
        this.icon = icon;
        return this;
    }

    public TimeDatePickerDialog setTitle(final String title) {
        this.title = title;
        return this;
    }


    public interface DateTimeListener {
        void onDateTimeSet( Calendar calendar);
        void onCancel();

    }

}
