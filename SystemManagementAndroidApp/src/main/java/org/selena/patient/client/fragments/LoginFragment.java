package org.selena.patient.client.fragments;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Messenger;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import org.selena.patient.client.R;
import org.selena.patient.client.invariable.MessageConstants;
import org.selena.patient.client.service.SyncService;

import static org.selena.patient.client.utility.UserCredentialsUtility.getProjectPreferences;

/**
 * This application uses ButterKnife. AndroidStudio has better support for
 * ButterKnife than Eclipse, but Eclipse was used for consistency with the other
 * courses in the series. If you have trouble getting the login button to work,
 * please follow these directions to enable annotation processing for this
 * Eclipse project:
 * <p/>
 * http://jakewharton.github.io/butterknife/ide-eclipse.html
 */
public class LoginFragment extends UserFragment {


    private static final String TAG = LoginFragment.class.getName();

    @InjectView(R.id.userName)
    protected EditText userName;

    @InjectView(R.id.password)
    protected EditText password;

    @InjectView(R.id.loginButton)
    protected Button login;

    private Messenger messenger;

    private String server;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.login_fragment,
                container, false);
        ButterKnife.inject(this, view);

        messenger = getArguments().getParcelable(MessageConstants.LISTENER);

        return view;
    }


    @Override
    public void onResume() {

        SharedPreferences prefs = getProjectPreferences(getActivity());

        String prefUserName= prefs.getString(getString(R.string.uname), "");
        userName.setText(prefUserName);
        if ( !prefUserName.equals(""))
            userName.setHint("");

        server= prefs.getString(getString(R.string.userver), getString(R.string.localhost));

        super.onResume();
    }

    public void updateUI() {
        if ( getActivity()==null ) return;
        login.setText(getUser().isNone() ? getString(R.string.register) : getString(R.string.reconnect));
    }

    @OnClick(R.id.loginButton)
    public void login() {

        Activity activity=getActivity();

        if( activity!=null) {

            Intent intent= new Intent(activity, SyncService.class);
            intent.setAction(MessageConstants.LOGIN_ACTION);
            intent.putExtra(MessageConstants.LISTENER, messenger);
            intent.putExtra(MessageConstants.USERNAME, userName.getText().toString().trim());
            intent.putExtra(MessageConstants.SERVER, server);
            intent.putExtra(MessageConstants.PASSWORD, password.getText().toString());

            activity.startService(intent);
            activity.setProgressBarIndeterminateVisibility(true);
        }



    }
}
