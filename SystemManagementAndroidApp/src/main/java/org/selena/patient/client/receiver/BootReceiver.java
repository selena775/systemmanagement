package org.selena.patient.client.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import org.selena.patient.client.service.SyncService;
import org.selena.patient.client.model.LoggedInUser;

import static org.selena.patient.client.utility.UserCredentialsUtility.anyValidUser;

/**
 * Created by sklasnja on 10/27/14.
 */
public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        LoggedInUser user= anyValidUser(context);

        // TODO comment this line if you want gcm
        SyncService.restartScheduledSync(context);
        if ( user.isPatient()){
            SyncService.restartScheduledCheck(context);
        }

    }
}
