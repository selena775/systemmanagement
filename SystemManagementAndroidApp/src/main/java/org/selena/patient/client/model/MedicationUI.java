package org.selena.patient.client.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.common.base.Objects;
import org.selena.patient.client.model.server.Medication;

import java.util.Date;


public class MedicationUI implements Parcelable{


    private long id;

    private String name;

    private Date startDate;

    private Date endDate;

    public MedicationUI() {
    }

    public MedicationUI(final Medication medication) {
        setId(medication.getId());
        setName(medication.getName());
        setEndDate(medication.getEndDate());
        setStartDate(medication.getStartDate());
    }

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(final Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(final Date endDate) {
        this.endDate = endDate;
    }

    /**
     * Two Videos will generate the same hashcode if they have exactly the same
     * values for their name, url, and duration.
     *
     */
    @Override
    public int hashCode() {
        // Google Guava provides great utilities for hashing
        return Objects.hashCode(name);
    }

    /**
     * Two Videos are considered equal if they have exactly the same values for
     * their name, url, and duration.
     *
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MedicationUI) {
            MedicationUI other = (MedicationUI) obj;
            // Google Guava provides great utilities for equals too!
            return  name.equals( other.name );
        } else {
            return false;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        Date startDate = getStartDate();
        Date endDate = getEndDate();
        dest.writeLong(getId());
        dest.writeLong(startDate != null ? startDate.getTime() : 0L);
        dest.writeLong(endDate != null ? endDate.getTime() : 0L);
        dest.writeString(getName());
    }

    /**
     * Used for writing a copy of this object to a Parcel, do not manually call.
     */
    public static final Creator<MedicationUI> CREATOR = new Creator<MedicationUI>() {
        public MedicationUI createFromParcel(Parcel in) {
            return new MedicationUI(in);
        }

        public MedicationUI[] newArray(int size) {
            return new MedicationUI[size];
        }
    };

    /**
     * Used for writing a copy of this object to a Parcel, do not manually call.
     */
    private MedicationUI(Parcel in) {
        long date;
        setId(in.readLong());
        setStartDate((date = in.readLong()) == 0 ? null : new Date(date));
        setEndDate((date = in.readLong()) == 0 ? null : new Date(date));
        setName(in.readString());
    }
}
