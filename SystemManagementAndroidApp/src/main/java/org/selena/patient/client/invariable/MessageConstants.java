package org.selena.patient.client.invariable;

import android.os.*;
import android.text.format.DateUtils;

/**
 * Created by sklasnja on 11/13/14.
 */
public class MessageConstants {
    public static final String LOGIN_ACTION = "LOGIN_ACTION";
    public static final String LOGIN_GCM_REG_ID_ACTION = "LOGIN_GCM_REG_ID_ACTION";
    public static final String DOCTOR_DETAILS_ACTION = "DOCTOR_DETAILS_ACTION";
    public static final String USERNAME = "USERNAME";
    public static final String SERVER = "SERVER";
    public static final String PASSWORD = "PASSWORD";
    public static final String LISTENER = "LISTENER";
    public static final String RESULT = "Result";
    public static final String ACTION = "Action";
    public static final String MESSAGE = "Message";
    public static final String OBJECT = "Object";
    public static final String SUCCESS = "200";
    public static final String FAILED = "0";
    public static final String CHECKIN_ACTION = "CHECKIN_ACTION";
    public static final long INITIAL_ALARM_DELAY = 100L;
    public static final long INTERVAL_TIME = 15 * DateUtils.MINUTE_IN_MILLIS;
    public static final String EDITABLE = "EDITABLE";
    public static final String PATIENT = "PATIENT";
    public static final String DOCTOR = "DOCTOR";
    public static final String NONE = "NONE";
    public static final String PATIENT_LIST_KEY = "PATIENT_LIST";
    public static final String PATIENT_KEY = "PATIENT";
    public static final String EMPTY_SET = "EMPTY_SET";
    public static final String LOCAL_NETWORK_DOWN = "LOCAL_NETWORK_DOWN";
    public static final String SERVER_IS_DOWN = "SERVER_IS_DOWN";
    public static final String ERROR_KEY = "ERROR";



    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static final String PROPERTY_REG_ID = "registration_id";
    public static final String PROPERTY_REG_ID_TO_REMOVE = "registration_id_to_remove";
    public static final String PLAY_SERVICE_ATTEMPT = "PLAY_SERVICE_ATTEMPT";
    public static final int MAX_PLAY_SERVICE_ATTEMPT = 3;
    public static final String PROPERTY_APP_VERSION = "appVersion";

    /**
     * Substitute you own sender ID here. This is the project number you got
     * from the API Console, as described in "Getting Started."
     */
    public static final String SENDER_ID = "802851361642";


    public static void sendMessage(Messenger messenger, String action, String result, String message) {
        sendMessage(messenger, action, result, message, null);
    }

    public static void sendMessage(Messenger messenger, String action, String result, String message, Parcelable object) {
        if (messenger != null) {
            Message msg = Message.obtain();
            Bundle data = new Bundle();
            data.putString(RESULT, result);
            data.putString(ACTION, action);
            data.putString(MESSAGE, message);
            data.putParcelable(OBJECT, object);
            msg.setData(data);
            try {
                messenger.send(msg);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public enum SYNC_TYPE {
        SYNC, SHOW_ALL_ALERTED, SHOW_NEW_ALERTED
    }
}
