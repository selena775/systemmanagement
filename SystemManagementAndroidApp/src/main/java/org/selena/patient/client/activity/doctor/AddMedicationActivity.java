package org.selena.patient.client.activity.doctor;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import org.selena.patient.client.R;
import org.selena.patient.client.fragments.doctor.ManageMedicationFragment;
import org.selena.patient.client.model.MedicationUI;
import org.selena.patient.client.utility.DateTimeUtility;
import org.selena.patient.client.widget.TimeDatePicker;

import java.util.Date;

import static org.selena.patient.client.widget.TimeDatePicker.showPicker;

/**
 * Created by sklasnja on 11/9/14.
 */
public class AddMedicationActivity extends Activity {

    @InjectView(R.id.medicationName)
    protected EditText medicationName;

    @InjectView(R.id.start_date_text)
    protected TextView startDate;

    @InjectView(R.id.end_date_text)
    protected TextView endDate;

    private  final MedicationUI medicationUI = new MedicationUI();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_medication);

        ButterKnife.inject(this);

        Date now= new Date();
        medicationUI.setStartDate(now);
        startDate.setText(DateTimeUtility.formatDob(now));

    }

    @OnClick(R.id.start_date_text)
    public void onStartDateClicked(){

        showPicker(this, "Select Start Date", new TimeDatePicker.DateAction() {
            @Override
            public void onDatePickerSelected(Date time) {
                medicationUI.setStartDate(time);
                startDate.setText(DateTimeUtility.formatDob(time));
            }
        });

    }

    @OnClick(R.id.end_date_text)
    public void onEndDateClicked(){
        showPicker(this, "Select End Date", new TimeDatePicker.DateAction() {
            @Override
            public void onDatePickerSelected(Date time) {
                medicationUI.setEndDate(time);
                endDate.setText(DateTimeUtility.formatDob(time));
            }
        });
    }

    @OnClick(R.id.submit)
    protected void saveMedication(){
        String name= medicationName.getText().toString().trim();
        if ( name.isEmpty()){
            Toast.makeText(this,
                    getString(R.string.missing_medication_name),
                    Toast.LENGTH_LONG).show();
        } else {
            medicationUI.setName(name);
            if ( medicationUI.getStartDate()==null){
               medicationUI.setStartDate(new Date());
            }
            Intent intent = new Intent();
            intent.putExtra(ManageMedicationFragment.NEW_MEDICATION, medicationUI);
            setResult(Activity.RESULT_OK, intent);
            finish();
        }
    }
}