package org.selena.patient.client.adapter.doctor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import org.selena.patient.client.R;
import org.selena.patient.client.model.MedicationUI;
import org.selena.patient.client.widget.TimeDatePicker;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static org.selena.patient.client.utility.DateTimeUtility.formatDob;
import static org.selena.patient.client.widget.TimeDatePicker.showPicker;

public class ManageMedicationAdapter extends BaseAdapter {
    private List<MedicationUI> wholeList;
    private List<MedicationUI> list;
    private LayoutInflater inflater;
    private Context mContext;
    boolean dirtyFlag = false;


    public ManageMedicationAdapter(final Context context) {
        this(context, new ArrayList<MedicationUI>());
    }

    public ManageMedicationAdapter(Context context, final List<MedicationUI> list){
        mContext= context;
        this.list = list;
        wholeList= new ArrayList<>();
        wholeList.addAll(list);
        inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
    }

    public int getCount() { return list.size();  }

    public MedicationUI getItem(int position) { return list.get(position);  }

    public long getItemId(int position) {
        return wholeList.indexOf(list.get(position));
    }

    public List<MedicationUI> getList() {  return list; }

    public View getView(int position, View view, ViewGroup parent) {

        final MedicationUI medicationUI = list.get(position);

        ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = inflater.inflate(R.layout.medication_item, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }

        holder.nameView.setText(medicationUI.getName());
//        holder.nameView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(final View view) {
//               startActivityForResult(new Intent(container.getContext(), AddMedicationActivity.class), ADD_MEDICATION);
//                        dirtyFlag= true;
//            }
//        });

        holder.startDate.setText(formatDob(medicationUI.getStartDate()));

        holder.startDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                showPicker(view.getContext(), "Select Start Date", new TimeDatePicker.DateAction() {
                    @Override
                    public void onDatePickerSelected(Date time) {
                        medicationUI.setStartDate(time);
                        dirtyFlag= true;
                        notifyDataSetInvalidated();
                    }
                });
            }
        });

        Date endDate;
        holder.endDate.setText((endDate = medicationUI.getEndDate()) != null ? formatDob(endDate) : mContext.getString(R.string.no_end_date));

        holder.endDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                showPicker(view.getContext(), "Select End Date", new TimeDatePicker.DateAction() {
                    @Override
                    public void onDatePickerSelected(Date time) {
                        medicationUI.setEndDate(time);
                        dirtyFlag= true;
                        notifyDataSetInvalidated();
                    }
                });
            }
        });

        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                list.remove(medicationUI);
                wholeList.remove(medicationUI);
                dirtyFlag= true;
                notifyDataSetChanged();
            }
        });

        return view;
    }

    public boolean isDirtyFlag() {
        return dirtyFlag;
    }

    public void add(MedicationUI listItem) {
        list.add(listItem);
        wholeList.add(listItem);
        dirtyFlag= true;
        notifyDataSetChanged();
    }

    public void setList(final List<MedicationUI> list) {
        wholeList= new ArrayList<>();
        wholeList.addAll(list);
        this.list = list;
    }

    static class ViewHolder {

        @InjectView(R.id.name) TextView nameView;
        @InjectView(R.id.start_date) TextView startDate;
        @InjectView(R.id.end_date) TextView endDate;
        @InjectView(R.id.delete_item) ImageView remove;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }

    public void removeAllViews(){
        list.clear();
        this.notifyDataSetChanged();
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        list.clear();
        if (charText.length() == 0) {
            list.addAll(wholeList);
        }
        else
        {
            for (MedicationUI medication : wholeList)
            {
                if (medication.getName().toLowerCase(Locale.getDefault()).contains(charText) )
                {
                    list.add(medication);
                }
            }
        }
        notifyDataSetChanged();
    }

}
