package org.selena.patient.client.fragments.doctor;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import com.google.common.collect.Lists;
import org.selena.patient.client.R;
import org.selena.patient.client.activity.doctor.ShowPatientActivity;

import java.util.List;

/**
 * Created by sklasnja on 11/10/14.
 */
public class ActionBarFragment extends Fragment{

    @InjectView(R.id.first_tab)
    TextView firstTab;

    @InjectView(R.id.second_tab)
    TextView secondTab;

    @InjectView(R.id.third_tab)
    TextView thirdTab;

    @OnClick(R.id.first_tab)
    public void onFirstTabClick(){
        onTabClick(0);
    }

    @OnClick(R.id.second_tab)
    public void onSecondTabClick(){
        onTabClick(1);
    }

    @OnClick(R.id.third_tab)
    public void onThirdTabClick(){
        onTabClick(2);
    }

    private List<TextView> textViews;

    private int mSelected= -1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Retain this fragment across configuration changes.
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.action_bar_layout, container, false);
        ButterKnife.inject(this, view);
        textViews= Lists.newArrayList(firstTab, secondTab, thirdTab);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ShowPatientActivity activity = (ShowPatientActivity) getActivity();

        for( int i=0; i<textViews.size(); i++ ) {
            textViews.get(i).setText(activity.getTabFragments().get(i).getTabTitle());
        }
        onTabClick(0);
    }



    private void onTabClick(int selected) {

        if( mSelected == selected ) return;

        mSelected= selected;

        ((ShowPatientActivity)getActivity()).onTabClick(selected);

        for( int i=0; i<textViews.size(); i++ ) {
            if ( i==selected ) {
                textViews.get(i).setBackgroundColor(0x6633b5e5);
            } else {
                textViews.get(i).setBackgroundColor(0x80bebebe);
            }
        }
    }
}
