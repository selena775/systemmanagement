package org.selena.patient.client.activity.doctor;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import org.selena.patient.client.invariable.MessageConstants;


/**
 * Created by sklasnja on 11/6/14.
 */
public class ErrorNotificationActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String title = "Unable to fetch the patient list: ";
        String errorText;
        String message;
        String positiveButtonText;
        String negativeButtonText;
        final int errorCase;
        String error= getIntent().getStringExtra(MessageConstants.ERROR_KEY);
        if( error==null) return;

        if( error.equals(MessageConstants.EMPTY_SET)) {
            title= "";
            errorText = "No such patient";
            message= "Do you want to try again?";
            positiveButtonText= "Yes";
            negativeButtonText= "No";
            errorCase=-1;
        } else if (error.equals(MessageConstants.LOCAL_NETWORK_DOWN) ) {
            errorText = "Local network is down.";
            message= "Do you want to try again?";
            positiveButtonText= "Yes";
            negativeButtonText= "No";
            errorCase=0;

        } else if ( error.equals(MessageConstants.SERVER_IS_DOWN) ) {
            errorText = "Server is down.";
            message= "Do you want to try again or change the server preference?";
            positiveButtonText= "Try Again";
            negativeButtonText= "Change Preferences";
            errorCase=1;
        } else {
            errorText = "Error " + error;
            message= "Do you want to try again or sign-out and reconnect?";
            positiveButtonText= "Try Again";
            negativeButtonText= "Reconnect";
            errorCase=2;
        }


        // TODO only if it is visible
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
                .setTitle(title + errorText)
                .setMessage(message)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(positiveButtonText, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //startService(DoctorService.getIntentForPatientList(ErrorNotificationActivity.this, getIntent().getExtras()));
                    }
                })
                .setNegativeButton(negativeButtonText, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
//                        final Context ctx = ErrorNotificationActivity.this;
//                        Intent intent;
//                        switch (errorCase) {
//                            case (1):
//                                intent= new Intent(ctx, DoctorViewAndUpdatePreferencesActivity.class);
//                                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
//                                ctx.showSettingsActivity(intent);
//                                break;
//                            case (2):
//                                signOut(ctx);
//                                intent= new Intent(ctx, StartActivity.class);
//                                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
//                                ctx.showSettingsActivity(intent);
//                                break;
//                        }
                    }
                }).show();

    }
}