package org.selena.patient.client.model.server;

/**
 * A simple object to represent a patient
 * 
 * @author sklasnja
 */

public class DoctorAndPatient extends Patient implements IDoctor, IPatient, IUserName {

    private String doctorId;

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(final String doctorId) {
        this.doctorId = doctorId;
    }


//    @Override
//    public int hashCode() {
//        // Google Guava provides great utilities for hashing
//        return Objects.hashCode(getFirstName(), getLastName(), getMedicalRecord(), getDob(), getDoctor(), getDoctorId());
//    }
//
//    /**
//     * Two Videos are considered equal if they have exactly the same values for
//     * their firstName, url, and duration.
//     *
//     */
//    @Override
//    public boolean equals(Object obj) {
//        if (obj instanceof DoctorAndPatient) {
//            DoctorAndPatient other = (DoctorAndPatient) obj;
//            // Google Guava provides great utilities for equals too!
//            return Objects.equal(getFirstName(), other.getFirstName())
//                    && Objects.equal(getLastName(), other.getLastName())
//                    && Objects.equal(getMedicalRecord(), other.getMedicalRecord())
//                    && Objects.equal(getDob(), other.getDob())
//                    && Objects.equal(getDoctor(), other.getDoctor())
//                    && Objects.equal(getDoctorId(), other.getDoctorId());
//        } else {
//            return false;
//        }
//    }

    @Override
    public UserType getType() {
        return UserType.BOTH;
    }

}
