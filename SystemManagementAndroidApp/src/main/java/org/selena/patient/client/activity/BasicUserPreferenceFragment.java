package org.selena.patient.client.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import org.selena.patient.client.R;

import static org.selena.patient.client.utility.UserCredentialsUtility.PREFERENCE_FILE;
import static org.selena.patient.client.utility.UserCredentialsUtility.PREFERENCE_MODE;

/**
 * Created by sklasnja on 11/5/14.
 */
public class BasicUserPreferenceFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    protected static final String TAG = "UserPrefsFragment";
    SharedPreferences prefs;
    PreferenceManager preferenceManager;
    String uname, userver, usesGCM;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        preferenceManager = getPreferenceManager();
        preferenceManager.setSharedPreferencesName(PREFERENCE_FILE);
        preferenceManager.setSharedPreferencesMode(PREFERENCE_MODE);

        // Load the preferences from an XML resource
        addPreferencesFromResource( getPreferencesResourceId());

        // Get SharedPreferences object managed by the PreferenceManager for
        // this Fragment
        prefs = preferenceManager.getSharedPreferences();

        // Invoke callback manually to display the current username
        onSharedPreferenceChanged(prefs, usesGCM);

    }
    protected int getPreferencesResourceId(){
        return R.xml.basic_prefs;
    }


    @Override
    public void onResume() {
        super.onResume();
        // Set up a listener whenever a key changes

        boolean usesGC= preferenceManager.getSharedPreferences().getBoolean( usesGCM, true );
        final CheckBoxPreference checkBoxPreference= (CheckBoxPreference) preferenceManager.findPreference(usesGCM);
        checkBoxPreference.setChecked(usesGC);

        prefs.registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        // Set up a listener whenever a key changes
        prefs.unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(
            SharedPreferences sharedPreferences, String key) {

        uname = getString(R.string.uname);
        userver = getString(R.string.userver);
        usesGCM = getString(R.string.usesGCM);

        preferenceManager.findPreference(uname).setSummary(sharedPreferences.getString(
                uname, "None Set").trim());
        preferenceManager.findPreference(userver).setSummary(sharedPreferences.getString(
                userver, getString(R.string.localhost)).trim());

        // TODO revoke GCM reg id if it is "uses gcm" de-checked and obtain a new one if it is checked
        preferenceManager.findPreference(usesGCM).setSummary(sharedPreferences.getBoolean(
                usesGCM, true) ? "GCM system will be used." : "Synchronisation method will be used.");

    }
}
