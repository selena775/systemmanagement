package org.selena.patient.client.activity.doctor;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;
import org.selena.patient.client.R;
import org.selena.patient.client.activity.BasicUserPreferenceFragment;

public class DoctorViewAndUpdatePreferencesActivity extends Activity {

    private static final String TAG = DoctorViewAndUpdatePreferencesActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.user_prefs_fragment);

        FragmentTransaction fragmentTransaction = getFragmentManager()
                .beginTransaction();
        fragmentTransaction.add( R.id.userPreferenceFragment, new UserPreferenceFragment() {});
        fragmentTransaction.commit();

    }

    // Fragment that displays the username preference
    public static class UserPreferenceFragment extends BasicUserPreferenceFragment {
        protected int getPreferencesResourceId(){
            return R.xml.doctor_prefs;
        }
    }

}
