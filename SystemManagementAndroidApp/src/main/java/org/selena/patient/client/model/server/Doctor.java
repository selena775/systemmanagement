package org.selena.patient.client.model.server;

import com.google.common.base.Objects;

/**
 * A simple object to represent a patient
 * 
 * @author sklasnja
 */

public class Doctor extends UserName implements IDoctor, IUserName {

    private String doctorId;

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(final String doctorId) {
        this.doctorId = doctorId;
    }


    /**
	 * Two Videos will generate the same hashcode if they have exactly the same
	 * values for their firstName, url, and duration.
	 * 
	 */

	@Override
	public int hashCode() {
		// Google Guava provides great utilities for hashing
		return Objects.hashCode(getFirstName(), getLastName(), doctorId, getDob());
	}

	/**
	 * Two Videos are considered equal if they have exactly the same values for
	 * their firstName, url, and duration.
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Doctor) {
			Doctor other = (Doctor) obj;
			// Google Guava provides great utilities for equals too!
			return Objects.equal(getFirstName(), other.getFirstName())
                    && Objects.equal(getLastName(), other.getLastName())
                    && Objects.equal(getDoctorId(), other.getDoctorId())
                    && Objects.equal(getDob(), other.getDob());
		} else {
			return false;
		}
	}
    @Override
    public UserType getType() {
        return UserType.DOCTOR;
    }
}
