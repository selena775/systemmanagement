/* 
 **
 ** Copyright 2014, Jules White
 **
 ** 
 */
package org.selena.patient.client.oauth;

import android.content.Context;
import android.content.SharedPreferences;
import com.google.common.io.BaseEncoding;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.commons.io.IOUtils;

import retrofit.*;
import retrofit.RestAdapter.Log;
import retrofit.RestAdapter.LogLevel;
import retrofit.client.*;
import retrofit.client.Client.Provider;
import retrofit.converter.Converter;
import retrofit.mime.FormUrlEncodedTypedOutput;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

import static org.selena.patient.client.utility.UserCredentialsUtility.*;

/**
 * A Builder class for a Retrofit REST Adapter. Extends the default implementation by providing logic to
 * handle an OAuth 2.0 password grant login flow. The RestAdapter that it produces uses an interceptor
 * to automatically obtain a bearer token from the authorization server and insert it into all client
 * requests.
 * <p/>
 * You can use it like this:
 * <p/>
 * private VideoSvcApi videoService = new SecuredRestBuilder()
 * .setLoginEndpoint(TEST_URL + VideoSvcApi.TOKEN_PATH)
 * .setUsername(USERNAME)
 * .setPassword(PASSWORD)
 * .setClientId(CLIENT_ID)
 * .setClient(new ApacheClient(UnsafeHttpsClient.createUnsafeClient()))
 * .setEndpoint(TEST_URL).setLogLevel(LogLevel.FULL).build()
 * .create(VideoSvcApi.class);
 *
 * @author Jules, Mitchell
 */
public class SecuredRestBuilder extends RestAdapter.Builder {
    private static final String TAG = SecuredRestBuilder.class.getName();

    private class OAuthHandler implements RequestInterceptor {

        /**
         * Every time a method on the client interface is invoked, this method is
         * going to get called. The method checks if the client has previously obtained
         * an OAuth 2.0 bearer token. If not, the method obtains the bearer token by
         * sending a password grant request to the server.
         * <p/>
         * Once this method has obtained a bearer token, all future invocations will
         * automatically insert the bearer token as the "Authorization" header in
         * outgoing HTTP requests.
         */
        @Override
        public void intercept(RequestFacade request) {
            // If we're not logged in, login and store the authentication token.
            if (accessToken == null) {
                retreiveUserToken();
            }
            request.addHeader("Authorization", "Bearer " + accessToken);
        }
    }

    private String username;
    private String password;
    private String loginUrl;
    private String endPoint;
    private String clientId;
    private String clientSecret = "";
    private Client client;
    private WeakReference<Context> context;
    private String accessToken;

    public SecuredRestBuilder setLoginEndpoint(String endpoint) {
        loginUrl = endpoint;
        return this;
    }

    @Override
    public SecuredRestBuilder setEndpoint(String endpoint) {
        super.setEndpoint(endpoint);
        endPoint = endpoint;
        return this;
    }

    @Override
    public SecuredRestBuilder setEndpoint(Endpoint endpoint) {
        super.setEndpoint(endpoint);
        endPoint = endpoint.getUrl();
        return this;
    }

    @Override
    public SecuredRestBuilder setClient(Client client) {
        this.client = client;
        return (SecuredRestBuilder) super.setClient(client);
    }

    @Override
    public SecuredRestBuilder setClient(Provider clientProvider) {
        client = clientProvider.get();
        return (SecuredRestBuilder) super.setClient(clientProvider);
    }

    @Override
    public SecuredRestBuilder setErrorHandler(ErrorHandler errorHandler) {

        return (SecuredRestBuilder) super.setErrorHandler(errorHandler);
    }

    @Override
    public SecuredRestBuilder setExecutors(Executor httpExecutor,
                                           Executor callbackExecutor) {

        return (SecuredRestBuilder) super.setExecutors(httpExecutor,
                callbackExecutor);
    }

    @Override
    public SecuredRestBuilder setRequestInterceptor(
            RequestInterceptor requestInterceptor) {

        return (SecuredRestBuilder) super
                .setRequestInterceptor(requestInterceptor);
    }

    @Override
    public SecuredRestBuilder setConverter(Converter converter) {

        return (SecuredRestBuilder) super.setConverter(converter);
    }

    @Override
    public SecuredRestBuilder setProfiler(@SuppressWarnings("rawtypes") Profiler profiler) {

        return (SecuredRestBuilder) super.setProfiler(profiler);
    }

    @Override
    public SecuredRestBuilder setLog(Log log) {

        return (SecuredRestBuilder) super.setLog(log);
    }

    @Override
    public SecuredRestBuilder setLogLevel(LogLevel logLevel) {

        return (SecuredRestBuilder) super.setLogLevel(logLevel);
    }

    public SecuredRestBuilder setUsername(String username) {
        this.username = username;
        return this;
    }

    public SecuredRestBuilder setPassword(String password) {
        this.password = password;
        return this;
    }

    public SecuredRestBuilder setClientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public SecuredRestBuilder setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
        return this;
    }

    public SecuredRestBuilder setAccessToken(final String accessToken) {
        this.accessToken = accessToken;
        return this;
    }

    public SecuredRestBuilder setContext(Context activity) {
        context = new WeakReference<Context>(activity);
        return this;
    }

    @Override
    public RestAdapter build() {
        if (accessToken == null && (username == null || password == null)) {
            throw new SecuredRestException(
                    "You must specify both a username and password for a "
                            + "SecuredRestBuilder before calling the build() method.");
        }

        if (client == null) {
            client = new OkClient();
        }
        OAuthHandler hdlr = new OAuthHandler();
        setRequestInterceptor(hdlr);

        return super.build();
    }

    private SecuredRestBuilder retreiveUserToken() throws SecuredRestException {
        try {
            // This code below programmatically builds an OAuth 2.0 password
            // grant request and sends it to the server.

            // Encode the username and password into the body of the request.
            FormUrlEncodedTypedOutput to = new FormUrlEncodedTypedOutput();
            to.addField("username", username);
            to.addField("password", password);

            // Add the client ID and client secret to the body of the request.
            to.addField("client_id", clientId);
            to.addField("client_secret", clientSecret);

            // Indicate that we're using the OAuth Password Grant Flow
            // by adding grant_type=password to the body
            to.addField("grant_type", "password");

            // The password grant requires BASIC authentication of the client.
            // In order to do BASIC authentication, we need to concatenate the
            // client_id and client_secret values together with a colon and then
            // Base64 encode them. The final value is added to the request as
            // the "Authorization" header and the value is set to "Basic "
            // concatenated with the Base64 client_id:client_secret value described
            // above.
            String base64Auth = BaseEncoding.base64().encode(new String(clientId + ":" + clientSecret).getBytes());
            // Add the basic authorization header
            List<Header> headers = new ArrayList<Header>();
            headers.add(new Header("Authorization", "Basic " + base64Auth));

            // Create the actual password grant request using the data above
            Request req = new Request("POST", loginUrl, headers, to);

            // Request the password grant.
            Response resp = client.execute(req);

            // Make sure the server responded with 200 OK
            if (resp.getStatus() < 200 || resp.getStatus() > 299) {
                // If not, we probably have bad credentials
                throw new SecuredRestException("Login failure: "
                        + resp.getStatus() + " - " + resp.getReason());
            } else {
                // Extract the string body from the response
                String body = IOUtils.toString(resp.getBody().in());

                // Extract the access_token (bearer token) from the response so that we
                // can add it to future requests.
                accessToken = new Gson().fromJson(body, JsonObject.class).get("access_token").getAsString();

                saveToken();

                return this;

            }
        } catch (Exception e) {
            throw new SecuredRestException(e);
        }
    }

    public void saveToken() {
        Context activity = context.get();
        if (activity == null) return;

        SharedPreferences settings = activity.getSharedPreferences(PREFERENCE_FILE, PREFERENCE_MODE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("TOKEN", accessToken);
        editor.commit();

    }

}