package org.selena.patient.client.model;


import android.os.Parcel;
import android.os.Parcelable;
import com.google.common.base.Objects;

import java.util.Date;


public class PatientUI implements Parcelable{

    private long id;

    // Should be unique across the whole system and it is related to access the system
    private String userName;

    private String firstName;

    private String lastName;

    private Date dob;

    private String doctor;

    private String medicalRecord;

    private boolean inAlarm;

    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(final String doctor) {
        this.doctor = doctor;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(final String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public boolean isInAlarm() {
        return inAlarm;
    }

    public void setInAlarm(final boolean inAlarm) {
        this.inAlarm = inAlarm;
    }

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(final String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(final Date dob) {
        this.dob = dob;
    }
    /**
     * Two Videos will generate the same hashcode if they have exactly the same
     * values for their firstName, url, and duration.
     *
     */

    @Override
    public int hashCode() {
        // Google Guava provides great utilities for hashing
        return Objects.hashCode(getFirstName(), getLastName(), getMedicalRecord(), getDob(), getDoctor());
    }

    /**
     * Two Videos are considered equal if they have exactly the same values for
     * their firstName, url, and duration.
     *
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof PatientUI) {
            PatientUI other = (PatientUI) obj;
            // Google Guava provides great utilities for equals too!
            return Objects.equal(getFirstName(), other.getFirstName())
                    && Objects.equal(getLastName(), other.getLastName())
                    && Objects.equal(getMedicalRecord(), other.getMedicalRecord())
                    && Objects.equal(getDob(), other.getDob())
                    && Objects.equal(getDoctor(), other.getDoctor());
        } else {
            return false;
        }
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        Date dob = getDob();
        dest.writeLong(getId());
        dest.writeString(getUserName());
        dest.writeString(getFirstName());
        dest.writeString(getLastName());
        dest.writeLong(dob != null ? dob.getTime() : 0L);
        dest.writeString(getDoctor());
        dest.writeString(getMedicalRecord());
        dest.writeByte(isInAlarm() ? (byte) 1 : (byte) 0);
    }

    /**
     * Used for writing a copy of this object to a Parcel, do not manually call.
     */
    public static final Creator<PatientUI> CREATOR = new Creator<PatientUI>() {
        public PatientUI createFromParcel(Parcel in) {
            return new PatientUI(in);
        }

        public PatientUI[] newArray(int size) {
            return new PatientUI[size];
        }
    };

    /**
     * Used for writing a copy of this object to a Parcel, do not manually call.
     */
    private PatientUI(Parcel in) {
        long dob;
        setId(in.readLong());
        setUserName(in.readString());
        setFirstName(in.readString());
        setLastName(in.readString());
        setDob((dob = in.readLong()) == 0 ? null : new Date(dob));
        setDoctor(in.readString());
        setMedicalRecord(in.readString());
        setInAlarm((in.readByte()) != 0);

    }

}
