package org.selena.patient.client.activity.doctor;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.Toast;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.google.common.collect.Lists;
import org.selena.patient.client.R;
import org.selena.patient.client.activity.BaseActivity;
import org.selena.patient.client.fragments.doctor.*;
import org.selena.patient.client.model.MedicationUI;
import org.selena.patient.client.model.MedicationUsage;
import org.selena.patient.client.model.PatientUI;
import org.selena.patient.client.model.server.CheckIn;
import org.selena.patient.client.service.DoctorServiceAsync;
import org.selena.patient.client.service.DoctorServiceCallback;
import org.selena.patient.client.service.DoctorServiceRequest;

import java.util.*;

import static org.selena.patient.client.invariable.MessageConstants.PATIENT_KEY;

/**
 * Created by sklasnja on 11/5/14.
 */
public class ShowPatientActivity extends BaseActivity implements ServiceConnection {

    private final String TAG = getClass().getName();

    @InjectView(R.id.patient_info_fragment)
    protected FrameLayout InfoFragmentLayout;

    @InjectView(R.id.action_bar)
    protected FrameLayout actionFragmentLayout;

    @InjectView(R.id.tab_container)
    protected FrameLayout tab_container;


    private PatientDetailsFragment patientDetailsFragment;

    private List<BindingFragment> tabFragments= Lists.newArrayList( new CheckInHistoryFragment(),
            new ManageMedicationFragment(), new TakenMedicationFragment());

    private PatientUI patientUI;
    private ArrayList<CheckIn> checkInList;
    private ArrayList<MedicationUI> medicationList;
    private Map<Long, CheckIn> checkInMap= new HashMap<>();

    private ArrayList<MedicationUsage> medicationUsageList = new ArrayList();

    private Comparator<MedicationUsage> comparator = new Comparator<MedicationUsage>() {
        @Override
        public int compare(MedicationUsage lhs, MedicationUsage rhs) {

                long res = rhs.getTime().getTime() - lhs.getTime().getTime();
                if (res < 0) return -1;
                else if (res > 0) return 1;
                else return lhs.getName().compareTo(rhs.getName());

        }
    };



    /**
     * The AIDL Interface that we will use to make oneway calls to the
     * DoctorServiceServiceAsync Service.  This plays the role of Requestor
     * in the Broker Pattern.  If it's null then there's no connection
     * to the Service.
     */
    private DoctorServiceRequest mDoctorServiceRequest = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

        setContentView(R.layout.show_patient_layout);

        ButterKnife.inject(this);


        FragmentTransaction fragmentTransaction = getFragmentManager()
                .beginTransaction();
        fragmentTransaction.add(R.id.patient_info_fragment,patientDetailsFragment= new PatientDetailsFragment());
        fragmentTransaction.add(R.id.action_bar, new ActionBarFragment());

        fragmentTransaction.commit();

        patientUI = getIntent().getParcelableExtra(PATIENT_KEY);

        Bundle args = new Bundle();
        args.putParcelable(PATIENT_KEY, patientUI);

        patientDetailsFragment.setArguments(args);
        for ( BindingFragment fragment : tabFragments ) {
            fragment.setArguments(args);
        }


        // Check for previously saved state, TODO what if the patient is different
//        if (savedInstanceState != null) {
//            checkInList =  savedInstanceState.getParcelableArrayList(CHECK_IN_LIST);
//            checkInMap.clear();
//            for ( CheckIn ch : checkInList ){
//                checkInMap.put(ch.getChkInId(), ch);
//            }
//              medicationUsageList...
//        }




    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

//        savedInstanceState.putParcelableArrayList(CHECK_IN_LIST, checkInList);
        // medicationUsageList
    }


    /**
     * This ServiceConnection is used to receive the DoctorServiceRequest
     * proxy after binding to the DoctorServiceServiceAsync Service using
     * bindService().
     */

    /**
     * Called after the KeyGeneratorService is connected to
     * convey the result returned from onBind().
     */
    @Override
    public void onServiceConnected(ComponentName name,
                                   IBinder service) {

        mDoctorServiceRequest = DoctorServiceRequest.Stub.asInterface(service);

        BindingFragment currentFragment= (BindingFragment)getFragmentManager().findFragmentById(R.id.tab_container);

        if ( currentFragment !=  null)
        {
            currentFragment.serviceConnected(mDoctorServiceRequest, mDoctorServiceCallback);
        }

        try {

            setProgressBarIndeterminateVisibility(true);
            Log.d(TAG, "Calling oneway DoctorServiceServiceAsync.callWebService()");

            //TODO progressBar
            // Invoke the oneway call, which doesn't block.
            getDoctorServiceRequest().getPatientCheckIns(patientUI.getId(),
                    getDoctorServiceCallback());

            getDoctorServiceRequest().getPatientMedications(patientUI.getId(),
                    getDoctorServiceCallback());
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * Called if the Service crashes and is no longer
     * available.  The ServiceConnection will remain bound,
     * but the service will not respond to any requests.
     */
    @Override
    public void onServiceDisconnected(ComponentName name) {
        mDoctorServiceRequest=null;
        BindingFragment currentFragment= (BindingFragment)getFragmentManager().findFragmentById(R.id.tab_container);

        if ( currentFragment !=  null)
        {
            currentFragment.onServiceDisconnected();
        }

    }

    public DoctorServiceRequest getDoctorServiceRequest() {
        return mDoctorServiceRequest;
    }

    public DoctorServiceCallback.Stub getDoctorServiceCallback() {
        return mDoctorServiceCallback;
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (mDoctorServiceRequest == null)
            bindService(DoctorServiceAsync.makeIntent(this),
                    this,
                    BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        if( mDoctorServiceRequest!=null){
            unbindService(this);
            mDoctorServiceRequest=null;
        }
        super.onStop();
    }



    public void onTabClick(final int i) {

        FragmentTransaction fragmentTransaction = getFragmentManager()
                .beginTransaction();

        Fragment currentFragment= getFragmentManager().findFragmentById(R.id.tab_container);
        if (currentFragment != null) {
            fragmentTransaction.remove(currentFragment);

        }
        BindingFragment fragment= tabFragments.get(i);
        fragmentTransaction.add(R.id.tab_container, fragment);
        fragmentTransaction.commit();

        fragment.serviceConnected(mDoctorServiceRequest, mDoctorServiceCallback);

    }

    public Map<Long, CheckIn> getCheckInMap() {
        return checkInMap;
    }

    public List<CheckIn> getCheckInList() {
        return checkInList;
    }

    public List<MedicationUI> getMedicationList() { return medicationList;  }

    public List<MedicationUsage> getMedicationUsageList() {
        return medicationUsageList;
    }

    /**
     * The implementation of the DoctorServiceCallback AIDL
     * Interface. Should be passed to the DoctorServiceServiceAsync
     * Service using the DoctorServiceRequest.callWebService() method.
     *
     * This implementation of DoctorServiceCallback.Stub plays the role of
     * Invoker in the Broker Pattern.
     */
    DoctorServiceCallback.Stub mDoctorServiceCallback = new DoctorServiceCallback.Stub() {
        @Override
        synchronized public void sendPatientList(final List<PatientUI> results, final String reason) throws RemoteException {
            // do nothing
        }

        @Override
        synchronized public void sendCheckInList(final List<CheckIn> results, final String reason) throws RemoteException {
            ShowPatientActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                    setProgressBarIndeterminateVisibility(false);
                }
            });
            if ("OK".equals(reason)) {
                if (!results.isEmpty() && !results.equals(checkInList)) {

                    //ArrayList<MedicationUsage> tmpListTaken = new ArrayList();
                    Set<MedicationUsage> tmpListSet = new HashSet();

                    checkInList = Lists.newArrayList(results);
                    medicationUsageList.clear();
                    checkInMap.clear();
                    for (CheckIn ch : checkInList) {
                        checkInMap.put(ch.getChkInId(), ch);
                        for ( MedicationUsage mu : ch.getMedicationUsage() ) {
                            if ( mu.getTaken()== MedicationUsage.MedicineTaken.YES) {
                                medicationUsageList.add(mu);
                            } else {
                                tmpListSet.add(mu);
                            }
                        }
                    }
                    tmpListSet.removeAll(medicationUsageList);
                    Collections.sort(medicationUsageList, comparator);
                    medicationUsageList.addAll(tmpListSet);


                    for (BindingFragment fragment : tabFragments) {
                        fragment.onSendCheckInList(results);
                    }
                }
            } else {
                checkInList=null;
                Log.d(TAG, "Error " + reason);
                ShowPatientActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(ShowPatientActivity.this, "Error: " + reason, Toast.LENGTH_LONG).show();
                    }
                });
            }
        }

        @Override
        synchronized public void sendMedicationList(final List<MedicationUI> results, final String reason) throws RemoteException {
            ShowPatientActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                    setProgressBarIndeterminateVisibility(false);
                }
            });
            if ("OK".equals(reason)) {
                if (!results.isEmpty()) {
                    medicationList = Lists.newArrayList(results);

                    for (BindingFragment fragment : tabFragments) {
                        fragment.onSendMedicationList(results);
                    }
                }
            } else {
                medicationList=null;
                Log.d(TAG, "Error " + reason);
                ShowPatientActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(ShowPatientActivity.this, "Error: " + reason, Toast.LENGTH_LONG).show();
                    }
                });
            }
        }

        @Override
        synchronized public void sendCheckInPhoto(final long chkId, final String fileName, final String reason) throws RemoteException {
            ShowPatientActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                    setProgressBarIndeterminateVisibility(false);
                }
            });
            if ("OK".equals(reason)) {
                for ( BindingFragment fragment : tabFragments ) {
                    fragment.onSendCheckInPhoto(chkId, fileName);
                }
            } else {
                Log.d(TAG, "Error " + reason );
                ShowPatientActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(ShowPatientActivity.this, "Error: " + reason, Toast.LENGTH_LONG).show();
                    }
                });
            }
        }

        @Override
        synchronized public void sendSaveMedicationsResult(final String result) throws RemoteException {
            ShowPatientActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                    setProgressBarIndeterminateVisibility(false);
                }
            });
            if ("OK".equals(result)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(ShowPatientActivity.this,
                                "Medications successfully submitted",
                                Toast.LENGTH_LONG).show();
                    }
                });

                for ( BindingFragment fragment : tabFragments ) {
                    fragment.onSendSaveMedicationsResult();
                }
            } else {
                Log.d(TAG, "Error " + result );
                ShowPatientActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(ShowPatientActivity.this, "Error: " + result, Toast.LENGTH_LONG).show();
                    }
                });
            }
        }

    };

    public List<BindingFragment> getTabFragments() {
        return tabFragments;
    }
}