package org.selena.patient.client.utility;

import android.text.format.DateUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by sklasnja on 10/23/14.
 */
public class DateTimeUtility {

    private static final SimpleDateFormat utcServerDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    private static final SimpleDateFormat shortDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    private static final SimpleDateFormat longDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    private static final SimpleDateFormat fileDateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
    private static final SimpleDateFormat utcSqlLiteDateFormat = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss");

    static {
//        utcServerDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
//        utcSqlLiteDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    public static String formatShortDateText(final Date date) {
        if ( date.getTime()==0 ) return "";
        return shortDateFormat.format(date);
    }

    public static String formatLongDateText(final Date date) {
        if ( date.getTime()==0 ) return "";
        return longDateFormat.format(date);
    }

    public static String formatFileDateText(final Date date) {
        if ( date.getTime()==0 ) return "";
        return fileDateFormat.format(date);
    }

    public static String formatUtcSqlDateTime(Date date) {

        return utcSqlLiteDateFormat.format(date);
    }
    public static String formatZeroUtcSqlDateTime() {

        return utcSqlLiteDateFormat.format(new Date(0));
    }
    public static Date parseUtcSqlDateTime(String text) throws ParseException {
        return utcSqlLiteDateFormat.parse(text);
    }

    public static String formatUtcServerDateTime(Date date) {

        return utcServerDateFormat.format(date);
    }

    public static Date parseUtcServerDateTime(String text) throws ParseException {
        return utcServerDateFormat.parse(text);
    }

    final static SimpleDateFormat localYear = (SimpleDateFormat) DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault());
    final static SimpleDateFormat localTime = (SimpleDateFormat) DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);

    public static String formatDob(final Date date) {

        if ( date==null || date.getTime()==0 ) return "";
        return localYear.format(date);
    }

    public static String formatMedicineDate(final Date date) {

        if ( date==null || date.getTime()==0 ) return "";
        return localTime.format(date);
    }

    public static String prettyTime(final long time)
    {
        final long days= time / DateUtils.DAY_IN_MILLIS;
        final long hours= ( time % DateUtils.DAY_IN_MILLIS ) / DateUtils.HOUR_IN_MILLIS;
        final long minutes= ( time % DateUtils.HOUR_IN_MILLIS ) / DateUtils.MINUTE_IN_MILLIS;

        final StringBuffer sb= new StringBuffer();

        int i = append(sb, days, "day") + append(sb, hours, "hour");
        if ( i<2 ) { appendZero(sb, minutes, "minute"); }

        return sb.toString();
    }

    private static int append(final StringBuffer sb, final long num, final String english) {

        if ( num > 0 ) {
            sb.append( " " ).append( num ).append( " " ).append( english);
            if( num > 1 ) sb.append("s");
            return 1;
        }
        return 0;
    }

    private static void appendZero(final StringBuffer sb, final long num, final String english) {

        sb.append( " " ).append( num ).append( " " ).append( english);
        if( num > 1 || num==0 ) sb.append("s");

    }


}
