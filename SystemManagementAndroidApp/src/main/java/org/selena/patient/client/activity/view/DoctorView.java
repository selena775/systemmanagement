package org.selena.patient.client.activity.view;

import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import org.selena.patient.client.R;
import org.selena.patient.client.activity.DoctorDetailsActivity;
import org.selena.patient.client.activity.doctor.DoctorViewAndUpdatePreferencesActivity;
import org.selena.patient.client.invariable.MessageConstants;
import org.selena.patient.client.service.SyncService;
import org.selena.patient.client.model.DoctorDetails;
import org.selena.patient.client.model.LoggedInUser;


public class DoctorView extends AbstractView implements UserView, View.OnCreateContextMenuListener {

    public boolean onPrepareOptionsMenu(Menu menu){

        menu.clear();
        menu.add(0, MENU_NEW_ALERTED, Menu.NONE, R.string.retrieve_new_alerted_patients).setIcon(R.drawable.ic_contact_picture);
        menu.add(0, MENU_ALL_ALERTED, Menu.NONE, R.string.alerted_patients).setIcon(R.drawable.ic_contact_picture);
        menu.add(0, MY_DETAILS, Menu.NONE, R.string.doctor_details).setIcon(R.drawable.ic_contact_picture);
        return super.onPrepareOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {


            case MENU_NEW_ALERTED: {
                SyncService.restartScheduledSync(getContext(), MessageConstants.SYNC_TYPE.SHOW_NEW_ALERTED, getMessenger());
                return true;
            }
            case MENU_ALL_ALERTED: {
                SyncService.restartScheduledSync(getContext(), MessageConstants.SYNC_TYPE.SHOW_ALL_ALERTED, getMessenger());

                return true;
            }
            case MY_DETAILS: {
                DoctorDetails doctor= new DoctorDetails(getUser());
                Intent intent = new Intent( getContext(), DoctorDetailsActivity.class);
                intent.putExtra(MessageConstants.OBJECT, doctor);
                intent.putExtra(MessageConstants.EDITABLE, true);
                getContext().startActivity(intent);

                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public String getIntroductionText() {
        return getContext().getString(R.string.welcome_doctor) + " " + getUser().getFirstName() + " " + getUser().getLastName() + "!";
    }
    public String getInfoText(){
        if( getUser().getStatus()== LoggedInUser.USER_STATUS.VALID) {
            return getContext().getString(R.string.add_doctor_text);
        } else {
            return getContext().getString(R.string.reconnect_text);
        }
    }

    @Override
    protected Class getPreferenceViewClass() {
        return DoctorViewAndUpdatePreferencesActivity.class;
    }

    protected String getViewString(){
        return MessageConstants.DOCTOR;
    }
}
