package org.selena.patient.client.fragments.doctor;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnTextChanged;
import org.selena.patient.client.R;
import org.selena.patient.client.activity.doctor.ShowPatientActivity;
import org.selena.patient.client.adapter.doctor.TakenMedicationAdapter;
import org.selena.patient.client.model.MedicationUsage;
import org.selena.patient.client.model.server.CheckIn;

import java.util.Collections;
import java.util.List;
import java.util.Locale;

/**
 * Created by sklasnja on 11/6/14.
 */
public class TakenMedicationFragment extends BindingFragment {

    private final String TAG = getClass().getName();

    @InjectView(R.id.listWithFilter)
    protected ListView patientView;

    @InjectView(R.id.filter_to_list)
    protected EditText filterToList;

    private TakenMedicationAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.list_with_filter, container, false);
        ButterKnife.inject(this, view);
        filterToList.setHint( getString(R.string.medication_filter_hint));
        return view;
    }

    @OnTextChanged(value = R.id.filter_to_list, callback = OnTextChanged.Callback.TEXT_CHANGED)
    void onFilterTextChanged(CharSequence text) {
        if ( mAdapter!=null ) {
            String textToFilter = text.toString().toLowerCase(Locale.getDefault());
            mAdapter.filter(textToFilter);
        }
    }

    @Override
    public void onActivityCreated(final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final ShowPatientActivity activity= (ShowPatientActivity)getActivity();
        patientView.setAdapter(mAdapter = new TakenMedicationAdapter(activity, Collections.<MedicationUsage>emptyList()));
    }

    @Override
    public void onResume() {
        super.onResume();
        final ShowPatientActivity activity= (ShowPatientActivity)getActivity();
        if( activity==null ) return;
        if ( activity.getCheckInList()!=null){
            updateDataModel(activity);
        }
    }

    @Override
    public void onSendCheckInList(final List<CheckIn> results) {

        final ShowPatientActivity activity= (ShowPatientActivity)getActivity();
        if ( activity!=null ) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateDataModel(activity);
                }
            });
        }
    }

    private void updateDataModel(final ShowPatientActivity activity) {
        if ( mAdapter==null ){
            mAdapter = new TakenMedicationAdapter(getActivity(), activity.getMedicationUsageList() );
        } else {
            mAdapter.setList(activity.getMedicationUsageList());
        }
    }

    @Override
    public int getTabTitle() {
        return R.string.taken_medications;
    }
}