package org.selena.patient.client.widget;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TimePicker;
import org.selena.patient.client.R;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by sklasnja on 11/8/14.
 */
public class TimeDatePicker {
    public static void showPicker(final Context context, String title, final Action action) {

        final LinearLayout linLayout = new LinearLayout(context);
        // specifying vertical orientation
        linLayout.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams lpView = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        if ( action instanceof TimeAction ) {
            TimePicker tp = new TimePicker(context);
            tp.setLayoutParams(lpView);
            tp.setId(R.id.timePicker);
            linLayout.addView(tp);
        } else {
            DatePicker dp = new DatePicker(context);
            dp.setLayoutParams(lpView);
            dp.setId(R.id.datePicker);
            linLayout.addView(dp);
        }


        new AlertDialog.Builder(context)
                .setTitle(title)
//                .setIcon(getIcon())
                .setPositiveButton(context.getString(R.string.set), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, final int which) {
                        if ( action instanceof TimeAction ) {
                            TimePicker tp = (TimePicker) linLayout.findViewById(R.id.timePicker);
                            String time = String.format("%02d:%02d", tp.getCurrentHour(), tp.getCurrentMinute());
                            ((TimeAction)action).onTimePickerSelected(time);
                        } else {
                            DatePicker dp = (DatePicker) linLayout.findViewById(R.id.datePicker);
                            final Calendar c = Calendar.getInstance();
                            c.set(Calendar.HOUR_OF_DAY,0);
                            c.set(Calendar.MINUTE, 0);
                            c.set(Calendar.SECOND, 0);
                            c.set(Calendar.MILLISECOND, 0);
                            c.set(Calendar.YEAR, dp.getYear());
                            c.set(Calendar.MONTH, dp.getMonth());
                            c.set(Calendar.DAY_OF_MONTH, dp.getDayOfMonth());
                            ((DateAction)action).onDatePickerSelected(c.getTime());
                        }
                    }
                })
                .setNegativeButton(android.R.string.cancel, null)
                .setView(linLayout)
                .show();
    }

    public interface Action{
    }

    public interface TimeAction extends Action{
        void onTimePickerSelected( String time );
    }

    public interface DateAction extends Action {
        void onDatePickerSelected( Date time );
    }
}
