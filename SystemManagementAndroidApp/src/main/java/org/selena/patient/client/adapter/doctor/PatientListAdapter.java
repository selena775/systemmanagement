package org.selena.patient.client.adapter.doctor;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import org.selena.patient.client.R;
import org.selena.patient.client.activity.doctor.ShowPatientActivity;
import org.selena.patient.client.model.PatientUI;
import org.selena.patient.client.utility.DateTimeUtility;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.selena.patient.client.invariable.MessageConstants.PATIENT_KEY;

public class PatientListAdapter extends BaseAdapter {

    private List<PatientUI> wholeList;
    private List<PatientUI> list;
    private static LayoutInflater inflater = null;
    private Context mContext;

    public PatientListAdapter(Context context, final List<PatientUI> list ){
        mContext = context;
        this.list = list;
        wholeList= new ArrayList<>();
        wholeList.addAll(list);
        inflater = LayoutInflater.from(mContext);
    }

    public int getCount() {
        return list.size();
    }

    public PatientUI getItem(int position) {
        return list.get(position);
    }

    public long getItemId(int position) {
        return wholeList.indexOf(list.get(position));
    }

    public View getView(int position, View view, ViewGroup parent) {

        final PatientUI patient = list.get(position);

        ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = inflater.inflate(R.layout.patient_item, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }


        holder.firstName.setText(patient.getFirstName());
        holder.lastName.setText(patient.getLastName());
        holder.patientDob.setText(DateTimeUtility.formatDob(patient.getDob()));
        holder.medRecord.setText(patient.getMedicalRecord());
        if( patient.isInAlarm()) {
            holder.alarmIcon.setImageResource(R.drawable.alert);
        } else {
            holder.alarmIcon.setImageURI(null);
        }

        view.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                startPatientViewActivity(v.getContext(), patient);
            }
        });

        return view;
    }

    public static void startPatientViewActivity(final Context ctx,  final PatientUI patientUI) {

        Intent intent = new Intent(ctx, ShowPatientActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Bundle bundle = new Bundle();
        bundle.putParcelable(PATIENT_KEY, patientUI);
        intent.putExtras(bundle);
        ctx.startActivity(intent);
    }

    static class ViewHolder {

        @InjectView(R.id.photo_icon) ImageView patientPhoto;
        @InjectView(R.id.patient_first_name) TextView firstName;
        @InjectView(R.id.patient_last_name) TextView lastName;
        @InjectView(R.id.patient_dob) TextView patientDob;
        @InjectView(R.id.med_record) TextView medRecord;
        @InjectView(R.id.alarm_icon) ImageView alarmIcon;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }

    public List<PatientUI> getList(){
        return list;
    }

    public void removeAllViews(){
        list.clear();
        this.notifyDataSetChanged();
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        list.clear();
        if (charText.length() == 0) {
            list.addAll(wholeList);
        }
        else
        {
            for (PatientUI patient : wholeList)
            {
                if (patient.getFirstName().toLowerCase(Locale.getDefault()).contains(charText) ||
                    patient.getLastName().toLowerCase(Locale.getDefault()).contains(charText) )
                {
                    list.add(patient);
                }
            }
        }
        notifyDataSetChanged();
    }
}
