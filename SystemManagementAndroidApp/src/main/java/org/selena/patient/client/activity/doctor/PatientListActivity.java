package org.selena.patient.client.activity.doctor;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.ListView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnTextChanged;
import org.selena.patient.client.R;
import org.selena.patient.client.activity.BaseActivity;
import org.selena.patient.client.adapter.doctor.PatientListAdapter;

import java.util.ArrayList;
import java.util.Locale;

import static org.selena.patient.client.invariable.MessageConstants.PATIENT_LIST_KEY;

public class PatientListActivity extends BaseActivity {

    private static final String TAG = PatientListActivity.class.getName();


    @InjectView(R.id.listWithFilter)
	protected ListView patientView;

    @InjectView(R.id.filter_to_list)
    protected EditText filterToList;

    private PatientListAdapter mAdapter;

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.list_with_filter);
		ButterKnife.inject(this);
        filterToList.setHint( getString(R.string.patient_filter_hint));
	}

    @OnTextChanged(value = R.id.filter_to_list, callback = OnTextChanged.Callback.TEXT_CHANGED)
    void onFilterTextChanged(CharSequence text) {
        if ( mAdapter!=null ) {
            String textToFilter = text.toString().toLowerCase(Locale.getDefault());
            mAdapter.filter(textToFilter);
        }
    }

	@Override
	protected void onResume() {
		super.onResume();

		refreshPatients();
	}

    private void refreshPatients() {

        ArrayList patientList= getIntent().getParcelableArrayListExtra(PATIENT_LIST_KEY);
        if ( patientList!= null ) {
            mAdapter = new PatientListAdapter(getApplicationContext(), patientList);
            patientView.setAdapter(mAdapter);
        }
	}

}
