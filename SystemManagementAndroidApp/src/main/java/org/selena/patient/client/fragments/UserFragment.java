package org.selena.patient.client.fragments;

import android.app.Fragment;
import android.os.Bundle;
import org.selena.patient.client.model.LoggedInUser;

public class UserFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Retain this fragment across configuration changes.
        setRetainInstance(true);
    }

    private LoggedInUser user;

    public LoggedInUser getUser() {
        return user;
    }

    public void setUser(final LoggedInUser user) {
        this.user = user;
    }

    public void updateUI(){};

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }

}
