package org.selena.patient.client.model;


import android.os.Parcel;
import android.os.Parcelable;
import com.google.common.base.Objects;

import java.util.Date;


public class MedicationUsage implements Parcelable{

    private long id;

    private long clientId;

    private String name;

    private MedicineTaken taken = MedicineTaken.NO;

    private Date time = new Date(0);

    public MedicationUsage() {
    }

    public MedicationUsage(final String name, final Date time, final MedicineTaken taken) {
        this.name = name;
        this.time = time;
        this.taken = taken;
    }
    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }


    public long getClientId() {
        return clientId;
    }

    public void setClientId(final long clientId) {
        this.clientId = clientId;
    }


    public MedicineTaken getTaken() {
        return taken;
    }

    public void setTaken(final MedicineTaken taken) {
        this.taken = taken;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(final Date time) {
        this.time = time;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        dest.writeLong(getId());
        dest.writeLong(getClientId());
        dest.writeString(getName());
        dest.writeInt(getTaken().ordinal());
        dest.writeLong((time = getTime()) != null ? time.getTime() : 0 ) ;
    }

    /**
     * Used for writing a copy of this object to a Parcel, do not manually call.
     */
    public static final Parcelable.Creator<MedicationUsage> CREATOR = new Parcelable.Creator<MedicationUsage>() {
        public MedicationUsage createFromParcel(Parcel in) {
            return new MedicationUsage(in);
        }

        public MedicationUsage[] newArray(int size) {
            return new MedicationUsage[size];
        }
    };

    /**
     * Used for writing a copy of this object to a Parcel, do not manually call.
     */
    private MedicationUsage(Parcel in) {
        setId(in.readLong());
        setClientId(in.readLong());
        setName(in.readString());
        setTaken( MedicineTaken.isTaken( in.readInt()));
        setTime(new Date(in.readLong()));

    }

    /**
     * Two Videos will generate the same hashcode if they have exactly the same
     * values for their name, url, and duration.
     *
     */
    @Override
    public int hashCode() {
        // Google Guava provides great utilities for hashing
        return Objects.hashCode(name, clientId);
    }

    /**
     * Two Videos are considered equal if they have exactly the same values for
     * their name, url, and duration.
     *
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MedicationUsage) {
            MedicationUsage other = (MedicationUsage) obj;
            // Google Guava provides great utilities for equals too!
            return name.equals(other.name) &&
                    clientId==other.clientId;
        } else {
            return false;
        }
    }
    public static enum MedicineTaken {
        NO, YES;

        public static MedicineTaken isTaken(int taken) {
            return taken==0 ? MedicineTaken.NO : MedicineTaken.YES;
        }

        public static MedicineTaken isTaken(boolean taken) {
            return !taken ? MedicineTaken.NO : MedicineTaken.YES;
        }
    }
}
