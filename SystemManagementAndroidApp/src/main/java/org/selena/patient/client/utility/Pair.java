/*
 * Copyright (c) 2015.
 * Copyright from gradle @copyright@
 * Version @version@
 */

package org.selena.patient.client.utility;

import com.google.common.base.Objects;

public class Pair<L,R> {

    private final L left;
    private final R right;

    public static <K, V> Pair<K, V> create(K left, V right) {
        return new Pair<K, V>(left, right);
    }

    public Pair(L left, R right) {
        this.left = left;
        this.right = right;
    }

    public L getLeft() { return left; }
    public R getRight() { return right; }


    @Override
    public int hashCode() {
        // Google Guava provides great utilities for hashing
        return Objects.hashCode(left, right);
    }


    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Pair) {
            Pair other = (Pair) obj;
            // Google Guava provides great utilities for equals too!
            return left.equals(other.left) &&
                    right==other.right;
        } else {
            return false;
        }
    }

}