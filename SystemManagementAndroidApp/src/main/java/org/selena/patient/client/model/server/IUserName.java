package org.selena.patient.client.model.server;

import java.util.Date;


public interface IUserName {

    public long getId();

    public void setId(final long id);

    public String getUserName();

    public void setUserName(final String userName);

    public String getFirstName();

    public void setFirstName(final String firstName);

    public String getLastName();

    public void setLastName(final String lastName);

    public Date getDob();

    public void setDob(final Date dob);

    public UserType getType();

    public enum UserType {
        DOCTOR, PATIENT, BOTH
    }
}
