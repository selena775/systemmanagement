package org.selena.patient.client.model.server;


import android.os.Parcel;
import android.os.Parcelable;
import com.google.common.base.Objects;
import org.selena.patient.client.model.MedicationUsage;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

public class CheckIn implements Parcelable {


    private long chkInId;

    private long patientId;

    private Date timestamp;

    private PainIntensity painIntensity;

    private EatingDisorder eatingDisorder;

    private Collection<MedicationUsage> medicationUsage = new HashSet<MedicationUsage>();

    private String photoFileName;

    public CheckIn(final long id, final long patientId, final Date timestamp, final PainIntensity painIntensity, final EatingDisorder eatingDisorder) {
        this.chkInId = id;
        this.patientId = patientId;
        this.timestamp = timestamp;
        this.painIntensity = painIntensity;
        this.eatingDisorder = eatingDisorder;
    }

    public long getChkInId() {
        return chkInId;
    }

    public void setChkInId(final long chkInId) {
        this.chkInId = chkInId;
    }

    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(final long patientId) {
        this.patientId = patientId;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(final Date timestamp) {
        this.timestamp = timestamp;
    }

    public PainIntensity getPainIntensity() {
        return painIntensity;
    }

    public void setPainIntensity(final PainIntensity painIntensity) {
        this.painIntensity = painIntensity;
    }

    public EatingDisorder getEatingDisorder() {
        return eatingDisorder;
    }

    public void setEatingDisorder(final EatingDisorder eatingDisorder) {
        this.eatingDisorder = eatingDisorder;
    }

    public Collection<MedicationUsage> getMedicationUsage() {
        return medicationUsage;
    }

    public void setMedicationUsage(final Collection<MedicationUsage> medicationUsage) {
        this.medicationUsage = medicationUsage;
    }

    public String getPhotoFileName() {
        return photoFileName;
    }

    public void setPhotoFileName(final String photoFileName) {
        this.photoFileName = photoFileName;
    }

    /**
     * Two Videos will generate the same hashcode if they have exactly the same
     * values for their name, url, and duration.
     *
     */
    @Override
    public int hashCode() {
        // Google Guava provides great utilities for hashing
        return Objects.hashCode(patientId, timestamp);
    }

    /**
     * Two Videos are considered equal if they have exactly the same values for
     * their name, url, and duration.
     *
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof CheckIn) {
            CheckIn other = (CheckIn) obj;
            // Google Guava provides great utilities for equals too!
            return     patientId == other.patientId
                    && timestamp.equals( other.timestamp );
        } else {
            return false;
        }
    }


    public static enum EatingDisorder {
        NO, SOME, CANNOT_EAT
    }

    public static enum PainIntensity {
        WELL_CONTROLLED, MODERATE, SEVERE
    }



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        Date date = timestamp;
        dest.writeLong(chkInId);
        dest.writeLong(patientId);
        dest.writeLong(date != null ? date.getTime() : 0L);
        dest.writeByte((byte)painIntensity.ordinal());
        dest.writeByte((byte)eatingDisorder.ordinal());
        dest.writeInt(medicationUsage.size());
        for ( MedicationUsage mu : medicationUsage ){
            date = mu.getTime();
            dest.writeLong( mu.getClientId() );
            dest.writeString(mu.getName());
            dest.writeByte((byte) mu.getTaken().ordinal());
            dest.writeLong(date != null ? date.getTime() : 0L);
        }
      }

    /**
     * Used for writing a copy of this object to a Parcel, do not manually call.
     */
    public static final Creator<CheckIn> CREATOR = new Creator<CheckIn>() {
        public CheckIn createFromParcel(Parcel in) {
            return new CheckIn(in);
        }

        public CheckIn[] newArray(int size) {
            return new CheckIn[size];
        }
    };

    /**
     * Used for writing a copy of this object to a Parcel, do not manually call.
     */
    private CheckIn(Parcel in) {
        long date;
        setChkInId(in.readLong());
        setPatientId(in.readLong());
        setTimestamp((date = in.readLong()) == 0 ? null : new Date(date));
        setPainIntensity( PainIntensity.values()[in.readByte()]);
        setEatingDisorder( EatingDisorder.values()[in.readByte()]);
        int num = in.readInt();
        medicationUsage = new HashSet<MedicationUsage>(num);
        for ( int i=0;i < num; i++ ){
            MedicationUsage mu= new MedicationUsage();
            mu.setClientId(in.readLong());
            mu.setName(in.readString());
            mu.setTaken(MedicationUsage.MedicineTaken.values()[in.readByte()]);
            mu.setTime((date = in.readLong()) == 0 ? null : new Date(date));
            medicationUsage.add(mu);
        }
    }

}
