package org.selena.patient.client.activity.view;

import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import com.google.common.collect.Sets;
import org.selena.patient.client.R;
import org.selena.patient.client.activity.patient.PatientDetailsActivity;
import org.selena.patient.client.activity.patient.PatientViewAndUpdatePreferencesActivity;
import org.selena.patient.client.invariable.MessageConstants;
import org.selena.patient.client.service.SyncService;
import org.selena.patient.client.utility.DateTimeUtility;
import org.selena.patient.client.model.LoggedInUser;

import java.util.Calendar;
import java.util.Set;

import static org.selena.patient.client.utility.UserCredentialsUtility.getProjectPreferences;


public class PatientView  extends AbstractView implements UserView, View.OnCreateContextMenuListener {


    public boolean onPrepareOptionsMenu(Menu menu){

        menu.clear();
        menu.add(0, MENU_SYNC, Menu.NONE, R.string.synchronize).setIcon(R.drawable.ic_contact_picture);
        menu.add(0, DOCTOR_DETAILS, Menu.NONE, R.string.doctor_details).setIcon(R.drawable.ic_contact_picture);
        return super.onPrepareOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case MENU_SYNC: {
                SyncService.restartScheduledSync(getContext());
                return true;
            }
            case DOCTOR_DETAILS: {
                Intent intent = new Intent( getContext(), SyncService.class);
                intent.putExtra(MessageConstants.LISTENER, getMessenger());
                intent.setAction(MessageConstants.DOCTOR_DETAILS_ACTION);
                getContext().startService(intent);
                return true;
            }
            case MY_DETAILS: {
                Intent intent = new Intent( getContext(), PatientDetailsActivity.class);
                intent.putExtra(MessageConstants.OBJECT, getUser());
                getContext().startActivity(intent);
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public String getIntroductionText() {
        return getContext().getString(R.string.welcome_patient) + " " + getUser().getFirstName() + " " + getUser().getLastName() + "!";
    }
    public String getInfoText(){
        if( getUser().getStatus()== LoggedInUser.USER_STATUS.VALID) {

            final Set<String> defaultSet = Sets.newHashSet(getContext().getResources().getStringArray(R.array.initial_timing));
            final Set<String> times = getProjectPreferences(getContext()).getStringSet("scheduled_times", defaultSet);

            long minTime = Long.MAX_VALUE;

            for (String time : times) {
                String values[] = time.split(":");
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY, Integer.valueOf(values[0]));
                calendar.set(Calendar.MINUTE, Integer.valueOf(values[1]));

                if ( calendar.getTimeInMillis() < System.currentTimeMillis() ) {
                    calendar.add(Calendar.DAY_OF_YEAR, 1);
                }

                long timeDiff = calendar.getTimeInMillis() - System.currentTimeMillis();

                if (timeDiff > 0 && timeDiff < minTime) {
                    minTime = timeDiff;
                }
            }

            return getContext().getString(R.string.next_chekin_text) + DateTimeUtility.prettyTime(minTime) +
                    getContext().getString(R.string.add_chekin_text);
        } else {
            return getContext().getString(R.string.reconnect_text);
        }

    }

    @Override
    protected Class getPreferenceViewClass() {
        return PatientViewAndUpdatePreferencesActivity.class;
    }

    protected String getViewString(){
        return MessageConstants.PATIENT;
    }
}
