package org.selena.patient.client.fragments.doctor;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import org.selena.patient.client.R;
import org.selena.patient.client.activity.doctor.DoctorViewAndUpdatePreferencesActivity;
import org.selena.patient.client.model.LoggedInUser;

public class DoctorActionFragment extends BindingFragment{

    private final String TAG = getClass().getName();

    @InjectView(R.id.actionListPatients)
    protected Button pListButton;

    @InjectView(R.id.searchPatient)
    protected Button pSearchButton;

    @InjectView(R.id.patient_filter)
    protected EditText pattern;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.doctor_action_fragment, container, false);
        ButterKnife.inject(this, view);
        setRetainInstance(true);
        return view;
    }

    @OnClick(R.id.pref_button)
    public void onPreferencesClick(View v) {
        startActivity(new Intent(getActivity(), DoctorViewAndUpdatePreferencesActivity.class));
    }

    @OnClick(R.id.searchPatient)
    public void onPatientSearchList(View v) {
        listPatients( pattern.getText().toString() );

    }

    @OnClick(R.id.actionListPatients)
    public void onPatientList(View v) {
        listPatients( null);
    }



    private void listPatients( final String pattern) {


        if (getDoctorServiceRequest() != null && getDoctorServiceCallback() != null) {
            Activity activity= getActivity();
            if( activity!=null)
                activity.setProgressBarIndeterminateVisibility(true);

            try {
                Log.d(TAG, "Calling oneway DoctorServiceServiceAsync.callWebService()");

                // Invoke the oneway call, which doesn't block.
                getDoctorServiceRequest().getPatientList(pattern,
                        getDoctorServiceCallback());
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void updateUI() {
        if ( getActivity()==null ) return;
        pListButton.setEnabled(getUser().getStatus()== LoggedInUser.USER_STATUS.VALID);
        pSearchButton.setEnabled(getUser().getStatus()== LoggedInUser.USER_STATUS.VALID);
    }

    @Override
    protected void onDisconnect() {
        pListButton.setEnabled(false);
        pSearchButton.setEnabled(false);
    }

    @Override
    public int getTabTitle() {
        return R.string.patient_list;
    }


}
