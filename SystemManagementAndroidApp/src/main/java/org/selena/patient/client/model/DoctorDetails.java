package org.selena.patient.client.model;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import org.selena.provider.tables.DoctorDetailsT;


public class DoctorDetails implements Parcelable {

    private long id;
    private String username;
    private String doctorId;
    private String firstName;
    private String lastName;

    public static DoctorDetails fromCursor( Cursor cursor) {

        DoctorDetails doctor= new DoctorDetails();
        doctor.setId(cursor.getInt(cursor.getColumnIndex(DoctorDetailsT.Cols.ID)));
        doctor.setUsername(cursor.getString(cursor.getColumnIndex(DoctorDetailsT.Cols.USERNAME)));
        doctor.setDoctorId(cursor.getString(cursor.getColumnIndex(DoctorDetailsT.Cols.DOCTOR_ID)));
        doctor.setFirstName(cursor.getString(cursor.getColumnIndex(DoctorDetailsT.Cols.FIRST_NAME)));
        doctor.setLastName(cursor.getString(cursor.getColumnIndex(DoctorDetailsT.Cols.LAST_NAME)));

        return doctor;
    }

    public DoctorDetails() {
    }
    public DoctorDetails(LoggedInUser doctor) {
        setUsername(doctor.getUsername());
        setFirstName(doctor.getFirstName());
        setLastName(doctor.getLastName());
        setDoctorId(doctor.getLicenceNumber());
    }



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {

        dest.writeString(getUsername());
        dest.writeLong(getId());
        dest.writeString(getFirstName());
        dest.writeString(getLastName());
        dest.writeString(getDoctorId());

    }

    /**
     * Used for writing a copy of this object to a Parcel, do not manually call.
     */
    public static final Creator<DoctorDetails> CREATOR = new Creator<DoctorDetails>() {
        public DoctorDetails createFromParcel(Parcel in) {
            return new DoctorDetails(in);
        }

        public DoctorDetails[] newArray(int size) {
            return new DoctorDetails[size];
        }
    };

    /**
     * Used for writing a copy of this object to a Parcel, do not manually call.
     */
    private DoctorDetails(Parcel in) {

        setUsername(in.readString());
        setId(in.readLong());
        setFirstName(in.readString());
        setLastName(in.readString());
        setDoctorId(in.readString());

    }

}
