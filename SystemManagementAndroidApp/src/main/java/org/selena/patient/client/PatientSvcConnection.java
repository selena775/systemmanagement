/* 
 **
 ** Copyright 2014, Jules White
 **
 ** 
 */
package org.selena.patient.client;

import android.content.Context;
import com.google.gson.Gson;
import org.selena.patient.client.model.server.MyGsonBuilder;
import org.selena.patient.client.oauth.SecuredRestBuilder;
import org.selena.patient.client.unsafe.EasyHttpClient;
import retrofit.RestAdapter.LogLevel;
import retrofit.client.ApacheClient;
import retrofit.converter.GsonConverter;

public class PatientSvcConnection {

    private static final String TAG = PatientSvcConnection.class.getName();

    public static final String CLIENT_ID = "mobile";


    public static ServiceClientSvcApi init(String server, String user,
                                                  String pass, Context ctx) {

       return new SecuredRestBuilder()
                .setLoginEndpoint(server + ServiceClientSvcApi.TOKEN_PATH)
                .setUsername(user)
                .setPassword(pass)
                .setClientId(CLIENT_ID)
                .setClient(new ApacheClient(new EasyHttpClient()))
                .setEndpoint(server).setLogLevel(LogLevel.FULL)
                .setContext(ctx)
                .setConverter(new GsonConverter(gson))
                .build()
                .create(ServiceClientSvcApi.class);

    }

    public static ServiceClientSvcApi init(String server, String token) {

        return new SecuredRestBuilder()
                .setLoginEndpoint(server + ServiceClientSvcApi.TOKEN_PATH)
                .setClient(new ApacheClient(new EasyHttpClient()))
                .setEndpoint(server).setLogLevel(LogLevel.FULL)
                .setAccessToken(token)
                .setConverter(new GsonConverter(gson))
                .build()
                .create(ServiceClientSvcApi.class);

    }

    public static Gson gson = new MyGsonBuilder().create();

}
