package org.selena.patient.client.model;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import org.selena.patient.client.model.server.IUserName;
import org.selena.provider.tables.UserCredentialsT;

import java.text.ParseException;
import java.util.Date;

import static org.selena.patient.client.model.server.IUserName.UserType.BOTH;
import static org.selena.patient.client.model.server.IUserName.UserType.DOCTOR;
import static org.selena.patient.client.model.server.IUserName.UserType.PATIENT;
import static org.selena.patient.client.utility.DateTimeUtility.parseUtcSqlDateTime;

public class LoggedInUser implements Parcelable {
    private USER_STATUS status;
    private String username;
    private String doctor;
    private String medicalRecord;
    private String licenceNumber;
    private long id;
    private long login_id;
    private IUserName.UserType userType;
    private String server;
    private String token;
    private String firstName;
    private String lastName;
    private Date dob;

//        public LoggedInUser(final USER_STATUS status, final String userName, final String doctor, String medicalRecord, final String server, final String token,
//                            final int id, final int login_id, final String type) {
//            this.status = status;
//            this.username = userName;
//            userType= IUserName.UserType.valueOf(type);
//            this.id = id;
//            this.login_id = login_id;
//            this.server= server;
//            this.doctor= doctor;
//            this.token = token;
//            this.medicalRecord = medicalRecord;
//        }


    public static LoggedInUser fromCursor( Cursor cursor) {

        LoggedInUser user= new LoggedInUser();
        user.setId( cursor.getInt(cursor.getColumnIndex(UserCredentialsT.Cols.ID)));
        user.setLogin_id(cursor.getInt(cursor.getColumnIndex(UserCredentialsT.Cols.LOGIN_ID)));
        user.setUsername(cursor.getString(cursor.getColumnIndex(UserCredentialsT.Cols.USERNAME)));
        user.setDoctor(cursor.getString(cursor.getColumnIndex(UserCredentialsT.Cols.DOCTOR)));
        user.setMedicalRecord(cursor.getString(cursor.getColumnIndex(UserCredentialsT.Cols.MEDICAL_RECORD)));
        user.setLicenceNumber(cursor.getString(cursor.getColumnIndex(UserCredentialsT.Cols.DOCTOR_ID)));
        user.setFirstName(cursor.getString(cursor.getColumnIndex(UserCredentialsT.Cols.FIRST_NAME)));
        user.setLastName(cursor.getString(cursor.getColumnIndex(UserCredentialsT.Cols.LAST_NAME)));
        try {
            user.setDob(parseUtcSqlDateTime(cursor.getString(cursor.getColumnIndex(UserCredentialsT.Cols.DOB))));
        } catch (ParseException e) {
            user.setDob(null);
        }
        user.setServer(cursor.getString(cursor.getColumnIndex(UserCredentialsT.Cols.SERVER)));
        user.setToken(cursor.getString(cursor.getColumnIndex(UserCredentialsT.Cols.TOKEN)));
        user.setUserType(IUserName.UserType.valueOf(cursor.getString(cursor.getColumnIndex(UserCredentialsT.Cols.TYPE))));
        int valid = cursor.getInt(cursor.getColumnIndex(UserCredentialsT.Cols.VALID));
        user.setStatus(valid ==1 ? USER_STATUS.VALID : USER_STATUS.NOT_VALID);
        return user;
    }

    public LoggedInUser() {
    }

    public USER_STATUS getStatus() {
        return status;
    }

    public void setStatus(USER_STATUS status) {
        this.status = status;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getLogin_id() {
        return login_id;
    }

    public void setLogin_id(long login_id) {
        this.login_id = login_id;
    }

    public IUserName.UserType getUserType() {
        return userType;
    }

    public void setUserType(IUserName.UserType userType) {
        this.userType = userType;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getLicenceNumber() {
        return licenceNumber;
    }

    public void setLicenceNumber(String licenceNumber) {
        this.licenceNumber = licenceNumber;
    }

    public  boolean isPatient() {
        return !isNone() && (getUserType()==BOTH || getUserType()== PATIENT);
    }
    public boolean isDoctor() {
        return !isNone() && (getUserType() == BOTH || getUserType() == DOCTOR);
    }

    public boolean isNone() {
        return getStatus() == USER_STATUS.NONE;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {

        dest.writeByte((byte)getStatus().ordinal());
        dest.writeString(getUsername());
        dest.writeString(getDoctor());
        dest.writeByte((byte) getUserType().ordinal());
        dest.writeLong(getId());
        dest.writeLong(getLogin_id());
        dest.writeString(getServer());
        dest.writeString(getToken());
        dest.writeString(getFirstName());
        dest.writeString(getLastName());
        dest.writeString(getMedicalRecord());
        dest.writeString(getLicenceNumber());
        dest.writeLong(dob != null ? dob.getTime() : 0L);
    }

    /**
     * Used for writing a copy of this object to a Parcel, do not manually call.
     */
    public static final Creator<LoggedInUser> CREATOR = new Creator<LoggedInUser>() {
        public LoggedInUser createFromParcel(Parcel in) {
            return new LoggedInUser(in);
        }

        public LoggedInUser[] newArray(int size) {
            return new LoggedInUser[size];
        }
    };

    /**
     * Used for writing a copy of this object to a Parcel, do not manually call.
     */
    private LoggedInUser(Parcel in) {
        setStatus(USER_STATUS.values()[in.readByte()]);
        setUsername(in.readString());
        setDoctor(in.readString());
        setUserType(IUserName.UserType.values()[in.readByte()]);
        setId(in.readLong());
        setLogin_id(in.readLong());
        setServer(in.readString());
        setToken(in.readString());
        setFirstName(in.readString());
        setLastName(in.readString());
        setMedicalRecord( in.readString());
        setLicenceNumber( in.readString());
        long dob;
        setDob((dob = in.readLong()) == 0 ? null : new Date(dob));
    }

    public enum USER_STATUS{
        NONE, VALID, NOT_VALID
    }
}
