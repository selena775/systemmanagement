/*
 * Copyright (c) 2015.
 * Copyright from gradle @copyright@
 * Version @version@
 */

package org.selena.patient.client.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import org.selena.patient.client.R;

import java.io.IOException;

import static org.selena.patient.client.invariable.MessageConstants.*;
import static org.selena.patient.client.utility.UserCredentialsUtility.getProjectPreferences;

/**
 * Created by sklasnja on 2/25/15.
 */
public class GCMUtility {

    private static final String TAG = GCMUtility.class.getName();

    public static String getRegistrationId(final Context context){
        // Check device for Play Services APK.
        final GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);

        try {
            return gcm.register(SENDER_ID);
        } catch (IOException ex) {
            // If there is an error, don't just keep trying to register.
            // Require the user to click a button again, or perform
            // exponential back-off.
            Log.i(TAG, "Error getting GCM RegId:" + ex.getMessage());
            return "";
        }
    }

    public static void storeRegistrationId(Context context, String regid) {

        int appVersion = getAppVersion(context);
        SharedPreferences.Editor editor = getProjectPreferences(context).edit();
        Log.i(TAG, "Saving regId on app version " + appVersion);
        editor.putString(PROPERTY_REG_ID, regid);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    /**
     * Gets the current registration ID for application on GCM service.
     * <p>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     *         registration ID.
     */
    public static String getGCMPreferences(Context context) {
        final SharedPreferences prefs = getProjectPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing registration ID is not guaranteed to work with
        // the new app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, 1);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");

            // this value will be used to inform the backend the reg id is not valid any more
            final SharedPreferences.Editor editor = prefs.edit();
            editor.putString(PROPERTY_REG_ID_TO_REMOVE, registrationId );
            editor.commit();

            return "";
        }
        return registrationId;
    }

    /**
     * Gets the number of play store installation attempts
     * @return number of getting the registration id failed
     */

    public static int getPreferencePlayServiceInstallAttempt(Context context) {
        final SharedPreferences prefs = getProjectPreferences(context);
        return prefs.getInt(PLAY_SERVICE_ATTEMPT, 0);
    }

    public static void incPreferencePlayServiceInstallAttempt(Context context) {

        SharedPreferences.Editor editor = getProjectPreferences(context).edit();
        editor.putInt(PLAY_SERVICE_ATTEMPT, getPreferencePlayServiceInstallAttempt(context) + 1);
        editor.commit();
    }
    public static void resetPreferencePlayServiceInstallAttempt(Context context) {

        SharedPreferences.Editor editor = getProjectPreferences(context).edit();
        editor.putInt(PLAY_SERVICE_ATTEMPT, 0);
        editor.commit();
    }



    public static boolean usesGCM(Context context) {
        final SharedPreferences prefs = getProjectPreferences(context);
        return prefs.getBoolean( context.getString(R.string.usesGCM), true);
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }
}
