package org.selena.patient.client.adapter.doctor;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import org.selena.patient.client.R;
import org.selena.patient.client.fragments.doctor.CheckInHistoryFragment;
import org.selena.patient.client.model.MedicationUsage;
import org.selena.patient.client.model.server.CheckIn;

import java.util.List;
import java.util.Map;

import static org.selena.patient.client.utility.DateTimeUtility.prettyTime;

public class CheckInAdapter extends BaseAdapter {

    private CheckInHistoryFragment chekcInFragment;
    private List<CheckIn> list;
    private Map<Long, CheckIn> checkInMap;
    private static LayoutInflater inflater = null;

    public CheckInAdapter(Context context, CheckInHistoryFragment fragment, final List<CheckIn> list, final Map<Long, CheckIn> map ){

        chekcInFragment = fragment;
        this.list = list;
        checkInMap = map;

        inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
    }

    public int getCount() {
        return list.size();
    }

    public CheckIn getItem(int position) {
        return list.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View view, ViewGroup parent) {

        final CheckIn checkIn = list.get(position);

        ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = inflater.inflate(R.layout.check_in_item, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }


        holder.date.setText(prettyTime(System.currentTimeMillis() - checkIn.getTimestamp().getTime()));


        int resId = 0;
        switch ( checkIn.getPainIntensity()) {
            case WELL_CONTROLLED:
                resId = R.drawable.normal;
                holder.painIcon.setImageResource(resId);
                break;
            case MODERATE:
                resId = R.drawable.warning;
                holder.painIcon.setImageResource(resId);
                break;
            case SEVERE:
                resId = R.drawable.alarm;
                holder.painIcon.setImageResource(resId);
                break;
        }



        switch ( checkIn.getEatingDisorder()) {
            case NO:
                resId = R.drawable.normal;
                holder.eatingIcon.setImageResource(resId);
                break;
            case SOME:
                resId = R.drawable.warning;
                holder.eatingIcon.setImageResource(resId);
                break;
            case CANNOT_EAT:
                resId = R.drawable.alarm;
                holder.eatingIcon.setImageResource(resId);
                break;
        }



        boolean some= false, taken=false;
        for ( MedicationUsage mu : checkIn.getMedicationUsage()) {
            taken= true;
            if (mu.getTaken()!= MedicationUsage.MedicineTaken.YES) {
                some = true;
            }
        }

        if ( some || taken ) {
            if ( some )holder.medicationIcon.setImageResource(R.drawable.some);
            else holder.medicationIcon.setImageResource(R.drawable.yes);
        }

        if ( checkIn.getPhotoFileName()!=null ) {
            final Bitmap bmp= BitmapFactory.decodeFile(checkIn.getPhotoFileName());
            holder.throatPhoto.setImageBitmap(bmp);
            holder.throatPhoto.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    chekcInFragment.onUpdatePhoto(bmp);
                }
            });
        } else {
            holder.throatPhoto.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    chekcInFragment.onUpdatePhoto(null);
                }
            });
        }

        holder.date.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chekcInFragment.onUpdatePhoto(null);
            }
        });
        holder.painIcon.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chekcInFragment.onUpdatePhoto(null);
            }
        });
        holder.eatingIcon.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chekcInFragment.onUpdatePhoto(null);
            }
        });
        holder.medicationIcon.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chekcInFragment.onUpdatePhoto(null);
            }
        });

        return view;
    }


    static class ViewHolder {

        @InjectView(R.id.check_in_date) TextView date;
        @InjectView(R.id.pain_icon) ImageView painIcon;
        @InjectView(R.id.eating_icon) ImageView eatingIcon;
        @InjectView(R.id.medication_icon) ImageView medicationIcon;
        @InjectView(R.id.throat_photo) ImageView throatPhoto;


        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }


    public void addPhoto(String fileName, long chkId) {
        CheckIn chIn = checkInMap.get(chkId);
        chIn.setPhotoFileName(fileName);
        notifyDataSetInvalidated();
    }

    public List<CheckIn> getList(){
        return list;
    }

    public void setList(final List<CheckIn> list, final Map<Long, CheckIn> map ){
        this.list = list;
        checkInMap= map;
        this.notifyDataSetChanged();
    }

}
