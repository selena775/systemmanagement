package org.selena.patient.client.utility;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import org.selena.patient.client.model.DoctorDetails;
import org.selena.patient.client.model.LoggedInUser;
import org.selena.patient.client.model.server.IDoctor;
import org.selena.patient.client.model.server.IPatient;
import org.selena.patient.client.model.server.IUserName;
import org.selena.patient.client.service.SyncService;
import org.selena.provider.tables.DoctorDetailsT;
import org.selena.provider.tables.UserCredentialsT;

import java.text.ParseException;
import java.util.Date;

import static org.selena.patient.client.model.server.IUserName.UserType.*;
import static org.selena.patient.client.utility.DateTimeUtility.*;
import static org.selena.patient.client.model.LoggedInUser.USER_STATUS;


public class UserCredentialsUtility {

    public static final String PREFERENCE_FILE = "patient_pref";
    public static final int PREFERENCE_MODE = Context.MODE_PRIVATE;

    public static boolean userIsPatient(final IUserName user) {
        return user.getType()==BOTH || user.getType()== PATIENT;
    }
    public static boolean userIsDoctor(final IUserName user) {
        return user.getType()==BOTH || user.getType()== DOCTOR;
    }

    public static LoggedInUser anyValidUser(Context ctx) {

        String sort = UserCredentialsT.Cols.LOGIN_TIME + " DESC";

        Cursor cursor = ctx.getContentResolver().query(UserCredentialsT.CONTENT_URI, UserCredentialsT.ALL_COLUMN_NAMES, null, null, sort);

        LoggedInUser user = new LoggedInUser();
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    user= LoggedInUser.fromCursor(cursor);
                    if ( user.getStatus()==USER_STATUS.VALID ) {
                        cursor.close();
                        return user;
                    }
                } while (cursor.moveToNext());
                cursor.close();
                return user;
            }
        }
        user.setStatus(USER_STATUS.NONE);
        return user;

    }

    public static Date getLastTimeFailed(Context ctx) {

        String where = UserCredentialsT.Cols.VALID + "=?";
        String[] whereArgs = {"1"};
        String sort = UserCredentialsT.Cols.LOGIN_TIME + " DESC";

        Cursor cursor = ctx.getContentResolver().query(UserCredentialsT.CONTENT_URI, new String[] {
                UserCredentialsT.Cols.LAST_TIME_FAILED,

        }, where, whereArgs, sort);
        if (cursor != null && cursor.moveToFirst()) {

            String timeFailed= cursor.getString(cursor.getColumnIndex(UserCredentialsT.Cols.LAST_TIME_FAILED));

            cursor.close();
            try {
                return parseUtcSqlDateTime(timeFailed);
            } catch (ParseException e) {
                e.printStackTrace();
                return new Date(0);
            }

        } else {
            return new Date(0);
        }
    }

    public static DoctorDetails getDoctor(Context ctx, String doctorUserName) {

        String where = DoctorDetailsT.Cols.USERNAME + "=?";
        String[] whereArgs = {doctorUserName};
        DoctorDetails doctor= null;

        Cursor cursor = ctx.getContentResolver().query(DoctorDetailsT.CONTENT_URI, DoctorDetailsT.ALL_COLUMN_NAMES, where, whereArgs, null);
        if (cursor != null && cursor.moveToFirst()) {
            doctor= DoctorDetails.fromCursor(cursor);
            cursor.close();
        }
        return doctor;
    }

    public static Date getLastTimeSynchronised(Context ctx) {

        String where = UserCredentialsT.Cols.VALID + "=?";
        String[] whereArgs = {"1"};
        String sort = UserCredentialsT.Cols.LOGIN_TIME + " DESC";

        Cursor cursor = ctx.getContentResolver().query(UserCredentialsT.CONTENT_URI, new String[] {
                UserCredentialsT.Cols.LAST_TIME_SYNC,

        }, where, whereArgs, sort);
        if (cursor != null && cursor.moveToFirst()) {

            String timeFailed= cursor.getString(cursor.getColumnIndex(UserCredentialsT.Cols.LAST_TIME_SYNC));

            cursor.close();
            try {
                return parseUtcSqlDateTime(timeFailed);
            } catch (ParseException e) {
                e.printStackTrace();
                return new Date(0);
            }

        } else {
            return new Date(0);
        }
    }

    public static void signOut(Context ctx){

        LoggedInUser user= anyValidUser(ctx);

        if ( user.getStatus()==USER_STATUS.VALID) {
            ContentValues updateValues = new ContentValues();
            updateValues.put(UserCredentialsT.Cols.VALID, "0");
            updateSingleUserCredentials(ctx, user.getId(), updateValues);
            SyncService.stopScheduledSync(ctx);
            SyncService.stopScheduledCheck(ctx);
        }
    }

    public static void signIn(Context ctx, final String server, final IUserName userName){

        SharedPreferences prefs = getProjectPreferences(ctx);
        String token= prefs.getString("TOKEN", null);
        if ( token==null) throw new RuntimeException("Error: Token is null");

        ContentValues updateValues = new ContentValues();
        updateValues.put(UserCredentialsT.Cols.USERNAME, userName.getUserName());
        if(userIsPatient(userName)) {
            updateValues.put(UserCredentialsT.Cols.DOCTOR, ((IPatient)userName).getDoctor());
            updateValues.put(UserCredentialsT.Cols.MEDICAL_RECORD, ((IPatient)userName).getMedicalRecord() );
        }
        if(userIsDoctor(userName)) {
            updateValues.put(UserCredentialsT.Cols.DOCTOR_ID, ((IDoctor)userName).getDoctorId());
        }

        Date dob= userName.getDob();
        updateValues.put(UserCredentialsT.Cols.TYPE, userName.getType().name());
        updateValues.put(UserCredentialsT.Cols.SERVER, server);
        updateValues.put(UserCredentialsT.Cols.FIRST_NAME, userName.getFirstName());
        updateValues.put(UserCredentialsT.Cols.LAST_NAME, userName.getLastName());
        updateValues.put(UserCredentialsT.Cols.DOB, dob!=null ? formatUtcSqlDateTime(dob) : "");
        updateValues.put(UserCredentialsT.Cols.TOKEN, token );
        updateValues.put(UserCredentialsT.Cols.LOGIN_ID, userName.getId());
        updateValues.put(UserCredentialsT.Cols.VALID, 1);
        updateValues.put(UserCredentialsT.Cols.LAST_TIME_FAILED, formatZeroUtcSqlDateTime());
        updateValues.put(UserCredentialsT.Cols.LAST_TIME_SYNC, formatZeroUtcSqlDateTime());
        updateValues.put(UserCredentialsT.Cols.LOGIN_TIME, formatUtcSqlDateTime(new Date()));
        ctx.getContentResolver().insert(UserCredentialsT.CONTENT_URI, updateValues);

        // TODO comment this line if you want gcm
        SyncService.restartScheduledSync(ctx);
        if ( userIsPatient(userName)) {
            SyncService.restartScheduledCheck(ctx);
        }
    }

    public static void fillTheDoctor(Context ctx, final IUserName doctorUserName){

        // if there is no docotr on the server side, nothing we can do it
        if(doctorUserName==null ) return;

        // is there a doctor in a sqlite3
        DoctorDetails doctor= getDoctor(ctx, doctorUserName.getUserName());

        ContentValues contentValues = new ContentValues();
        contentValues.put(UserCredentialsT.Cols.USERNAME, doctorUserName.getUserName());
        contentValues.put(UserCredentialsT.Cols.DOCTOR_ID, ((IDoctor) doctorUserName).getDoctorId());
        contentValues.put(UserCredentialsT.Cols.FIRST_NAME, doctorUserName.getFirstName());
        contentValues.put(UserCredentialsT.Cols.LAST_NAME, doctorUserName.getLastName());

        if ( null!=doctor) {
            String mSelectionClause = UserCredentialsT.Cols.ID + "=?";
            String[] mSelectionArgs = {String.valueOf(doctor.getId())};
            ctx.getContentResolver().update(DoctorDetailsT.CONTENT_URI, contentValues, mSelectionClause, mSelectionArgs);
        } else {
            ctx.getContentResolver().insert(DoctorDetailsT.CONTENT_URI, contentValues);
        }
    }


    public static int updateSingleUserCredentials(Context ctx, long id, ContentValues mUpdateValues) {

        String mSelectionClause = UserCredentialsT.Cols.ID + "=?";
        String[] mSelectionArgs = {String.valueOf(id)};
        return ctx.getContentResolver().update(UserCredentialsT.CONTENT_URI, mUpdateValues, mSelectionClause, mSelectionArgs);
    }

    public static SharedPreferences getProjectPreferences(Context ctx) {
        return ctx.getSharedPreferences(PREFERENCE_FILE, PREFERENCE_MODE);
    }

}
