package org.selena.patient.client.utility;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import org.selena.patient.client.model.PatientUI;
import org.selena.patient.client.model.server.CheckIn;
import org.selena.patient.client.model.MedicationUsage;
import org.selena.provider.tables.AlertedPatientT;
import org.selena.provider.tables.CheckInT;
import org.selena.provider.tables.MedicationUsageT;

import java.text.ParseException;
import java.util.*;

import static org.selena.patient.client.utility.DateTimeUtility.parseUtcSqlDateTime;

/**
 * Created by sklasnja on 10/24/14.
 */
public class DbUtility {

    public static Collection<CheckIn> retrieveCheckInFromDb(Context context) throws ParseException {
        String where = CheckInT.Cols.SYNCHRONISED + "=? ";
        String[] whereArgs = {"0"};
        String sort = CheckInT.Cols.TIME + " ASC";

        Cursor cursor = context.getContentResolver().query(CheckInT.CONTENT_URI, CheckInT.ALL_COLUMN_NAMES, where, whereArgs, sort);

        CheckIn checkIn;
        Collection<CheckIn> rValue = new ArrayList<>();
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    rValue.add(checkIn= CheckInT.getCheckInDataFromCursor(cursor));
                    checkIn.setMedicationUsage( getMedicationUsage(context, checkIn.getChkInId()));

                } while (cursor.moveToNext());
            }
        }
        return rValue;
    }

    public static Collection<AlertedPatientT.HOLDER> retrieveAlertedPatientsFromDb(long login_id, Context context) {

        String where = AlertedPatientT.Cols.LOGIN_ID + "=? ";
        String[] whereArgs = {String.valueOf(login_id)};

        Cursor cursor = context.getContentResolver().query(AlertedPatientT.CONTENT_URI, AlertedPatientT.ALL_COLUMN_NAMES, where, whereArgs, null);

        Collection<AlertedPatientT.HOLDER> rValue = new ArrayList<>();
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    rValue.add(AlertedPatientT.HOLDER.fromCursor(cursor));
                } while (cursor.moveToNext());
            }
        }
        return rValue;
    }

    public static Date getLastCheckInTime(Context context) throws ParseException {

        String sort = CheckInT.Cols.TIME + " DESC";

        Cursor cursor = context.getContentResolver().query(CheckInT.CONTENT_URI, new String[]{CheckInT.Cols.TIME}, null, null, sort);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                   return  parseUtcSqlDateTime(cursor.getString(cursor
                            .getColumnIndex(CheckInT.Cols.TIME)));

                } while (cursor.moveToNext());
            }
        }
        return null;
    }



    public static Collection<CheckInT.PhotoHolder> retrieveImageFileNameFromDb(Context context) throws ParseException {
        String where = CheckInT.Cols.SYNCHRONISED + "!=? and " + CheckInT.Cols.IMAGE_FILE_NAME + "!=? and " + CheckInT.Cols.SYNCHRONISED_PHOTO + "=?";
        String[] whereArgs = {"0", "", "0"};
        String sort = CheckInT.Cols.TIME + " ASC";

        Cursor cursor = context.getContentResolver().query(CheckInT.CONTENT_URI,
                new String[]{CheckInT.Cols.ID,  CheckInT.Cols.LOGIN_ID, CheckInT.Cols.SYNCHRONISED, CheckInT.Cols.IMAGE_FILE_NAME},
                    where, whereArgs, sort);


        Collection<CheckInT.PhotoHolder> rValue = new ArrayList<>();
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {

                    rValue.add(CheckInT.PhotoHolder.fromCursor(cursor));

                } while (cursor.moveToNext());
            }
        }
        return rValue;
    }

    public static Collection<MedicationUsage> getMedicationUsage(final Context context, final long checkInId) throws ParseException {
        String where = MedicationUsageT.Cols.CHECK_IN_ID + "=?";
        String[] whereArgs = {String.valueOf(checkInId)};
        String sort = MedicationUsageT.Cols.NAME + " ASC";

        Cursor cursor = context.getContentResolver().query(MedicationUsageT.CONTENT_URI, MedicationUsageT.ALL_QUERY_COLUMNS, where, whereArgs, sort);

        Collection<MedicationUsage> rValue = new ArrayList<>();
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    rValue.add( MedicationUsageT.getMedicationUsageDataFromCursor(cursor));

                } while (cursor.moveToNext());
            }
        }
        return rValue;
    }

    public static int updatePatientInAlert(Context ctx, final long id, boolean not_in_alert) {
        ContentValues updateValues = new ContentValues();
        updateValues.put(AlertedPatientT.Cols.NOT_IN_ALERT, String.valueOf(not_in_alert));

        String mSelectionClause = CheckInT.Cols.ID + "=?";
        String[] mSelectionArgs = {String.valueOf(id)};
        return ctx.getContentResolver().update(AlertedPatientT.CONTENT_URI, updateValues, mSelectionClause, mSelectionArgs);
    }

    public static int updateCheckInSynchronised(Context ctx, final long id, long serverId) {
        ContentValues updateValues = new ContentValues();
        updateValues.put(CheckInT.Cols.SYNCHRONISED, serverId);

        String mSelectionClause = CheckInT.Cols.ID + "=?";
        String[] mSelectionArgs = {String.valueOf(id)};
        return ctx.getContentResolver().update(CheckInT.CONTENT_URI, updateValues, mSelectionClause, mSelectionArgs);
    }
    public static int updateCheckInSynchronisedImage(Context ctx, final long id) {
        ContentValues updateValues = new ContentValues();
        updateValues.put(CheckInT.Cols.SYNCHRONISED_PHOTO, 1);

        String mSelectionClause = CheckInT.Cols.ID + "=?";
        String[] mSelectionArgs = {String.valueOf(id)};
        return ctx.getContentResolver().update(CheckInT.CONTENT_URI, updateValues, mSelectionClause, mSelectionArgs);
    }
}
