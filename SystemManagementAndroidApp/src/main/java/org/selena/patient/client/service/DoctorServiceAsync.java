package org.selena.patient.client.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import com.google.common.collect.Lists;
import org.apache.http.HttpStatus;
import org.selena.patient.client.PatientSvcConnection;
import org.selena.patient.client.ServiceClientSvcApi;
import org.selena.patient.client.invariable.MessageConstants;
import org.selena.patient.client.model.MedicationUI;
import org.selena.patient.client.model.PatientUI;
import org.selena.patient.client.model.server.CheckIn;
import org.selena.patient.client.model.LoggedInUser;
import retrofit.client.Response;

import java.io.*;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.selena.patient.client.utility.ExceptionUtility.isLocalNetworkDown;
import static org.selena.patient.client.utility.ExceptionUtility.isServerDown;
import static org.selena.patient.client.utility.UserCredentialsUtility.anyValidUser;

/**
 * @class DoctorServiceServiceAsync
 *
 * @brief This Service performs calls to the DoctorService Web service
 *        using asynchronous AIDL interactions.  The component (which
 *        in this example application is DoctorServiceActivity) that binds
 *        to this service should receive an IBinder Proxy that
 *        references an instance of DoctorServiceRequest, which extends
 *        IBinder.  The component can then interact with this Service
 *        by invoking oneway method calls via the DoctorServiceRequest
 *        proxy. Specifically, the component can ask this Service to
 *        call the DoctorService Web service, passing in a DoctorServiceCallback
 *        object.  Once the Web service call is finished, this Service
 *        will send the results back to the calling component by
 *        invoking sendResults() on the DoctorServiceCallback object.
 *  
 *        AIDL and the Binder framework are an example of the Broker
 *        Pattern, in which all interprocess communication details are
 *        hidden behind the AIDL interfaces.
 */
public class DoctorServiceAsync extends Service {

    /**
     * Used for debugging.
     */
    private final String TAG = this.getClass().getSimpleName();

    /**
     * The concrete implementation of the AIDL Interface
     * DoctorServiceRequest.  We extend the Stub class, which implements
     * the DoctorServiceRequest interface, so that Android can properly
     * handle calls across process boundaries.
     * <p/>
     * This implementation plays the role of Invoker in the Broker
     * Pattern.
     */
    DoctorServiceRequest.Stub mDoctorServiceRequestImpl = new DoctorServiceRequest.Stub() {
        /**
         * Invoke a call to the DoctorService webservice at the
         * provided Internet uri.  The Service uses the
         * DoctorServiceCallback parameter to return a string
         * containing the results back to the Activity.
         *
         * Use the method defined in DoctorServiceUtils for code
         * brevity.
         */
        @Override
        public void getPatientList(String pattern,
                                   DoctorServiceCallback callback)
                throws RemoteException {
            Log.d(TAG, "Calling getPatientList()");


            try {
                Collection<PatientUI> list = getPatientUIList(pattern, false);
                callback.sendPatientList(Lists.newArrayList(list), "OK");
            } catch (Exception ex) {
                callback.sendPatientList(null, getErrorReason(ex));
            }
        }
        @Override
        public void getPatientCheckIns ( long patientId,
                                         DoctorServiceCallback callback) throws RemoteException {

            // TODO delete
//            File outputDir = getApplicationContext().getCacheDir();


            final LoggedInUser user = anyValidUser(getApplicationContext());
            final ServiceClientSvcApi svc = PatientSvcConnection.init(user.getServer(), user.getToken());

            List<CheckIn> list;
            try {
                list = Lists.newArrayList(svc.getPatientCheckIns(patientId));
                callback.sendCheckInList(list, "OK");
            } catch (Exception ex) {
                callback.sendCheckInList(null, getErrorReason(ex));
                return;
            }

            try {
                if ( list!=null && !list.isEmpty()) {
                    for (CheckIn chkIn : list) {
                        Response response = svc.getPhoto(patientId, chkIn.getChkInId());

                        // Bug in spring Response Entity is null. See: https://jira.spring.io/browse/SPR-7911
                        //if ( HttpStatus.SC_NO_CONTENT == response.getStatus() ) continue;

                        if ( HttpStatus.SC_OK != response.getStatus()) throw new Exception( "Error response code: " + response.getStatus());

                        InputStream in = response.getBody().in();
                        File outputDir = getApplicationContext().getCacheDir();
                        File outputFile = File.createTempFile("patient_", "_photo", outputDir);
                        outputFile.deleteOnExit();

                        final OutputStream out = new FileOutputStream(outputFile);
                        copy(in, out);
                        callback.sendCheckInPhoto( chkIn.getChkInId(), outputFile.getAbsolutePath(), "OK" );
                    }

                }
            } catch (Exception ex) {
                callback.sendCheckInList(null, getErrorReason(ex));
            }

        }

        @Override
        public void getPatientMedications(final long patientId, final DoctorServiceCallback callback) throws RemoteException {
            final LoggedInUser user = anyValidUser(getApplicationContext());
            final ServiceClientSvcApi svc = PatientSvcConnection.init(user.getServer(), user.getToken());

            List<MedicationUI> list;
            try {
                list = Lists.newArrayList(svc.getPatientMedications(patientId));
                callback.sendMedicationList(list, "OK");
            } catch (Exception ex) {
                callback.sendMedicationList(null, getErrorReason(ex));
            }
        }

        @Override
        public void saveMedications(final long patientId, final List<MedicationUI> toSave, final DoctorServiceCallback callback) throws RemoteException {


            final LoggedInUser user = anyValidUser(getApplicationContext());
            final ServiceClientSvcApi svc = PatientSvcConnection.init(user.getServer(), user.getToken());
            try {
                svc.addMedications(patientId, toSave);
                callback.sendSaveMedicationsResult("OK");
            } catch (Exception ex) {
                callback.sendSaveMedicationsResult( getErrorReason(ex));
            }

        }

    };

    /**
     * Called when a component calls bindService() with the proper
     * intent.  Return the concrete implementation of DoctorServiceRequest
     * cast as an IBinder.
     */
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "calling onBind()");
        return mDoctorServiceRequestImpl;
    }

    /**
     * Make an Intent that will start this service when passed to
     * bindService().
     *
     * @param context The context of the calling component.
     */
    public static Intent makeIntent(Context context) {
        return new Intent(context,
                org.selena.patient.client.service.DoctorServiceAsync.class);
    }



    private Collection<PatientUI> getPatientUIList(String pattern, boolean inAlarm) {
        final LoggedInUser user = anyValidUser(this);


        final ServiceClientSvcApi svc = PatientSvcConnection.init(user.getServer(), user.getToken());

        Context ctx = getBaseContext();

        if (svc != null) {
            Collection<PatientUI> patients;
            if (pattern != null) {
                patients = svc.findByDoctorAndPatientName(user.getUsername(), pattern);
            } else if (inAlarm) {
                patients = svc.findAlertedPatientsByDoctor(user.getUsername());
            } else {
                patients = svc.findByDoctor(user.getUsername());
            }
            return patients;
        }
        return Collections.emptyList();
    }


    public static String getErrorReason(final Exception ex) {
        if( ex==null) {
            return MessageConstants.EMPTY_SET;
        } else if ( isLocalNetworkDown(ex) ) {
            return MessageConstants.LOCAL_NETWORK_DOWN;
        } else if ( isServerDown(ex) ) {
            return MessageConstants.SERVER_IS_DOWN;
        } else {
            return ex.getMessage();
        }
    }

    /**
     * Copy the contents of an InputStream into an OutputStream.
     *
     * @param in
     * @param out
     * @return
     * @throws java.io.IOException
     */
    private static int copy(final InputStream in,
                     final OutputStream out) throws IOException {
        final int BUFFER_LENGTH = 1024;
        final byte[] buffer = new byte[BUFFER_LENGTH];
        int totalRead = 0;
        int read = 0;

        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
            totalRead += read;
        }

        return totalRead;
    }
}
