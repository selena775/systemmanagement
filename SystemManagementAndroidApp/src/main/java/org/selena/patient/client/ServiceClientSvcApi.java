package org.selena.patient.client;

import org.selena.patient.client.model.MedicationUI;
import org.selena.patient.client.model.PatientUI;
import org.selena.patient.client.model.server.*;
import org.selena.patient.client.utility.Pair;
import retrofit.client.Response;
import retrofit.http.*;
import retrofit.mime.TypedFile;

import java.util.Collection;
import java.util.List;

import static org.selena.patient.client.invariable.PathConstants.*;

/**
 * This interface defines an API for the System Management Resources Protocol. The
 * interface is used to provide a contract for client/server
 * interactions. The interface is annotated with Retrofit
 * annotations so that clients can automatically convert the
 *
 * @author sklasnja
 */
public interface ServiceClientSvcApi {

    public static final String TOKEN_PATH = "/oauth/token";

    @DELETE(PATIENT_SVC_PATH)
    public Void deletePatients();

    @GET(PATIENT_SVC_PATH)
    public Collection<Patient> getPatientList();

    @POST(PATIENT_SVC_PATH)
    public Patient addPatient(@Body Patient v);

    @POST(DOCTOR_AND_PATIENT_SVC_PATH)
    public DoctorAndPatient addDoctorAndPatient(@Body DoctorAndPatient v);

    @POST(DOCTOR_SVC_PATH)
    public Doctor addDoctor(@Body Doctor d);

    @GET(DOCTOR_FIRST_NAME_SEARCH_PATH)
    public Collection<IDoctor> findDoctorByFirstName(@Query(FIRST_NAME_PARAMETER) String firstName);

    @DELETE(DOCTOR_SVC_PATH)
    public Void deleteDoctors();

    @GET(DOCTOR_SVC_PATH)
    public Collection<IDoctor> getDoctorList();

    @GET(PATIENT_ID_SVC_PATH)
    public Patient getPatientById(@Path(ID) long id);


    @GET(FIND_BY_PATIENT_NAME)
    public Collection<Patient> findByFirstName(@Query(NAME_PARAMETER) String name);

    @GET(FIND_BY_USER_NAME)
    public IUserName findByUserName(@Query(USERNAME_PARAMETER) String name);

    @POST(USER_SVC_PATH)
    public Void addRegistrationId(@Body Pair<String,String> registrationID);

    @GET(CHECK_ROLES)
    public Collection<String> getRoles(@Query(USERNAME_PARAMETER) String name);


    @GET(FIND_BY_DOCTOR)
    public Collection<PatientUI> findByDoctor(@Query(DOCTOR_PARAMETER) String name);

    @GET(FIND_ALERTED_PATIENTS_BY_DOCTOR)
    public Collection<PatientUI> findAlertedPatientsByDoctor(@Query(DOCTOR_PARAMETER) String name);

    @GET(FIND_BY_DOCTOR_PATIENT_USERNAME)
    public Patient findByDoctorAndPatientUserName(@Query(DOCTOR_PARAMETER) String doctorName, @Query(USERNAME_PARAMETER) String userName);

    @GET(FIND_BY_DOCTOR_PATIENT_NAME)
    public Collection<PatientUI> findByDoctorAndPatientName(@Query(DOCTOR_PARAMETER) String doctorName, @Query(PATTERN_PARAMETER) String userName);

    @GET(PATIENT_DOCTOR)
    public IUserName getPatientsDoctor(@Path(ID) long id);

    @GET(PATIENT_CHECK_IN_SVC_PATH)
    public Collection<CheckIn> getPatientCheckIns(@Path(ID) long id);


    @POST(PATIENT_CHECK_IN_SVC_PATH)
    public long addCheckIn(@Path(ID) long id, @Body CheckIn checkIn);

    @GET(PATIENT_CHECK_IN_ID_SVC_PATH)
    public CheckIn geCheckIn(@Path(ID) long id, @Path(CHECK_IN_ID) long cId);


    @Multipart
    @POST(PATIENT_CHECK_IN_PHOTO)
    public String setPhoto(@Path(ID) long id, @Path(CHECK_IN_ID) long cId, @Part(PHOTO_PARAMETER) TypedFile photoData);


    @Streaming
    @GET(PATIENT_CHECK_IN_PHOTO)
    Response getPhoto(@Path(ID) long id, @Path(CHECK_IN_ID) long cId);


    @POST(PATIENT_ID_MEDICATIONS)
    public Void addMedications(@Path(ID) long id, @Body List<MedicationUI> medications);

    @GET(PATIENT_ID_MEDICATIONS)
    public Collection<MedicationUI> getPatientMedications(@Path(ID) long id);

    @GET(FIND_BY_PATIENT_MEDICATION_AND_DATE)
    public Collection<Medication> getPatientMedicationsForTheDay(@Path(ID) long id,
                                                                 @Query(DATE_PARAMETER) long day);

    @GET(FIND_BY_PATIENT_ID_AND_TIME)
    public CheckIn findCheckInByDate(@Path(ID) long id, @Query(CHECK_IN_TIMESTAMP_PARAMETER) long timestamp);
}
