package org.selena.patient.client.utility;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

/**
 * Created by sklasnja on 10/23/14.
 */
public class NotificationUtility {

    public static final int LOST_CREDENTIALS_ID = 11151999;
    public static final int CHECK_IN_INTENT = 11152000;
    public static final int SYNC_ERROR_CREDENTIALS = 11152001;
    public static final int SYNC_ERROR_SERVER = 11152002;
    public static final int ALERTED_PATIENTS = 11152003;

    public static void notifyUser( Context ctx, Intent intent, int NOTIFICATION_ID, String contentTitle, String contentText, String tickerText, boolean autoCancel ) {

        final PendingIntent pendingIntent =PendingIntent.getActivity(ctx, 0,
                intent, PendingIntent.FLAG_CANCEL_CURRENT);

        Notification.Builder notificationBuilder = new Notification.Builder(
                ctx)
                .setContentTitle(contentTitle)
                .setContentText(contentText)
                .setTicker(tickerText)
                .setSmallIcon(android.R.drawable.stat_sys_warning)
                .setAutoCancel(autoCancel)
                .setContentIntent(pendingIntent)
                .setSound(null)
                .setVibrate(null);


        int sdkVersion = android.os.Build.VERSION.SDK_INT;

        if ( sdkVersion >= 16 ) {
            notifyNewApi(ctx, NOTIFICATION_ID, notificationBuilder);


        } else {
            notifyOldApi(ctx, NOTIFICATION_ID, notificationBuilder);
        }


    }
    @TargetApi(16)
    public static void notifyNewApi( Context ctx , int NOTIFICATION_ID, Notification.Builder notificationBuilder ) {
        getNotificationManager(ctx).notify(NOTIFICATION_ID, notificationBuilder.build());
    }

    public static void notifyOldApi( Context ctx , int NOTIFICATION_ID, Notification.Builder notificationBuilder ) {
        getNotificationManager(ctx).notify(NOTIFICATION_ID, notificationBuilder.getNotification());
    }


    public static NotificationManager getNotificationManager( Context ctx ){
        return (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
    }
}
