package org.selena.patient.client.activity;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;
import org.selena.patient.client.R;

public class BasicViewAndUpdatePreferencesActivity extends Activity {

    private static final String TAG = BasicViewAndUpdatePreferencesActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.user_prefs_fragment);

        FragmentTransaction fragmentTransaction = getFragmentManager()
                .beginTransaction();
        fragmentTransaction.add( R.id.userPreferenceFragment, new BasicUserPreferenceFragment() {});
        fragmentTransaction.commit();

    }

    // Fragment that displays the username preference
    public static class UserPreferenceFragment extends BasicUserPreferenceFragment {
        protected int getPreferencesResourceId(){
            return R.xml.basic_prefs;
        }
    }

}
