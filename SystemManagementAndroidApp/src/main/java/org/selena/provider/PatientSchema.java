// ST:BODY:start

package org.selena.provider;

import android.content.UriMatcher;
import android.net.Uri;
import org.selena.provider.tables.*;

/**
 * 
 * <p>
 * based on the work by Vladimir Vivien (http://vladimirvivien.com/), which
 * provides a very logical organization of the meta-data of the Database and
 * Content Provider
 * <p>
 * This note might be moved to a 'Special Thanks' section once one is created
 * and moved out of future test code.
 * 
 * @author Michael A. Walker
 */
public class PatientSchema {

    /**
     * Project Related Constants
     */

    public static final String ORGANIZATIONAL_NAME = "org.selena";
    public static final String PROJECT_NAME = "client";
    public static final String DATABASE_NAME = "patient.db";
    public static final int DATABASE_VERSION = 1;

    /**
     * ConentProvider Related Constants
     */
    public static final String AUTHORITY = ORGANIZATIONAL_NAME + "."
            + PROJECT_NAME + ".provider";
    public static final Uri BASE_URI = Uri.parse("content://" + AUTHORITY);
    public static final UriMatcher URI_MATCHER = buildUriMatcher();

    // register identifying URIs for Restaurant entity
    // the TOKEN value is associated with each URI registered
    private static UriMatcher buildUriMatcher() {

        // add default 'no match' result to matcher
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);

        matcher.addURI(AUTHORITY, MedicationT.PATH, MedicationT.PATH_TOKEN);
        matcher.addURI(AUTHORITY, MedicationT.PATH_FOR_ID, MedicationT.PATH_FOR_ID_TOKEN);

        matcher.addURI(AUTHORITY, CheckInT.PATH, CheckInT.PATH_TOKEN);
        matcher.addURI(AUTHORITY, CheckInT.PATH_FOR_ID, CheckInT.PATH_FOR_ID_TOKEN);

        matcher.addURI(AUTHORITY, MedicationUsageT.PATH, MedicationUsageT.PATH_TOKEN);
        matcher.addURI(AUTHORITY, MedicationUsageT.PATH_FOR_ID, MedicationUsageT.PATH_FOR_ID_TOKEN);

        matcher.addURI(AUTHORITY, UserCredentialsT.PATH, UserCredentialsT.PATH_TOKEN);
        matcher.addURI(AUTHORITY, UserCredentialsT.PATH_FOR_ID, UserCredentialsT.PATH_FOR_ID_TOKEN);

        matcher.addURI(AUTHORITY, AlertedPatientT.PATH, AlertedPatientT.PATH_TOKEN);
        matcher.addURI(AUTHORITY, AlertedPatientT.PATH_FOR_ID, AlertedPatientT.PATH_FOR_ID_TOKEN);

        matcher.addURI(AUTHORITY, DoctorDetailsT.PATH, DoctorDetailsT.PATH_TOKEN);
        matcher.addURI(AUTHORITY, DoctorDetailsT.PATH_FOR_ID, DoctorDetailsT.PATH_FOR_ID_TOKEN);

        return matcher;

    }

}
