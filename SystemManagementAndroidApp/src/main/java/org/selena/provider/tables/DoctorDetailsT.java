package org.selena.provider.tables;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;
import org.selena.provider.PatientSchema;

/**
 * Created by sklasnja on 10/23/14.
 */
public class DoctorDetailsT {
    public static final String TABLE_NAME = "doctor_details_table";

    public static String TAG = DoctorDetailsT.class.getName();

    // SQL Statement to create a new database table.
    private static final String DATABASE_CREATE = "create table "
            + TABLE_NAME + " (" // start table
            + Cols.ID + " integer primary key autoincrement, " // setup
            + Cols.USERNAME + " TEXT ," //
            + Cols.FIRST_NAME + " TEXT ," //
            + Cols.LAST_NAME + " TEXT ," //
            + Cols.DOCTOR_ID + " TEXT " //
            + " );"; // end table

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(TAG, "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(database);
    }

    public static final String PATH = "doctor_details";
    public static final int PATH_TOKEN = 610;

    public static final String PATH_FOR_ID = "doctor_details/*";
    public static final int PATH_FOR_ID_TOKEN = 620;

    // URI for all content stored as Restaurant entity
    public static final Uri CONTENT_URI = PatientSchema.BASE_URI.buildUpon()
            .appendPath(PATH).build();


    private final static String MIME_TYPE_END = "doctor_details";

    // define the MIME type of data in the content provider
    public static final String CONTENT_TYPE_DIR = PatientSchema.ORGANIZATIONAL_NAME
            + ".cursor.dir/" + PatientSchema.ORGANIZATIONAL_NAME + "." + MIME_TYPE_END;
    public static final String CONTENT_ITEM_TYPE = PatientSchema.ORGANIZATIONAL_NAME
            + ".cursor.item/" + PatientSchema.ORGANIZATIONAL_NAME + "." + MIME_TYPE_END;

    // the names and order of ALL columns, including internal use ones
    public static final String[] ALL_COLUMN_NAMES = {
            Cols.ID,  Cols.USERNAME, Cols.DOCTOR_ID, Cols.FIRST_NAME, Cols.LAST_NAME
    };


    public static ContentValues initializeWithDefault(
            final ContentValues assignedValues) {
        // final Long now = Long.valueOf(System.currentTimeMillis());
        final ContentValues setValues = (assignedValues == null) ? new ContentValues()
                : assignedValues;
        if (!setValues.containsKey(Cols.USERNAME)) {
            setValues.put(Cols.USERNAME, "");
        }
        if (!setValues.containsKey(Cols.DOCTOR_ID)) {
            setValues.put(Cols.DOCTOR_ID, "");
        }

        if (!setValues.containsKey(Cols.FIRST_NAME)) {
            setValues.put(Cols.FIRST_NAME, "");
        }
        if (!setValues.containsKey(Cols.LAST_NAME)) {
            setValues.put(Cols.LAST_NAME, "");
        }


        return setValues;
    }

    // a static class to store columns in entity
    public static class Cols {
        public static final String ID = BaseColumns._ID; // convention
        public static final String USERNAME = "USERNAME";
        public static final String FIRST_NAME = "FIRST_NAME";
        public static final String LAST_NAME = "LAST_NAME";
        public static final String DOCTOR_ID = "DOCTOR_ID";

    }


}
