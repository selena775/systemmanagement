package org.selena.provider.tables;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;
import org.selena.patient.client.model.server.CheckIn;
import org.selena.provider.PatientSchema;

import java.text.ParseException;
import java.util.Date;

import static org.selena.patient.client.utility.DateTimeUtility.parseUtcSqlDateTime;

/**
* Created by sklasnja on 10/23/14.
*/ // Define a static class that represents description of stored content
// entity.
public class CheckInT {
    // an identifying name for entity

    public static String TAG = CheckInT.class.getName();

    public static class PhotoHolder {
        long id;
        long login_Id ;
        long serverCheckInId;
        String imageFileName;

        public PhotoHolder(long id, long login_Id, long serverCheckInId, String imageFileName) {
            this.id = id;
            this.login_Id = login_Id;
            this.serverCheckInId = serverCheckInId;
            this.imageFileName = imageFileName;
        }

        public static PhotoHolder fromCursor( Cursor cursor) {
            long id = cursor.getLong(cursor.getColumnIndex(CheckInT.Cols.ID));
            long login_Id = cursor.getLong(cursor.getColumnIndex(CheckInT.Cols.LOGIN_ID));
            long serverCheckInId = cursor.getLong(cursor.getColumnIndex(CheckInT.Cols.SYNCHRONISED));
            String imageFileName = cursor.getString(cursor.getColumnIndex(CheckInT.Cols.IMAGE_FILE_NAME));
            return (new PhotoHolder(id, login_Id, serverCheckInId, imageFileName));
        }

        public long getId() {
            return id;
        }

        public long getLogin_Id() {
            return login_Id;
        }

        public long getServerCheckInId() {
            return serverCheckInId;
        }

        public String getImageFileName() {
            return imageFileName;
        }
    }

    public static CheckIn getCheckInDataFromCursor(Cursor cursor) throws ParseException {

        long id = cursor.getLong(cursor
                .getColumnIndex(CheckInT.Cols.ID));
        long loginId = cursor.getLong(cursor
                .getColumnIndex(CheckInT.Cols.LOGIN_ID));
        CheckIn.PainIntensity painIntensity = CheckIn.PainIntensity.valueOf(cursor.getString(cursor
                .getColumnIndex(CheckInT.Cols.PAIN_INTENSITY)));
        CheckIn.EatingDisorder eatingDisorder = CheckIn.EatingDisorder.valueOf(cursor.getString(cursor
                .getColumnIndex(CheckInT.Cols.EATING_DISORDER)));
        Date time = parseUtcSqlDateTime(cursor.getString(cursor
                .getColumnIndex(CheckInT.Cols.TIME)));
        return new CheckIn(id, loginId, time, painIntensity,eatingDisorder);
    }


    public static final String TABLE_NAME = "check_in_table";

    // SQL Statement to create a new database table.
    private static final String DATABASE_CREATE = "create table "
            + TABLE_NAME + " ("
            + Cols.ID + " integer primary key autoincrement, "
            + Cols.LOGIN_ID + " INTEGER ,"
            + Cols.TIME + " INTEGER ,"
            + Cols.PAIN_INTENSITY + " TEXT ,"
            + Cols.EATING_DISORDER + " TEXT ,"
            + Cols.IMAGE_FILE_NAME + " TEXT  ,"
            + Cols.SYNCHRONISED + " INTEGER ,"
            + Cols.SYNCHRONISED_PHOTO + " INTEGER "
            + " );";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(TAG, "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(database);
    }

    // define a URI paths to access entity
    // BASE_URI/tag - for list of tag(s)
    // BASE_URI/tag/* - retrieve specific tag by id
    // the token value are used to register path in matcher (see above)
    public static final String PATH = "check_in";
    public static final int PATH_TOKEN = 210;

    public static final String PATH_FOR_ID = "check_in/*";
    public static final int PATH_FOR_ID_TOKEN = 220;

    // URI for all content stored as Restaurant entity
    public static final Uri CONTENT_URI = PatientSchema.BASE_URI.buildUpon()
            .appendPath(PATH).build();

    public static final String[] ALL_KEY_COLUMNS = new String[] {Cols.EATING_DISORDER, Cols.PAIN_INTENSITY,
            Cols.TIME, Cols.LOGIN_ID, Cols.IMAGE_FILE_NAME, Cols.SYNCHRONISED, Cols.SYNCHRONISED_PHOTO };

    // ST:relationKeyClause:complete

    private final static String MIME_TYPE_END = "check_in";

    // define the MIME type of data in the content provider
    public static final String CONTENT_TYPE_DIR = PatientSchema.ORGANIZATIONAL_NAME
            + ".cursor.dir/" + PatientSchema.ORGANIZATIONAL_NAME + "." + MIME_TYPE_END;
    public static final String CONTENT_ITEM_TYPE = PatientSchema.ORGANIZATIONAL_NAME
            + ".cursor.item/" + PatientSchema.ORGANIZATIONAL_NAME + "." + MIME_TYPE_END;

    // the names and order of ALL columns, including internal use ones
    public static final String[] ALL_COLUMN_NAMES = { Cols.ID,
            // ST:getColumnNames:inline
            Cols.LOGIN_ID, Cols.TIME, Cols.PAIN_INTENSITY, Cols.EATING_DISORDER, Cols.IMAGE_FILE_NAME,
            Cols.SYNCHRONISED, Cols.SYNCHRONISED_PHOTO
    // ST:getColumnNames:complete
    };

    public static ContentValues initializeWithDefault(
            final ContentValues assignedValues) {
        // final Long now = Long.valueOf(System.currentTimeMillis());
        final ContentValues setValues = (assignedValues == null) ? new ContentValues()
                : assignedValues;
        if (!setValues.containsKey(Cols.LOGIN_ID)) {
            setValues.put(Cols.LOGIN_ID, 0);
        }
        if (!setValues.containsKey(Cols.TIME)) {
            setValues.put(Cols.TIME, 0);
        }
        if (!setValues.containsKey(Cols.PAIN_INTENSITY)) {
            setValues.put(Cols.PAIN_INTENSITY, "");
        }
        if (!setValues.containsKey(Cols.EATING_DISORDER)) {
            setValues.put(Cols.EATING_DISORDER, "");
        }
        if (!setValues.containsKey(Cols.IMAGE_FILE_NAME)) {
            setValues.put(Cols.IMAGE_FILE_NAME, "");
        }
        if (!setValues.containsKey(Cols.SYNCHRONISED)) {
            setValues.put(Cols.SYNCHRONISED, 0);
        }
        if (!setValues.containsKey(Cols.SYNCHRONISED_PHOTO)) {
            setValues.put(Cols.SYNCHRONISED_PHOTO, 0);
        }
        return setValues;
    }

    // a static class to store columns in entity
    public static class Cols {
        public static final String ID = BaseColumns._ID; // convention
        // The name and column index of each column in your database
        // ST:getColumnDeclaration:inline
        public static final String LOGIN_ID = "LOGIN_ID";
        public static final String TIME = "TIME";
        public static final String PAIN_INTENSITY = "PAIN_INTENSITY";
        public static final String EATING_DISORDER = "EATING_DISORDER";
        public static final String IMAGE_FILE_NAME = "IMAGE_FILE_NAME";
        public static final String SYNCHRONISED = "SYNCHRONISED";
        public static final String SYNCHRONISED_PHOTO = "SYNCHRONISED_PHOTO";
        // ST:getColumnDeclaration:complete
    }
}
