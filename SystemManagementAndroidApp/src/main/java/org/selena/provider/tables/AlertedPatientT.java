package org.selena.provider.tables;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;
import org.selena.provider.PatientSchema;

/**
 * Created by sklasnja on 10/23/14.
 */
public class AlertedPatientT {
    public static class HOLDER {
        private long id;
        private String userName;
        private boolean notInAlert;

        public static HOLDER fromCursor( Cursor cursor) {

            HOLDER alertedPatientT= new HOLDER();
            alertedPatientT.setId(Integer.valueOf(cursor.getString(cursor.getColumnIndex(AlertedPatientT.Cols.ID))));

            alertedPatientT.setUserName(cursor.getString(cursor.getColumnIndex(AlertedPatientT.Cols.USERNAME)));
            alertedPatientT.setNotInAlert(Boolean.valueOf(cursor.getString(cursor.getColumnIndex(AlertedPatientT.Cols.NOT_IN_ALERT))));
            return alertedPatientT;
        }


        public long getId() {
            return id;
        }

        public String getUserName() {
            return userName;
        }

        public boolean notInAlert() {
            return notInAlert;
        }

        public void setId(long id) {
            this.id = id;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public void setNotInAlert(boolean notInAlert) {
            this.notInAlert = notInAlert;
        }

        public boolean isNotInAlert() {
            return notInAlert;
        }
    }

    public static final String TABLE_NAME = "alerted_patient_table";

    public static String TAG = AlertedPatientT.class.getName();

    // SQL Statement to create a new database table.
    private static final String DATABASE_CREATE = "create table "
            + TABLE_NAME + " (" // start table
            + Cols.ID + " integer primary key autoincrement, " // setup
            + Cols.LOGIN_ID + " INTEGER ,"
            + Cols.USERNAME + " TEXT ," //
            + Cols.NOT_IN_ALERT + " INTEGER " //
            + " );"; // end table

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(TAG, "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(database);
    }

    public static final String PATH = "alerted_patient";
    public static final int PATH_TOKEN = 510;

    public static final String PATH_FOR_ID = "alerted_patient/*";
    public static final int PATH_FOR_ID_TOKEN = 520;

    // URI for all content stored as Restaurant entity
    public static final Uri CONTENT_URI = PatientSchema.BASE_URI.buildUpon()
            .appendPath(PATH).build();


    private final static String MIME_TYPE_END = "alerted_patient";

    // define the MIME type of data in the content provider
    public static final String CONTENT_TYPE_DIR = PatientSchema.ORGANIZATIONAL_NAME
            + ".cursor.dir/" + PatientSchema.ORGANIZATIONAL_NAME + "." + MIME_TYPE_END;
    public static final String CONTENT_ITEM_TYPE = PatientSchema.ORGANIZATIONAL_NAME
            + ".cursor.item/" + PatientSchema.ORGANIZATIONAL_NAME + "." + MIME_TYPE_END;

    // the names and order of ALL columns, including internal use ones
    public static final String[] ALL_COLUMN_NAMES = {
            Cols.ID, Cols.USERNAME, Cols.LOGIN_ID, Cols.NOT_IN_ALERT
    };


    public static ContentValues initializeWithDefault(
            final ContentValues assignedValues) {
        // final Long now = Long.valueOf(System.currentTimeMillis());
        final ContentValues setValues = (assignedValues == null) ? new ContentValues()
                : assignedValues;
        if (!setValues.containsKey(Cols.USERNAME)) {
            setValues.put(Cols.USERNAME, "");
        }

        if (!setValues.containsKey(Cols.LOGIN_ID)) {
            setValues.put(Cols.LOGIN_ID, 0);
        }

        if (!setValues.containsKey(Cols.NOT_IN_ALERT)) {
            setValues.put(Cols.NOT_IN_ALERT, "");
        }
        return setValues;
    }

    // a static class to store columns in entity
    public static class Cols {
        public static final String ID = BaseColumns._ID; // convention
        public static final String USERNAME = "USERNAME";
        public static final String LOGIN_ID = "LOGIN_ID";
        public static final String NOT_IN_ALERT = "NOT_IN_ALERT";
    }

}
