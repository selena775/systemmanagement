package org.selena.provider.tables;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;
import org.selena.patient.client.model.MedicationUsage;
import org.selena.provider.PatientSchema;

import java.text.ParseException;
import java.util.Date;

import static org.selena.patient.client.utility.DateTimeUtility.parseUtcSqlDateTime;

/**
* Created by sklasnja on 10/23/14.
*/ // Define a static class that represents description of stored content
// entity.
public class MedicationUsageT {
    // an identifying name for entity

    public static String TAG = MedicationUsageT.class.getName();

    public static MedicationUsage getMedicationUsageDataFromCursor(Cursor cursor) throws ParseException {

        String name = cursor.getString(cursor
                .getColumnIndex(MedicationUsageT.Cols.NAME));
        Date time = parseUtcSqlDateTime(cursor.getString(cursor
                .getColumnIndex(MedicationUsageT.Cols.TIME)));
        MedicationUsage.MedicineTaken taken = MedicationUsage.MedicineTaken.valueOf(cursor.getString(cursor
                .getColumnIndex(MedicationUsageT.Cols.TAKEN)));
        return new MedicationUsage(name, time, taken);
    }


    public static final String TABLE_NAME = "medication_usage_table";

    private static final String DATABASE_CREATE = "create table "
            + TABLE_NAME + " (" // start table
            + Cols.ID + " integer primary key autoincrement, " // setup
            + Cols.CHECK_IN_ID + " INTEGER ," //
            + Cols.NAME + " TEXT ," //
            + Cols.TIME + " DATETIME, " //
            + Cols.TAKEN + " TEXT " //
            + " );"; // end table

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(TAG, "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(database);
    }

    // define a URI paths to access entity
    // BASE_URI/tag - for list of tag(s)
    // BASE_URI/tag/* - retrieve specific tag by id
    // the token value are used to register path in matcher (see above)
    public static final String PATH = "medication_usage";
    public static final int PATH_TOKEN = 310;

    public static final String PATH_FOR_ID = "medication_usage/*";
    public static final int PATH_FOR_ID_TOKEN = 320;

    // URI for all content stored as Restaurant entity
    public static final Uri CONTENT_URI = PatientSchema.BASE_URI.buildUpon().appendPath(PATH).build();

    public static final String[] ALL_QUERY_COLUMNS = new String[] { Cols.NAME, Cols.TIME, Cols.TAKEN };

    private final static String MIME_TYPE_END = "medication_usage";

    // define the MIME type of data in the content provider
    public static final String CONTENT_TYPE_DIR = PatientSchema.ORGANIZATIONAL_NAME
            + ".cursor.dir/" + PatientSchema.ORGANIZATIONAL_NAME + "." + MIME_TYPE_END;
    public static final String CONTENT_ITEM_TYPE = PatientSchema.ORGANIZATIONAL_NAME
            + ".cursor.item/" + PatientSchema.ORGANIZATIONAL_NAME + "." + MIME_TYPE_END;

    // the names and order of ALL columns, including internal use ones
    public static final String[] ALL_COLUMN_NAMES = { Cols.ID, Cols.NAME, Cols.CHECK_IN_ID, Cols.TIME, Cols.TAKEN };

    public static ContentValues initializeWithDefault(
            final ContentValues assignedValues) {
        // final Long now = Long.valueOf(System.currentTimeMillis());
        final ContentValues setValues = (assignedValues == null) ? new ContentValues()
                : assignedValues;
        if (!setValues.containsKey(Cols.CHECK_IN_ID)) {
            setValues.put(Cols.CHECK_IN_ID, 0);
        }
        if (!setValues.containsKey(Cols.NAME)) {
            setValues.put(Cols.NAME, 0);
        }
        if (!setValues.containsKey(Cols.TIME)) {
            setValues.put(Cols.TIME, "");
        }
        if (!setValues.containsKey(Cols.TAKEN)) {
            setValues.put(Cols.TAKEN, "NO");
        }
        return setValues;
    }

    // a static class to store columns in entity
    public static class Cols {
        public static final String ID = BaseColumns._ID; // convention
        public static final String CHECK_IN_ID = "CHECK_IN_ID";
        public static final String NAME = "NAME";
        public static final String TIME = "TIME";
        public static final String TAKEN = "TAKEN";

    }
}
