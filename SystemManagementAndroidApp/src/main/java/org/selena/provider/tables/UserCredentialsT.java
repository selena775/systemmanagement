package org.selena.provider.tables;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;
import org.selena.provider.PatientSchema;

/**
 * Created by sklasnja on 10/23/14.
 */
public class UserCredentialsT {
    public static final String TABLE_NAME = "user_credentials_table";

    public static String TAG = UserCredentialsT.class.getName();

    // SQL Statement to create a new database table.
    private static final String DATABASE_CREATE = "create table "
            + TABLE_NAME + " (" // start table
            + Cols.ID + " integer primary key autoincrement, " // setup
            + Cols.LOGIN_ID + " INTEGER ," //
            + Cols.USERNAME + " TEXT ," //
            + Cols.TYPE + " TEXT ," //
            + Cols.SERVER + " TEXT ," //
            + Cols.DOCTOR + " TEXT ," //
            + Cols.FIRST_NAME + " TEXT ," //
            + Cols.LAST_NAME + " TEXT ," //
            + Cols.DOB + " DATETIME ," //
            + Cols.MEDICAL_RECORD + " TEXT ," //
            + Cols.DOCTOR_ID + " TEXT ," //
            + Cols.TOKEN + " TEXT ," //
            + Cols.LAST_TIME_SYNC + " DATETIME ," //
            + Cols.LAST_TIME_FAILED + " DATETIME ," //
            + Cols.LOGIN_TIME + " DATETIME ," //
            + Cols.VALID+ " INTEGER " //
            + " );"; // end table

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(TAG, "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(database);
    }

    public static final String PATH = "user_credentials";
    public static final int PATH_TOKEN = 410;

    public static final String PATH_FOR_ID = "user_credentials/*";
    public static final int PATH_FOR_ID_TOKEN = 420;

    // URI for all content stored as Restaurant entity
    public static final Uri CONTENT_URI = PatientSchema.BASE_URI.buildUpon()
            .appendPath(PATH).build();


    private final static String MIME_TYPE_END = "user_credentials";

    // define the MIME type of data in the content provider
    public static final String CONTENT_TYPE_DIR = PatientSchema.ORGANIZATIONAL_NAME
            + ".cursor.dir/" + PatientSchema.ORGANIZATIONAL_NAME + "." + MIME_TYPE_END;
    public static final String CONTENT_ITEM_TYPE = PatientSchema.ORGANIZATIONAL_NAME
            + ".cursor.item/" + PatientSchema.ORGANIZATIONAL_NAME + "." + MIME_TYPE_END;

    // the names and order of ALL columns, including internal use ones
    public static final String[] ALL_COLUMN_NAMES = {
            Cols.ID,
            // ST:getColumnNames:inline
            Cols.LOGIN_ID, Cols.USERNAME, Cols.DOCTOR, Cols.SERVER,Cols.MEDICAL_RECORD, Cols.DOCTOR_ID,
            Cols.TOKEN, Cols.TYPE, Cols.LAST_TIME_SYNC, Cols.LAST_TIME_FAILED, Cols.LOGIN_TIME, Cols.VALID,
            Cols.FIRST_NAME, Cols.LAST_NAME, Cols.DOB
    };


    public static ContentValues initializeWithDefault(
            final ContentValues assignedValues) {
        // final Long now = Long.valueOf(System.currentTimeMillis());
        final ContentValues setValues = (assignedValues == null) ? new ContentValues()
                : assignedValues;
        if (!setValues.containsKey(Cols.LOGIN_ID)) {
            setValues.put(Cols.LOGIN_ID, 0L);
        }
        if (!setValues.containsKey(Cols.USERNAME)) {
            setValues.put(Cols.USERNAME, "");
        }
        if (!setValues.containsKey(Cols.MEDICAL_RECORD)) {
            setValues.put(Cols.MEDICAL_RECORD, "");
        }
        if (!setValues.containsKey(Cols.DOCTOR_ID)) {
            setValues.put(Cols.DOCTOR_ID, "");
        }
        if (!setValues.containsKey(Cols.DOCTOR)) {
            setValues.put(Cols.DOCTOR, "");
        }
        if (!setValues.containsKey(Cols.SERVER)) {
            setValues.put(Cols.SERVER, "");
        }
        if (!setValues.containsKey(Cols.TOKEN)) {
            setValues.put(Cols.TOKEN, "");
        }
        if (!setValues.containsKey(Cols.TYPE)) {
            setValues.put(Cols.TYPE, "");
        }
        if (!setValues.containsKey(Cols.LAST_TIME_SYNC)) {
            setValues.put(Cols.LAST_TIME_SYNC, 0L);
        }
        if (!setValues.containsKey(Cols.LAST_TIME_FAILED)) {
            setValues.put(Cols.LAST_TIME_FAILED, 0L);
        }
        if (!setValues.containsKey(Cols.LOGIN_TIME)) {
            setValues.put(Cols.LOGIN_TIME, 0L);
        }
        if (!setValues.containsKey(Cols.VALID)) {
            setValues.put(Cols.VALID, 0L);
        }
        if (!setValues.containsKey(Cols.FIRST_NAME)) {
            setValues.put(Cols.FIRST_NAME, "");
        }
        if (!setValues.containsKey(Cols.LAST_NAME)) {
            setValues.put(Cols.LAST_NAME, "");
        }
        if (!setValues.containsKey(Cols.DOB)) {
            setValues.put(Cols.DOB, 0L);
        }

        return setValues;
    }

    // a static class to store columns in entity
    public static class Cols {
        public static final String ID = BaseColumns._ID; // convention
        // The name and column index of each column in your database
        // ST:getColumnDeclaration:inline
        public static final String LOGIN_ID = "LOGIN_ID";
        public static final String USERNAME = "USERNAME";
        public static final String SERVER = "SERVER";
        public static final String DOCTOR = "DOCTOR";
        public static final String TOKEN = "TOKEN";
        public static final String LAST_TIME_SYNC = "LAST_TIME_SYNC";
        public static final String LAST_TIME_FAILED = "LAST_TIME_FAILED";
        public static final String LOGIN_TIME = "LOGIN_TIME";
        public static final String VALID = "VALID";
        public static final String TYPE = "TYPE";
        public static final String FIRST_NAME = "FIRST_NAME";
        public static final String LAST_NAME = "LAST_NAME";
        public static final String DOB = "DOB";
        public static final String MEDICAL_RECORD = "MEDICAL_RECORD";
        public static final String DOCTOR_ID = "DOCTOR_ID";


        // ST:getColumnDeclaration:complete
    }


}
