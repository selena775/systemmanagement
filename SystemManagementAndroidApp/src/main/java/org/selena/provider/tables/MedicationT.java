package org.selena.provider.tables;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;
import org.selena.provider.PatientSchema;

/**
* Created by sklasnja on 10/23/14.
*/ // ST:createRelationMetaData:inline
// Define a static class that represents description of stored content
// entity.
public class MedicationT {
    // an identifying name for entity

    public static String TAG = MedicationT.class.getName();

    public static final String TABLE_NAME = "medication_table";

    // ST:databaseTableCreationStrings:start
    // SQL Statement to create a new database table.
    private static final String DATABASE_CREATE = "create table "
            + TABLE_NAME + " (" // start table
            + Cols.ID + " integer primary key autoincrement, " // setup
            + Cols.LOGIN_ID + " INTEGER ," //
            + Cols.NAME + " TEXT ," //
            + Cols.START_DATE + " DATETIME ," //
            + Cols.END_DATE + " DATETIME " //

            + " );"; // end table

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(TAG, "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(database);
    }

    // define a URI paths to access entity
    // BASE_URI/story - for list of story(s)
    // BASE_URI/story/* - retrieve specific story by id
    // the token value are used to register path in matcher (see above)
    public static final String PATH = "medication";
    public static final int PATH_TOKEN = 110;

    public static final String PATH_FOR_ID = "medication/*";
    public static final int PATH_FOR_ID_TOKEN = 120;

    // URI for all content stored as Restaurant entity
    public static final Uri CONTENT_URI = PatientSchema.BASE_URI.buildUpon()
            .appendPath(PATH).build();

    private final static String MIME_TYPE_END = "medication";

    // define the MIME type of data in the content provider
    public static final String CONTENT_TYPE_DIR = PatientSchema.ORGANIZATIONAL_NAME
            + ".cursor.dir/" + PatientSchema.ORGANIZATIONAL_NAME + "." + MIME_TYPE_END;
    public static final String CONTENT_ITEM_TYPE = PatientSchema.ORGANIZATIONAL_NAME
            + ".cursor.item/" + PatientSchema.ORGANIZATIONAL_NAME + "." + MIME_TYPE_END;

    // the names and order of ALL columns, including internal use ones
    public static final String[] ALL_COLUMN_NAMES = {
            Cols.ID,
            // ST:getColumnNames:inline
            Cols.LOGIN_ID, Cols.NAME, Cols.START_DATE, Cols.END_DATE
    // ST:getColumnNames:complete
    };

    public static ContentValues initializeWithDefault(
            final ContentValues assignedValues) {
        // final Long now = Long.valueOf(System.currentTimeMillis());
        final ContentValues setValues = (assignedValues == null) ? new ContentValues()
                : assignedValues;
        if (!setValues.containsKey(Cols.LOGIN_ID)) {
            setValues.put(Cols.LOGIN_ID, 0);
        }
        if (!setValues.containsKey(Cols.NAME)) {
            setValues.put(Cols.NAME, 0);
        }
        if (!setValues.containsKey(Cols.START_DATE)) {
            setValues.put(Cols.START_DATE, "");
        }
        if (!setValues.containsKey(Cols.END_DATE)) {
            setValues.put(Cols.END_DATE, "");
        }
        return setValues;
    }

    // a static class to store columns in entity
    public static class Cols {
        public static final String ID = BaseColumns._ID; // convention
        // The name and column index of each column in your database
        // ST:getColumnDeclaration:inline
        public static final String LOGIN_ID = "LOGIN_ID";
        public static final String NAME = "NAME";
        public static final String START_DATE = "START_DATE";
        public static final String END_DATE = "END_DATE";


        // ST:getColumnDeclaration:complete
    }
}
