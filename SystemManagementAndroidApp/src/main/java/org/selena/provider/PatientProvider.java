// ST:BODY:start

package org.selena.provider;

import android.content.*;
import android.database.Cursor;
import android.database.SQLException;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import org.selena.provider.tables.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The Content Provider for this application.
 * <p>
 * Content providers are one of the primary building blocks of Android
 * applications, providing content to applications. They encapsulate data and
 * provide it to applications through the single ContentResolver interface. A
 * content provider is only required if you need to share data between multiple
 * applications. For example, the contacts data is used by multiple applications
 * and must be stored in a content provider. If you don't need to share data
 * amongst multiple applications you can use a database directly via
 * SQLiteDatabase.
 * 
 * @author Michael A. Walker
 * 
 */
public class PatientProvider extends ContentProvider {

    private final static String LOG_TAG = PatientProvider.class.getCanonicalName();

    // Local backend DB
    PatientDBAdapter mDB;


    public static String AUTHORITY = PatientSchema.AUTHORITY;

    // ST:createShortURIMatchingTokens:begin
    public static final int MEDICATION_ALL_ROWS = MedicationT.PATH_TOKEN;
    public static final int MEDICATION_SINGLE_ROW = MedicationT.PATH_FOR_ID_TOKEN;
    public static final int CHECK_IN_ALL_ROWS = CheckInT.PATH_TOKEN;
    public static final int CHECK_IN_SINGLE_ROW = CheckInT.PATH_FOR_ID_TOKEN;
    public static final int MEDICATION_USAGE_ALL_ROWS = MedicationUsageT.PATH_TOKEN;
    public static final int MEDICATION_USAGE_SINGLE_ROW = MedicationUsageT.PATH_FOR_ID_TOKEN;
    public static final int USER_CREDENTIALS_ALL_ROWS = UserCredentialsT.PATH_TOKEN;
    public static final int USER_CREDENTIALS_SINGLE_ROW = UserCredentialsT.PATH_FOR_ID_TOKEN;
    public static final int ALERTED_PATIENT_ALL_ROWS = AlertedPatientT.PATH_TOKEN;
    public static final int ALERTED_PATIENT_SINGLE_ROW = AlertedPatientT.PATH_FOR_ID_TOKEN;
    public static final int DOCTOR_DETAILS_ALL_ROWS = DoctorDetailsT.PATH_TOKEN;
    public static final int DOCTOR_DETAILS_SINGLE_ROW = DoctorDetailsT.PATH_FOR_ID_TOKEN;
    // ST:createShortURIMatchingTokens:finish

    private static final UriMatcher uriMatcher = PatientSchema.URI_MATCHER;

    @Override
    /**
     * Implement this to initialize your content provider on startup.
     * This method is called for all registered content providers on the application
     * main thread at application launch time. It must not perform lengthy operations,
     * or application startup will be delayed.
     */
    synchronized public boolean onCreate() {
        Log.d(LOG_TAG, "onCreate()");
        mDB = new PatientDBAdapter(getContext());
        mDB.open();
        return true;
    }

    @Override
    synchronized public void shutdown() {
        super.shutdown();
        mDB.close();
    }

    @Override
    /**
     * Implement this to handle requests for the MIME type of the data at the given URI. 
     * The returned MIME type should start with vnd.android.cursor.item for a single record,
     * or vnd.android.cursor.dir/ for multiple items. This method can be called from multiple 
     * threads, as described in Processes and Threads.
     */
    synchronized public String getType(Uri uri) {
        Log.d(LOG_TAG, "getType()");
        switch (uriMatcher.match(uri)) {

        case MEDICATION_ALL_ROWS:
            return MedicationT.CONTENT_TYPE_DIR;
        case MEDICATION_SINGLE_ROW:
            return MedicationT.CONTENT_ITEM_TYPE;
        case CHECK_IN_ALL_ROWS:
            return CheckInT.CONTENT_TYPE_DIR;
        case CHECK_IN_SINGLE_ROW:
            return CheckInT.CONTENT_ITEM_TYPE;
        case MEDICATION_USAGE_ALL_ROWS:
            return MedicationUsageT.CONTENT_TYPE_DIR;
        case MEDICATION_USAGE_SINGLE_ROW:
            return MedicationUsageT.CONTENT_ITEM_TYPE;
        case USER_CREDENTIALS_ALL_ROWS:
          return UserCredentialsT.CONTENT_TYPE_DIR;
        case USER_CREDENTIALS_SINGLE_ROW:
            return UserCredentialsT.CONTENT_ITEM_TYPE;
        case ALERTED_PATIENT_ALL_ROWS:
          return AlertedPatientT.CONTENT_TYPE_DIR;
        case ALERTED_PATIENT_SINGLE_ROW:
            return AlertedPatientT.CONTENT_ITEM_TYPE;
        case DOCTOR_DETAILS_ALL_ROWS:
          return DoctorDetailsT.CONTENT_TYPE_DIR;
        case DOCTOR_DETAILS_SINGLE_ROW:
            return DoctorDetailsT.CONTENT_ITEM_TYPE;


        default:
            throw new UnsupportedOperationException("URI " + uri
                    + " is not supported.");
        }
    }

    @Override
    /**
     * Retrieve data from your provider. Use the arguments to select the table to query,
     * the rows and columns to return, and the sort order of the result. Return the data as a Cursor object.
     */
    synchronized public Cursor query(final Uri uri, final String[] projection,
            final String selection, final String[] selectionArgs,
            final String sortOrder) {
        Log.d(LOG_TAG, "query()");
        String modifiedSelection = selection;
        switch (uriMatcher.match(uri)) {

            case MEDICATION_SINGLE_ROW: {
                modifiedSelection = modifiedSelection + MedicationT.Cols.ID
                        + " = " + uri.getLastPathSegment();
            }
            case MEDICATION_ALL_ROWS: {
                return query(uri, MedicationT.TABLE_NAME, projection,
                        modifiedSelection, selectionArgs, sortOrder);
            }
            case CHECK_IN_SINGLE_ROW: {
                modifiedSelection = modifiedSelection + CheckInT.Cols.ID
                        + " = " + uri.getLastPathSegment();
            }
            case CHECK_IN_ALL_ROWS: {
                return query(uri, CheckInT.TABLE_NAME, projection,
                        modifiedSelection, selectionArgs, sortOrder);
            }
            case MEDICATION_USAGE_SINGLE_ROW: {
                modifiedSelection = modifiedSelection + MedicationUsageT.Cols.ID
                        + " = " + uri.getLastPathSegment();
            }
            case MEDICATION_USAGE_ALL_ROWS: {
                return query(uri, MedicationUsageT.TABLE_NAME, projection,
                        modifiedSelection, selectionArgs, sortOrder);
            }
            case USER_CREDENTIALS_SINGLE_ROW: {
                modifiedSelection = modifiedSelection + UserCredentialsT.Cols.ID
                        + " = " + uri.getLastPathSegment();
            }
            case USER_CREDENTIALS_ALL_ROWS: {
                return query(uri, UserCredentialsT.TABLE_NAME, projection,
                        modifiedSelection, selectionArgs, sortOrder);
            }
            case ALERTED_PATIENT_SINGLE_ROW: {
                modifiedSelection = modifiedSelection + AlertedPatientT.Cols.ID
                        + " = " + uri.getLastPathSegment();
            }
            case ALERTED_PATIENT_ALL_ROWS: {
                return query(uri, AlertedPatientT.TABLE_NAME, projection,
                        modifiedSelection, selectionArgs, sortOrder);
            }
            case DOCTOR_DETAILS_SINGLE_ROW: {
                modifiedSelection = modifiedSelection + DoctorDetailsT.Cols.ID
                        + " = " + uri.getLastPathSegment();
            }
            case DOCTOR_DETAILS_ALL_ROWS: {
                return query(uri, DoctorDetailsT.TABLE_NAME, projection,
                        modifiedSelection, selectionArgs, sortOrder);
            }

            default:
                return null;
        }
    }

    /*
     * Private query that does the actual query based on the table
     */

    synchronized private Cursor query(final Uri uri, final String tableName,
            final String[] projection, final String selection,
            final String[] selectionArgs, final String sortOrder) {

        // Perform a query on the database with the given parameters
        Cursor c= mDB.query( tableName, projection, selection, selectionArgs, sortOrder );

        c.setNotificationUri(getContext().getContentResolver(), uri);

        return c;

    }

    @Override
    /**
     * Implement this to handle requests to insert a new row. As a courtesy,
     * call notifyChange() after inserting. This method can be called from multiple threads, 
     * as described in Processes and Threads.
     * <p>
     * (non-Javadoc)
     * @see android.content.ContentProvider#insert(android.net.Uri, android.content.ContentValues)
     */
    synchronized public Uri insert(Uri uri, ContentValues assignedValues) {

        Log.d(LOG_TAG, "query()");
        final int match = uriMatcher.match(uri);
        switch (match) {


            case MEDICATION_ALL_ROWS: {
                final ContentValues values = MedicationT
                        .initializeWithDefault(assignedValues);
                values.remove(MedicationT.Cols.ID);

                final long rowID = mDB.insert(MedicationT.TABLE_NAME, values);
                if (rowID < 0) {
                    Log.d(LOG_TAG, "query()");
                    return null;
                }
                final Uri insertedID = ContentUris.withAppendedId(
                        MedicationT.CONTENT_URI, rowID);
                getContext().getContentResolver().notifyChange(insertedID, null);
                return ContentUris.withAppendedId(MedicationT.CONTENT_URI, rowID);
            }
            case CHECK_IN_ALL_ROWS: {
                final ContentValues values = CheckInT
                        .initializeWithDefault(assignedValues);
                values.remove(CheckInT.Cols.ID);

                final long rowID = mDB.insert(CheckInT.TABLE_NAME, values);
                if (rowID < 0) {
                    Log.d(LOG_TAG, "query()");
                    return null;
                }
                final Uri insertedID = ContentUris.withAppendedId(CheckInT.CONTENT_URI,
                        rowID);
                getContext().getContentResolver().notifyChange(insertedID, null);
                return ContentUris.withAppendedId(CheckInT.CONTENT_URI, rowID);
            }
            case MEDICATION_USAGE_ALL_ROWS: {
                final ContentValues values = MedicationUsageT
                        .initializeWithDefault(assignedValues);
                values.remove(MedicationUsageT.Cols.ID);

                final long rowID = mDB.insert(MedicationUsageT.TABLE_NAME, values);
                if (rowID < 0) {
                    Log.d(LOG_TAG, "query()");
                    return null;
                }
                final Uri insertedID = ContentUris.withAppendedId(MedicationUsageT.CONTENT_URI,
                        rowID);
                getContext().getContentResolver().notifyChange(insertedID, null);
                return ContentUris.withAppendedId(MedicationUsageT.CONTENT_URI, rowID);
            }
            case USER_CREDENTIALS_ALL_ROWS: {
                final ContentValues values = UserCredentialsT
                        .initializeWithDefault(assignedValues);
                values.remove(UserCredentialsT.Cols.ID);

                final long rowID = mDB.insert(UserCredentialsT.TABLE_NAME, values);
                if (rowID < 0) {
                    Log.d(LOG_TAG, "query()");
                    return null;
                }
                final Uri insertedID = ContentUris.withAppendedId(UserCredentialsT.CONTENT_URI,
                        rowID);
                getContext().getContentResolver().notifyChange(insertedID, null);
                return ContentUris.withAppendedId(UserCredentialsT.CONTENT_URI, rowID);
            }
            case ALERTED_PATIENT_ALL_ROWS: {
                final ContentValues values = AlertedPatientT
                        .initializeWithDefault(assignedValues);
                values.remove(AlertedPatientT.Cols.ID);

                final long rowID = mDB.insert(AlertedPatientT.TABLE_NAME, values);
                if (rowID < 0) {
                    Log.d(LOG_TAG, "query()");
                    return null;
                }
                final Uri insertedID = ContentUris.withAppendedId(AlertedPatientT.CONTENT_URI,
                        rowID);
                getContext().getContentResolver().notifyChange(insertedID, null);
                return ContentUris.withAppendedId(AlertedPatientT.CONTENT_URI, rowID);
            }
            case DOCTOR_DETAILS_ALL_ROWS: {
                final ContentValues values = DoctorDetailsT
                        .initializeWithDefault(assignedValues);
                values.remove(DoctorDetailsT.Cols.ID);

                final long rowID = mDB.insert(DoctorDetailsT.TABLE_NAME, values);
                if (rowID < 0) {
                    Log.d(LOG_TAG, "query()");
                    return null;
                }
                final Uri insertedID = ContentUris.withAppendedId(DoctorDetailsT.CONTENT_URI,
                        rowID);
                getContext().getContentResolver().notifyChange(insertedID, null);
                return ContentUris.withAppendedId(DoctorDetailsT.CONTENT_URI, rowID);
            }


        // breaks intentionally omitted
        case MEDICATION_SINGLE_ROW:
        case CHECK_IN_SINGLE_ROW:
        case ALERTED_PATIENT_SINGLE_ROW:
        case DOCTOR_DETAILS_SINGLE_ROW:
        case USER_CREDENTIALS_SINGLE_ROW:
        case MEDICATION_USAGE_SINGLE_ROW:{
            throw new IllegalArgumentException(
                    "Unsupported URI, unable to insert into specific row: "
                            + uri);
        }
        default: {
            throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
        }
    }

    @Override
    /**
     * Override this to handle requests to open a file blob.
     */
    public ParcelFileDescriptor openFile(Uri uri, String mode) {
        int imode = 0;
        try {
            if (mode.contains("w")) {
                imode |= ParcelFileDescriptor.MODE_WRITE_ONLY;
            }
            if (mode.contains("r")) {
                imode |= ParcelFileDescriptor.MODE_READ_ONLY;
            }
            if (mode.contains("+")) {
                imode |= ParcelFileDescriptor.MODE_APPEND;
            }
        } finally {
        }

        int token = PatientSchema.URI_MATCHER.match(uri);
        File imageDirectory = getContext().getCacheDir();
        switch (token) {
            case MedicationT.PATH_FOR_ID_TOKEN: {
                final List<String> segments = uri.getPathSegments();
                final File storyFile = new File(imageDirectory, "medication"
                        + segments.get(1));
                try {
                    if (!storyFile.exists()) {
                        storyFile.createNewFile();
                    }
                    return ParcelFileDescriptor.open(storyFile, imode);
                } catch (FileNotFoundException ex) {
                } catch (IOException ex) {
                }
            }
            break;
            case CheckInT.PATH_FOR_ID_TOKEN: {
                final List<String> segments = uri.getPathSegments();
                final File tagsFile = new File(imageDirectory, "check_in"
                        + segments.get(1));
                try {
                    if (!tagsFile.exists()) {
                        tagsFile.createNewFile();
                    }
                    return ParcelFileDescriptor.open(tagsFile, imode);
                } catch (FileNotFoundException ex) {
                } catch (IOException ex) {
                }
            }
            break;
            case MedicationUsageT.PATH_FOR_ID_TOKEN: {
                final List<String> segments = uri.getPathSegments();
                final File storyFile = new File(imageDirectory, "medication_usage"
                        + segments.get(1));
                try {
                    if (!storyFile.exists()) {
                        storyFile.createNewFile();
                    }
                    return ParcelFileDescriptor.open(storyFile, imode);
                } catch (FileNotFoundException ex) {
                } catch (IOException ex) {
                }
            }
            break;
            case UserCredentialsT.PATH_FOR_ID_TOKEN: {
                final List<String> segments = uri.getPathSegments();
                final File storyFile = new File(imageDirectory, "user_credentials"
                        + segments.get(1));
                try {
                    if (!storyFile.exists()) {
                        storyFile.createNewFile();
                    }
                    return ParcelFileDescriptor.open(storyFile, imode);
                } catch (FileNotFoundException ex) {
                } catch (IOException ex) {
                }
            }
            break;
            case AlertedPatientT.PATH_FOR_ID_TOKEN: {
                final List<String> segments = uri.getPathSegments();
                final File storyFile = new File(imageDirectory, "alert_patient"
                        + segments.get(1));
                try {
                    if (!storyFile.exists()) {
                        storyFile.createNewFile();
                    }
                    return ParcelFileDescriptor.open(storyFile, imode);
                } catch (FileNotFoundException ex) {
                } catch (IOException ex) {
                }
            }
            break;
            case DoctorDetailsT.PATH_FOR_ID_TOKEN: {
                final List<String> segments = uri.getPathSegments();
                final File storyFile = new File(imageDirectory, "doctor_details"
                        + segments.get(1));
                try {
                    if (!storyFile.exists()) {
                        storyFile.createNewFile();
                    }
                    return ParcelFileDescriptor.open(storyFile, imode);
                } catch (FileNotFoundException ex) {
                } catch (IOException ex) {
                }
            }
            break;
            default: {
                throw new UnsupportedOperationException("URI: " + uri
                        + " not supported.");
            }
            }
            return null;
    }

    @Override
    /**
     * Implement this to handle requests to delete one or more rows.
     */
    synchronized public int delete(Uri uri, String whereClause,
            String[] whereArgs) {

        switch (uriMatcher.match(uri)) {

            case MEDICATION_SINGLE_ROW:
                whereClause = whereClause + MedicationT.Cols.ID + " = "
                        + uri.getLastPathSegment();
                // no break here on purpose
            case MEDICATION_ALL_ROWS: {
                return deleteAndNotify(uri, MedicationT.TABLE_NAME,
                        whereClause, whereArgs);
            }
            case CHECK_IN_SINGLE_ROW:
                whereClause = whereClause + CheckInT.Cols.ID + " = "
                        + uri.getLastPathSegment();
                // no break here on purpose
            case CHECK_IN_ALL_ROWS: {
                return deleteAndNotify(uri, CheckInT.TABLE_NAME,
                        whereClause, whereArgs);
            }
            case MEDICATION_USAGE_SINGLE_ROW:
                whereClause = whereClause + MedicationUsageT.Cols.ID + " = "
                        + uri.getLastPathSegment();
                // no break here on purpose
            case MEDICATION_USAGE_ALL_ROWS: {
                return deleteAndNotify(uri, MedicationUsageT.TABLE_NAME,
                        whereClause, whereArgs);
            }
            case USER_CREDENTIALS_SINGLE_ROW:
                whereClause = whereClause + UserCredentialsT.Cols.ID + " = "
                        + uri.getLastPathSegment();
                // no break here on purpose
            case USER_CREDENTIALS_ALL_ROWS: {
                return deleteAndNotify(uri, UserCredentialsT.TABLE_NAME,
                        whereClause, whereArgs);
            }
            case ALERTED_PATIENT_SINGLE_ROW:
                whereClause = whereClause + AlertedPatientT.Cols.ID + " = "
                        + uri.getLastPathSegment();
                // no break here on purpose
            case ALERTED_PATIENT_ALL_ROWS: {
                return deleteAndNotify(uri, AlertedPatientT.TABLE_NAME,
                        whereClause, whereArgs);
            }
            case DOCTOR_DETAILS_SINGLE_ROW:
                whereClause = whereClause + DoctorDetailsT.Cols.ID + " = "
                        + uri.getLastPathSegment();
                // no break here on purpose
            case DOCTOR_DETAILS_ALL_ROWS: {
                return deleteAndNotify(uri, DoctorDetailsT.TABLE_NAME,
                        whereClause, whereArgs);
            }


        default:
            throw new IllegalArgumentException("Unsupported URI: " + uri);
        }

    }

    /*
     * Private method to both attempt the delete command, and then to notify of
     * the changes
     */
    private int deleteAndNotify(final Uri uri, final String tableName,
            final String whereClause, final String[] whereArgs) {
        int count = mDB.delete(tableName, whereClause, whereArgs);
        if (count > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return count;
    }

    @Override
    /**
     * Implement this to handle requests to update one or more rows.
     */
    synchronized public int update(Uri uri, ContentValues values,
            String whereClause, String[] whereArgs) {
        Log.d(LOG_TAG, "query()");

        switch (uriMatcher.match(uri)) {

            case MEDICATION_SINGLE_ROW:
                whereClause = whereClause + MedicationT.Cols.ID + " = "
                        + uri.getLastPathSegment();
            case MEDICATION_ALL_ROWS: {
                return updateAndNotify(uri, MedicationT.TABLE_NAME, values,
                        whereClause, whereArgs);

            }
            case CHECK_IN_SINGLE_ROW:
                whereClause = whereClause + CheckInT.Cols.ID + " = "
                        + uri.getLastPathSegment();
            case CHECK_IN_ALL_ROWS: {
                return updateAndNotify(uri, CheckInT.TABLE_NAME, values,
                        whereClause, whereArgs);

            }
            case MEDICATION_USAGE_SINGLE_ROW:
                whereClause = whereClause + MedicationUsageT.Cols.ID + " = "
                        + uri.getLastPathSegment();
            case MEDICATION_USAGE_ALL_ROWS: {
                return updateAndNotify(uri, MedicationUsageT.TABLE_NAME, values,
                        whereClause, whereArgs);

            }
            case USER_CREDENTIALS_SINGLE_ROW:
                whereClause = whereClause + UserCredentialsT.Cols.ID + " = "
                        + uri.getLastPathSegment();
            case USER_CREDENTIALS_ALL_ROWS: {
                return updateAndNotify(uri, UserCredentialsT.TABLE_NAME, values,
                        whereClause, whereArgs);

            }
            case ALERTED_PATIENT_SINGLE_ROW:
                whereClause = whereClause + AlertedPatientT.Cols.ID + " = "
                        + uri.getLastPathSegment();
            case ALERTED_PATIENT_ALL_ROWS: {
                return updateAndNotify(uri, AlertedPatientT.TABLE_NAME, values,
                        whereClause, whereArgs);
            }
            case DOCTOR_DETAILS_SINGLE_ROW:
                whereClause = whereClause + DoctorDetailsT.Cols.ID + " = "
                        + uri.getLastPathSegment();
            case DOCTOR_DETAILS_ALL_ROWS: {
                return updateAndNotify(uri, DoctorDetailsT.TABLE_NAME, values,
                        whereClause, whereArgs);
            }

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
    }

    /*
     * private update function that updates based on parameters, then notifies
     * change
     */
    private int updateAndNotify(final Uri uri, final String tableName,
            final ContentValues values, final String whereClause,
            final String[] whereArgs) {
        int count = mDB.update(tableName, values, whereClause, whereArgs);
        if (count > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return count;
    }

    @Override
    synchronized public ContentProviderResult[] applyBatch(final ArrayList<ContentProviderOperation> operations) throws OperationApplicationException {

        ContentProviderResult[] results = null;
        try {
            mDB.beginTransaction();

            super.applyBatch(operations);
            // your sql stuff
            mDB.setTransactionSuccessful();
            return results;
        } catch(SQLException e) {
            throw e;
            // do some error handling
        } finally {
            mDB.endTransaction();
        }
    }
}