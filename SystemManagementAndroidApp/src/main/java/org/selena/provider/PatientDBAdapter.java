// ST:BODY:startTransaction

package org.selena.provider;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import org.selena.provider.tables.*;

/**
 * This is the class that actually interacts with the SQLite3 database and does
 * the operations to manipulate the data within the database.
 * 
 * @author Michael A. Walker
 * 
 */
public class PatientDBAdapter {

    private static final String LOG_TAG = PatientDBAdapter.class
            .getCanonicalName();

    private static final String DATABASE_NAME = "patient.db";


    static final int DATABASE_VERSION = 1;



    // ST:databaseTableCreationStrings:finish

    // Variable to hold the database instance.
    private SQLiteDatabase db;
    // Context of the application using the database.
    private final Context context;
    // Database open/upgrade helper
    private myDbHelper dbHelper;
    // if the DB is in memory or to file.
    private boolean MEMORY_ONLY_DB = false;

    /**
     * constructor that accepts the context to be associated with
     * 
     * @param _context
     */
    public PatientDBAdapter(Context _context) {
        Log.d(LOG_TAG, "MyDBAdapter constructor");

        context = _context;
        dbHelper = new myDbHelper(context, DATABASE_NAME, null,
                DATABASE_VERSION);
    }

    /**
     * constructor that accepts the context to be associated with, and if this
     * DB should be created in memory only(non-persistent).
     * 
     * @param _context
     */
    public PatientDBAdapter(Context _context, boolean memory_only_db) {
        Log.d(LOG_TAG, "MyDBAdapter constructor w/ mem only =" + memory_only_db);

        context = _context;
        MEMORY_ONLY_DB = memory_only_db;
        if (MEMORY_ONLY_DB == true) {
            dbHelper = new myDbHelper(context, null, null, DATABASE_VERSION);
        } else {
            dbHelper = new myDbHelper(context, DATABASE_NAME, null,
                    DATABASE_VERSION);
        }
    }

    /**
     * open the DB Get Memory or File version of DB, and write/read access or
     * just read access if that is all that is possible.
     * 
     * @return this MoocDataDBAdaptor
     * @throws android.database.SQLException
     */
    public PatientDBAdapter open() throws SQLException {
        Log.d(LOG_TAG, "open()");
        try {
            db = dbHelper.getWritableDatabase();
        } catch (SQLException ex) {
            db = dbHelper.getReadableDatabase();
        }
        return this;
    }

    /**
     * Remove a row of the DB where the rowIndex matches.
     * 
     * @param rowIndex
     *            row to remove from DB
     * @return if the row was removed
     */
    public int delete(final String table, long _id) {
        Log.d(LOG_TAG, "delete(" + _id + ") ");
        return db.delete(table, android.provider.BaseColumns._ID + " = " + _id,
                null);
    }

    /**
     * Delete row(s) that match the whereClause and whereArgs(if used).
     * <p>
     * the whereArgs is an String[] of values to substitute for the '?'s in the
     * whereClause
     * 
     * @param whereClause
     * @param whereArgs
     * @return
     */
    public int delete(final String table, final String whereClause,
            final String[] whereArgs) {
        Log.d(LOG_TAG, "delete(" + whereClause + ") ");
        return db.delete(table, whereClause, whereArgs);
    }

    /**
     * Query the Database with the provided specifics.
     * 
     * @param projection
     * @param selection
     * @param selectionArgs
     * @param sortOrder
     * @return Cursor of results
     */
    public Cursor query(final String table, final String[] projection,
            final String selection, final String[] selectionArgs,
            final String sortOrder) {

        // Perform a query on the database with the given parameters
        return db.query(table, projection, selection, selectionArgs, null, null, sortOrder);
    }

    /**
     * close the DB.
     */
    public void close() {
        Log.d(LOG_TAG, "close()");
        db.close();
    }

    /**
     * Start a transaction.
     */
    public void beginTransaction() {
        Log.d(LOG_TAG, "startTransaction()");
        db.beginTransaction();
    }


    public void setTransactionSuccessful() {
        Log.d(LOG_TAG, "setTransactionSuccessful()");
        db.setTransactionSuccessful();
    }

    /**
     * End a transaction.
     */
    public void endTransaction() {
        Log.d(LOG_TAG, "endTransaction()");
        db.endTransaction();
    }

    /**
     * Get the underlying Database.
     * 
     * @return
     */
    SQLiteDatabase getDB() {
        return db;
    }

    /**
     * Insert a ContentValues into the DB.
     *
     * @return row's '_id' of the newly inserted ContentValues
     */
    public long insert(final String table, final ContentValues cv) {
        Log.d(LOG_TAG, "insert(CV)");
        return db.insert(table, null, cv);
    }

    /**
     * Update Value(s) in the DB.
     * 
     * @param values
     * @param whereClause
     * @param whereArgs
     * @return number of rows changed.
     */
    public int update(final String table, final ContentValues values,
            final String whereClause, final String[] whereArgs) {
        return db.update(table, values, whereClause, whereArgs);
    }

    @Override
    /**
     * finalize operations to this DB, and close it.
     */
    protected void finalize() throws Throwable {
        try {
            db.close();
        } catch (Exception e) {
            Log.d(LOG_TAG, "exception on finalize():" + e.getMessage());
        }
        super.finalize();
    }

    /**
     * This class can support running the database in a non-persistent mode,
     * this tells you if that is happening.
     * 
     * @return boolean true/false of if this DBAdaptor is persistent or in
     *         memory only.
     */
    public boolean isMemoryOnlyDB() {
        return MEMORY_ONLY_DB;
    }

    /**
     * DB Helper Class.
     * 
     * @author mwalker
     * 
     */
    private static class myDbHelper extends SQLiteOpenHelper {

        public myDbHelper(Context context, String name, CursorFactory factory,
                int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            Log.d(LOG_TAG, "DATABASE_CREATE: version: " + DATABASE_VERSION);

            CheckInT.onCreate(db);
            MedicationT.onCreate(db);
            MedicationUsageT.onCreate(db);
            UserCredentialsT.onCreate(db);
            AlertedPatientT.onCreate(db);
            DoctorDetailsT.onCreate(db);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // Log version upgrade.
            Log.w(LOG_TAG + "DBHelper", "Upgrading from version " + oldVersion
                    + " to " + newVersion + ", which will destroy all old data");

            CheckInT.onUpgrade(db, oldVersion, newVersion);
            MedicationT.onUpgrade(db, oldVersion, newVersion);
            MedicationUsageT.onUpgrade(db, oldVersion, newVersion);
            UserCredentialsT.onUpgrade(db, oldVersion, newVersion);
            AlertedPatientT.onUpgrade(db, oldVersion, newVersion);
        }

    }

}
